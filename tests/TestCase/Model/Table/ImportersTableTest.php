<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ImportersTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ImportersTable Test Case
 */
class ImportersTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ImportersTable
     */
    public $Importers;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Importers',
        'app.Countries'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Importers') ? [] : ['className' => ImportersTable::class];
        $this->Importers = TableRegistry::getTableLocator()->get('Importers', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Importers);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
