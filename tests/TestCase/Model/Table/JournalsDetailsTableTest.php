<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\JournalsDetailsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\JournalsDetailsTable Test Case
 */
class JournalsDetailsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\JournalsDetailsTable
     */
    public $JournalsDetails;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.JournalsDetails',
        'app.Journals',
        'app.CodeAccounts'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('JournalsDetails') ? [] : ['className' => JournalsDetailsTable::class];
        $this->JournalsDetails = TableRegistry::getTableLocator()->get('JournalsDetails', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->JournalsDetails);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
