<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\StocksLocationsLogsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\StocksLocationsLogsTable Test Case
 */
class StocksLocationsLogsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\StocksLocationsLogsTable
     */
    public $StocksLocationsLogs;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.StocksLocationsLogs',
        'app.StockLocations',
        'app.Referensis'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('StocksLocationsLogs') ? [] : ['className' => StocksLocationsLogsTable::class];
        $this->StocksLocationsLogs = TableRegistry::getTableLocator()->get('StocksLocationsLogs', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->StocksLocationsLogs);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
