<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\InvoicingsPaymentsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\InvoicingsPaymentsTable Test Case
 */
class InvoicingsPaymentsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\InvoicingsPaymentsTable
     */
    public $InvoicingsPayments;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.InvoicingsPayments',
        'app.Invoicings',
        'app.Currencies'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('InvoicingsPayments') ? [] : ['className' => InvoicingsPaymentsTable::class];
        $this->InvoicingsPayments = TableRegistry::getTableLocator()->get('InvoicingsPayments', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->InvoicingsPayments);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
