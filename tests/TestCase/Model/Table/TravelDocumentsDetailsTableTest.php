<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TravelDocumentsDetailsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TravelDocumentsDetailsTable Test Case
 */
class TravelDocumentsDetailsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\TravelDocumentsDetailsTable
     */
    public $TravelDocumentsDetails;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.TravelDocumentsDetails',
        'app.TravelDocuments',
        'app.ExpendituresPickingListsDetails'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('TravelDocumentsDetails') ? [] : ['className' => TravelDocumentsDetailsTable::class];
        $this->TravelDocumentsDetails = TableRegistry::getTableLocator()->get('TravelDocumentsDetails', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TravelDocumentsDetails);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
