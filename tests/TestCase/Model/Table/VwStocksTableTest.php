<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\VwStocksTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\VwStocksTable Test Case
 */
class VwStocksTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\VwStocksTable
     */
    public $VwStocks;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.VwStocks'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('VwStocks') ? [] : ['className' => VwStocksTable::class];
        $this->VwStocks = TableRegistry::getTableLocator()->get('VwStocks', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->VwStocks);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
