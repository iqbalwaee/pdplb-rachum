<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TravelDocumentsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TravelDocumentsTable Test Case
 */
class TravelDocumentsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\TravelDocumentsTable
     */
    public $TravelDocuments;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.TravelDocuments',
        'app.Customers',
        'app.Expenditures',
        'app.TravelDocumentsDetails'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('TravelDocuments') ? [] : ['className' => TravelDocumentsTable::class];
        $this->TravelDocuments = TableRegistry::getTableLocator()->get('TravelDocuments', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TravelDocuments);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
