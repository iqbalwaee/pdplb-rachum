<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CodeAccountsDumpTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CodeAccountsDumpTable Test Case
 */
class CodeAccountsDumpTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\CodeAccountsDumpTable
     */
    public $CodeAccountsDump;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.CodeAccountsDump'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('CodeAccountsDump') ? [] : ['className' => CodeAccountsDumpTable::class];
        $this->CodeAccountsDump = TableRegistry::getTableLocator()->get('CodeAccountsDump', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CodeAccountsDump);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
