<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ExpendituresPickingListsDetailsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ExpendituresPickingListsDetailsTable Test Case
 */
class ExpendituresPickingListsDetailsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ExpendituresPickingListsDetailsTable
     */
    public $ExpendituresPickingListsDetails;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.ExpendituresPickingListsDetails',
        'app.ExpendituresPickingLists',
        'app.PickingListsDetails'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('ExpendituresPickingListsDetails') ? [] : ['className' => ExpendituresPickingListsDetailsTable::class];
        $this->ExpendituresPickingListsDetails = TableRegistry::getTableLocator()->get('ExpendituresPickingListsDetails', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ExpendituresPickingListsDetails);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
