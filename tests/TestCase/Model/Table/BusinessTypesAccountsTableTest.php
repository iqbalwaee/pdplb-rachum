<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\BusinessTypesAccountsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\BusinessTypesAccountsTable Test Case
 */
class BusinessTypesAccountsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\BusinessTypesAccountsTable
     */
    public $BusinessTypesAccounts;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.BusinessTypesAccounts',
        'app.BusinessTypes',
        'app.CodeAccounts'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('BusinessTypesAccounts') ? [] : ['className' => BusinessTypesAccountsTable::class];
        $this->BusinessTypesAccounts = TableRegistry::getTableLocator()->get('BusinessTypesAccounts', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->BusinessTypesAccounts);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
