<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\InvoicingsExpendituresDetailsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\InvoicingsExpendituresDetailsTable Test Case
 */
class InvoicingsExpendituresDetailsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\InvoicingsExpendituresDetailsTable
     */
    public $InvoicingsExpendituresDetails;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.InvoicingsExpendituresDetails',
        'app.InvoicingsExpenditures',
        'app.ExpendituresPickingListsDetails'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('InvoicingsExpendituresDetails') ? [] : ['className' => InvoicingsExpendituresDetailsTable::class];
        $this->InvoicingsExpendituresDetails = TableRegistry::getTableLocator()->get('InvoicingsExpendituresDetails', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->InvoicingsExpendituresDetails);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
