<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PutAwaysDetailsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PutAwaysDetailsTable Test Case
 */
class PutAwaysDetailsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\PutAwaysDetailsTable
     */
    public $PutAwaysDetails;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.PutAwaysDetails',
        'app.PutAways',
        'app.Stocks',
        'app.StocksLocations'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('PutAwaysDetails') ? [] : ['className' => PutAwaysDetailsTable::class];
        $this->PutAwaysDetails = TableRegistry::getTableLocator()->get('PutAwaysDetails', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->PutAwaysDetails);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
