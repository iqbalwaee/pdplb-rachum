<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PickingListsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PickingListsTable Test Case
 */
class PickingListsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\PickingListsTable
     */
    public $PickingLists;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.PickingLists',
        'app.Customers',
        'app.ExpendituresPickingListsDetails',
        'app.PickingListsDetails',
        'app.Expenditures'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('PickingLists') ? [] : ['className' => PickingListsTable::class];
        $this->PickingLists = TableRegistry::getTableLocator()->get('PickingLists', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->PickingLists);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
