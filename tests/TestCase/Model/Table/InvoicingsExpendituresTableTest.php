<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\InvoicingsExpendituresTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\InvoicingsExpendituresTable Test Case
 */
class InvoicingsExpendituresTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\InvoicingsExpendituresTable
     */
    public $InvoicingsExpenditures;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.InvoicingsExpenditures',
        'app.Invoicings',
        'app.Expenditures',
        'app.InvoicingsExpendituresDetails'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('InvoicingsExpenditures') ? [] : ['className' => InvoicingsExpendituresTable::class];
        $this->InvoicingsExpenditures = TableRegistry::getTableLocator()->get('InvoicingsExpenditures', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->InvoicingsExpenditures);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
