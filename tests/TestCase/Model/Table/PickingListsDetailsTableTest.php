<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PickingListsDetailsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PickingListsDetailsTable Test Case
 */
class PickingListsDetailsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\PickingListsDetailsTable
     */
    public $PickingListsDetails;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.PickingListsDetails',
        'app.PickingLists',
        'app.StockLocations',
        'app.Expenditures'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('PickingListsDetails') ? [] : ['className' => PickingListsDetailsTable::class];
        $this->PickingListsDetails = TableRegistry::getTableLocator()->get('PickingListsDetails', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->PickingListsDetails);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
