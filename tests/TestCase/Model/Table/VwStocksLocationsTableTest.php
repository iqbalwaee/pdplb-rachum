<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\VwStocksLocationsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\VwStocksLocationsTable Test Case
 */
class VwStocksLocationsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\VwStocksLocationsTable
     */
    public $VwStocksLocations;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.VwStocksLocations'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('VwStocksLocations') ? [] : ['className' => VwStocksLocationsTable::class];
        $this->VwStocksLocations = TableRegistry::getTableLocator()->get('VwStocksLocations', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->VwStocksLocations);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
