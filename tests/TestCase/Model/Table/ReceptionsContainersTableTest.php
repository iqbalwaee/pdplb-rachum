<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ReceptionsContainersTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ReceptionsContainersTable Test Case
 */
class ReceptionsContainersTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ReceptionsContainersTable
     */
    public $ReceptionsContainers;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.ReceptionsContainers',
        'app.Receptions',
        'app.ReceptionsContainersDetails'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('ReceptionsContainers') ? [] : ['className' => ReceptionsContainersTable::class];
        $this->ReceptionsContainers = TableRegistry::getTableLocator()->get('ReceptionsContainers', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ReceptionsContainers);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
