<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CodeAccountsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CodeAccountsTable Test Case
 */
class CodeAccountsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\CodeAccountsTable
     */
    public $CodeAccounts;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.CodeAccounts',
        'app.BusinessTypesAccounts'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('CodeAccounts') ? [] : ['className' => CodeAccountsTable::class];
        $this->CodeAccounts = TableRegistry::getTableLocator()->get('CodeAccounts', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CodeAccounts);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
