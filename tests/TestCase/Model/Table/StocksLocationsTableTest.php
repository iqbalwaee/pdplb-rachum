<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\StocksLocationsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\StocksLocationsTable Test Case
 */
class StocksLocationsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\StocksLocationsTable
     */
    public $StocksLocations;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.StocksLocations',
        'app.Stocks',
        'app.PutAwaysDetails'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('StocksLocations') ? [] : ['className' => StocksLocationsTable::class];
        $this->StocksLocations = TableRegistry::getTableLocator()->get('StocksLocations', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->StocksLocations);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
