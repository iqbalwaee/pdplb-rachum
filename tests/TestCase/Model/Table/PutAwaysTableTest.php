<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PutAwaysTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PutAwaysTable Test Case
 */
class PutAwaysTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\PutAwaysTable
     */
    public $PutAways;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.PutAways',
        'app.Locations',
        'app.PutAwaysDetails'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('PutAways') ? [] : ['className' => PutAwaysTable::class];
        $this->PutAways = TableRegistry::getTableLocator()->get('PutAways', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->PutAways);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
