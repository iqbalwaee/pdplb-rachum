<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\InvoicingsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\InvoicingsTable Test Case
 */
class InvoicingsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\InvoicingsTable
     */
    public $Invoicings;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Invoicings',
        'app.Customers',
        'app.Expenditures'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Invoicings') ? [] : ['className' => InvoicingsTable::class];
        $this->Invoicings = TableRegistry::getTableLocator()->get('Invoicings', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Invoicings);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
