<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * JournalsDetail Entity
 *
 * @property int $id
 * @property int|null $journal_id
 * @property int|null $code_account_id
 * @property float|null $debit
 * @property float|null $credit
 *
 * @property \App\Model\Entity\Journal $journal
 * @property \App\Model\Entity\CodeAccount $code_account
 */
class JournalsDetail extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'journal_id' => true,
        'code_account_id' => true,
        'debit' => true,
        'credit' => true,
        'journal' => true,
        'code_account' => true
    ];
}
