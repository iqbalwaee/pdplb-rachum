<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * TravelDocument Entity
 *
 * @property int $id
 * @property int|null $customer_id
 * @property int|null $expenditure_id
 * @property string|null $code
 * @property \Cake\I18n\FrozenDate|null $date
 * @property string|null $driver_name
 * @property string|null $license_plate
 * @property \Cake\I18n\FrozenTime|null $created
 * @property int|null $created_by
 * @property \Cake\I18n\FrozenTime|null $modified
 * @property int|null $modified_by
 *
 * @property \App\Model\Entity\Customer $customer
 * @property \App\Model\Entity\Expenditure $expenditure
 * @property \App\Model\Entity\TravelDocumentsDetail[] $travel_documents_details
 */
class TravelDocument extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'customer_id' => true,
        'expenditure_id' => true,
        'code' => true,
        'date' => true,
        'driver_name' => true,
        'license_plate' => true,
        'created' => true,
        'created_by' => true,
        'modified' => true,
        'modified_by' => true,
        'customer' => true,
        'expenditure' => true,
        'travel_documents_details' => true
    ];
}
