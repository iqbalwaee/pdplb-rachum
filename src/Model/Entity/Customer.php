<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Customer Entity
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $address
 * @property int|null $country_id
 * @property string|null $phone
 * @property string|null $fax
 * @property string|null $email
 * @property string|null $pic
 * @property string|null $pic_phone
 * @property bool|null $status
 * @property \Cake\I18n\FrozenTime|null $created
 * @property int|null $created_by
 * @property \Cake\I18n\FrozenTime|null $modified
 * @property int|null $modified_by
 *
 * @property \App\Model\Entity\Country $country
 * @property \App\Model\Entity\Expenditure[] $expenditures
 * @property \App\Model\Entity\PickingList[] $picking_lists
 * @property \App\Model\Entity\Reception[] $receptions
 */
class Customer extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'address' => true,
        'country_id' => true,
        'phone' => true,
        'fax' => true,
        'email' => true,
        'pic' => true,
        'pic_phone' => true,
        'status' => true,
        'created' => true,
        'created_by' => true,
        'modified' => true,
        'modified_by' => true,
        'country' => true,
        'expenditures' => true,
        'picking_lists' => true,
        'receptions' => true
    ];
}
