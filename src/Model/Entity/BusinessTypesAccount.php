<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * BusinessTypesAccount Entity
 *
 * @property int $id
 * @property int|null $business_type_id
 * @property int|null $code_account_id
 *
 * @property \App\Model\Entity\BusinessType $business_type
 * @property \App\Model\Entity\CodeAccount $code_account
 */
class BusinessTypesAccount extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'business_type_id' => true,
        'code_account_id' => true,
        'business_type' => true,
        'code_account' => true
    ];
}
