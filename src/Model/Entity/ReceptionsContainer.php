<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ReceptionsContainer Entity
 *
 * @property int $id
 * @property int $reception_id
 * @property string|null $code
 * @property string|null $size
 * @property string|null $type
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Reception $reception
 * @property \App\Model\Entity\ReceptionsContainersDetail[] $receptions_containers_details
 */
class ReceptionsContainer extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'reception_id' => true,
        'code' => true,
        'size' => true,
        'type' => true,
        'created' => true,
        'modified' => true,
        'reception' => true,
        'receptions_containers_details' => true
    ];
}
