<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ExpendituresPickingList Entity
 *
 * @property int $id
 * @property int|null $expenditure_id
 * @property int $picking_list_id
 * @property \Cake\I18n\FrozenTime $created
 * @property int $created_by
 * @property \Cake\I18n\FrozenTime|null $modified
 * @property int|null $modified_by
 *
 * @property \App\Model\Entity\Expenditure $expenditure
 * @property \App\Model\Entity\PickingList $picking_list
 * @property \App\Model\Entity\ExpendituresPickingListsDetail[] $expenditures_picking_lists_details
 */
class ExpendituresPickingList extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'expenditure_id' => true,
        'picking_list_id' => true,
        'created' => true,
        'created_by' => true,
        'modified' => true,
        'modified_by' => true,
        'expenditure' => true,
        'picking_list' => true,
        'expenditures_picking_lists_details' => true
    ];
}
