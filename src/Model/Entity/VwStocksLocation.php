<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * VwStocksLocation Entity
 *
 * @property int $StocksLocationID
 * @property float|null $StocksLocationQtyStock
 * @property float|null $StocksLocationActualStock
 * @property int $PutAwaysDetailID
 * @property int $PutAwayID
 * @property string|null $PutAwayCode
 * @property \Cake\I18n\FrozenDate $PutAwayDate
 * @property int $LocationID
 * @property string|null $LocationName
 * @property int $StockID
 * @property string|null $StockCode
 * @property float|null $StockQty
 * @property int $RcdID
 * @property string|null $RcdCategory
 * @property string|null $RcdFasilitas
 * @property string|null $RcdNoUrut
 * @property float|null $RcdQtyPackaging
 * @property string|null $RcdPackaging
 * @property float|null $RcdValue
 * @property int $ProductID
 * @property string $ProductCode
 * @property string|null $ProductHsCode
 * @property string $ProductName
 * @property string|null $ProductUnit
 * @property int $CountryId
 * @property string $CountryName
 * @property int $RcID
 * @property string|null $RcCode
 * @property string|null $RcSize
 * @property string|null $RcType
 * @property string|null $RBCCode
 * @property \Cake\I18n\FrozenDate $RBCDate
 * @property string|null $RCode
 * @property \Cake\I18n\FrozenDate $RDate
 * @property string|null $RRegisterCode
 * @property \Cake\I18n\FrozenDate|null $RRegisterDate
 * @property string|null $RInvoiceCode
 * @property \Cake\I18n\FrozenDate|null $RInvoiceDate
 * @property string|null $RBlCode
 * @property \Cake\I18n\FrozenDate|null $RBlDate
 * @property string|null $RValueType
 * @property float|null $RKursIdr
 * @property int $CurrencyId
 * @property string $CurrencyName
 * @property int $ImporterId
 * @property string|null $ImporterName
 * @property int $SenderId
 * @property string|null $SenderName
 * @property int $PoId
 * @property string|null $PoCode
 * @property int $PoDetailId
 * @property float $PoDetailQty
 * @property float|null $PoDetailPrice
 * @property float|null $PoDetailPriceIdr
 * @property int $SupplierId
 * @property string|null $SupplierName
 */
class VwStocksLocation extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'StocksLocationID' => true,
        'StocksLocationQtyStock' => true,
        'StocksLocationActualStock' => true,
        'PutAwaysDetailID' => true,
        'PutAwayID' => true,
        'PutAwayCode' => true,
        'PutAwayDate' => true,
        'LocationID' => true,
        'LocationName' => true,
        'StockID' => true,
        'StockCode' => true,
        'StockQty' => true,
        'RcdID' => true,
        'RcdCategory' => true,
        'RcdFasilitas' => true,
        'RcdNoUrut' => true,
        'RcdQtyPackaging' => true,
        'RcdPackaging' => true,
        'RcdValue' => true,
        'ProductID' => true,
        'ProductCode' => true,
        'ProductHsCode' => true,
        'ProductName' => true,
        'ProductUnit' => true,
        'CountryId' => true,
        'CountryName' => true,
        'RcID' => true,
        'RcCode' => true,
        'RcSize' => true,
        'RcType' => true,
        'RBCCode' => true,
        'RBCDate' => true,
        'RCode' => true,
        'RDate' => true,
        'RRegisterCode' => true,
        'RRegisterDate' => true,
        'RInvoiceCode' => true,
        'RInvoiceDate' => true,
        'RBlCode' => true,
        'RBlDate' => true,
        'RValueType' => true,
        'RKursIdr' => true,
        'CurrencyId' => true,
        'CurrencyName' => true,
        'ImporterId' => true,
        'ImporterName' => true,
        'SenderId' => true,
        'SenderName' => true,
        'PoId' => true,
        'PoCode' => true,
        'PoDetailId' => true,
        'PoDetailQty' => true,
        'PoDetailPrice' => true,
        'PoDetailPriceIdr' => true,
        'SupplierId' => true,
        'SupplierName' => true
    ];
}
