<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Expenditure Entity
 *
 * @property int $id
 * @property string|null $code
 * @property \Cake\I18n\FrozenDate $date
 * @property string $bc_type
 * @property string $bc_code
 * @property \Cake\I18n\FrozenDate $bc_date
 * @property int $customer_id
 * @property string|null $register_code
 * @property \Cake\I18n\FrozenDate|null $register_date
 * @property int|null $currency_id
 * @property float|null $kurs_idr
 * @property string $file
 * @property string $file_dir
 * @property string|null $note
 * @property int|null $status
 * @property \Cake\I18n\FrozenTime|null $created
 * @property int|null $created_by
 * @property \Cake\I18n\FrozenTime|null $modified
 * @property int|null $modified_by
 *
 * @property \App\Model\Entity\Customer $customer
 * @property \App\Model\Entity\Currency $currency
 * @property \App\Model\Entity\PickingList[] $picking_lists
 * @property \App\Model\Entity\PickingListsDetail[] $picking_lists_details
 */
class Expenditure extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'code' => true,
        'date' => true,
        'bc_type' => true,
        'bc_code' => true,
        'bc_date' => true,
        'customer_id' => true,
        'register_code' => true,
        'register_date' => true,
        'currency_id' => true,
        'kurs_idr' => true,
        'file' => true,
        'file_dir' => true,
        'note' => true,
        'status' => true,
        'created' => true,
        'created_by' => true,
        'modified' => true,
        'modified_by' => true,
        'customer' => true,
        'currency' => true,
        'expenditures_picking_lists' => true,
        'picking_lists' => true,
        'picking_lists_details' => true
    ];
}
