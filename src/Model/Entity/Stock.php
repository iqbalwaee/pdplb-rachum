<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Stock Entity
 *
 * @property int $id
 * @property int|null $receptions_containers_detail_id
 * @property string|null $code
 * @property float|null $qty
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\ReceptionsContainersDetail $receptions_containers_detail
 * @property \App\Model\Entity\PutAwaysDetail[] $put_aways_details
 * @property \App\Model\Entity\Location[] $locations
 */
class Stock extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'receptions_containers_detail_id' => true,
        'code' => true,
        'qty' => true,
        'created' => true,
        'modified' => true,
        'receptions_containers_detail' => true,
        'put_aways_details' => true,
        'locations' => true
    ];
}
