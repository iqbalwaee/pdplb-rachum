<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ReceptionsContainersDetail Entity
 *
 * @property int $id
 * @property int|null $purchase_orders_detail_id
 * @property int $receptions_container_id
 * @property int $product_id
 * @property float $qty
 * @property float|null $qty_packaging
 * @property string|null $packaging
 * @property string|null $category
 * @property string|null $fasilitas
 * @property string|null $no_urut
 * @property int|null $country_id
 * @property float|null $value
 * @property float|null $value_per_unit
 * @property int|null $created_by
 * @property int|null $modified_by
 * @property bool|null $status
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\PurchaseOrdersDetail $purchase_orders_detail
 * @property \App\Model\Entity\ReceptionsContainer $receptions_container
 * @property \App\Model\Entity\Product $product
 * @property \App\Model\Entity\Country $country
 * @property \App\Model\Entity\Stock[] $stocks
 */
class ReceptionsContainersDetail extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'purchase_orders_detail_id' => true,
        'receptions_container_id' => true,
        'product_id' => true,
        'qty' => true,
        'qty_packaging' => true,
        'packaging' => true,
        'category' => true,
        'fasilitas' => true,
        'no_urut' => true,
        'country_id' => true,
        'value' => true,
        'value_per_unit' => true,
        'created_by' => true,
        'modified_by' => true,
        'status' => true,
        'created' => true,
        'modified' => true,
        'purchase_orders_detail' => true,
        'receptions_container' => true,
        'product' => true,
        'country' => true,
        'stocks' => true
    ];
}
