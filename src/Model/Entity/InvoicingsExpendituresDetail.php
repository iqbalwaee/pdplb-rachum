<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * InvoicingsExpendituresDetail Entity
 *
 * @property int $id
 * @property int|null $invoicings_expenditure_id
 * @property int|null $expenditures_picking_lists_detail_id
 * @property float|null $qty
 * @property float|null $amount
 * @property float|null $subtotal
 *
 * @property \App\Model\Entity\InvoicingsExpenditure $invoicings_expenditure
 * @property \App\Model\Entity\ExpendituresPickingListsDetail $expenditures_picking_lists_detail
 */
class InvoicingsExpendituresDetail extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'invoicings_expenditure_id' => true,
        'expenditures_picking_lists_detail_id' => true,
        'qty' => true,
        'amount' => true,
        'hpp' => true,
        'subtotal' => true,
        'invoicings_expenditure' => true,
        'expenditures_picking_lists_detail' => true
    ];
}
