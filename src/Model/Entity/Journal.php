<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Journal Entity
 *
 * @property int $id
 * @property int|null $business_type_id
 * @property \Cake\I18n\FrozenDate|null $date
 * @property string|null $code
 * @property int|null $type
 * @property string|null $title
 * @property string|null $description
 * @property string|null $ref_id
 * @property float|null $debit
 * @property float|null $credit
 * @property string|null $ref_table
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 * @property int|null $created_by
 * @property int|null $modified_by
 *
 * @property \App\Model\Entity\BusinessType $business_type
 * @property \App\Model\Entity\Ref $ref
 * @property \App\Model\Entity\JournalsDetail[] $journals_details
 */
class Journal extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'business_type_id' => true,
        'date' => true,
        'code' => true,
        'type' => true,
        'title' => true,
        'description' => true,
        'ref_id' => true,
        'debit' => true,
        'credit' => true,
        'ref_table' => true,
        'created' => true,
        'modified' => true,
        'created_by' => true,
        'modified_by' => true,
        'business_type' => true,
        'ref' => true,
        'journals_details' => true
    ];
}
