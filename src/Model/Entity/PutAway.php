<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * PutAway Entity
 *
 * @property int $id
 * @property string|null $code
 * @property \Cake\I18n\FrozenDate $date
 * @property int $location_id
 * @property \Cake\I18n\FrozenTime $created
 * @property int $created_by
 * @property \Cake\I18n\FrozenTime|null $modified
 * @property int|null $modified_by
 *
 * @property \App\Model\Entity\Location $location
 * @property \App\Model\Entity\PutAwaysDetail[] $put_aways_details
 */
class PutAway extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'code' => true,
        'date' => true,
        'location_id' => true,
        'created' => true,
        'created_by' => true,
        'modified' => true,
        'modified_by' => true,
        'location' => true,
        'put_aways_details' => true
    ];
}
