<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * InvoicingsExpenditure Entity
 *
 * @property int $id
 * @property int|null $invoicing_id
 * @property int|null $expenditure_id
 *
 * @property \App\Model\Entity\Invoicing $invoicing
 * @property \App\Model\Entity\Expenditure $expenditure
 * @property \App\Model\Entity\InvoicingsExpendituresDetail[] $invoicings_expenditures_details
 */
class InvoicingsExpenditure extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'invoicing_id' => true,
        'expenditure_id' => true,
        'invoicing' => true,
        'expenditure' => true,
        'invoicings_expenditures_details' => true
    ];
}
