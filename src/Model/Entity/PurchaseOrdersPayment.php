<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * PurchaseOrdersPayment Entity
 *
 * @property int $id
 * @property int|null $purchase_order_id
 * @property string|null $code
 * @property \Cake\I18n\FrozenDate|null $date
 * @property int|null $currency_id
 * @property float|null $kurs
 * @property float|null $payment_amount
 * @property float|null $payment_amount_idr
 * @property \Cake\I18n\FrozenTime|null $created
 * @property int|null $created_by
 * @property \Cake\I18n\FrozenTime|null $modified
 * @property int|null $modified_by
 *
 * @property \App\Model\Entity\PurchaseOrder $purchase_order
 * @property \App\Model\Entity\Currency $currency
 */
class PurchaseOrdersPayment extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'purchase_order_id' => true,
        'code' => true,
        'date' => true,
        'currency_id' => true,
        'kurs' => true,
        'payment_amount' => true,
        'payment_amount_idr' => true,
        'created' => true,
        'created_by' => true,
        'modified' => true,
        'modified_by' => true,
        'purchase_order' => true,
        'currency' => true
    ];
}
