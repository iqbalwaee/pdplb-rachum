<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ExpendituresPickingListsDetail Entity
 *
 * @property int $id
 * @property int $expenditures_picking_list_id
 * @property int $picking_lists_detail_id
 * @property float|null $qty
 * @property float|null $amount
 * @property \Cake\I18n\FrozenTime $created
 * @property int $created_by
 * @property \Cake\I18n\FrozenTime|null $modified
 * @property int|null $modified_by
 *
 * @property \App\Model\Entity\ExpendituresPickingList $expenditures_picking_list
 * @property \App\Model\Entity\PickingListsDetail $picking_lists_detail
 */
class ExpendituresPickingListsDetail extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'expenditures_picking_list_id' => true,
        'picking_lists_detail_id' => true,
        'qty' => true,
        'amount' => true,
        'created' => true,
        'created_by' => true,
        'modified' => true,
        'modified_by' => true,
        'expenditures_picking_list' => true,
        'picking_lists_detail' => true
    ];
}
