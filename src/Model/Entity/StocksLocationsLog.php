<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * StocksLocationsLog Entity
 *
 * @property int $id
 * @property int|null $stock_location_id
 * @property string|null $type
 * @property float|null $qty
 * @property int|null $referensi_id
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\Referensi $referensi
 * @property \App\Model\Entity\StockLocation $stocks_location
 */
class StocksLocationsLog extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'stocks_location_id' => true,
        'type' => true,
        'qty' => true,
        'referensi_id' => true,
        'referensi' => true,
        'created' => true,
        'modified' => true,
        'stocks_location' => true
    ];
}
