<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Reception Entity
 *
 * @property int $id
 * @property int|null $purchase_order_id
 * @property string|null $code
 * @property \Cake\I18n\FrozenDate $date
 * @property string $bc_type
 * @property string $bc_code
 * @property \Cake\I18n\FrozenDate $bc_date
 * @property int $importer_id
 * @property int|null $sender_id
 * @property string|null $register_code
 * @property \Cake\I18n\FrozenDate|null $register_date
 * @property string|null $bl_code
 * @property \Cake\I18n\FrozenDate|null $bl_date
 * @property string|null $invoice_code
 * @property \Cake\I18n\FrozenDate|null $invoice_date
 * @property int|null $currency_id
 * @property float|null $kurs_idr
 * @property string|null $value_type
 * @property string $file
 * @property string $file_dir
 * @property string|null $note
 * @property int|null $status
 * @property \Cake\I18n\FrozenTime|null $created
 * @property int|null $created_by
 * @property \Cake\I18n\FrozenTime|null $modified
 * @property int|null $modified_by
 *
 * @property \App\Model\Entity\PurchaseOrder $purchase_order
 * @property \App\Model\Entity\Importer $importer
 * @property \App\Model\Entity\Sender $sender
 * @property \App\Model\Entity\Currency $currency
 * @property \App\Model\Entity\ReceptionsContainer[] $receptions_containers
 */
class Reception extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'purchase_order_id' => true,
        'code' => true,
        'date' => true,
        'bc_type' => true,
        'bc_code' => true,
        'bc_date' => true,
        'importer_id' => true,
        'sender_id' => true,
        'register_code' => true,
        'register_date' => true,
        'bl_code' => true,
        'bl_date' => true,
        'invoice_code' => true,
        'invoice_date' => true,
        'currency_id' => true,
        'kurs_idr' => true,
        'value_type' => true,
        'file' => true,
        'file_dir' => true,
        'note' => true,
        'status' => true,
        'created' => true,
        'created_by' => true,
        'modified' => true,
        'modified_by' => true,
        'purchase_order' => true,
        'importer' => true,
        'sender' => true,
        'currency' => true,
        'receptions_containers' => true
    ];
}
