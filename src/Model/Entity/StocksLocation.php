<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * StocksLocation Entity
 *
 * @property int $id
 * @property int|null $stock_id
 * @property int|null $put_aways_detail_id
 * @property float|null $qty_stock
 * @property float|null $actual_stock
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\Stock $stock
 * @property \App\Model\Entity\PutAwaysDetail $put_aways_detail
 */
class StocksLocation extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'stock_id' => true,
        'put_aways_detail_id' => true,
        'qty_stock' => true,
        'actual_stock' => true,
        'created' => true,
        'modified' => true,
        'stock' => true,
        'stocks_locations_logs' => true,
        'put_aways_detail' => true
    ];
}
