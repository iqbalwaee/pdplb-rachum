<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * TravelDocumentsDetail Entity
 *
 * @property int $id
 * @property int|null $travel_document_id
 * @property int|null $expenditures_picking_lists_detail_id
 * @property float|null $qty
 *
 * @property \App\Model\Entity\TravelDocument $travel_document
 * @property \App\Model\Entity\ExpendituresPickingListsDetail $expenditures_picking_lists_detail
 */
class TravelDocumentsDetail extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'travel_document_id' => true,
        'expenditures_picking_lists_detail_id' => true,
        'qty' => true,
        'travel_document' => true,
        'expenditures_picking_lists_detail' => true
    ];
}
