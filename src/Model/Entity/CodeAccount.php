<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * CodeAccount Entity
 *
 * @property int $id
 * @property int|null $parent_id
 * @property string|null $code
 * @property string|null $name
 * @property bool $normal_balance
 * @property int|null $lft
 * @property int|null $rght
 * @property int|null $level
 * @property int|null $laba_rugi
 * @property int|null $neraca
 * @property int|null $arus_kas
 * @property int|null $sort
 * @property bool|null $status
 * @property int|null $created_by
 * @property \Cake\I18n\FrozenTime|null $created
 * @property int|null $modified_by
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\ParentCodeAccount $parent_code_account
 * @property \App\Model\Entity\BusinessTypesAccount[] $business_types_accounts
 * @property \App\Model\Entity\ChildCodeAccount[] $child_code_accounts
 */
class CodeAccount extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'parent_id' => true,
        'code' => true,
        'name' => true,
        'normal_balance' => true,
        'lft' => true,
        'rght' => true,
        'level' => true,
        'laba_rugi' => true,
        'neraca' => true,
        'arus_kas' => true,
        'sort' => true,
        'status' => true,
        'created_by' => true,
        'created' => true,
        'modified_by' => true,
        'modified' => true,
        'parent_code_account' => true,
        'business_types_accounts' => true,
        'child_code_accounts' => true
    ];
}
