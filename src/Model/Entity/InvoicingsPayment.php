<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * InvoicingsPayment Entity
 *
 * @property int $id
 * @property int|null $invoicing_id
 * @property string|null $code
 * @property \Cake\I18n\FrozenDate|null $date
 * @property float|null $payment_amount
 * @property \Cake\I18n\FrozenTime|null $created
 * @property int|null $created_by
 * @property \Cake\I18n\FrozenTime|null $modified
 * @property int|null $modified_by
 *
 * @property \App\Model\Entity\Invoicing $invoicing
 * @property \App\Model\Entity\Currency $currency
 */
class InvoicingsPayment extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'invoicing_id' => true,
        'code' => true,
        'date' => true,
        'payment_amount' => true,
        'created' => true,
        'created_by' => true,
        'modified' => true,
        'modified_by' => true,
        'invoicing' => true,
        'currency' => true
    ];
}
