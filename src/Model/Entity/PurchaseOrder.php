<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * PurchaseOrder Entity
 *
 * @property int $id
 * @property string|null $code
 * @property int $supplier_id
 * @property \Cake\I18n\FrozenDate $date
 * @property int|null $currency_id
 * @property float|null $kurs
 * @property string|null $note
 * @property float|null $total_amount
 * @property float|null $total_amount_idr
 * @property int|null $status
 * @property \Cake\I18n\FrozenTime|null $created
 * @property int|null $created_by
 * @property \Cake\I18n\FrozenTime|null $modified
 * @property int|null $modified_by
 *
 * @property \App\Model\Entity\Supplier $supplier
 * @property \App\Model\Entity\Currency $currency
 * @property \App\Model\Entity\PurchaseOrdersDetail[] $purchase_orders_details
 * @property \App\Model\Entity\Reception[] $receptions
 */
class PurchaseOrder extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'code' => true,
        'supplier_id' => true,
        'date' => true,
        'currency_id' => true,
        'kurs' => true,
        'note' => true,
        'total_amount' => true,
        'total_amount_idr' => true,
        'status' => true,
        'created' => true,
        'created_by' => true,
        'modified' => true,
        'modified_by' => true,
        'supplier' => true,
        'currency' => true,
        'purchase_orders_details' => true,
        'receptions' => true
    ];
}
