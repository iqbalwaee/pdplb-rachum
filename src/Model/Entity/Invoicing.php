<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Invoicing Entity
 *
 * @property int $id
 * @property string|null $code
 * @property int|null $customer_id
 * @property \Cake\I18n\FrozenDate|null $date
 * @property float|null $total_amount
 * @property int|null $status
 * @property \Cake\I18n\FrozenTime|null $created
 * @property int|null $created_by
 * @property \Cake\I18n\FrozenTime|null $modified
 * @property int|null $modified_by
 *
 * @property \App\Model\Entity\Customer $customer
 * @property \App\Model\Entity\Expenditure[] $expenditures
 */
class Invoicing extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'code' => true,
        'customer_id' => true,
        'date' => true,
        'total_amount' => true,
        'total_hpp' => true,
        'status' => true,
        'created' => true,
        'created_by' => true,
        'modified' => true,
        'modified_by' => true,
        'customer' => true,
        'expenditures' => true,
        'invoicings_expenditures' => true,
    ];
}
