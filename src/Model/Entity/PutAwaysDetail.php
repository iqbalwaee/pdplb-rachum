<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * PutAwaysDetail Entity
 *
 * @property int $id
 * @property int $put_away_id
 * @property int $stock_id
 * @property float $qty
 * @property \Cake\I18n\FrozenTime $created
 * @property int $created_by
 * @property \Cake\I18n\FrozenTime|null $modified
 * @property int|null $modified_by
 *
 * @property \App\Model\Entity\PutAway $put_away
 * @property \App\Model\Entity\Stock $stock
 * @property \App\Model\Entity\StocksLocation[] $stocks_locations
 */
class PutAwaysDetail extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'put_away_id' => true,
        'stock_id' => true,
        'qty' => true,
        'created' => true,
        'created_by' => true,
        'modified' => true,
        'modified_by' => true,
        'put_away' => true,
        'stock' => true,
        'stocks_locations' => true
    ];
}
