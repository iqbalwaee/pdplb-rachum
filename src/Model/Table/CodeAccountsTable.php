<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * CodeAccounts Model
 *
 * @property \App\Model\Table\CodeAccountsTable|\Cake\ORM\Association\BelongsTo $ParentCodeAccounts
 * @property \App\Model\Table\BusinessTypesAccountsTable|\Cake\ORM\Association\HasMany $BusinessTypesAccounts
 * @property \App\Model\Table\CodeAccountsTable|\Cake\ORM\Association\HasMany $ChildCodeAccounts
 *
 * @method \App\Model\Entity\CodeAccount get($primaryKey, $options = [])
 * @method \App\Model\Entity\CodeAccount newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\CodeAccount[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\CodeAccount|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CodeAccount|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CodeAccount patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\CodeAccount[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\CodeAccount findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 * @mixin \Cake\ORM\Behavior\TreeBehavior
 */
class CodeAccountsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('code_accounts');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Tree', [
            'level' => 'level', // Defaults to null, i.e. no level saving
        ]);
        $this->belongsTo('ParentCodeAccounts', [
            'className' => 'CodeAccounts',
            'foreignKey' => 'parent_id'
        ]);
        $this->hasMany('BusinessTypesAccounts', [
            'foreignKey' => 'code_account_id'
        ]);
        $this->hasMany('ChildCodeAccounts', [
            'className' => 'CodeAccounts',
            'foreignKey' => 'parent_id'
        ]);
        $this->hasMany('JournalsDetails', [
            'className' => 'CodeAccounts',
            'foreignKey' => 'code_account_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->scalar('code')
            ->maxLength('code', 50)
            ->allowEmptyString('code');

        $validator
            ->scalar('name')
            ->maxLength('name', 225)
            ->allowEmptyString('name');

        $validator
            ->boolean('normal_balance')
            ->requirePresence('normal_balance', 'create')
            ->allowEmptyString('normal_balance', false);

        $validator
            ->integer('level')
            ->allowEmptyString('level');

        $validator
            ->allowEmptyString('laba_rugi');

        $validator
            ->allowEmptyString('neraca');

        $validator
            ->allowEmptyString('arus_kas');

        $validator
            ->integer('sort')
            ->allowEmptyString('sort');

        $validator
            ->boolean('status')
            ->allowEmptyString('status');

        $validator
            ->integer('created_by')
            ->allowEmptyString('created_by');

        $validator
            ->integer('modified_by')
            ->allowEmptyString('modified_by');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['parent_id'], 'ParentCodeAccounts'));

        return $rules;
    }

    public function getLabaRugi($date)
    {
        $addWhere = "";
        $codeAccountsPendapatan                 = $this->find()
                                        ->where([
                                            'CodeAccounts.laba_rugi' => 1,
                                            'CodeAccounts.code LIKE "4%"'
                                        ])
                                        ->order([
                                            'CodeAccounts.code ASC'
                                        ])
                                        ->select($this)
                                        ->select([
                                            'amount' => '(
                                                CASE
                                                    WHEN ((CodeAccounts.lft - CodeAccounts.rght) = -1) THEN
                                                        (
                                                            SELECT 
                                                                (
                                                                    CASE WHEN (`CodeAccounts`.`normal_balance` = 0) THEN
                                                                        IFNULL(SUM(journals_details.debit - journals_details.credit),0)
                                                                    ELSE 
                                                                        IFNULL(SUM(journals_details.credit - journals_details.debit),0)
                                                                    END
                                                                ) as AMT 
                                                            FROM journals_details
                                                            INNER JOIN journals on journals.id = journals_details.journal_id
                                                            WHERE 
                                                            journals_details.code_account_id = CodeAccounts.id
                                                            AND
                                                            (
                                                                journals.date < "'.$date.'"
                                                            )'.
                                                            $addWhere
                                                            .'
                                                        )
                                                    ELSE
                                                        (
                                                            SELECT 
                                                            (
                                                                CASE WHEN (`CDA`.`normal_balance` = 0) THEN
                                                                    IFNULL(SUM(journals_details.debit - journals_details.credit),0)
                                                                ELSE 
                                                                    IFNULL(SUM(journals_details.credit - journals_details.debit),0)
                                                                END
                                                            ) as AMT
                                                            FROM journals_details
                                                            INNER JOIN
                                                            journals on journals.id = journals_details.journal_id
                                                            INNER JOIN
                                                            code_accounts as CDA on CDA.id = journals_details.code_account_id 
                                                            WHERE
                                                            `CDA`.`lft` > `CodeAccounts`.`lft` AND CDA.rght < `CodeAccounts`.`rght`
                                                            AND (CDA.lft - CDA.rght) = -1
                                                            AND
                                                            (
                                                                journals.date < "'.$date.'"
                                                            )'.
                                                            $addWhere
                                                            .'
                                                        )
                                                END
                                            )'
                                        ]);

        
        $codeAccountsBiayaMaterial              = $this->find()
                                            ->where([
                                                'CodeAccounts.laba_rugi' => 1,
                                                '(CodeAccounts.lft - CodeAccounts.rght) != -1',
                                                'CodeAccounts.code LIKE "5%"'
                                            ])
                                            ->order([
                                                'CodeAccounts.code ASC'
                                            ])
                                            ->select($this)
                                            ->select([
                                                'amount' => '(
                                                    CASE
                                                        WHEN ((CodeAccounts.lft - CodeAccounts.rght) = -1) THEN
                                                            (
                                                                SELECT 
                                                                    (
                                                                        CASE WHEN (`CodeAccounts`.`normal_balance` = 0) THEN
                                                                            IFNULL(SUM(journals_details.debit - journals_details.credit),0)
                                                                        ELSE 
                                                                            IFNULL(SUM(journals_details.credit - journals_details.debit),0)
                                                                        END
                                                                    ) as AMT 
                                                                FROM journals_details
                                                                INNER JOIN journals on journals.id = journals_details.journal_id
                                                                WHERE 
                                                                journals_details.code_account_id = CodeAccounts.id
                                                                AND
                                                                (
                                                                    journals.date < "'.$date.'"
                                                                )'.
                                                                $addWhere
                                                                .'
                                                            )
                                                        ELSE
                                                            (
                                                                SELECT 
                                                                (
                                                                    CASE WHEN (`CDA`.`normal_balance` = 0) THEN
                                                                        IFNULL(SUM(journals_details.debit - journals_details.credit),0)
                                                                    ELSE 
                                                                        IFNULL(SUM(journals_details.credit - journals_details.debit),0)
                                                                    END
                                                                ) as AMT
                                                                FROM journals_details
                                                                INNER JOIN
                                                                journals on journals.id = journals_details.journal_id
                                                                INNER JOIN
                                                                code_accounts as CDA on CDA.id = journals_details.code_account_id 
                                                                WHERE
                                                                `CDA`.`lft` > `CodeAccounts`.`lft` AND CDA.rght < `CodeAccounts`.`rght`
                                                                AND (CDA.lft - CDA.rght) = -1
                                                                AND
                                                                (
                                                                    journals.date < "'.$date.'"
                                                                )'.
                                                                $addWhere
                                                                .'
                                                            )
                                                    END
                                                )'
                                            ]);

        
        $codeAccountsBiayaAdum                  = $this->find()
                                        ->where([
                                            'CodeAccounts.laba_rugi' => 1,
                                            '(CodeAccounts.lft - CodeAccounts.rght) != -1',
                                            'CodeAccounts.code LIKE "6%"'
                                        ])
                                        ->order([
                                            'CodeAccounts.code ASC'
                                        ])
                                        ->select($this)
                                        ->select([
                                            'amount' => '(
                                                CASE
                                                    WHEN ((CodeAccounts.lft - CodeAccounts.rght) = -1) THEN
                                                        (
                                                            SELECT 
                                                                (
                                                                    CASE WHEN (`CodeAccounts`.`normal_balance` = 0) THEN
                                                                        IFNULL(SUM(journals_details.debit - journals_details.credit),0)
                                                                    ELSE 
                                                                        IFNULL(SUM(journals_details.credit - journals_details.debit),0)
                                                                    END
                                                                ) as AMT 
                                                            FROM journals_details
                                                            INNER JOIN journals on journals.id = journals_details.journal_id
                                                            WHERE 
                                                            journals_details.code_account_id = CodeAccounts.id
                                                            AND
                                                            (
                                                                journals.date < "'.$date.'"
                                                            )'.
                                                            $addWhere
                                                            .'
                                                        )
                                                    ELSE
                                                        (
                                                            SELECT 
                                                            (
                                                                CASE WHEN (`CDA`.`normal_balance` = 0) THEN
                                                                    IFNULL(SUM(journals_details.debit - journals_details.credit),0)
                                                                ELSE 
                                                                    IFNULL(SUM(journals_details.credit - journals_details.debit),0)
                                                                END
                                                            ) as AMT
                                                            FROM journals_details
                                                            INNER JOIN
                                                            journals on journals.id = journals_details.journal_id
                                                            INNER JOIN
                                                            code_accounts as CDA on CDA.id = journals_details.code_account_id 
                                                            WHERE
                                                            `CDA`.`lft` > `CodeAccounts`.`lft` AND CDA.rght < `CodeAccounts`.`rght`
                                                            AND (CDA.lft - CDA.rght) = -1
                                                            AND
                                                            (
                                                                journals.date < "'.$date.'"
                                                            )'.
                                                            $addWhere
                                                            .'
                                                        )
                                                END
                                            )'
                                        ]);
        
        $codeAccountsPendapatanLainnya          = $this->find()
                                ->where([
                                    'CodeAccounts.laba_rugi' => 1,
                                    // '(CodeAccounts.lft - CodeAccounts.rght) != -1',
                                    'CodeAccounts.code LIKE "7%"'
                                ])
                                ->order([
                                    'CodeAccounts.code ASC'
                                ])
                                ->select($this)
                                ->select([
                                    'amount' => '(
                                        CASE
                                            WHEN ((CodeAccounts.lft - CodeAccounts.rght) = -1) THEN
                                                (
                                                    SELECT 
                                                        (
                                                            CASE WHEN (`CodeAccounts`.`normal_balance` = 0) THEN
                                                                IFNULL(SUM(journals_details.debit - journals_details.credit),0)
                                                            ELSE 
                                                                IFNULL(SUM(journals_details.credit - journals_details.debit),0)
                                                            END
                                                        ) as AMT 
                                                    FROM journals_details
                                                    INNER JOIN journals on journals.id = journals_details.journal_id
                                                    WHERE 
                                                    journals_details.code_account_id = CodeAccounts.id
                                                    AND
                                                    (
                                                        journals.date < "'.$date.'"
                                                    )'.
                                                    $addWhere
                                                    .'
                                                )
                                            ELSE
                                                (
                                                    SELECT 
                                                    (
                                                        CASE WHEN (`CDA`.`normal_balance` = 0) THEN
                                                            IFNULL(SUM(journals_details.debit - journals_details.credit),0)
                                                        ELSE 
                                                            IFNULL(SUM(journals_details.credit - journals_details.debit),0)
                                                        END
                                                    ) as AMT
                                                    FROM journals_details
                                                    INNER JOIN
                                                    journals on journals.id = journals_details.journal_id
                                                    INNER JOIN
                                                    code_accounts as CDA on CDA.id = journals_details.code_account_id 
                                                    WHERE
                                                    `CDA`.`lft` > `CodeAccounts`.`lft` AND CDA.rght < `CodeAccounts`.`rght`
                                                    AND (CDA.lft - CDA.rght) = -1
                                                    AND
                                                    (
                                                        journals.date < "'.$date.'"
                                                    )'.
                                                    $addWhere
                                                    .'
                                                )
                                        END
                                    )'
                                ]);
        
        $codeAccountsBiayaLainnya               = $this->find()
                                ->where([
                                    'CodeAccounts.laba_rugi' => 1,
                                    // '(CodeAccounts.lft - CodeAccounts.rght) != -1',
                                    'CodeAccounts.code LIKE "8%"'
                                ])
                                ->order([
                                    'CodeAccounts.code ASC'
                                ])
                                ->select($this)
                                ->select([
                                    'amount' => '(
                                        CASE
                                            WHEN ((CodeAccounts.lft - CodeAccounts.rght) = -1) THEN
                                                (
                                                    SELECT 
                                                        (
                                                            CASE WHEN (`CodeAccounts`.`normal_balance` = 0) THEN
                                                                IFNULL(SUM(journals_details.debit - journals_details.credit),0)
                                                            ELSE 
                                                                IFNULL(SUM(journals_details.credit - journals_details.debit),0)
                                                            END
                                                        ) as AMT 
                                                    FROM journals_details
                                                    INNER JOIN journals on journals.id = journals_details.journal_id
                                                    WHERE 
                                                    journals_details.code_account_id = CodeAccounts.id
                                                    AND
                                                    (
                                                        journals.date < "'.$date.'"
                                                    )'.
                                                    $addWhere
                                                    .'
                                                )
                                            ELSE
                                                (
                                                    SELECT 
                                                    (
                                                        CASE WHEN (`CDA`.`normal_balance` = 0) THEN
                                                            IFNULL(SUM(journals_details.debit - journals_details.credit),0)
                                                        ELSE 
                                                            IFNULL(SUM(journals_details.credit - journals_details.debit),0)
                                                        END
                                                    ) as AMT
                                                    FROM journals_details
                                                    INNER JOIN
                                                    journals on journals.id = journals_details.journal_id
                                                    INNER JOIN
                                                    code_accounts as CDA on CDA.id = journals_details.code_account_id 
                                                    WHERE
                                                    `CDA`.`lft` > `CodeAccounts`.`lft` AND CDA.rght < `CodeAccounts`.`rght`
                                                    AND (CDA.lft - CDA.rght) = -1
                                                    AND
                                                    (
                                                        journals.date < "'.$date.'"
                                                    )'.
                                                    $addWhere
                                                    .'
                                                )
                                        END
                                    )'
                                ]);
        
        
        //PENDAPATAN//
        $codeAccountsPendapatan = $codeAccountsPendapatan->find('threaded')->toArray();
        $totalPendapatan = 0;
        if(!empty($codeAccountsPendapatan)){
            $totalPendapatan = $codeAccountsPendapatan[0]->amount;
        }                  
        //END PENDAPATAN//
        
        //BIAYA MATERIAL//
        $codeAccountsBiayaMaterial = $codeAccountsBiayaMaterial->find('threaded')->toArray();
        $totalBiayaMaterial = 0;
        if(!empty($codeAccountsBiayaMaterial)){
            $totalBiayaMaterial = $codeAccountsBiayaMaterial[0]->amount;
        }
        //END BIAYA MATERIAL//


        //BIAYA ADUM//
        $codeAccountsBiayaAdum = $codeAccountsBiayaAdum->find('threaded')->toArray();

        $totalBiayaAdum = 0;
        if(!empty($codeAccountsBiayaAdum)){
            $totalBiayaAdum = $codeAccountsBiayaAdum[0]->amount;
        }
        $totalBeban = $totalBiayaAdum + $totalBiayaMaterial;
        //END BIAYA ADUM
        
        //PENDAPATAN LAINNYA//
        $codeAccountsPendapatanLainnya = $codeAccountsPendapatanLainnya->find('threaded')->toArray();
        $totalPendapatanLainnya = 0;
        if(!empty($codeAccountsPendapatanLainnya)){
            $totalPendapatanLainnya = $codeAccountsPendapatanLainnya[0]->amount;
        }
        //END PENDAPATAN LAINNYA//

        //BIAYA LAINNYA//
        $codeAccountsBiayaLainnya = $codeAccountsBiayaLainnya->find('threaded')->toArray();      
        $totalBiayaLainnya = 0;
        if(!empty($codeAccountsBiayaLainnya)){
            $totalBiayaLainnya = $codeAccountsBiayaLainnya[0]->amount;
        }
        //BIAYA LAINNYA//

        $totalLaba = ($totalPendapatan + $totalPendapatanLainnya) - ($totalBeban + $totalBiayaLainnya);
        return $totalLaba;
    }

}
