<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TravelDocumentsDetails Model
 *
 * @property \App\Model\Table\TravelDocumentsTable|\Cake\ORM\Association\BelongsTo $TravelDocuments
 * @property \App\Model\Table\ExpendituresPickingListsDetailsTable|\Cake\ORM\Association\BelongsTo $ExpendituresPickingListsDetails
 *
 * @method \App\Model\Entity\TravelDocumentsDetail get($primaryKey, $options = [])
 * @method \App\Model\Entity\TravelDocumentsDetail newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\TravelDocumentsDetail[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\TravelDocumentsDetail|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TravelDocumentsDetail|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TravelDocumentsDetail patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\TravelDocumentsDetail[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\TravelDocumentsDetail findOrCreate($search, callable $callback = null, $options = [])
 */
class TravelDocumentsDetailsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('travel_documents_details');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('TravelDocuments', [
            'foreignKey' => 'travel_document_id'
        ]);
        $this->belongsTo('ExpendituresPickingListsDetails', [
            'foreignKey' => 'expenditures_picking_lists_detail_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmptyString('id', 'create')
            ->add('id', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->decimal('qty')
            ->allowEmptyString('qty');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['id']));
        $rules->add($rules->existsIn(['travel_document_id'], 'TravelDocuments'));
        $rules->add($rules->existsIn(['expenditures_picking_lists_detail_id'], 'ExpendituresPickingListsDetails'));

        return $rules;
    }
}
