<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Expenditures Model
 *
 * @property \App\Model\Table\CustomersTable|\Cake\ORM\Association\BelongsTo $Customers
 * @property \App\Model\Table\CurrenciesTable|\Cake\ORM\Association\BelongsTo $Currencies
 * @property \App\Model\Table\PickingListsTable|\Cake\ORM\Association\BelongsToMany $PickingLists
 * @property \App\Model\Table\PickingListsDetailsTable|\Cake\ORM\Association\BelongsToMany $PickingListsDetails
 *
 * @method \App\Model\Entity\Expenditure get($primaryKey, $options = [])
 * @method \App\Model\Entity\Expenditure newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Expenditure[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Expenditure|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Expenditure|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Expenditure patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Expenditure[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Expenditure findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ExpendituresTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('expenditures');
        $this->setDisplayField('code');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Customers', [
            'foreignKey' => 'customer_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Currencies', [
            'foreignKey' => 'currency_id'
        ]);
        // $this->belongsToMany('PickingLists', [
        //     'foreignKey' => 'expenditure_id',
        //     'targetForeignKey' => 'picking_list_id',
        //     'joinTable' => 'expenditures_picking_lists'
        // ]);
        // $this->belongsToMany('PickingListsDetails', [
        //     'foreignKey' => 'expenditure_id',
        //     'targetForeignKey' => 'picking_lists_detail_id',
        //     'joinTable' => 'expenditures_picking_lists_details'
        // ]);
        $this->hasMany('ExpendituresPickingLists',[
            'foreignKey' => 'expenditure_id',
            'dependent' => true,
            'cascadeCallbacks' => true,
        ]);
        $this->belongsTo('CreatedUsers', [
            'foreignKey' => 'created_by',
            'className'=>'Users'
        ]);
        $this->belongsTo('ModifiedUsers', [
            'foreignKey' => 'modified_by',
            'className'=>'Users'
        ]);
        $this->addBehavior('Josegonzalez/Upload.Upload', [
            'file' => [
                'fields' => [
                    'dir' => 'file_dir',
                ],
            ],
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmptyString('id', 'create');

        $validator
            ->scalar('code')
            ->maxLength('code', 100)
            ->allowEmptyString('code');

        $validator
            ->date('date')
            ->requirePresence('date', 'create')
            ->allowEmptyDate('date', false);

        $validator
            ->scalar('bc_type')
            ->maxLength('bc_type', 225)
            ->requirePresence('bc_type', 'create')
            ->allowEmptyString('bc_type', false);

        $validator
            ->scalar('bc_code')
            ->maxLength('bc_code', 225)
            ->requirePresence('bc_code', 'create')
            ->allowEmptyString('bc_code', false);

        $validator
            ->date('bc_date')
            ->requirePresence('bc_date', 'create')
            ->allowEmptyDate('bc_date', false);

        $validator
            ->scalar('register_code')
            ->maxLength('register_code', 225)
            ->allowEmptyString('register_code');

        $validator
            ->date('register_date')
            ->allowEmptyDate('register_date');

        $validator
            ->decimal('kurs_idr')
            ->allowEmptyString('kurs_idr');

        $validator
            ->scalar('note')
            ->allowEmptyString('note');

        $validator
            ->allowEmptyString('status');

        $validator
            ->integer('created_by')
            ->allowEmptyString('created_by');

        $validator
            ->integer('modified_by')
            ->allowEmptyString('modified_by');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['customer_id'], 'Customers'));
        $rules->add($rules->existsIn(['currency_id'], 'Currencies'));

        return $rules;
    }

    public function beforeSave(\Cake\Event\Event $event, \Cake\ORM\Entity $entity, 
    \ArrayObject $options)
    {
        if($entity->isNew()){
            $date       = $entity->date->format('Y-m-d');
            $year       = substr($date,0,4);
            $month      = substr($date,5,2);
            
            $getLastCode = $this->find()->select([
                'codeNum'=>'(CAST(SUBSTR(`code`,13,5) AS UNSIGNED) )'
            ])
            ->where([
                'YEAR(date)'    => $year,
                'MONTH(date)'   => $month 
            ])
            ->order('codeNum DESC')
            ->first();
            if(!empty($getLastCode)){
                $lastCode = $getLastCode->codeNum;
            }else{
                $lastCode = 0;
            }
            $lastCode       = $lastCode +1;
            $entity->code   = "DEV.".$year.".".$month.".".str_pad($lastCode,5,0,STR_PAD_LEFT);
        }
        return true;
    }
}
