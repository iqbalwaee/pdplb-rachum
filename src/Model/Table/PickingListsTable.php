<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * PickingLists Model
 *
 * @property \App\Model\Table\CustomersTable|\Cake\ORM\Association\BelongsTo $Customers
 * @property \App\Model\Table\ExpendituresPickingListsDetailsTable|\Cake\ORM\Association\HasMany $ExpendituresPickingListsDetails
 * @property \App\Model\Table\PickingListsDetailsTable|\Cake\ORM\Association\HasMany $PickingListsDetails
 * @property \App\Model\Table\ExpendituresTable|\Cake\ORM\Association\BelongsToMany $Expenditures
 *
 * @method \App\Model\Entity\PickingList get($primaryKey, $options = [])
 * @method \App\Model\Entity\PickingList newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\PickingList[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\PickingList|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PickingList|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PickingList patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\PickingList[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\PickingList findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class PickingListsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('picking_lists');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Customers', [
            'foreignKey' => 'customer_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('ExpendituresPickingListsDetails', [
            'foreignKey' => 'picking_list_id'
        ]);
        $this->hasMany('PickingListsDetails', [
            'foreignKey' => 'picking_list_id',
            'dependent' => true,
            'cascadeCallbacks' => true,
        ]);
        $this->belongsToMany('Expenditures', [
            'foreignKey' => 'picking_list_id',
            'targetForeignKey' => 'expenditure_id',
            'joinTable' => 'expenditures_picking_lists'
        ]);

        $this->belongsTo('CreatedUsers', [
            'foreignKey' => 'created_by',
            'className'=>'Users'
        ]);
        $this->belongsTo('ModifiedUsers', [
            'foreignKey' => 'modified_by',
            'className'=>'Users'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmptyString('id', 'create');

        $validator
            ->scalar('code')
            ->maxLength('code', 225)
            ->allowEmptyString('code');

        $validator
            ->date('date')
            ->requirePresence('date', 'create')
            ->allowEmptyDate('date', false);

        $validator
            ->integer('created_by')
            ->allowEmptyString('created_by');

        $validator
            ->integer('modified_by')
            ->allowEmptyString('modified_by');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['customer_id'], 'Customers'));

        return $rules;
    }

    public function beforeSave(\Cake\Event\Event $event, \Cake\ORM\Entity $entity, 
    \ArrayObject $options)
    {
        if($entity->isNew()){
            $date       = $entity->date->format('Y-m-d');
            $year       = substr($date,0,4);
            $month      = substr($date,5,2);
            
            $getLastCode = $this->find()->select([
                'codeNum'=>'(CAST(SUBSTR(`code`,12,5) AS UNSIGNED) )'
            ])
            ->where([
                'YEAR(date)'    => $year,
                'MONTH(date)'   => $month 
            ])
            ->order('codeNum DESC')
            ->first();
            if(!empty($getLastCode)){
                $lastCode = $getLastCode->codeNum;
            }else{
                $lastCode = 0;
            }
            $lastCode       = $lastCode +1;
            $entity->code   = "PL.".$year.".".$month.".".str_pad($lastCode,5,0,STR_PAD_LEFT);
        }
        return true;
    }
}
