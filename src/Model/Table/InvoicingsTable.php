<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Invoicings Model
 *
 * @property \App\Model\Table\CustomersTable|\Cake\ORM\Association\BelongsTo $Customers
 * @property \App\Model\Table\ExpendituresTable|\Cake\ORM\Association\BelongsToMany $Expenditures
 *
 * @method \App\Model\Entity\Invoicing get($primaryKey, $options = [])
 * @method \App\Model\Entity\Invoicing newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Invoicing[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Invoicing|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Invoicing|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Invoicing patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Invoicing[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Invoicing findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class InvoicingsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('invoicings');
        $this->setDisplayField('code');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Customers', [
            'foreignKey' => 'customer_id'
        ]);
        $this->belongsToMany('Expenditures', [
            'foreignKey' => 'invoicing_id',
            'targetForeignKey' => 'expenditure_id',
            'joinTable' => 'invoicings_expenditures'
        ]);
        $this->hasMany('InvoicingsExpenditures', [
            'foreignKey' => 'invoicing_id',
            'dependent' => true,
            'cascadeCallbacks' => true,
        ]);
        $this->hasMany('InvoicingsPayments', [
            'foreignKey' => 'invoicing_id',
            'dependent' => true,
            'cascadeCallbacks' => true,
        ]);

        $this->belongsTo('CreatedUsers', [
            'foreignKey' => 'created_by',
            'className'=>'Users'
        ]);
        $this->belongsTo('ModifiedUsers', [
            'foreignKey' => 'modified_by',
            'className'=>'Users'
        ]);
        
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmptyString('id', 'create')
            ->add('id', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('code')
            ->maxLength('code', 100)
            ->allowEmptyString('code');

        $validator
            ->date('date')
            ->allowEmptyDate('date');

        $validator
            ->decimal('total_amount')
            ->allowEmptyString('total_amount');

        $validator
            ->allowEmptyString('status');

        $validator
            ->integer('created_by')
            ->allowEmptyString('created_by');

        $validator
            ->integer('modified_by')
            ->allowEmptyString('modified_by');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['id']));
        $rules->add($rules->existsIn(['customer_id'], 'Customers'));

        return $rules;
    }

    public function beforeSave(\Cake\Event\Event $event, \Cake\ORM\Entity $entity, 
    \ArrayObject $options)
    {
        if($entity->isNew()){
            $date       = $entity->date->format('Y-m-d');
            $year       = substr($date,0,4);
            $month      = substr($date,5,2);
            
            $getLastCode = $this->find()->select([
                'codeNum'=>'(CAST(SUBSTR(`code`,13,5) AS UNSIGNED) )'
            ])
            ->where([
                'YEAR(date)'    => $year,
                'MONTH(date)'   => $month 
            ])
            ->order('codeNum DESC')
            ->first();
            if(!empty($getLastCode)){
                $lastCode = $getLastCode->codeNum;
            }else{
                $lastCode = 0;
            }
            $lastCode       = $lastCode +1;
            $entity->code   = "INV.".$year.".".$month.".".str_pad($lastCode,5,0,STR_PAD_LEFT);
        }
        return true;
    }
}
