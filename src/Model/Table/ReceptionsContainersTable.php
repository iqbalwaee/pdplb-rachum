<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ReceptionsContainers Model
 *
 * @property \App\Model\Table\ReceptionsTable|\Cake\ORM\Association\BelongsTo $Receptions
 * @property \App\Model\Table\ReceptionsContainersDetailsTable|\Cake\ORM\Association\HasMany $ReceptionsContainersDetails
 *
 * @method \App\Model\Entity\ReceptionsContainer get($primaryKey, $options = [])
 * @method \App\Model\Entity\ReceptionsContainer newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ReceptionsContainer[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ReceptionsContainer|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ReceptionsContainer|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ReceptionsContainer patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ReceptionsContainer[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ReceptionsContainer findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ReceptionsContainersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('receptions_containers');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Receptions', [
            'foreignKey' => 'reception_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('ReceptionsContainersDetails', [
            'foreignKey' => 'receptions_container_id',
            'dependent' => true,
            'cascadeCallbacks' => true,
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmptyString('id', 'create');

        $validator
            ->scalar('code')
            ->maxLength('code', 225)
            ->allowEmptyString('code');

        $validator
            ->scalar('size')
            ->maxLength('size', 225)
            ->allowEmptyString('size');

        $validator
            ->scalar('type')
            ->maxLength('type', 225)
            ->allowEmptyString('type');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['reception_id'], 'Receptions'));

        return $rules;
    }


}
