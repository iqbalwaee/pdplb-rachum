<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * VwStocksLocations Model
 *
 * @method \App\Model\Entity\VwStocksLocation get($primaryKey, $options = [])
 * @method \App\Model\Entity\VwStocksLocation newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\VwStocksLocation[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\VwStocksLocation|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\VwStocksLocation|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\VwStocksLocation patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\VwStocksLocation[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\VwStocksLocation findOrCreate($search, callable $callback = null, $options = [])
 */
class VwStocksLocationsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('vw_stocks_locations');
    }

}
