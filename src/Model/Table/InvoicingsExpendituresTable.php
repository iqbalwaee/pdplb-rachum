<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * InvoicingsExpenditures Model
 *
 * @property \App\Model\Table\InvoicingsTable|\Cake\ORM\Association\BelongsTo $Invoicings
 * @property \App\Model\Table\ExpendituresTable|\Cake\ORM\Association\BelongsTo $Expenditures
 * @property \App\Model\Table\InvoicingsExpendituresDetailsTable|\Cake\ORM\Association\HasMany $InvoicingsExpendituresDetails
 *
 * @method \App\Model\Entity\InvoicingsExpenditure get($primaryKey, $options = [])
 * @method \App\Model\Entity\InvoicingsExpenditure newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\InvoicingsExpenditure[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\InvoicingsExpenditure|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\InvoicingsExpenditure|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\InvoicingsExpenditure patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\InvoicingsExpenditure[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\InvoicingsExpenditure findOrCreate($search, callable $callback = null, $options = [])
 */
class InvoicingsExpendituresTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('invoicings_expenditures');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Invoicings', [
            'foreignKey' => 'invoicing_id'
        ]);
        $this->belongsTo('Expenditures', [
            'foreignKey' => 'expenditure_id'
        ]);
        $this->hasMany('InvoicingsExpendituresDetails', [
            'foreignKey' => 'invoicings_expenditure_id',
            'dependent' => true,
            'cascadeCallbacks' => true,
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmptyString('id', 'create')
            ->add('id', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['id']));
        $rules->add($rules->existsIn(['invoicing_id'], 'Invoicings'));
        $rules->add($rules->existsIn(['expenditure_id'], 'Expenditures'));

        return $rules;
    }
}
