<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TravelDocuments Model
 *
 * @property \App\Model\Table\CustomersTable|\Cake\ORM\Association\BelongsTo $Customers
 * @property \App\Model\Table\ExpendituresTable|\Cake\ORM\Association\BelongsTo $Expenditures
 * @property \App\Model\Table\TravelDocumentsDetailsTable|\Cake\ORM\Association\HasMany $TravelDocumentsDetails
 *
 * @method \App\Model\Entity\TravelDocument get($primaryKey, $options = [])
 * @method \App\Model\Entity\TravelDocument newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\TravelDocument[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\TravelDocument|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TravelDocument|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TravelDocument patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\TravelDocument[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\TravelDocument findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class TravelDocumentsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('travel_documents');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Customers', [
            'foreignKey' => 'customer_id'
        ]);
        $this->belongsTo('Expenditures', [
            'foreignKey' => 'expenditure_id'
        ]);
        $this->hasMany('TravelDocumentsDetails', [
            'foreignKey' => 'travel_document_id',
            'dependent' => true,
            'cascadeCallbacks' => true,
        ]);

        $this->belongsTo('CreatedUsers', [
            'foreignKey' => 'created_by',
            'className'=>'Users'
        ]);
        $this->belongsTo('ModifiedUsers', [
            'foreignKey' => 'modified_by',
            'className'=>'Users'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmptyString('id', 'create')
            ->add('id', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('code')
            ->maxLength('code', 100)
            ->allowEmptyString('code');

        $validator
            ->date('date')
            ->allowEmptyDate('date');

        $validator
            ->scalar('driver_name')
            ->maxLength('driver_name', 225)
            ->allowEmptyString('driver_name');

        $validator
            ->scalar('license_plate')
            ->maxLength('license_plate', 100)
            ->allowEmptyString('license_plate');

        $validator
            ->integer('created_by')
            ->allowEmptyString('created_by');

        $validator
            ->integer('modified_by')
            ->allowEmptyString('modified_by');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['id']));
        $rules->add($rules->existsIn(['customer_id'], 'Customers'));
        $rules->add($rules->existsIn(['expenditure_id'], 'Expenditures'));

        return $rules;
    }

    public function beforeSave(\Cake\Event\Event $event, \Cake\ORM\Entity $entity, 
    \ArrayObject $options)
    {
        if($entity->isNew()){
            $date       = $entity->date->format('Y-m-d');
            $year       = substr($date,0,4);
            $month      = substr($date,5,2);
            
            $getLastCode = $this->find()->select([
                'codeNum'=>'(CAST(SUBSTR(`code`,12,5) AS UNSIGNED) )'
            ])
            ->where([
                'YEAR(date)'    => $year,
                'MONTH(date)'   => $month 
            ])
            ->order('codeNum DESC')
            ->first();
            if(!empty($getLastCode)){
                $lastCode = $getLastCode->codeNum;
            }else{
                $lastCode = 0;
            }
            $lastCode       = $lastCode +1;
            $entity->code   = "SJ.".$year.".".$month.".".str_pad($lastCode,5,0,STR_PAD_LEFT);
        }
        return true;
    }
}
