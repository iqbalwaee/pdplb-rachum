<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Receptions Model
 *
 * @property \App\Model\Table\PurchaseOrdersTable|\Cake\ORM\Association\BelongsTo $PurchaseOrders
 * @property \App\Model\Table\ImportersTable|\Cake\ORM\Association\BelongsTo $Importers
 * @property \App\Model\Table\SendersTable|\Cake\ORM\Association\BelongsTo $Senders
 * @property \App\Model\Table\CurrenciesTable|\Cake\ORM\Association\BelongsTo $Currencies
 * @property \App\Model\Table\ReceptionsContainersTable|\Cake\ORM\Association\HasMany $ReceptionsContainers
 *
 * @method \App\Model\Entity\Reception get($primaryKey, $options = [])
 * @method \App\Model\Entity\Reception newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Reception[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Reception|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Reception|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Reception patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Reception[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Reception findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ReceptionsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('receptions');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('PurchaseOrders', [
            'foreignKey' => 'purchase_order_id'
        ]);
        $this->belongsTo('Importers', [
            'foreignKey' => 'importer_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Senders', [
            'className'=>'Suppliers',
            'foreignKey' => 'sender_id'
        ]);
        $this->belongsTo('Currencies', [
            'foreignKey' => 'currency_id'
        ]);
        $this->hasMany('ReceptionsContainers', [
            'foreignKey' => 'reception_id',
            'dependent' => true,
            'cascadeCallbacks' => true,
        ]);

        $this->belongsTo('CreatedUsers', [
            'foreignKey' => 'created_by',
            'className'=>'Users'
        ]);
        $this->belongsTo('ModifiedUsers', [
            'foreignKey' => 'modified_by',
            'className'=>'Users'
        ]);
        
        $this->addBehavior('Josegonzalez/Upload.Upload', [
            'file' => [
                'fields' => [
                    'dir' => 'file_dir',
                ],
            ],
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmptyString('id', 'create');

        $validator
            ->scalar('code')
            ->maxLength('code', 100)
            ->allowEmptyString('code');

        $validator
            ->date('date')
            ->requirePresence('date', 'create')
            ->allowEmptyDate('date', false);

        $validator
            ->scalar('bc_type')
            ->maxLength('bc_type', 225)
            ->requirePresence('bc_type', 'create')
            ->allowEmptyString('bc_type', false);

        $validator
            ->scalar('bc_code')
            ->maxLength('bc_code', 225)
            ->requirePresence('bc_code', 'create')
            ->allowEmptyString('bc_code', false);

        $validator
            ->date('bc_date')
            ->requirePresence('bc_date', 'create')
            ->allowEmptyDate('bc_date', false);

        $validator
            ->scalar('register_code')
            ->maxLength('register_code', 225)
            ->allowEmptyString('register_code');

        $validator
            ->date('register_date')
            ->allowEmptyDate('register_date');

        $validator
            ->scalar('bl_code')
            ->maxLength('bl_code', 225)
            ->allowEmptyString('bl_code');

        $validator
            ->date('bl_date')
            ->allowEmptyDate('bl_date');

        $validator
            ->scalar('invoice_code')
            ->maxLength('invoice_code', 225)
            ->allowEmptyString('invoice_code');

        $validator
            ->date('invoice_date')
            ->allowEmptyDate('invoice_date');

        $validator
            ->decimal('kurs_idr')
            ->allowEmptyString('kurs_idr');

        $validator
            ->scalar('value_type')
            ->maxLength('value_type', 225)
            ->allowEmptyString('value_type');

        $validator
            ->scalar('note')
            ->allowEmptyString('note');

        $validator
            ->allowEmptyString('status');

        $validator
            ->integer('created_by')
            ->allowEmptyString('created_by');

        $validator
            ->integer('modified_by')
            ->allowEmptyString('modified_by');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['purchase_order_id'], 'PurchaseOrders'));
        $rules->add($rules->existsIn(['importer_id'], 'Importers'));
        $rules->add($rules->existsIn(['sender_id'], 'Senders'));
        $rules->add($rules->existsIn(['currency_id'], 'Currencies'));

        return $rules;
    }

    public function beforeSave(\Cake\Event\Event $event, \Cake\ORM\Entity $entity, 
    \ArrayObject $options)
    {
        if($entity->isNew()){
            $date       = $entity->date->format('Y-m-d');
            $year       = substr($date,0,4);
            $month      = substr($date,5,2);
            
            $getLastCode = $this->find()->select([
                'codeNum'=>'(CAST(SUBSTR(`code`,12,5) AS UNSIGNED) )'
            ])
            ->where([
                'YEAR(date)'    => $year,
                'MONTH(date)'   => $month 
            ])
            ->order('codeNum DESC')
            ->first();
            if(!empty($getLastCode)){
                $lastCode = $getLastCode->codeNum;
            }else{
                $lastCode = 0;
            }
            $lastCode       = $lastCode +1;
            $entity->code   = "RC.".$year.".".$month.".".str_pad($lastCode,5,0,STR_PAD_LEFT);
        }
        return true;
    }
}
