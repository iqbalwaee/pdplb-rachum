<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * InvoicingsPayments Model
 *
 * @property \App\Model\Table\InvoicingsTable|\Cake\ORM\Association\BelongsTo $Invoicings
 *
 * @method \App\Model\Entity\InvoicingsPayment get($primaryKey, $options = [])
 * @method \App\Model\Entity\InvoicingsPayment newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\InvoicingsPayment[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\InvoicingsPayment|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\InvoicingsPayment|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\InvoicingsPayment patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\InvoicingsPayment[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\InvoicingsPayment findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class InvoicingsPaymentsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('invoicings_payments');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Invoicings', [
            'foreignKey' => 'invoicing_id'
        ]);
        $this->belongsTo('CreatedUsers', [
            'foreignKey' => 'created_by',
            'className'=>'Users'
        ]);
        $this->belongsTo('ModifiedUsers', [
            'foreignKey' => 'modified_by',
            'className'=>'Users'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmptyString('id', 'create')
            ->add('id', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('code')
            ->maxLength('code', 100)
            ->allowEmptyString('code');

        $validator
            ->date('date')
            ->allowEmptyDate('date');

        $validator
            ->decimal('payment_amount')
            ->allowEmptyString('payment_amount');

        $validator
            ->integer('created_by')
            ->allowEmptyString('created_by');

        $validator
            ->integer('modified_by')
            ->allowEmptyString('modified_by');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['id']));
        $rules->add($rules->existsIn(['invoicing_id'], 'Invoicings'));

        return $rules;
    }

    public function updateInvoicePayment($idInvoicing)
    {
        $invoicing = $this->Invoicings->find()
        ->select($this->Invoicings)
        ->select([
            'total_dibayar' => $this->find()->select([
                'saldo'=>'(IFNULL(SUM(`payment_amount`),0))'
            ])->where([
                '`invoicing_Id`' => $idInvoicing
            ])
        ])
        ->where([
            '`Invoicings`.`id`' => $idInvoicing
        ])
        ->first();
        
        if($invoicing->total_dibayar >= $invoicing->total_amount){
            $this->Invoicings->updateAll(['status'=>2],['id'=>$idInvoicing]);
        }elseif($invoicing->total_dibayar != 0 && $invoicing->total_dibayar < $invoicing->total_amount){
            $this->Invoicings->updateAll(['status'=>1],['id'=>$idInvoicing]);
        }else{
            $this->Invoicings->updateAll(['status'=>0],['id'=>$idInvoicing]);
        }
        return true;
    }

    public function beforeSave(\Cake\Event\Event $event, \Cake\ORM\Entity $entity, 
    \ArrayObject $options)
    {
        if($entity->isNew()){
            $date       = $entity->date->format('Y-m-d');
            $year       = substr($date,0,4);
            $month      = substr($date,5,2);
            
            $getLastCode = $this->find()->select([
                'codeNum'=>'(CAST(SUBSTR(`code`,17,5) AS UNSIGNED) )'
            ])
            ->where([
                'YEAR(date)'    => $year,
                'MONTH(date)'   => $month 
            ])
            ->order('codeNum DESC')
            ->first();
            if(!empty($getLastCode)){
                $lastCode = $getLastCode->codeNum;
            }else{
                $lastCode = 0;
            }
            $lastCode       = $lastCode +1;
            $entity->code   = "PMB-INV.".$year.".".$month.".".str_pad($lastCode,5,0,STR_PAD_LEFT);
        }
        return true;
    }

    public function afterSave(\Cake\Event\Event $event, \Cake\ORM\Entity $entity, 
    \ArrayObject $options)
    {
        $this->updateInvoicePayment($entity->invoicing_id);
        return true;
    }

    public function afterDelete(\Cake\Event\Event $event, \Cake\ORM\Entity $entity, 
    \ArrayObject $options)
    {
        $this->updateInvoicePayment($entity->invoicing_id);
        return true;
    }

}
