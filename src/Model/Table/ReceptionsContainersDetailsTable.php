<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ReceptionsContainersDetails Model
 *
 * @property \App\Model\Table\PurchaseOrdersDetailsTable|\Cake\ORM\Association\BelongsTo $PurchaseOrdersDetails
 * @property \App\Model\Table\ReceptionsContainersTable|\Cake\ORM\Association\BelongsTo $ReceptionsContainers
 * @property \App\Model\Table\ProductsTable|\Cake\ORM\Association\BelongsTo $Products
 * @property \App\Model\Table\CountriesTable|\Cake\ORM\Association\BelongsTo $Countries
 * @property \App\Model\Table\StocksTable|\Cake\ORM\Association\HasMany $Stocks
 *
 * @method \App\Model\Entity\ReceptionsContainersDetail get($primaryKey, $options = [])
 * @method \App\Model\Entity\ReceptionsContainersDetail newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ReceptionsContainersDetail[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ReceptionsContainersDetail|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ReceptionsContainersDetail|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ReceptionsContainersDetail patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ReceptionsContainersDetail[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ReceptionsContainersDetail findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ReceptionsContainersDetailsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('receptions_containers_details');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('PurchaseOrdersDetails', [
            'foreignKey' => 'purchase_orders_detail_id'
        ]);
        $this->belongsTo('ReceptionsContainers', [
            'foreignKey' => 'receptions_container_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Products', [
            'foreignKey' => 'product_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Countries', [
            'foreignKey' => 'country_id'
        ]);
        $this->hasOne('Stocks', [
            'foreignKey' => 'receptions_containers_detail_id',
            'dependent' => true,
            'cascadeCallbacks' => true,
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmptyString('id', 'create');

        $validator
            ->decimal('qty')
            ->requirePresence('qty', 'create')
            ->allowEmptyString('qty', false);

        $validator
            ->decimal('qty_packaging')
            ->allowEmptyString('qty_packaging');

        $validator
            ->scalar('packaging')
            ->maxLength('packaging', 225)
            ->allowEmptyString('packaging');

        $validator
            ->scalar('category')
            ->maxLength('category', 225)
            ->allowEmptyString('category');

        $validator
            ->scalar('fasilitas')
            ->maxLength('fasilitas', 225)
            ->allowEmptyString('fasilitas');

        $validator
            ->scalar('no_urut')
            ->maxLength('no_urut', 225)
            ->allowEmptyString('no_urut');

        $validator
            ->decimal('value')
            ->allowEmptyString('value');

        $validator
            ->decimal('value_per_unit')
            ->allowEmptyString('value_per_unit');

        $validator
            ->integer('created_by')
            ->allowEmptyString('created_by');

        $validator
            ->integer('modified_by')
            ->allowEmptyString('modified_by');

        $validator
            ->boolean('status')
            ->allowEmptyString('status');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['purchase_orders_detail_id'], 'PurchaseOrdersDetails'));
        $rules->add($rules->existsIn(['receptions_container_id'], 'ReceptionsContainers'));
        $rules->add($rules->existsIn(['product_id'], 'Products'));
        $rules->add($rules->existsIn(['country_id'], 'Countries'));

        return $rules;
    }

    public function afterSave(\Cake\Event\Event $event, \Cake\ORM\Entity $entity, 
    \ArrayObject $options)
    {
        $param      = [
            'code' => '1000001',
            'receptions_containers_detail_id' => $entity->id,
            'qty' => $entity->qty
        ];
        if($entity->isNew()){
            $stock       = $this->Stocks->newEntity();
        }else{
            $getIdStock = $this->Stocks->find()->where([
                'receptions_containers_detail_id' => $entity->id
            ])->first();
            $stock = $this->Stocks->get($getIdStock->id);
        }
        $stock = $this->Stocks->patchEntity($stock,$param);
        $this->Stocks->save($stock);
        return true;
    }
}
