<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * StocksLocations Model
 *
 * @property \App\Model\Table\StocksTable|\Cake\ORM\Association\BelongsTo $Stocks
 * @property \App\Model\Table\PutAwaysDetailsTable|\Cake\ORM\Association\BelongsTo $PutAwaysDetails
 *
 * @method \App\Model\Entity\StocksLocation get($primaryKey, $options = [])
 * @method \App\Model\Entity\StocksLocation newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\StocksLocation[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\StocksLocation|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\StocksLocation|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\StocksLocation patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\StocksLocation[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\StocksLocation findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class StocksLocationsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('stocks_locations');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Stocks', [
            'foreignKey' => 'stock_id'
        ]);
        $this->hasMany('StocksLocationsLogs', [
            'foreignKey' => 'stocks_location_id',
            'dependent' => true,
            'cascadeCallbacks' => true,
        ]);
        $this->belongsTo('VwStocks', [
            'foreignKey' => 'StockID'
        ]);
        $this->belongsTo('PutAwaysDetails', [
            'foreignKey' => 'put_aways_detail_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmptyString('id', 'create');

        $validator
            ->decimal('qty_stock')
            ->allowEmptyString('qty_stock');

        $validator
            ->decimal('actual_stock')
            ->allowEmptyString('actual_stock');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['stock_id'], 'Stocks'));
        $rules->add($rules->existsIn(['put_aways_detail_id'], 'PutAwaysDetails'));

        return $rules;
    }
}
