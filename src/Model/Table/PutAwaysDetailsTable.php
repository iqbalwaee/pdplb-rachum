<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * PutAwaysDetails Model
 *
 * @property \App\Model\Table\PutAwaysTable|\Cake\ORM\Association\BelongsTo $PutAways
 * @property \App\Model\Table\StocksTable|\Cake\ORM\Association\BelongsTo $Stocks
 * @property \App\Model\Table\StocksLocationsTable|\Cake\ORM\Association\HasMany $StocksLocations
 *
 * @method \App\Model\Entity\PutAwaysDetail get($primaryKey, $options = [])
 * @method \App\Model\Entity\PutAwaysDetail newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\PutAwaysDetail[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\PutAwaysDetail|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PutAwaysDetail|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PutAwaysDetail patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\PutAwaysDetail[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\PutAwaysDetail findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class PutAwaysDetailsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('put_aways_details');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('PutAways', [
            'foreignKey' => 'put_away_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Stocks', [
            'foreignKey' => 'stock_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('StocksLocations', [
            'foreignKey' => 'put_aways_detail_id',
            'dependent' => true,
            'cascadeCallbacks' => true,
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmptyString('id', 'create');

        $validator
            ->decimal('qty')
            ->requirePresence('qty', 'create')
            ->allowEmptyString('qty', false);

        $validator
            ->integer('created_by')
            ->allowEmptyString('created_by');

        $validator
            ->integer('modified_by')
            ->allowEmptyString('modified_by');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['put_away_id'], 'PutAways'));
        $rules->add($rules->existsIn(['stock_id'], 'Stocks'));

        return $rules;
    }

    public function afterSave(\Cake\Event\Event $event, \Cake\ORM\Entity $entity, 
    \ArrayObject $options)
    {
        $param      = [
            'put_aways_detail_id' => $entity->id,
            'stock_id' => $entity->stock_id,
            'qty_stock' => $entity->qty,
            'actual_stock' => 0,
            'stocks_locations_logs' => [
                [
                    'qty' => $entity->qty,
                    'type' => "IN",
                    'referensi_id' => $entity->id,
                    'referensi' => 'PUT AWAY'
                ]
            ]
        ];
        if($entity->isNew()){
            $stockLocation       = $this->StocksLocations->newEntity();
        }else{
            $getIdStocksLocation = $this->StocksLocations->find()->where([
                'put_aways_detail_id' => $entity->id
            ])->first();
            $stockLocation = $this->StocksLocations->get($getIdStocksLocation->id,[
                'contain' => [
                    'StocksLocationsLogs' => function($q) use ($entity,$getIdStocksLocation){
                        return $q->where([
                            'type' => "IN",
                            'referensi_id' => $entity->id,
                            'referensi' => 'PUT AWAY'
                        ]);
                    }
                ]
            ]);
            unset($stockLocation->actual_stock,$param['actual_stock']);
            $param['stocks_locations_logs'][0]['id'] = $stockLocation->stocks_locations_logs[0]->id; 
        }
        $stockLocation = $this->StocksLocations->patchEntity($stockLocation,$param);
        // dd($stockLocation);
        $this->StocksLocations->save($stockLocation);
        return true;
    }
}
