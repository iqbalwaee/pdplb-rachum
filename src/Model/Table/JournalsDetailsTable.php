<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * JournalsDetails Model
 *
 * @property \App\Model\Table\JournalsTable|\Cake\ORM\Association\BelongsTo $Journals
 * @property \App\Model\Table\CodeAccountsTable|\Cake\ORM\Association\BelongsTo $CodeAccounts
 *
 * @method \App\Model\Entity\JournalsDetail get($primaryKey, $options = [])
 * @method \App\Model\Entity\JournalsDetail newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\JournalsDetail[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\JournalsDetail|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\JournalsDetail|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\JournalsDetail patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\JournalsDetail[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\JournalsDetail findOrCreate($search, callable $callback = null, $options = [])
 */
class JournalsDetailsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('journals_details');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Journals', [
            'foreignKey' => 'journal_id'
        ]);
        $this->belongsTo('CodeAccounts', [
            'foreignKey' => 'code_account_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmptyString('id', 'create')
            ->add('id', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->decimal('debit')
            ->allowEmptyString('debit');

        $validator
            ->decimal('credit')
            ->allowEmptyString('credit');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['id']));
        $rules->add($rules->existsIn(['journal_id'], 'Journals'));
        $rules->add($rules->existsIn(['code_account_id'], 'CodeAccounts'));

        return $rules;
    }
}
