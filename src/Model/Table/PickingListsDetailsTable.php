<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * PickingListsDetails Model
 *
 * @property \App\Model\Table\PickingListsTable|\Cake\ORM\Association\BelongsTo $PickingLists
 * @property \App\Model\Table\StockLocationsTable|\Cake\ORM\Association\BelongsTo $StockLocations
 * @property \App\Model\Table\ExpendituresTable|\Cake\ORM\Association\BelongsToMany $Expenditures
 *
 * @method \App\Model\Entity\PickingListsDetail get($primaryKey, $options = [])
 * @method \App\Model\Entity\PickingListsDetail newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\PickingListsDetail[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\PickingListsDetail|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PickingListsDetail|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PickingListsDetail patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\PickingListsDetail[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\PickingListsDetail findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class PickingListsDetailsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('picking_lists_details');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('PickingLists', [
            'foreignKey' => 'picking_list_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('StocksLocations', [
            'foreignKey' => 'stocks_location_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsToMany('Expenditures', [
            'foreignKey' => 'picking_lists_detail_id',
            'targetForeignKey' => 'expenditure_id',
            'joinTable' => 'expenditures_picking_lists_details'
        ]);

        $this->belongsTo('VwStocksLocations', [
            'foreignKey' => 'stocks_location_id',
            'bindingKey' => 'StocksLocationId'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmptyString('id', 'create');

        $validator
            ->decimal('qty')
            ->requirePresence('qty', 'create')
            ->allowEmptyString('qty', false);

        $validator
            ->integer('created_by')
            ->allowEmptyString('created_by');

        $validator
            ->integer('modified_by')
            ->allowEmptyString('modified_by');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['picking_list_id'], 'PickingLists'));
        $rules->add($rules->existsIn(['stocks_location_id'], 'StocksLocations'));

        return $rules;
    }
}
