<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * BusinessTypes Model
 *
 * @property \App\Model\Table\BusinessTypesAccountsTable|\Cake\ORM\Association\HasMany $BusinessTypesAccounts
 *
 * @method \App\Model\Entity\BusinessType get($primaryKey, $options = [])
 * @method \App\Model\Entity\BusinessType newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\BusinessType[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\BusinessType|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\BusinessType|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\BusinessType patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\BusinessType[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\BusinessType findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class BusinessTypesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('business_types');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('BusinessTypesAccounts', [
            'foreignKey' => 'business_type_id'
        ]);
        $this->belongsToMany('CodeAccounts', [
            'joinTable' => 'business_types_accounts'
        ]);
        
        $this->hasMany('Journals', [
            'foreignKey' => 'business_type_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmptyString('id', 'create')
            ->add('id', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->allowEmptyString('type');

        $validator
            ->scalar('name')
            ->maxLength('name', 225)
            ->allowEmptyString('name');

        $validator
            ->boolean('status')
            ->allowEmptyString('status');

        $validator
            ->integer('created_by')
            ->allowEmptyString('created_by');

        $validator
            ->integer('modified_by')
            ->allowEmptyString('modified_by');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['id']));

        return $rules;
    }
}
