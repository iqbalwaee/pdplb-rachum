<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * CodeAccountsDump Model
 *
 * @property \App\Model\Table\CodeAccountsDumpTable|\Cake\ORM\Association\BelongsTo $ParentCodeAccountsDump
 * @property \App\Model\Table\CodeAccountsDumpTable|\Cake\ORM\Association\HasMany $ChildCodeAccountsDump
 *
 * @method \App\Model\Entity\CodeAccountsDump get($primaryKey, $options = [])
 * @method \App\Model\Entity\CodeAccountsDump newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\CodeAccountsDump[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\CodeAccountsDump|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CodeAccountsDump|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CodeAccountsDump patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\CodeAccountsDump[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\CodeAccountsDump findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 * @mixin \Cake\ORM\Behavior\TreeBehavior
 */
class CodeAccountsDumpTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('code_accounts_dump');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Tree');

        $this->belongsTo('ParentCodeAccountsDump', [
            'className' => 'CodeAccountsDump',
            'foreignKey' => 'parent_id'
        ]);
        $this->hasMany('ChildCodeAccountsDump', [
            'className' => 'CodeAccountsDump',
            'foreignKey' => 'parent_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->scalar('code')
            ->maxLength('code', 50)
            ->allowEmptyString('code');

        $validator
            ->scalar('name')
            ->maxLength('name', 225)
            ->allowEmptyString('name');

        $validator
            ->boolean('normal_balance')
            ->requirePresence('normal_balance', 'create')
            ->allowEmptyString('normal_balance', false);

        $validator
            ->integer('level')
            ->allowEmptyString('level');

        $validator
            ->allowEmptyString('laba_rugi');

        $validator
            ->allowEmptyString('neraca');

        $validator
            ->allowEmptyString('arus_kas');

        $validator
            ->integer('sort')
            ->allowEmptyString('sort');

        $validator
            ->boolean('status')
            ->allowEmptyString('status');

        $validator
            ->integer('created_by')
            ->allowEmptyString('created_by');

        $validator
            ->integer('modified_by')
            ->allowEmptyString('modified_by');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['parent_id'], 'ParentCodeAccountsDump'));

        return $rules;
    }
}
