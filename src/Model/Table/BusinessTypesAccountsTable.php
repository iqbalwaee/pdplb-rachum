<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * BusinessTypesAccounts Model
 *
 * @property \App\Model\Table\BusinessTypesTable|\Cake\ORM\Association\BelongsTo $BusinessTypes
 * @property \App\Model\Table\CodeAccountsTable|\Cake\ORM\Association\BelongsTo $CodeAccounts
 *
 * @method \App\Model\Entity\BusinessTypesAccount get($primaryKey, $options = [])
 * @method \App\Model\Entity\BusinessTypesAccount newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\BusinessTypesAccount[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\BusinessTypesAccount|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\BusinessTypesAccount|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\BusinessTypesAccount patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\BusinessTypesAccount[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\BusinessTypesAccount findOrCreate($search, callable $callback = null, $options = [])
 */
class BusinessTypesAccountsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('business_types_accounts');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('BusinessTypes', [
            'foreignKey' => 'business_type_id'
        ]);
        $this->belongsTo('CodeAccounts', [
            'foreignKey' => 'code_account_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmptyString('id', 'create')
            ->add('id', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['id']));
        $rules->add($rules->existsIn(['business_type_id'], 'BusinessTypes'));
        $rules->add($rules->existsIn(['code_account_id'], 'CodeAccounts'));

        return $rules;
    }
}
