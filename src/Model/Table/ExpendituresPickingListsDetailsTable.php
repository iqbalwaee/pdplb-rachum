<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ExpendituresPickingListsDetails Model
 *
 * @property \App\Model\Table\ExpendituresPickingListsTable|\Cake\ORM\Association\BelongsTo $ExpendituresPickingLists
 * @property \App\Model\Table\PickingListsDetailsTable|\Cake\ORM\Association\BelongsTo $PickingListsDetails
 *
 * @method \App\Model\Entity\ExpendituresPickingListsDetail get($primaryKey, $options = [])
 * @method \App\Model\Entity\ExpendituresPickingListsDetail newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ExpendituresPickingListsDetail[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ExpendituresPickingListsDetail|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ExpendituresPickingListsDetail|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ExpendituresPickingListsDetail patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ExpendituresPickingListsDetail[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ExpendituresPickingListsDetail findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ExpendituresPickingListsDetailsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('expenditures_picking_lists_details');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('ExpendituresPickingLists', [
            'foreignKey' => 'expenditures_picking_list_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('PickingListsDetails', [
            'foreignKey' => 'picking_lists_detail_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmptyString('id', 'create');

        $validator
            ->decimal('qty')
            ->allowEmptyString('qty');

        $validator
            ->decimal('amount')
            ->allowEmptyString('amount');

        $validator
            ->integer('created_by')
            ->allowEmptyString('created_by');

        $validator
            ->integer('modified_by')
            ->allowEmptyString('modified_by');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['expenditures_picking_list_id'], 'ExpendituresPickingLists'));
        $rules->add($rules->existsIn(['picking_lists_detail_id'], 'PickingListsDetails'));

        return $rules;
    }
}
