<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * StocksLocationsLogs Model
 *
 * @property \App\Model\Table\StockLocationsTable|\Cake\ORM\Association\BelongsTo $StockLocations
 * @property \App\Model\Table\ReferensisTable|\Cake\ORM\Association\BelongsTo $Referensis
 *
 * @method \App\Model\Entity\StocksLocationsLog get($primaryKey, $options = [])
 * @method \App\Model\Entity\StocksLocationsLog newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\StocksLocationsLog[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\StocksLocationsLog|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\StocksLocationsLog|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\StocksLocationsLog patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\StocksLocationsLog[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\StocksLocationsLog findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class StocksLocationsLogsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('stocks_locations_logs');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('StockLocations', [
            'foreignKey' => 'stock_location_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmptyString('id', 'create');

        $validator
            ->scalar('type')
            ->allowEmptyString('type');

        $validator
            ->decimal('qty')
            ->allowEmptyString('qty');

        $validator
            ->scalar('referensi')
            ->allowEmptyString('referensi');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        // $rules->add($rules->existsIn(['stocks_location_id'], 'StocksLocations'));
        // $rules->add($rules->existsIn(['referensi_id'], 'Referensis'));

        return $rules;
    }
}
