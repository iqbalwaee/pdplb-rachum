<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * InvoicingsExpendituresDetails Model
 *
 * @property \App\Model\Table\InvoicingsExpendituresTable|\Cake\ORM\Association\BelongsTo $InvoicingsExpenditures
 * @property \App\Model\Table\ExpendituresPickingListsDetailsTable|\Cake\ORM\Association\BelongsTo $ExpendituresPickingListsDetails
 *
 * @method \App\Model\Entity\InvoicingsExpendituresDetail get($primaryKey, $options = [])
 * @method \App\Model\Entity\InvoicingsExpendituresDetail newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\InvoicingsExpendituresDetail[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\InvoicingsExpendituresDetail|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\InvoicingsExpendituresDetail|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\InvoicingsExpendituresDetail patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\InvoicingsExpendituresDetail[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\InvoicingsExpendituresDetail findOrCreate($search, callable $callback = null, $options = [])
 */
class InvoicingsExpendituresDetailsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('invoicings_expenditures_details');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('InvoicingsExpenditures', [
            'foreignKey' => 'invoicings_expenditure_id'
        ]);
        $this->belongsTo('ExpendituresPickingListsDetails', [
            'foreignKey' => 'expenditures_picking_lists_detail_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmptyString('id', 'create')
            ->add('id', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->decimal('qty')
            ->allowEmptyString('qty');

        $validator
            ->decimal('amount')
            ->allowEmptyString('amount');

        $validator
            ->decimal('subtotal')
            ->allowEmptyString('subtotal');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['id']));
        $rules->add($rules->existsIn(['invoicings_expenditure_id'], 'InvoicingsExpenditures'));
        $rules->add($rules->existsIn(['expenditures_picking_lists_detail_id'], 'ExpendituresPickingListsDetails'));

        return $rules;
    }
}
