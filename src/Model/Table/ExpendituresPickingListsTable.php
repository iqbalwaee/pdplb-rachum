<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ExpendituresPickingLists Model
 *
 * @property \App\Model\Table\ExpendituresTable|\Cake\ORM\Association\BelongsTo $Expenditures
 * @property \App\Model\Table\PickingListsTable|\Cake\ORM\Association\BelongsTo $PickingLists
 * @property \App\Model\Table\ExpendituresPickingListsDetailsTable|\Cake\ORM\Association\HasMany $ExpendituresPickingListsDetails
 *
 * @method \App\Model\Entity\ExpendituresPickingList get($primaryKey, $options = [])
 * @method \App\Model\Entity\ExpendituresPickingList newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ExpendituresPickingList[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ExpendituresPickingList|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ExpendituresPickingList|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ExpendituresPickingList patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ExpendituresPickingList[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ExpendituresPickingList findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ExpendituresPickingListsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('expenditures_picking_lists');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Expenditures', [
            'foreignKey' => 'expenditure_id'
        ]);
        $this->belongsTo('PickingLists', [
            'foreignKey' => 'picking_list_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('ExpendituresPickingListsDetails', [
            'foreignKey' => 'expenditures_picking_list_id',
            'dependent' => true,
            'cascadeCallbacks' => true,
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmptyString('id', 'create');

        $validator
            ->integer('created_by')
            ->allowEmptyString('created_by');

        $validator
            ->integer('modified_by')
            ->allowEmptyString('modified_by');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['expenditure_id'], 'Expenditures'));
        $rules->add($rules->existsIn(['picking_list_id'], 'PickingLists'));

        return $rules;
    }

    public function afterSave(\Cake\Event\Event $event, \Cake\ORM\Entity $entity, 
    \ArrayObject $options)
    {
        $this->PickingLists->updateAll([
            'status' => 1
        ],[
            'id' => $entity->picking_list_id
        ]);
        return true;
    }
    
    public function afterDelete(\Cake\Event\Event $event, \Cake\ORM\Entity $entity, 
    \ArrayObject $options)
    {
        $this->PickingLists->updateAll([
            'status' => 0
        ],[
            'id' => $entity->picking_list_id
        ]);
        return true;
    }
}
