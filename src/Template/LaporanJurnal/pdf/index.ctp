<!DOCTYPE html>
    <head>
        <style>
            body{
                margin:0px;
                padding:0px;
                font-size:11px;
                font-family: "Arial";
            }
            .text-left{
                text-align:left;
            }
            .text-center{
                text-align:center;
            }
            .text-right{
                text-align:right;
            }
            table{
                width:100%;
                margin:0px;
                padding:0px;
                border:1px solid #000;
                border-collapse: collapse;
            }
            table td, table th{
                border-bottom :1px solid #000;
                border-left :1px solid #000;
                word-break:break-all;
            }
            table thead th{
                padding:5px;
                height:20px;
            }
            table tbody td{
                vertical-align:top;
                padding:5px;
                height:40px;
            }
            table tbody tr.summary td{
                vertical-align:middle;
                padding:5px;
                height:20px !important;
                font-weight:bold;
            }
            tfoot { display: table-row-group; }
            tr { page-break-inside: avoid; }
            .page-break{
                page-break-after:always;
            }
            .header-optional{
                display:none;
            }
            @page{
                size:1024px 900px;
            }
            @media print{
                .header-optional{
                    display:table-header-group;
                }
            }
        </style>
    </head>
    <body>
        <?php 
            
        ?>
        <table>
            <?= $this->Utilities->createHeader($start,$end);?>
            <tbody>
                <?php $oldData = 1;;?>
                <?php foreach($results as $key => $result):?>
                    <?php 
                        $rowspan = count($result->journals_details);
                        if($rowspan == 1){
                            $rowspan = 0;
                        }
                        $totalDebit=0; $totalKredit=0;
                    ?>
                    <?php foreach($result->journals_details as $jd_key => $result_detail):?>
                        <?php if($oldData != $key):?>
                            <tr>
                                <td width="5%" <?=($rowspan > 0 ? "rowspan=".$rowspan : '');?>>
                                    <?=$key+1;?>
                                </td>
                                <td width="5%" <?=($rowspan > 0 ? "rowspan=".$rowspan : '');?> class="text-center">
                                    <?=$result->code;?>
                                </td>
                                <td width="10%" <?=($rowspan > 0 ? "rowspan=".$rowspan : '');?> class="text-center">
                                    <?=$result->date->format('d-m-Y');?>
                                </td>
                                <td width="10%">
                                    <?=$result_detail->code_account->code;?>
                                </td>
                                <td width="15%">
                                    <?=$result_detail->code_account->name;?>
                                </td>
                                <td width="25%" <?=($rowspan > 0 ? "rowspan=".$rowspan : '');?>>
                                    <?=$result->description ;?>
                                </td>
                                <td width="10%" class="text-right">
                                    <?=number_format($result_detail->debit,2);?>
                                </td>
                                <td width="10%" class="text-right">
                                    <?=number_format($result_detail->credit,2);?>
                                </td>
                            </tr>
                        <?php else:?>
                            <tr>
                                <td><?=$result_detail->code_account->code;?>
                                </td>
                                <td><?=$result_detail->code_account->name;?>
                                </td>
                                <td class="text-right"><?=number_format($result_detail->debit,2);?>
                                </td>
                                <td class="text-right"><?=number_format($result_detail->credit,2);?>
                                </td>
                            </tr>
                        <?php endif;?>
                    <?php $oldData = $key;?>
                    <?php $totalDebit+=$result_detail->debit; $totalKredit+=$result_detail->credit;?>
                    <?php endforeach;?>
                    <tr class="summary">
                        <td colspan="6" style="background:#ddd;" class="text-right">&nbsp;
                        </td>
                        <td width="10%" style="background:#ddd;" class="text-right"><?=number_format($totalDebit,2);?>
                        </td>
                        <td width="10%" style="background:#ddd;" class="text-right"><?=number_format($totalKredit,2);?>
                        </td>
                    </tr>
                <?php endforeach;?>
            </tbody>
        </table>
    </body>
</html>