<html>
    <head>
        <meta charset="utf-8"/>
        <title><?=$defaultAppSettings['App.Name'];?><?=(!empty($titleModule) ? ' | '.$titleModule : '');?></title>
        <style>
            html,body{
                padding:5px;
                margin:0px;
                font-family:sans-serif;
            }
            table{
                font-size:11px;
                line-height:1.3;
                border-collapse: collapse;
                width:100%;
            }
            .table-header{
                margin-bottom:15px;
            }
            .table-main{
                border-top:4px double #000;
                border-bottom:4px double #000;
            }
            .table-main th{
                vertical-align:top;
            }
            .table-header th,.table-main th{
                text-align:left;
            }
            .company-name{
                font-size:14px;
            }
            .document-type{
                border: 2px solid #000;
                padding:5px;
                text-align:center;
            }
            .document-name{
                border-bottom: 1px solid #000;
                
            }
            .table-content{
                margin-top:10px;
                border:1px solid #000;
            }
            .table-content th,.table-content td{
                border-right:1px solid #000;
                border-bottom:1px solid #000;
            }
            .table-content td{
                vertical-align:top;
            }

            .text-center{
                text-align:center;
            }
            .text-right{
                text-align:right;
            }
            .text-left{
                text-align:left;
            }
            .table-signature{
                width:400px;
                margin-top:15px;
                text-align:center;
            }
        </style>
    </head>
    <body>
        <table class="table-header">
            <thead>
                <tr>
                    <th width="400px">
                        <div class="company-name"><?=$defaultAppSettings['Company.Name'];?></div>
                        <div class="company-address"><?=$defaultAppSettings['Company.Address'];?></div>
                    </th>
                    <th></th>
                    <th width="150px">
                        <div class="document-type">
                            <div class="document-name">PENGELUARAN BARANG</div>
                            <div class="document-code"><?=$record->code;?></div>
                        </div>
                    </th>
                </tr>
            </thead>
        </table>
        <table class="table-main">
            <tbody>
                <tr>
                    <th width="20%">DOKUMEN PENGAJUAN</th>
                    <th width="2%">:</th>
                    <th width="21%"><?=$record->bc_type.$record->bc_code;?></th>
                    <th width="10%"></th>
                    <th width="20%">TGL. PENGAJUAN</th>
                    <th width="2%">:</th>
                    <th width=""><?=$record->bc_date->format('d-m-Y');?></th>
                </tr>
                <tr>
                    <th>NO. PENDAFTARAN</th>
                    <th>:</th>
                    <th><?=$record->register_code;?></th>
                    <th></th>
                    <th>TGL. PENDAFTARAN</th>
                    <th>:</th>
                    <th><?=$record->register_date->format('d-m-Y');?></th>
                </tr>
            </tbody>
        </table>
        <?php foreach($record->expenditures_picking_lists as $keyPl => $expenditurePickingLists):?>
            <table class="table-content" style="margin-bottom:0px;">
                <thead>
                    <tr>
                        <th colspan="8">
                            NO. PICKING LIST : <?=$expenditurePickingLists->picking_list->code;?>
                        </th>
                    </tr>
                    <tr>
                        <th width="10%">NO. PENERIMAAN</th>
                        <th width="10%">TGL. PENERIMAAN</th>
                        <th width="10%">NO. PENGAJUAN</th>
                        <th width="10%">LOKASI</th>
                        <th>NAMA BARANG</th>
                        <th width="10%">SATUAN</th>
                        <th width="10%">QTY</th>
                        <th width="15%">NILAI</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($expenditurePickingLists->expenditures_picking_lists_details as $keyPld => $expendituresPickingListDetail):?>
                        <?php 
                            $vwStocksLocation = $expendituresPickingListDetail->picking_lists_detail->vw_stocks_location;

                            $pickingListsDetail = $expendituresPickingListDetail->picking_lists_detail;
                        ?>
                        <tr>
                            <td><?=$vwStocksLocation->RCode;?></td>
                            <td><?=$vwStocksLocation->RDate->format('d-m-Y');?></td>
                            <td><?=$vwStocksLocation->RBCCode;?></td>
                            <td><?=$vwStocksLocation->LocationName;?></td>
                            <td>[<?=$vwStocksLocation->ProductCode;?>] <?=$vwStocksLocation->ProductName;?></td>
                            <td class="text-center"><?=$vwStocksLocation->ProductUnit;?></td>
                            <td class="text-right"><?=number_format($pickingListsDetail->qty);?></td>
                            <td class="text-right">
                                <?=number_format($expendituresPickingListDetail->amount);?>
                            </td>
                        </tr>
                    <?php endforeach;?>
                </tbody>
            </table>
        <?php endforeach;?>
        <table class="table-signature">
            <tbody>
                <tr>
                    <td width="50%">
                        USER PEMBUAT<br><br><br><br>
                        (<?=$record->created_user->name;?>)
                    </td>
                    <td width="50%">
                        MENGETAHUI<br><br><br><br>
                        (..................................)
                    </td>
                </tr>
            </tbody>
        </table>
    </body>
</html>