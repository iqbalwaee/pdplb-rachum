<?php
    $this->start('sub_header_toolbar');
?>
    <?php if($this->Acl->check(['action'=>'index']) == true):?>
        <a href="<?=$this->Url->build(['action'=>'index']);?>" class="btn btn-warning">
            <i class="la la-angle-double-left"></i> Kembali
        </a>
    <?php endif;?>
<?php
    $this->end();
?>
<div class="row">
    <div class="col-md-12">
        <div class="kt-portlet">
            <div class="kt-portlet__head">
				<div class="kt-portlet__head-label">
					<h3 class="kt-portlet__head-title">
						<?=$titlesubModule;?>
					</h3>
				</div>
			</div>
			<!--begin::Form-->
			<?= $this->Form->create($record,['class'=>'kt-form','type'=>'file']) ?>
				<div class="kt-portlet__body">
					<div class="kt-section kt-section--first">
                        <h3 class="kt-section__title">1. Informasi Dokumen Pabean:</h3>
                        <div class="row">
                            <div class="col-md-6 col-lg-4">
                                <?=$this->Form->control('bc_type',[
                                    'class'=>'form-control m-input',
                                    'type' => 'select',
                                    'options' => [
                                        'BC 2.7' => 'BC 2.7',
                                        'BC 2.8' => 'BC 2.8',
                                        'BC 4.1' => 'BC 4.1',
                                        'P3BET' => 'P3BET'
                                    ],
                                    'autocomplete' => 'off',
                                    'templateVars' => [
                                        'colsize' => 'col-lg-8 col-md-7',
                                    ],
                                    'label' => [
                                        'class'=> 'col-lg-4 col-md-5 col-form-label',
                                        'text'=>'Jenis BC'
                                    ],
                                ]);?>
                            </div>
                            <div class="col-md-6 col-lg-4">
                                <?=$this->Form->control('bc_code',[
                                    'class'=>'form-control m-input',
                                    'type' => 'text',
                                    'autocomplete' => 'off',
                                    'templateVars' => [
                                        'colsize' => 'col-lg-8 col-md-7',
                                    ],
                                    'label' => [
                                        'class'=> 'col-lg-4 col-md-5 col-form-label',
                                        'text'=>'No. Pengajuan *'
                                    ],
                                ]);?>
                            </div>
                            <div class="col-md-6 col-lg-4">
                                <?=$this->Form->control('bc_date',[
                                    'class'=>'form-control m-input date-picker',
                                    'type' => 'text',
                                    'autocomplete' => 'off',
                                    'templateVars' => [
                                        'colsize' => 'col-lg-8 col-md-7',
                                    ],
                                    'label' => [
                                        'class'=> 'col-lg-4 col-md-5 col-form-label',
                                        'text'=>'Tgl. Pengajuan *'
                                    ],
                                ]);?>
                            </div>
                            <div class="col-md-6 col-lg-4">
                                <?=$this->Form->control('file',[
                                    'class'=>'form-control m-input',
                                    'type' => 'file',
                                    'autocomplete' => 'off',
                                    'templateVars' => [
                                        'colsize' => 'col-lg-8 col-md-7',
                                        'helper' => '<span class="form-text text-muted">* Hanya PDF</span>'
                                    ],
                                    'label' => [
                                        'class'=> 'col-lg-4 col-md-5 col-form-label',
                                        'text'=>'Lampiran *'
                                    ],
                                ]);?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-lg-4">
                                <?=$this->Form->control('register_code',[
                                    'class'=>'form-control m-input',
                                    'type' => 'text',
                                    'autocomplete' => 'off',
                                    'templateVars' => [
                                        'colsize' => 'col-lg-8 col-md-7',
                                    ],
                                    'label' => [
                                        'class'=> 'col-lg-4 col-md-5 col-form-label',
                                        'text'=>'No. Pendaftaran *'
                                    ],
                                ]);?>
                            </div>
                            <div class="col-md-6 col-lg-4">
                                <?=$this->Form->control('register_date',[
                                    'class'=>'form-control m-input date-picker',
                                    'type' => 'text',
                                    'autocomplete' => 'off',
                                    'templateVars' => [
                                        'colsize' => 'col-lg-8 col-md-7',
                                    ],
                                    'label' => [
                                        'class'=> 'col-lg-4 col-md-5 col-form-label',
                                        'text'=>'Tgl. Pendaftaran *'
                                    ],
                                ]);?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-lg-4">
                                <?=$this->Form->control('currency_id',[
                                    'class'=>'form-control m-input',
                                    'empty' => 'PILIH MATA UANG',
                                    'templateVars' => [
                                        'colsize' => 'col-lg-8 col-md-7',
                                    ],
                                    'label' => [
                                        'class'=> 'col-lg-4 col-md-5 col-form-label',
                                        'text'=>'MATA UANG *'
                                    ],
                                ]);?>
                            </div>
                            <div class="col-md-6 col-lg-4">
                                <?=$this->Form->control('kurs_idr',[
                                    'class'=>'form-control m-input onlyNumber',
                                    'type' => 'text',
                                    'autocomplete' => 'off',
                                    'readonly' => true,
                                    'templateVars' => [
                                        'colsize' => 'col-lg-8 col-md-7',
                                    ],
                                    'label' => [
                                        'class'=> 'col-lg-4 col-md-5 col-form-label',
                                        'text'=>'KURS *'
                                    ],
                                ]);?>
                            </div>
                        </div>
                    </div>

                    <div class="kt-section">
                        <h3 class="kt-section__title">2. Informasi Pengeluaran:</h3>
                        <div class="row">
                            <div class="col-md-6 col-lg-4">
                                <?=$this->Form->control('date',[
                                    'class'=>'form-control m-input date-picker',
                                    'type' => 'text',
                                    'autocomplete' => 'off',
                                    'templateVars' => [
                                        'colsize' => 'col-lg-8 col-md-7',
                                    ],
                                    'label' => [
                                        'class'=> 'col-lg-4 col-md-5 col-form-label',
                                        'text'=>'Tgl. Pengeluaran *'
                                    ],
                                ]);?>
                            </div>
                            <div class="col-md-6 col-lg-4">
                                <?=$this->Form->control('customer_id',[
                                    'class'=>'form-control m-input',
                                    'type' => 'select',
                                    'empty' => 'PILIH CUSTOMER',
                                    'templateVars' => [
                                        'colsize' => 'col-lg-8 col-md-7',
                                    ],
                                    'label' => [
                                        'class'=> 'col-lg-4 col-md-5 col-form-label',
                                        'text'=>'Customer *'
                                    ],
                                ]);?>
                            </div>
                        </div>
                    </div>
                    <div class="kt-section">
                        <h3 class="kt-section__title">3. Picking Lists:</h3>
                        <div class="row">
                            <div class="col-md-12 col-lg-6">
                                <div class="input-group flex-nowrap mb-3">
                                    <?=$this->Form->select('picking_list_id',[],
                                    [
                                        'class'=>'form-control kt-select m-input',
                                        'type' => 'select',
                                        'id' => 'picking-list-id',
                                        'empty' => 'PILIH PICKING LIST',
                                        'label' => false
                                    ]);?>
                                    <div class="input-group-apend">
                                        <a href="#" class="btn btn-brand btn-add-pl">
                                             Tambah
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <div class="pl-data">
                                    <?php foreach($record->expenditures_picking_lists as $keyPl => $expendituresPickingList):?>
                                        <table class="table table-sm table-bordered table-input table-input-container">
                                            <thead>
                                                <tr>
                                                    <th colspan="7">
                                                        NO. PICKING LIST : <?=$expendituresPickingList->picking_list->code;?>
                                                        <input type="hidden" name="expenditures_picking_lists[<?=$keyPl;?>][picking_list_id]" value="<?=$expendituresPickingList->picking_list_id;?>" class="form-control m-input">
                                                        <input type="hidden" name="expenditures_picking_lists[<?=$keyPl;?>][id]" value="<?=$expendituresPickingList->id;?>" class="form-control m-input eplId">
                                                    </th>
                                                    <td class="td-input"><a href="#" class="btn btn-block btn-danger btn-sm"><i class="la la-trash"></i> HAPUS PL</a></td>
                                                </tr>
                                                <tr>
                                                    <th width="10%">NO. PENERIMAAN</th>
                                                    <th width="10%">TGL. PENERIMAAN</th>
                                                    <th width="10%">NO. PENGAJUAN</th>
                                                    <th width="10%">LOKASI</th>
                                                    <th>NAMA BARANG</th>
                                                    <th width="10%">SATUAN</th>
                                                    <th width="10%">QTY</th>
                                                    <th width="15%">NILAI</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach($expendituresPickingList->expenditures_picking_lists_details as $keyPld => $expendituresPickingListDetail):?>
                                                    <?php 
                                                        $vwStocksLocation = $expendituresPickingListDetail->picking_lists_detail->vw_stocks_location;

                                                        $pickingListsDetail = $expendituresPickingListDetail->picking_lists_detail;
                                                    ?>
                                                    <tr>
                                                        <td><?=$vwStocksLocation->RCode;?></td>
                                                        <td><?=$vwStocksLocation->RDate->format('d-m-Y');?></td>
                                                        <td><?=$vwStocksLocation->RBCCode;?></td>
                                                        <td><?=$vwStocksLocation->LocationName;?></td>
                                                        <td>[<?=$vwStocksLocation->ProductCode;?>] <?=$vwStocksLocation->ProductName;?></td>
                                                        <td class="text-center"><?=$vwStocksLocation->ProductUnit;?></td>
                                                        <td class="text-right"><?=$pickingListsDetail->qty;?></td>
                                                        <td class="text-right td-input">
                                                            <input type="text" name="expenditures_picking_lists[<?=$keyPl;?>][expenditures_picking_lists_details][<?=$keyPld;?>][amount]" value="<?=$expendituresPickingListDetail->amount;?>" class="form-control m-input onlyNumber"  autocomplete="off">
                                                            <input type="hidden" name="expenditures_picking_lists[<?=$keyPl;?>][expenditures_picking_lists_details][<?=$keyPld;?>][qty]" value="<?=$pickingListsDetail->qty;?>" class="form-control m-input">
                                                            <input type="hidden" name="expenditures_picking_lists[<?=$keyPl;?>][expenditures_picking_lists_details][<?=$keyPld;?>][picking_lists_detail_id]" value="<?=$pickingListsDetail->id;?>" class="form-control m-input">
                                                            <input type="hidden" name="expenditures_picking_lists[<?=$keyPl;?>][expenditures_picking_lists_details][<?=$keyPld;?>][id]" value="<?=$expendituresPickingListDetail->id;?>" class="form-control m-input">
                                                        </td>
                                                    </tr>
                                                <?php endforeach;?>
                                            </tbody>
                                        </table>
                                    <?php endforeach;?>
                                </div>
                            </div>
                        </div>

                    </div>
	            </div>
	            <div class="kt-portlet__foot">
					<div class="kt-form__actions">
						<div class="row">
							<div class="col-lg-12">
                                <button type="submit" class="btn btn-primary btn-submit">Submit</button>
								<button type="reset" class="btn btn-warning">Reset</button>
							</div>
						</div>
					</div>
				</div>
			<?=$this->Form->end();?>
			<!--end::Form-->
		</div>
    </div>
</div>

<?php $this->start('script');?>
    <script>
        var counterPl = <?=$keyPl+1;?>;
        var plSelected = {};
        var removedData = [];
        var counterItem      = {0:0};
        var currenciesJson = <?=json_encode($currenciesKurs);?>;
        $("#currency-id").select2({}).on("select2:select",function(e){
            $("#kurs-idr").val(currenciesJson[$(this).val()]);
        });

        $(".btn-add-pl").on("click",function(e){
            e.preventDefault();
            if($("#picking-list-id").val() != ""){
                var detailHtml = ``;
                $.each(plSelected.picking_lists_details, function(key,item){
                    var Rdate = moment(item.vw_stocks_location.Rdate).format("DD-MM-YYYY");
                    detailHtml += `<tr>
                        <td>${item.vw_stocks_location.RCode}</td>
                        <td>${Rdate}</td>
                        <td>${item.vw_stocks_location.RBCCode}</td>
                        <td>${item.vw_stocks_location.LocationName}</td>
                        <td>[${item.vw_stocks_location.ProductCode}] ${item.vw_stocks_location.ProductName}</td>
                        <td class="text-center">${item.vw_stocks_location.ProductUnit}</td>
                        <td class="text-right">${$.number(item.qty)}</td>
                        <td class="text-right td-input">
                            <input type="text" name="expenditures_picking_lists[${counterPl}][expenditures_picking_lists_details][${key}][amount]" value="0" class="form-control m-input onlyNumber">
                            <input type="hidden" name="expenditures_picking_lists[${counterPl}][expenditures_picking_lists_details][${key}][qty]" value="${item.qty}" class="form-control m-input">
                            <input type="hidden" name="expenditures_picking_lists[${counterPl}][expenditures_picking_lists_details][${key}][picking_lists_detail_id]" value="${item.id}" class="form-control m-input">
                        </td>
                    </tr>`;
                })
                var html = `<table class="table table-sm table-bordered table-input table-input-container">
                    <thead>
                        <tr>
                            <th colspan="7">
                                NO. PICKING LIST : ${plSelected.code}
                                <input type="hidden" name="expenditures_picking_lists[${counterPl}][picking_list_id]" value="${plSelected.id}" class="form-control m-input">
                            </th>
                            <td class="td-input"><a href="#" class="btn btn-block btn-danger btn-sm"><i class="la la-trash"></i> HAPUS PL</a></td>
                        </tr>
                        <tr>
                            <th width="10%">NO. PENERIMAAN</th>
                            <th width="10%">TGL. PENERIMAAN</th>
                            <th width="10%">NO. PENGAJUAN</th>
                            <th width="10%">LOKASI</th>
                            <th>NAMA BARANG</th>
                            <th width="10%">SATUAN</th>
                            <th width="10%">QTY</th>
                            <th width="15%">NILAI</th>
                        </tr>
                    </thead>
                    <tbody>
                        ${detailHtml}
                    </tbody>
                </table>`;
                var html = $(html);
                html.find(".onlyNumber").number( true, 2 );
                $(".pl-data").append(html);
                counterPl = counterPl + 1;
            }else{
                alert("PILIH PICKING LIST");
            }
            
        })
        

        $("body").on("click",".btn-remove-pl",function(e){
            e.preventDefault();
            var table = $(this).closest("table");
            table.remove();
        })

        $("#customer-id").select2({}).on("select2:select",function(e){
            if($(this).val() != ""){
                $.ajax({
                    url : `<?=$this->Url->build(['controller'=>'Apis','action'=>'getPickingListsByCustomer']);?>/${$("#customer-id").val()}`,
                    dataType : 'json',
                    beforeSend : function(){

                    },
                    success : function(response){
                        var dataResults = response;
                        plSelected = {};
                        $("#picking-list-id").select2('destroy');
                        $("#picking-list-id").select2({
                            data : dataResults,
                            width:'100%',
                        }).on("select2:select",function(x){
                            plSelected = x.params.data;
                        });
                    }
                })
            }else{
                $("#picking-list-id").select2('destroy');
                $("#picking-list-id").select2({
                    data : [],
                    width:'100%',
                })
            }
        });

        $("#bc-type").select2({
            minimumResultsForSearch: -1,
            width:'100%'
        });
        $("#picking-list-id").select2({
            width:'100%',
            data : <?=json_encode($pickingLists);?>
        }).on("select2:select",function(x){
            plSelected = x.params.data;
        });;


        
        var form = $('.kt-form');
        form.submit(function (e) {
			e.preventDefault();
            var btn = $('.btn-submit');
            var rulesValidate = {
                purchase_order_id: {
                    required: true
                },
                bc_code: {
                    required: true
                },
                bc_date: {
                    required: true
                },
                invoice_code: {
                    required: true
                },
                invoice_date: {
                    required: true
                },
                bl_code: {
                    required: true
                },
                bl_date: {
                    required: true
                }
            };
            // console.log(rulesValidate);

            form.validate({
                rules: rulesValidate,
            });

			if (!form.valid()) {
				return;
			}

			btn.addClass('kt-loader kt-loader--right kt-loader--light').attr('disabled', true);
            KTApp.block($("#kt_wrapper"))

			form.ajaxSubmit({
                dataType : 'json',
                beforeSend : function(){
                },
				success: function (response, status, xhr, $form) {
                    KTApp.unblock($("#kt_wrapper"))
                    btn.removeClass('kt-loader kt-loader--right kt-loader--light').attr('disabled', false);
                    if(response.code == 200){
                        window.location.href='<?=$this->Url->build(['action'=>'index']);?>'
                    }else{
                        Utils.showAlertMsg(form, 'danger', response.message,response.errors);
                        KTUtil.scrollTo("kt_wrapper",-200)
                    }
                    
                },
                error:function(){
                    KTApp.unblock($("#kt_wrapper"))
                    btn.removeClass('kt-loader kt-loader--right kt-loader--light').attr('disabled', false);
                    Utils.showAlertMsg(form, 'danger', 'TERJADI KESALAHAN',[]);
                    KTUtil.scrollTo("kt_wrapper",-200)
                }
			});
		});
    </script>
<?php $this->end();?>