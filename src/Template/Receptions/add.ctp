<?php
    $this->start('sub_header_toolbar');
?>
    <?php if($this->Acl->check(['action'=>'index']) == true):?>
        <a href="<?=$this->Url->build(['action'=>'index']);?>" class="btn btn-warning">
            <i class="la la-angle-double-left"></i> Kembali
        </a>
    <?php endif;?>
<?php
    $this->end();
?>
<div class="row">
    <div class="col-md-12">
        <div class="kt-portlet">
            <div class="kt-portlet__head">
				<div class="kt-portlet__head-label">
					<h3 class="kt-portlet__head-title">
						<?=$titlesubModule;?>
					</h3>
				</div>
			</div>
			<!--begin::Form-->
			<?= $this->Form->create($record,['class'=>'kt-form','type'=>'file']) ?>
				<div class="kt-portlet__body">
					<div class="kt-section kt-section--first">
                        <h3 class="kt-section__title">1. Informasi Pembelian:</h3>
                        <div class="row">
                            <div class="col-md-6 col-lg-8">
                                <?=$this->Form->control('purchase_order_id',[
                                    'class'=>'form-control m-input',
                                    'empty' => 'PILIH PO',
                                    'templateVars' => [
                                        'colsize' => 'col-lg-10 col-md-7',
                                    ],
                                    'label' => [
                                        'class'=> 'col-lg-2 col-md-5 col-form-label',
                                        'text'=>'PURCHASE ORDER *'
                                    ],
                                ]);?>
                            </div>
                            <div class="col-md-6 col-lg-4">
                                <?=$this->Form->control('purchase_order_date',[
                                    'class'=>'form-control m-input',
                                    'type' => 'text',
                                    'readonly' => true,
                                    'autocomplete' => 'off',
                                    'templateVars' => [
                                        'colsize' => 'col-lg-8 col-md-7',
                                    ],
                                    'label' => [
                                        'class'=> 'col-lg-4 col-md-5 col-form-label',
                                        'text'=>'TGL. PO'
                                    ],
                                ]);?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-lg-4">
                                <?=$this->Form->control('supplier_po',[
                                    'class'=>'form-control m-input',
                                    'type' => 'text',
                                    'readonly' => true,
                                    'autocomplete' => 'off',
                                    'templateVars' => [
                                        'colsize' => 'col-lg-8 col-md-7',
                                    ],
                                    'label' => [
                                        'class'=> 'col-lg-4 col-md-5 col-form-label',
                                        'text'=>'SUPLIER'
                                    ],
                                ]);?>
                            </div>
                            <div class="col-md-6 col-lg-4">
                                <?=$this->Form->hidden('currency_id',['id'=>'currency-id']);?>
                                <?=$this->Form->control('currency_name',[
                                    'class'=>'form-control m-input',
                                    'type' => 'text',
                                    'readonly' => true,
                                    'autocomplete' => 'off',
                                    'templateVars' => [
                                        'colsize' => 'col-lg-8 col-md-7',
                                    ],
                                    'label' => [
                                        'class'=> 'col-lg-4 col-md-5 col-form-label',
                                        'text'=>'MATA UANG'
                                    ],
                                ]);?>
                            </div>
                            <div class="col-md-6 col-lg-4">
                                <?=$this->Form->control('kurs_idr',[
                                    'class'=>'form-control m-input onlyNumber',
                                    'type' => 'text',
                                    'autocomplete' => 'off',
                                    'readonly' => true,
                                    'templateVars' => [
                                        'colsize' => 'col-lg-8 col-md-7',
                                    ],
                                    'label' => [
                                        'class'=> 'col-lg-4 col-md-5 col-form-label',
                                        'text'=>'KURS'
                                    ],
                                ]);?>
                            </div>
                            <div class="col-md-6 col-lg-4">
                                <?=$this->Form->control('value_type',[
                                    'class'=>'form-control m-input',
                                    'type' => 'text',
                                    'value' => 'CIF',
                                    'autocomplete' => 'off',
                                    'required' => true,
                                    'templateVars' => [
                                        'colsize' => 'col-lg-4 col-md-7',
                                    ],
                                    'label' => [
                                        'class'=> 'col-lg-4 col-md-5 col-form-label',
                                        'text'=>'Jenis Nilai'
                                    ],
                                ]);?>
                            </div>
                        </div>
		            </div>
                    <div class="kt-section">
                        <h3 class="kt-section__title">2. Informasi Dokumen Pabean:</h3>
                        <div class="row">
                            <div class="col-md-6 col-lg-4">
                                <?=$this->Form->control('bc_type',[
                                    'class'=>'form-control m-input',
                                    'type' => 'select',
                                    'options' => [
                                        'BC 1.6' => 'BC 1.6',
                                        'BC 2.7' => 'BC 2.7',
                                        'BC 4.0' => 'BC 4.0'
                                    ],
                                    'autocomplete' => 'off',
                                    'templateVars' => [
                                        'colsize' => 'col-lg-8 col-md-7',
                                    ],
                                    'label' => [
                                        'class'=> 'col-lg-4 col-md-5 col-form-label',
                                        'text'=>'Jenis BC'
                                    ],
                                ]);?>
                            </div>
                            <div class="col-md-6 col-lg-4">
                                <?=$this->Form->control('bc_code',[
                                    'class'=>'form-control m-input',
                                    'type' => 'text',
                                    'autocomplete' => 'off',
                                    'templateVars' => [
                                        'colsize' => 'col-lg-8 col-md-7',
                                    ],
                                    'label' => [
                                        'class'=> 'col-lg-4 col-md-5 col-form-label',
                                        'text'=>'No. Pengajuan *'
                                    ],
                                ]);?>
                            </div>
                            <div class="col-md-6 col-lg-4">
                                <?=$this->Form->control('bc_date',[
                                    'class'=>'form-control m-input date-picker',
                                    'type' => 'text',
                                    'autocomplete' => 'off',
                                    'templateVars' => [
                                        'colsize' => 'col-lg-8 col-md-7',
                                    ],
                                    'label' => [
                                        'class'=> 'col-lg-4 col-md-5 col-form-label',
                                        'text'=>'Tgl. Pengajuan *'
                                    ],
                                ]);?>
                            </div>
                            <div class="col-md-6 col-lg-4">
                                <?=$this->Form->control('file',[
                                    'class'=>'form-control m-input',
                                    'type' => 'file',
                                    'autocomplete' => 'off',
                                    'templateVars' => [
                                        'colsize' => 'col-lg-8 col-md-7',
                                        'helper' => '<span class="form-text text-muted">* Hanya PDF</span>'
                                    ],
                                    'label' => [
                                        'class'=> 'col-lg-4 col-md-5 col-form-label',
                                        'text'=>'Lampiran *'
                                    ],
                                ]);?>
                            </div>
                            <div class="col-md-6 col-lg-4">
                                <?=$this->Form->control('invoice_code',[
                                    'class'=>'form-control m-input',
                                    'type' => 'text',
                                    'autocomplete' => 'off',
                                    'templateVars' => [
                                        'colsize' => 'col-lg-8 col-md-7',
                                    ],
                                    'label' => [
                                        'class'=> 'col-lg-4 col-md-5 col-form-label',
                                        'text'=>'No. Invoice *'
                                    ],
                                ]);?>
                            </div>
                            <div class="col-md-6 col-lg-4">
                                <?=$this->Form->control('invoice_date',[
                                    'class'=>'form-control m-input date-picker',
                                    'type' => 'text',
                                    'autocomplete' => 'off',
                                    'templateVars' => [
                                        'colsize' => 'col-lg-8 col-md-7',
                                    ],
                                    'label' => [
                                        'class'=> 'col-lg-4 col-md-5 col-form-label',
                                        'text'=>'Tgl. Invoice *'
                                    ],
                                ]);?>
                            </div>
                            <div class="col-md-6 col-lg-4">
                                <?=$this->Form->control('bl_code',[
                                    'class'=>'form-control m-input',
                                    'type' => 'text',
                                    'autocomplete' => 'off',
                                    'templateVars' => [
                                        'colsize' => 'col-lg-8 col-md-7',
                                    ],
                                    'label' => [
                                        'class'=> 'col-lg-4 col-md-5 col-form-label',
                                        'text'=>'No. BL *'
                                    ],
                                ]);?>
                            </div>
                            <div class="col-md-6 col-lg-4">
                                <?=$this->Form->control('bl_date',[
                                    'class'=>'form-control m-input date-picker',
                                    'type' => 'text',
                                    'autocomplete' => 'off',
                                    'templateVars' => [
                                        'colsize' => 'col-lg-8 col-md-7',
                                    ],
                                    'label' => [
                                        'class'=> 'col-lg-4 col-md-5 col-form-label',
                                        'text'=>'Tgl. BL *'
                                    ],
                                ]);?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-lg-4">
                                <?=$this->Form->control('register_code',[
                                    'class'=>'form-control m-input',
                                    'type' => 'text',
                                    'autocomplete' => 'off',
                                    'templateVars' => [
                                        'colsize' => 'col-lg-8 col-md-7',
                                    ],
                                    'label' => [
                                        'class'=> 'col-lg-4 col-md-5 col-form-label',
                                        'text'=>'No. Pendaftaran *'
                                    ],
                                ]);?>
                            </div>
                            <div class="col-md-6 col-lg-4">
                                <?=$this->Form->control('register_date',[
                                    'class'=>'form-control m-input date-picker',
                                    'type' => 'text',
                                    'autocomplete' => 'off',
                                    'templateVars' => [
                                        'colsize' => 'col-lg-8 col-md-7',
                                    ],
                                    'label' => [
                                        'class'=> 'col-lg-4 col-md-5 col-form-label',
                                        'text'=>'Tgl. Pendaftaran *'
                                    ],
                                ]);?>
                            </div>
                        </div>
                    </div>

                    <div class="kt-section">
                        <h3 class="kt-section__title">3. Informasi Penerimaan:</h3>
                        <div class="row">
                            <div class="col-md-6 col-lg-4">
                                <?=$this->Form->control('date',[
                                    'class'=>'form-control m-input date-picker',
                                    'type' => 'text',
                                    'autocomplete' => 'off',
                                    'templateVars' => [
                                        'colsize' => 'col-lg-8 col-md-7',
                                    ],
                                    'label' => [
                                        'class'=> 'col-lg-4 col-md-5 col-form-label',
                                        'text'=>'Tgl. Penerimaan *'
                                    ],
                                ]);?>
                            </div>
                            <div class="col-md-6 col-lg-4">
                                <?=$this->Form->control('importer_id',[
                                    'class'=>'form-control m-input',
                                    'type' => 'select',
                                    'empty' => 'PILIH IMPORTIR',
                                    'templateVars' => [
                                        'colsize' => 'col-lg-8 col-md-7',
                                    ],
                                    'label' => [
                                        'class'=> 'col-lg-4 col-md-5 col-form-label',
                                        'text'=>'Importir *'
                                    ],
                                ]);?>
                            </div>
                            <div class="col-md-6 col-lg-4">
                                <?=$this->Form->control('sender_id',[
                                    'class'=>'form-control m-input',
                                    'type' => 'select',
                                    'empty' => 'PILIH PENGIRIM',
                                    'templateVars' => [
                                        'colsize' => 'col-lg-8 col-md-7',
                                    ],
                                    'label' => [
                                        'class'=> 'col-lg-4 col-md-5 col-form-label',
                                        'text'=>'Pengirim *'
                                    ],
                                ]);?>
                            </div>
                        </div>
                    </div>
                    <div class="kt-section">
                        <h3 class="kt-section__title">4. Container & Barang:</h3>
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <a href="#" class="btn btn-sm btn-brand btn-add-container">
                                    <i class="fa fa-plus"></i> Tambah Container
                                </a><br><br>
                                <div class="container-data"></div>
                            </div>
                        </div>

                    </div>
	            </div>
	            <div class="kt-portlet__foot">
					<div class="kt-form__actions">
						<div class="row">
							<div class="col-lg-12">
                                <button type="submit" class="btn btn-primary btn-submit">Submit</button>
								<button type="reset" class="btn btn-warning">Reset</button>
							</div>
						</div>
					</div>
				</div>
			<?=$this->Form->end();?>
			<!--end::Form-->
		</div>
    </div>
</div>
<!--begin::Modal-->
<div class="modal fade" id="modal-add-item" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <?=$this->Form->create(null,['class'=>'form-modal form-modal-item']);?>
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Data Barang</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    </button>
                </div>
                <div class="modal-body">
                    <?=$this->Form->control('purchase_orders_detail_id',[
                        'class'=>'form-control m-input',
                        'type' => 'select',
                        'options' => [],
                        'empty' => 'Pilih barang',
                        'required' => true,
                        'autocomplete' => 'off',
                        'templateVars' => [
                            'colsize' => 'col-lg-12',
                        ],
                        'label' => [
                            'class'=> 'col-lg-12 col-form-label',
                            'text'=>'Nama Barang'
                        ],
                    ]);?>
                    <div class="row">
                        <div class="col-md-6 col-lg-4">
                            <?=$this->Form->hidden('product_id',['id'=>'product-id']);?>
                            <?=$this->Form->hidden('product_name',['id'=>'product-name']);?>
                            <?=$this->Form->hidden('key_container',['id'=>'key-container']);?>
                            <?=$this->Form->control('hs_code',[
                                'class'=>'form-control m-input',
                                'type' => 'text',
                                'readonly' => true,
                                'autocomplete' => 'off',
                                'templateVars' => [
                                    'colsize' => 'col-lg-12',
                                ],
                                'label' => [
                                    'class'=> 'col-lg-12 col-form-label',
                                    'text'=>'HS'
                                ],
                            ]);?>
                        </div>
                        <div class="col-md-6 col-lg-4">
                            <?=$this->Form->control('country_id',[
                                'class'=>'form-control m-input',
                                'type' => 'select',
                                'required' => true,
                                'empty' => 'Pilih Negara',
                                'templateVars' => [
                                    'colsize' => 'col-lg-12',
                                ],
                                'label' => [
                                    'class'=> 'col-lg-12 col-form-label',
                                    'text'=>'Asal Negara'
                                ],
                            ]);?>
                        </div>
                        <div class="col-md-6 col-lg-4">
                            <?=$this->Form->control('category',[
                                'class'=>'form-control m-input',
                                'type' => 'select',
                                'required' => true,
                                'options' => [
                                    'DITIMBUN' => 'DITIMBUN',
                                    'KEPERLUAN PENGUSAHA' => 'KEPERLUAN PENGUSAHA',
                                    'CONTOH' => 'CONTOH'
                                ],
                                'empty' => 'Pilih kategori',
                                'templateVars' => [
                                    'colsize' => 'col-lg-12',
                                ],
                                'label' => [
                                    'class'=> 'col-lg-12 col-form-label',
                                    'text'=>'Kategori'
                                ],
                            ]);?>
                        </div>
                        <div class="col-md-6 col-lg-4">
                            <?=$this->Form->control('fasilitas',[
                                'class'=>'form-control m-input',
                                'type' => 'text',
                                'required' => true,
                                'autocomplete' => 'off',
                                'templateVars' => [
                                    'colsize' => 'col-lg-12',
                                ],
                                'label' => [
                                    'class'=> 'col-lg-12 col-form-label',
                                    'text'=>'Fasilitas'
                                ],
                            ]);?>
                        </div>
                        <div class="col-md-6 col-lg-4">
                            <?=$this->Form->control('no_urut',[
                                'class'=>'form-control m-input',
                                'type' => 'text',
                                'required' => true,
                                'autocomplete' => 'off',
                                'templateVars' => [
                                    'colsize' => 'col-lg-12',
                                ],
                                'label' => [
                                    'class'=> 'col-lg-12 col-form-label',
                                    'text'=>'No. Urut'
                                ],
                            ]);?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-lg-2">
                            <?=$this->Form->control('qty',[
                                'class'=>'form-control m-input onlyNumber',
                                'type' => 'text',
                                'autocomplete' => 'off',
                                'required' => true,
                                'templateVars' => [
                                    'colsize' => 'col-lg-12',
                                ],
                                'label' => [
                                    'class'=> 'col-lg-12 col-form-label',
                                    'text'=>'Jumlah'
                                ],
                            ]);?>
                        </div>
                        <div class="col-md-6 col-lg-3">
                            <?=$this->Form->control('unit',[
                                'class'=>'form-control m-input',
                                'type' => 'text',
                                'required' => true,
                                'readonly' => 'readonly',
                                'templateVars' => [
                                    'colsize' => 'col-lg-12',
                                ],
                                'label' => [
                                    'class'=> 'col-lg-12 col-form-label',
                                    'text'=>'Satuan'
                                ],
                            ]);?>
                        </div>
                        <div class="col-md-6 col-lg-2">
                            <?=$this->Form->control('qty_packaging',[
                                'class'=>'form-control m-input onlyNumber',
                                'type' => 'text',
                                'autocomplete' => 'off',
                                'required' => true,
                                'templateVars' => [
                                    'colsize' => 'col-lg-12',
                                ],
                                'label' => [
                                    'class'=> 'col-lg-12 col-form-label',
                                    'text'=>'Jumlah Kemasan'
                                ],
                            ]);?>
                        </div>
                        <div class="col-md-6 col-lg-3">
                            <?=$this->Form->control('packaging',[
                                'class'=>'form-control m-input',
                                'type' => 'text',
                                'autocomplete' => 'off',
                                'required' => true,
                                'templateVars' => [
                                    'colsize' => 'col-lg-12',
                                ],
                                'label' => [
                                    'class'=> 'col-lg-12 col-form-label',
                                    'text'=>'Kemasan'
                                ],
                            ]);?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-lg-4">
                            <?=$this->Form->control('value',[
                                'class'=>'form-control m-input onlyNumber',
                                'type' => 'text',
                                'required' => true,
                                'autocomplete' => 'off',
                                'templateVars' => [
                                    'colsize' => 'col-lg-12',
                                ],
                                'label' => [
                                    'class'=> 'col-lg-12 col-form-label',
                                    'text'=>'Nilai'
                                ],
                            ]);?>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            <?=$this->Form->end();?>
        </div>
    </div>
</div>
<!--end::Modal-->

<?php $this->start('script');?>
    <script>
        var counterContainer = 0;
        var counterItem      = {0:0};
        $(".btn-add-container").on("click",function(e){
            e.preventDefault();
            var html = `<table class="table table-sm table-bordered table-input table-input-container">
                <thead>
                    <tr>
                        <th width="30%">NO. CONTAINER</th>
                        <th width="30%">UKURAN</th>
                        <th width="30%">TIPE</th>
                        <th>#</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="td-input"><input type="text" autocomplete="off" name="receptions_containers[${counterContainer}][code]" class="form-control m-input" required="true" placeholder="Masukan no. container"></td>
                        <td class="td-input"><input type="text" autocomplete="off" name="receptions_containers[${counterContainer}][size]" class="form-control m-input" required="true" placeholder="Masukan ukuran container"></td>
                        <td class="td-input"><input type="text" autocomplete="off" name="receptions_containers[${counterContainer}][type]" class="form-control m-input" required="true" placeholder="Masukan tipe container"></td>
                        <td class="text-center">
                            <a href="#" class="btn btn-sm btn-block btn-danger btn-remove-container"><i class="la la-trash"></i></a>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3"><b>DETAIL BARANG</b></td>
                        <td class="text-center">
                            <a href="#" class="btn btn-sm btn-info btn-add-item btn-block" data-key="${counterContainer}"><i class="la la-plus"></i></a>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" class="td-input">
                            <table class="table table-sm table-bordered table-input table-input-item" style="margin-bottom:0px;" data-ck="${counterContainer}">
                                <thead>
                                    <tr>
                                        <th>
                                            - HS<br>
                                            - Uraian Barang<br>
                                            - Negara Asal
                                        </th>
                                        <th width="25%">
                                            - Kategori Barang<br>
                                            - Fasilitas & Nomor Urut
                                        </th>
                                        <th width="25%">
                                            - Jumlah & Jenis Satuan Barang<br>
                                            - Jumlah & Jenis Kemasan
                                        </th>
                                        <th width="15%">
                                            - Nilai
                                        </th>
                                        <th width="5%" class="text-center">
                                            #
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>`;
            $(".container-data").append(html);
            counterContainer = counterContainer + 1;
            counterItem[counterContainer] = 0;
        })



        $("body").on("click",".btn-add-item",function(e){
            e.preventDefault();
            var dataKey = $(this).data('key');
            $("#key-container").val(dataKey);
            $("#modal-add-item").modal('show');
        })
        
        $("#purchase-order-id").select2({}).on("select2:select",function(e){
            if($(this).val() != ""){
                $.ajax({
                    url : `<?=$this->Url->build(['controller'=>'Apis','action'=>'getPurchaseOrderFull']);?>/${$("#purchase-order-id").val()}`,
                    dataType : 'json',
                    beforeSend : function(){

                    },
                    success : function(response){
                        var data = response.result;
                        var poDate = moment(data.date).format("DD-MM-YYYY");
                        $("#purchase-order-date").val(poDate);
                        $("#supplier-po").val(data.supplier.name);
                        $("#currency-name").val(data.currency.name);
                        $("#currency-id").val(data.currency.id);
                        $("#kurs-idr").val(data.kurs);
                        var dataProducts = [];
                        $.each(data.purchase_orders_details, function(key,item){
                            dataProducts[key] = item;
                            dataProducts[key]['text'] = `[${item.product.code}] ${item.product.name}` 
                        })
                        $("#purchase-orders-detail-id").select2('destroy');
                        $("#purchase-orders-detail-id").select2({
                            data : dataProducts,
                            width:'100%',
                            dropdownParent: $('#modal-add-item')
                        }).on("select2:select",function(x){
                            var selectData = x.params.data;
                            if(selectData != ""){
                                $("#hs-code").val(selectData.product.hs_code);
                                $("#unit").val(selectData.product.unit);
                                $("#product-id").val(selectData.product.id);
                                $("#product-name").val(selectData.text);
                                $("#qty").val(selectData.qty);
                                $("#value").val(selectData.subtotal);
                            }else{
                                $("#hs-code").val("");
                                $("#unit").val("");
                                $("#product-id").val("");
                                $("#product-name").val("");
                                $("#qty").val("");
                                $("#value").val("");
                            }
                        });
                    }
                })
            }else{
                $("#purchase-orders-detail-id").select2('destroy');
                $("#purchase-orders-detail-id").select2({
                    data : [],
                    width:'100%',
                    dropdownParent: $('#modal-add-item')
                })
            }
        });

        $("body").on("click",".btn-remove-container",function(e){
            e.preventDefault();
            var table = $(this).closest("table");
            table.remove();
        });

        $("body").on("click",".btn-remove-item",function(e){
            e.preventDefault();
            var tr = $(this).closest("tr");
            tr.remove();
        })

        $(".form-modal-item").on("submit",function(e){
            e.preventDefault();
            var purchaseOrdersDetailId = $("#purchase-orders-detail-id"),
                productId              = $("#product-id"),
                keyContainer           = $("#key-container"),
                productName            = $("#product-name"),
                hsCode                 = $("#hs-code"),
                countryId              = $("#country-id"),
                countryName            = countryId.find("option:selected").text()
                category               = $("#category"),
                fasilitas              = $("#fasilitas"),
                noUrut                 = $("#no-urut"),
                qty                    = $("#qty"),
                unit                   = $("#unit"),
                qtyPackaging           = $("#qty-packaging"),
                packaging              = $("#packaging"),
                value                  = $("#value"),
                tableTarget            = $(`.table-input-item[data-ck=${keyContainer.val()}]`),
                tbodyTarget            = tableTarget.find('tbody'),
                keyItem                = counterItem[keyContainer.val()]
                ;
            var html = `
                <tr>
                    <td>
                        - ${hsCode.val()}<br>
                        - ${productName.val()}<br>
                        - ${countryName}
                    </td>
                    <td>
                        - ${category.val()}<br>
                        - ${fasilitas.val()} & ${noUrut.val()}
                    </td>
                    <td>
                        - ${qty.val()} ${unit.val()}<br>
                        - ${qtyPackaging.val()} ${packaging.val()}
                    </td>
                    <td>
                        - ${$.number(value.val())}
                        <input type="hidden" name="receptions_containers[${keyContainer.val()}][receptions_containers_details][${keyItem}][purchase_orders_detail_id]" value="${purchaseOrdersDetailId.val()}">
                        <input type="hidden" name="receptions_containers[${keyContainer.val()}][receptions_containers_details][${keyItem}][product_id]" value="${productId.val()}">
                        <input type="hidden" name="receptions_containers[${keyContainer.val()}][receptions_containers_details][${keyItem}][country_id]" value="${countryId.val()}">
                        <input type="hidden" name="receptions_containers[${keyContainer.val()}][receptions_containers_details][${keyItem}][category]" value="${category.val()}">
                        <input type="hidden" name="receptions_containers[${keyContainer.val()}][receptions_containers_details][${keyItem}][fasilitas]" value="${fasilitas.val()}">
                        <input type="hidden" name="receptions_containers[${keyContainer.val()}][receptions_containers_details][${keyItem}][no_urut]" value="${noUrut.val()}">
                        <input type="hidden" name="receptions_containers[${keyContainer.val()}][receptions_containers_details][${keyItem}][qty]" value="${qty.val()}">
                        <input type="hidden" name="receptions_containers[${keyContainer.val()}][receptions_containers_details][${keyItem}][qty_packaging]" value="${qtyPackaging.val()}">
                        <input type="hidden" name="receptions_containers[${keyContainer.val()}][receptions_containers_details][${keyItem}][packaging]" value="${packaging.val()}">
                        <input type="hidden" name="receptions_containers[${keyContainer.val()}][receptions_containers_details][${keyItem}][value]" value="${value.val()}">
                    </td>
                    <td class="text-center">
                        <a href="#" class="btn btn-sm btn-block btn-danger btn-remove-item"><i class="la la-trash"></i></a>
                    </td>
                </tr>
            `;
            counterItem[keyContainer.val()] = keyItem +1;
            tbodyTarget.append(html);

            $("#modal-add-item").modal('hide');
            resetModal();
        })

        function resetModal(){
            var selectInput = $(".form-modal-item").find("select");
            selectInput.val("").trigger("change");
            var input = $(".form-modal-item").find("input:not(#key-container)");
            input.val("");
        }
        

        $("#bc-type").select2({
            minimumResultsForSearch: -1,
            width:'100%'
        });
        $("#importer-id,#sender-id").select2({
            width:'100%'
        });
        $("#country-id").select2({
            dropdownParent: $('#modal-add-item'),
            width:'100%'
        });
        $("#category").select2({
            minimumResultsForSearch: -1,
            width:'100%',
            dropdownParent: $('#modal-add-item')
        });
        $("#purchase-orders-detail-id").select2({
            width:'100%',
            dropdownParent: $('#modal-add-item')
        });
        
        var form = $('.kt-form');
        form.submit(function (e) {
			e.preventDefault();
            var btn = $('.btn-submit');
            var rulesValidate = {
                purchase_order_id: {
                    required: true
                },
                bc_code: {
                    required: true
                },
                bc_date: {
                    required: true
                },
                invoice_code: {
                    required: true
                },
                invoice_date: {
                    required: true
                },
                bl_code: {
                    required: true
                },
                bl_date: {
                    required: true
                }
            };
            // console.log(rulesValidate);

            form.validate({
                rules: rulesValidate,
            });

			if (!form.valid()) {
				return;
			}

			btn.addClass('kt-loader kt-loader--right kt-loader--light').attr('disabled', true);
            KTApp.block($("#kt_wrapper"))

			form.ajaxSubmit({
                dataType : 'json',
                beforeSend : function(){
                },
				success: function (response, status, xhr, $form) {
                    KTApp.unblock($("#kt_wrapper"))
                    btn.removeClass('kt-loader kt-loader--right kt-loader--light').attr('disabled', false);
                    if(response.code == 200){
                        window.location.href='<?=$this->Url->build(['action'=>'index']);?>'
                    }else{
                        Utils.showAlertMsg(form, 'danger', response.message,response.errors);
                        KTUtil.scrollTo("kt_wrapper",-200)
                    }
                    
                },
                error:function(){
                    KTApp.unblock($("#kt_wrapper"))
                    btn.removeClass('kt-loader kt-loader--right kt-loader--light').attr('disabled', false);
                    Utils.showAlertMsg(form, 'danger', 'TERJADI KESALAHAN',[]);
                    KTUtil.scrollTo("kt_wrapper",-200)
                }
			});
		});
    </script>
<?php $this->end();?>