<html>
    <head>
        <meta charset="utf-8"/>
        <title><?=$defaultAppSettings['App.Name'];?><?=(!empty($titleModule) ? ' | '.$titleModule : '');?></title>
        <style>
            html,body{
                padding:5px;
                margin:0px;
                font-family:sans-serif;
            }
            table{
                font-size:11px;
                line-height:1.3;
                border-collapse: collapse;
                width:100%;
            }
            .table-header{
                margin-bottom:15px;
            }
            .table-main{
                border-top:4px double #000;
                border-bottom:4px double #000;
            }
            .table-main th{
                vertical-align:top;
            }
            .table-header th,.table-main th{
                text-align:left;
            }
            .company-name{
                font-size:14px;
            }
            .document-type{
                border: 2px solid #000;
                padding:5px;
                text-align:center;
            }
            .document-name{
                border-bottom: 1px solid #000;
                
            }
            .table-content{
                margin-top:10px;
                border:1px solid #000;
            }
            .table-content th,.table-content td{
                border-right:1px solid #000;
                border-bottom:1px solid #000;
            }
            .table-content td{
                vertical-align:top;
            }

            .text-center{
                text-align:center;
            }
            .text-right{
                text-align:right;
            }
            .text-left{
                text-align:left;
            }
            .table-signature{
                width:400px;
                margin-top:15px;
                text-align:center;
            }
        </style>
    </head>
    <body>
        <table class="table-header">
            <thead>
                <tr>
                    <th width="400px">
                        <div class="company-name"><?=$defaultAppSettings['Company.Name'];?></div>
                        <div class="company-address"><?=$defaultAppSettings['Company.Address'];?></div>
                    </th>
                    <th></th>
                    <th width="150px">
                        <div class="document-type">
                            <div class="document-name">PENERIMAAN BARANG</div>
                            <div class="document-code"><?=$record->code;?></div>
                        </div>
                    </th>
                </tr>
            </thead>
        </table>
        <table class="table-main">
            <tbody>
                <tr>
                    <th width="20%">DOKUMEN PENGAJUAN</th>
                    <th width="2%">:</th>
                    <th width="21%"><?=$record->bc_type.$record->bc_code;?></th>
                    <th width="10%"></th>
                    <th width="20%">TGL. PENGAJUAN</th>
                    <th width="2%">:</th>
                    <th width=""><?=$record->bc_date->format('d-m-Y');?></th>
                </tr>
                <tr>
                    <th>NO. PENDAFTARAN</th>
                    <th>:</th>
                    <th><?=$record->register_code;?></th>
                    <th></th>
                    <th>TGL. PENDAFTARAN</th>
                    <th>:</th>
                    <th><?=$record->register_date->format('d-m-Y');?></th>
                </tr>
                <tr>
                    <th>NO. INVOICE</th>
                    <th>:</th>
                    <th><?=$record->invoice_code;?></th>
                    <th></th>
                    <th>TGL. INVOICE</th>
                    <th>:</th>
                    <th><?=$record->invoice_date->format('d-m-Y');?></th>
                </tr>
                <tr>
                    <th>NO. BL</th>
                    <th>:</th>
                    <th><?=$record->bl_code;?></th>
                    <th></th>
                    <th>TGL. BL</th>
                    <th>:</th>
                    <th><?=$record->bl_date->format('d-m-Y');?></th>
                </tr>
                <tr>
                    <th>NO. PO</th>
                    <th>:</th>
                    <th><?=$record->purchase_order->code;?></th>
                    <th></th>
                    <th>TANGGAL PENERIMAAN</th>
                    <th>:</th>
                    <th><?=$record->date->format('d-m-Y');?></th>
                </tr>
                <tr>
                    <th>SUPLIER</th>
                    <th>:</th>
                    <th>
                        <?=$record->purchase_order->supplier->name;?>
                    </th>
                    <th></th>
                    <th>MATA UANG</th>
                    <th>:</th>
                    <th><?=$record->currency->name;?></th>
                </tr>
                <tr>
                    <th>IMPORTIR</th>
                    <th>:</th>
                    <th>
                        <?=$record->importer->name;?>
                    </th>
                    <th></th>
                    <th>PENGIRIM</th>
                    <th>:</th>
                    <th colspan="5">
                        <?=$record->sender->name;?>
                    </th>
                </tr>
            </tbody>
        </table>
        <?php foreach($record->receptions_containers as $keyContainer => $receptionsContainer):?>
            <table class="table-content" style="margin-bottom:0px;">
                <thead>
                    <tr>
                        <th colspan="4">DATA CONTAINER</th>
                    </tr>
                    <tr>
                        <th width="10%">NO.</th>
                        <th>NO. CONTAINER</th>
                        <th width="30%">UKURAN</th>
                        <th width="30%">TIPE</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="text-center">
                            <?=$keyContainer+1;?>
                        </td>
                        <td class="text-center">
                            <?=$receptionsContainer->code;?>
                        </td>
                        <td class="text-center">
                            <?=$receptionsContainer->size;?>
                        </td>
                        <td class="text-center">
                            <?=$receptionsContainer->type;?>
                        </td>
                    </tr>
                </tbody>
            </table>
            <table class="table-content" style="margin-top:0px;">
                <thead>
                    <tr>
                        <th width="5%" class="text-left">
                            NO.
                        </th>
                        <th class="text-left">
                            - HS<br>
                            - Uraian Barang<br>
                            - Negara Asal
                        </th>
                        <th class="text-left" width="25%">
                            - Kategori Barang<br>
                            - Fasilitas & Nomor Urut
                        </th>
                        <th class="text-left" width="25%">
                            - Jumlah & Jenis Satuan Barang<br>
                            - Jumlah & Jenis Kemasan
                        </th>
                        <th class="text-left" width="15%">
                            - Nilai
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($receptionsContainer->receptions_containers_details as $keyItem => $receptionsContainerDetail):?>
                        <tr>
                            <td><?=$keyItem+1;?></td>
                            <td>
                                - <?=$receptionsContainerDetail->product->hs_code;?><br>
                                - [<?=$receptionsContainerDetail->product->code;?>] <?=$receptionsContainerDetail->product->name;?><br>
                                - <?=$receptionsContainerDetail->country->name;?>
                            </td>
                            <td>
                                - <?=$receptionsContainerDetail->category;?><br>
                                - <?=$receptionsContainerDetail->fasilitas;?> &amp; <?=$receptionsContainerDetail->no_urut;?>
                            </td>
                            <td>
                                - <?=$receptionsContainerDetail->qty;?> <?=$receptionsContainerDetail->product->unit;?><br>
                                - <?=$receptionsContainerDetail->qty_packaging;?> <?=$receptionsContainerDetail->packaging;?>
                            </td>
                            <td>
                                - <?=number_format($receptionsContainerDetail->value);?>
                            </td>
                        </tr>
                    <?php endforeach;?>
                </tbody>
            </table>
        <?php endforeach;?>
        <table class="table-signature">
            <tbody>
                <tr>
                    <td width="50%">
                        USER PEMBUAT<br><br><br><br>
                        (<?=$record->created_user->name;?>)
                    </td>
                    <td width="50%">
                        MENGETAHUI<br><br><br><br>
                        (..................................)
                    </td>
                </tr>
            </tbody>
        </table>
    </body>
</html>