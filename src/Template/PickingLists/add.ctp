<?php
    $this->start('sub_header_toolbar');
?>
    <?php if($this->Acl->check(['action'=>'index']) == true):?>
        <a href="<?=$this->Url->build(['action'=>'index']);?>" class="btn btn-warning">
            <i class="la la-angle-double-left"></i> Kembali
        </a>
    <?php endif;?>
<?php
    $this->end();
?>
<div class="row">
    <div class="col-md-12">
        <div class="kt-portlet">
            <div class="kt-portlet__head">
				<div class="kt-portlet__head-label">
					<h3 class="kt-portlet__head-title">
						<?=$titlesubModule;?>
					</h3>
				</div>
			</div>
			<!--begin::Form-->
			<?= $this->Form->create($record,['class'=>'kt-form','type'=>'file']) ?>
				<div class="kt-portlet__body">
					<div class="kt-section kt-section--first">
                        <div class="row">
                            <div class="col-md-6">
                                <?=$this->Form->control('date',[
                                    'class'=>'form-control m-input date-picker',
                                    'type' => 'text',
                                    'value'=> date('d-m-Y'),
                                    'autocomplete' => 'off',
                                    'templateVars' => [
                                        'colsize' => 'col-lg-8 col-md-7',
                                    ],
                                    'label' => [
                                        'class'=> 'col-lg-4 col-md-5 col-form-label',
                                        'text'=>'TGL. PICKING LIST*'
                                    ],
                                ]);?>
                            </div>
                            <div class="col-md-6">
                                <?=$this->Form->control('customer_id',[
                                    'class'=>'form-control m-input',
                                    'empty' => 'PILIH CUSTOOMER',
                                    'templateVars' => [
                                        'colsize' => 'col-lg-8 col-md-7',
                                    ],
                                    'label' => [
                                        'class'=> 'col-lg-4 col-md-5 col-form-label',
                                        'text'=>'CUSTOMER *'
                                    ],
                                ]);?>
                            </div>
                        </div>
		            </div>
                    <div class="kt-section kt-section--last">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table class="table table-sm table-bordered table-input">
                                        <thead>
                                            <tr>
                                                <th width="50px" class="text-center">
                                                    <label class="kt-checkbox kt-checkbox--tick kt-checkbox--brand">
                                                        <input type="checkbox" class="checkall">
                                                        <span></span>
                                                    </label>
                                                </th>
                                                <th width="10%">NO. PENERIMAAN</th>
                                                <th width="10%">TGL. PENERIMAAN</th>
                                                <th width="10%">NO. PENGAJUAN</th>
                                                <th width="10%">LOKASI</th>
                                                <th>NAMA BARANG</th>
                                                <th width="10%">SATUAN</th>
                                                <th width="10%">SALDO</th>
                                                <th width="10%">QTY</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach($stocksLocations as $key => $stockLocation):?>
                                                <tr class="tr-input">
                                                    <td class="text-center">
                                                        <input type="hidden" class="hiddenid" value="0" name="picking_lists_details[<?=$key;?>][stocks_location_id]">
                                                        <label class="kt-checkbox kt-checkbox--tick kt-checkbox--brand">
                                                            <input type="checkbox" class="checkid" value="<?=$stockLocation->StocksLocationID;?>" name="picking_lists_details[<?=$key;?>][stocks_location_id]">
                                                            <span></span>
                                                        </label>
                                                    </td>
                                                    <td><?=$stockLocation->RCode;?></td>
                                                    <td><?=$stockLocation->RDate->format('d-m-Y');?></td>
                                                    <td><?=$stockLocation->RBCCode;?></td>
                                                    <td><?=$stockLocation->LocationName;?></td>
                                                    <td><?=$stockLocation->ProductName;?></td>
                                                    <td><?=$stockLocation->ProductUnit;?></td>
                                                    <td class="text-right"><?=number_format($stockLocation->StocksLocationTemporaryStock,2);?></td>
                                                    <td class="td-input">
                                                        <input type="text" value="<?=$stockLocation->StocksLocationTemporaryStock;?>" class="form-control m-input onlyNumber" name="picking_lists_details[<?=$key;?>][qty]">
                                                    </td>
                                                </tr>
                                            <?php endforeach;?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
	            </div>
	            <div class="kt-portlet__foot">
					<div class="kt-form__actions">
						<div class="row">
							<div class="col-lg-12">
                                <button type="submit" class="btn btn-primary btn-submit">Submit</button>
								<button type="reset" class="btn btn-warning">Reset</button>
							</div>
						</div>
					</div>
				</div>
			<?=$this->Form->end();?>
			<!--end::Form-->
		</div>
    </div>
</div>

<?php $this->start('script');?>
    <script>
        $("#customer-id").select2({});
        $(".checkall").on("click",function(e){
            if($(this).prop("checked")){
                $(".checkid").prop("checked",true);
            }else{
                $(".checkid").prop("checked",false);
            }
        })

        var form = $('.kt-form');
        

        form.submit(function (e) {
			e.preventDefault();
            var btn = $('.btn-submit');
            var rulesValidate = {
                date: {
                    required: true
                },
                location_id: {
                    required: true
                },
            };

            form.validate({
                rules: rulesValidate,
            });

			if (!form.valid()) {
				return;
			}

			btn.addClass('kt-loader kt-loader--right kt-loader--light').attr('disabled', true);
            KTApp.block($("#kt_wrapper"))

			form.ajaxSubmit({
                dataType : 'json',
                beforeSend : function(){
                },
				success: function (response, status, xhr, $form) {
                    KTApp.unblock($("#kt_wrapper"))
                    btn.removeClass('kt-loader kt-loader--right kt-loader--light').attr('disabled', false);
                    if(response.code == 200){
                        window.location.href='<?=$this->Url->build(['action'=>'index']);?>'
                    }else{
                        Utils.showAlertMsg(form, 'danger', response.message,response.errors);
                        KTUtil.scrollTo("kt_wrapper",-200)
                    }
                    
                },
                error : function(){
                    KTApp.unblock($("#kt_wrapper"))
                    Utils.showAlertMsg(form, 'danger', 'GAGAL DISIMPAN',[]);
                    KTUtil.scrollTo("kt_wrapper",-200)
                }
			});
		});
    </script>
<?php $this->end();?>