<?php
    $this->start('sub_header_toolbar');
?>
    <?php if($this->Acl->check(['action'=>'index']) == true):?>
        <a href="<?=$this->Url->build(['action'=>'index']);?>" class="btn btn-warning">
            <i class="la la-angle-double-left"></i> Kembali
        </a>
    <?php endif;?>
    <?php if($this->Acl->check(['action'=>'add']) == true):?>
        <a href="<?=$this->Url->build(['action'=>'add']);?>" class="btn btn-primary">
            <i class="la la-plus-circle"></i> Tambah Data
        </a>
    <?php endif;?>
    <?php if($this->Acl->check(['action'=>'edit']) == true):?>
        <a href="<?=$this->Url->build(['action'=>'edit',$record->id]);?>" class="btn btn-success">
            <i class="la la-edit"></i> Edit Data
        </a>
    <?php endif;?>
<?php
    $this->end();
?>
<div class="row">
    <div class="col-md-12">
        <div class="kt-portlet">
            <div class="kt-portlet__head">
				<div class="kt-portlet__head-label">
					<h3 class="kt-portlet__head-title">
						<?=$titlesubModule;?>
					</h3>
				</div>
			</div>
            <div class="kt-portlet__body">
                <table class="table table-striped table-bordered table-hover table-checkable">
                    <tr>
                        <th scope="row" width="20%"><?= __('Tanggal Jurnal') ?></th>
                        <td  width="30%"><?= h($record->date->format('d-m-Y')) ?></td>
                        <th scope="row" width="20%"><?= __('Kode Jurnal') ?></th>
                        <td><?= h($record->code) ?></td>
                    </tr>
                    <tr>
                        <th scope="row"><?= __('Keterangan Jurnal') ?></th>
                        <td colspan="3"><?= h($record->description) ?></td>
                    </tr>
                    <tr>
                        <th scope="row" colspan="4"><?= __('DETAIL AKUN') ?></th>
                    </tr>
                    <tr>
                        <td scope="row" colspan="4" class="no-padding">
                            <table class="table table-bordered table-details-view mb-0">
                                <thead>
                                    <tr>
                                        <th width="5%">NO</th>
                                        <th>AKUN</th>
                                        <th width="15%">DEBIT</th>
                                        <th width="15%">KREDIT</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $totalCredit =0; $totalDedit =0;?>
                                    <?php foreach($record->journals_details as $key => $journalDetail):?>
                                        <?php $totalCredit += $journalDetail->credit; $totalDedit += $journalDetail->debit;?>
                                        <tr>
                                            <td><?=$key+1;?></td>
                                            <td>[<?=$journalDetail->code_account->code;?>] <?=$journalDetail->code_account->name;?></td>
                                            <td class="text-right"><?=number_format($journalDetail->debit,2);?></td>
                                            <td class="text-right"><?=number_format($journalDetail->credit,2);?></td>
                                        </tr>
                                    <?php endforeach;?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th colspan="2" class="text-right">TOTAL</th>
                                        <th class="text-right"><?=number_format($totalDedit,2);?></th>
                                        <th class="text-right"><?=number_format($totalCredit,2);?></th>
                                    </tr>
                                </tfoot>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
		</div>
    </div>
</div>