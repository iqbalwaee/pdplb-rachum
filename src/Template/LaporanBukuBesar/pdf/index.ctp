<!DOCTYPE html>
    <head>
        <title>
            Laporan Buku Besar
        </title>
        <style>
            body{
                margin:0px;
                padding:0px;
                font-size:11px;
                font-family: "Arial";
            }
            .text-left{
                text-align:left;
            }
            .text-center{
                text-align:center;
            }
            .text-right{
                text-align:right;
            }
            table{
                width:100%;
                margin:0px;
                padding:0px;
                border:1px solid #000;
                border-collapse: collapse;
            }
            table td, table th{
                border-bottom :1px solid #000;
                border-left :1px solid #000;
            }
            table thead th{
                padding:5px;
                height:20px;
            }
            table tbody td{
                vertical-align:top;
                padding:5px;
                height:40px;
            }
            table tbody tr.summary td{
                vertical-align:middle;
                padding:5px;
                height:20px !important;
                font-weight:bold;
            }
            thead { display: table-header-group; }
            tfoot { display: table-row-group; }
            tr { page-break-inside: avoid; }
            .page-break{
                page-break-after:always;
            }
            .header-optional{
                display:none;
            }
            @media print{
                .header-optional{
                    display:table-header-group;
                }
            }
        </style>
    </head>
    <body>
        <?php foreach($codeAccounts as $ckey => $codeAccount):?>
        <?php
            $results = $codeAccount->journals;
            $header = '<tr><th class="text-left" colspan="6">BUKU BESAR<br>';
            $header .= 'PERIODE : '.strtoupper($this->Utilities->indonesiaDateFormat($start)).' S.D '.strtoupper($this->Utilities->indonesiaDateFormat($end)).'<br>';
            if(!empty($businessType)):
                $header .= 'JENIS USAHA : '.$this->Utilities->businessTypeVal($businessType->type).' - '.$businessType->name.'';
            else:
                $header .= 'SEMUA JENIS USAHA';
            endif;
            $header .= '<br>COA : ['.$codeAccount->code.'] '.$codeAccount->name.'</th></tr>';
            $header .= '<tr>
                <th >No.</th>
                <th>No. Jurnal</th>
                <th>Tanggal</th>
                <th>Remark</th>
                <th>Debit</th>
                <th>Kredit</th>
            </tr>';
        ?>
        <table>
            <thead>
                <?=$header;?>
            </thead>
            <tbody>
                <?php $totalDebit = 0; $totalKredit = 0;?>
                <?php 
                    if($codeAccount->normal_balance == 0){
                        $totalDebit     += $codeAccount->saldoAwal;
                    }else{
                        $totalKredit    += $codeAccount->saldoAwal;
                    }
                ?>
                <tr>
                    <td width="2%"></td>
                    <td width="5%" class="text-center"></td>
                    <td width="10%" class="text-center"><?=$start;?></td>
                    <td>SALDO AWAL</td>
                    <td width="10%" class="text-right"><?=number_format($totalDebit);?></td>
                    <td width="10%" class="text-right"><?=number_format($totalKredit);?></td>
                </tr>
                <?php foreach($results as $key => $result):?>
                    <?php $totalDebit += $result->debit; $totalKredit += $result->credit;?>
                        <tr>
                            <td width="2%"><?=$key+1;?></td>
                            <td width="5%" class="text-center"><?=$result->journal->code;?></td>
                            <td width="10%" class="text-center"><?=$result->journal->date->format('d-m-Y');?></td>
                            <td><?=$result->journal->description ;?></td>
                            <td width="10%" class="text-right"><?=number_format($result->debit);?></td>
                            <td width="10%" class="text-right"><?=number_format($result->credit);?></td>
                        </tr>
                <?php endforeach;?>
                <tr class="summary">
                    <td colspan="4" style="background:#ddd;" class="text-right">&nbsp;</td>
                    <td width="10%" style="background:#ddd;" class="text-right"><?=number_format($totalDebit);?></td>
                    <td width="10%" style="background:#ddd;" class="text-right"><?=number_format($totalKredit);?></td>
                </tr>
            </tbody>
        </table>
        <?php if(!empty($codeAccounts[$ckey+1])):?>
        <div class="page-break"></div>
        <?php endif;?>
        <?php endforeach;?>
    </body>
</html>