<?php
    $this->start('sub_header_toolbar');
?>
    <?php if($this->Acl->check(['action'=>'index']) == true):?>
        <a href="<?=$this->Url->build(['action'=>'index']);?>" class="btn btn-warning">
            <i class="la la-angle-double-left"></i> Kembali
        </a>
    <?php endif;?>
<?php
    $this->end();
?>
<div class="row">
    <div class="col-md-12">
        <div class="kt-portlet">
            <div class="kt-portlet__head">
				<div class="kt-portlet__head-label">
					<h3 class="kt-portlet__head-title">
						Laporan Buku Besar
					</h3>
				</div>
			</div>
            <!--begin::Form-->
			<?= $this->Form->create(null,['class'=>'kt-form','type'=>'file']) ?>
				<div class="kt-portlet__body">
					<div class="kt-section kt-section--first">
                        <div class="row">
                            <div class="col-md-6 col-lg-8">
                                <?php
                                    echo $this->Form->control('code_account_id',[
                                        'class'=>'form-control m-input',
                                        'templateVars' => [
                                            'colsize' => 'col-lg-8 col-xl-8',
                                        ],
                                        "multiple" => "multiple",
                                        'label' => [
                                            'class'=> 'col-lg-4 col-xl-2 col-form-label',
                                            'text'=>'Pilih COA *'
                                        ],
                                        'empty' => 'Semua COA'
                                    ]);
                                ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-10 col-lg-8">
                                <?php
                                    echo $this->Form->control('periode',[
                                        'class'=>'form-control m-input',
                                        'templateVars' => [
                                            'colsize' => 'col-lg-8 col-xl-8',
                                        ],
                                        'class'=>'datepicker-range-report form-control',
                                        'label' => [
                                            'class'=> 'col-lg-2 col-xl-2 col-form-label',
                                            'text'=>'Periode *'
                                        ],
                                    ]);
                                ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-10 col-lg-8">
                                <?php
                                    echo $this->Form->control('output',[
                                        'class'=>'form-control m-input',
                                        'templateVars' => [
                                            'colsize' => 'col-lg-8 col-xl-8',
                                        ],
                                        'options' => [
                                            'webview' => 'Webview',
                                            'pdf' => 'PDF',
                                            'excel' => 'Excel'
                                        ],
                                        'class'=>' form-control',
                                        'label' => [
                                            'class'=> 'col-lg-2 col-xl-2 col-form-label',
                                            'text'=>'Output *'
                                        ],
                                    ]);
                                ?>
                            </div>
                        </div>
		            </div>
	            </div>
	            <div class="kt-portlet__foot">
					<div class="kt-form__actions">
						<div class="row">
							<div class="col-lg-9">
                                <button type="submit" class="btn btn-primary btn-submit">Submit</button>
								<button type="reset" class="btn btn-warning">Reset</button>
							</div>
						</div>
					</div>
				</div>
			<?=$this->Form->end();?>
			<!--end::Form-->
		</div>
    </div>
</div>

<?php $this->start('script');?>
    <script>
        $('.datepicker-range-report').daterangepicker({
            buttonClasses: ' btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary',
            autoApply : true,
            locale:{
                format:'YYYY-MM-DD',
                "separator": " / ",
            }
        });
        $(".kt-form").validate({
            rules: {
                periode: {
                    required: true 
                }
            },
            messages : {
                periode: {
                    required: "Pilih periode" 
                }
            },
            errorPlacement: function ( error, element ) {

                if(element.parent().hasClass('input-group')){
                    error.insertAfter( element.parent() );
                }else{
                    error.insertAfter( element );
                }

            },
            submitHandler: function (form) {
                var periode = $("#periode").val();
                var businessTypeId    = $("#business-type-id").val();
                var codeAccount       = $("#code-account-id").val();
                var output    = $("#output").val();
                var explode_periode = periode.split(" / ");
                var date_1 = explode_periode[0];
                var date_2 = explode_periode[1];
                var urlTarget = "<?=$this->Url->build(['action'=>'index']);?>?print=true&date_1="+date_1+"&date_2="+date_2+"&business-type-id=&code-account-id="+codeAccount+"&output="+output;
                window.open(urlTarget);
            }
        })
        function initSelect2(el){
            el.select2({
                ajax: {
                    url: '<?=$this->Url->build(['controller'=>'Apis','action'=>'getAccounts']);?>',
                    dataType: 'json',
                    data: function (params) {
                        var query = {
                            search: params.term,
                            business_type_id : $("#business-type-id").val()
                        }
                        return query;
                    },
                    processResults: function (data) {
                        return {
                            results: data
                        };
                    }
                },
            });
        }
        initSelect2($("#code-account-id"));
        $(".kt-form").on('submit',function(e){
            e.preventDefault();
            
        })
    </script>
<?php $this->end();?>