<?php
    $this->start('sub_header_toolbar');
?>
    <?php if($this->Acl->check(['action'=>'index']) == true):?>
        <a href="<?=$this->Url->build(['action'=>'index']);?>" class="btn btn-warning">
            <i class="la la-angle-double-left"></i> Kembali
        </a>
    <?php endif;?>
<?php
    $this->end();
?>
<div class="row">
    <div class="col-md-12">
        <div class="kt-portlet">
            <div class="kt-portlet__head">
				<div class="kt-portlet__head-label">
					<h3 class="kt-portlet__head-title">
						<?=$titlesubModule;?>
					</h3>
				</div>
			</div>
            <!--begin::Form-->
			<?= $this->Form->create($record,['class'=>'kt-form','type'=>'file']) ?>
				<div class="kt-portlet__body">
					<div class="kt-section kt-section--first">
                        <div class="row">
                            <div class="col-md-6">
                                <?php
                                    echo $this->Form->control('date',[
                                        'class'=>'form-control m-input',
                                        'templateVars' => [
                                            'colsize' => 'col-lg-8 col-xl-8',
                                        ],
                                        'label' => [
                                            'class'=> 'col-lg-4 col-xl-4 col-form-label',
                                            'text'=>'Tanggal *'
                                        ],
                                        'value' => date('d-m-Y'),
                                        'type' => 'text',
                                        'datepicker' => 'true',
                                        'autocomplete' => 'off'
                                    ]);
                                ?>
                            </div>
                        </div>
                        <?php
                            echo $this->Form->control('description',[
                                'class'=>'form-control m-input',
                                'required'=>true,
                                'templateVars' => [
                                    'colsize' => 'col-lg-10 col-xl-10',
                                ],
                                'label' => [
                                    'class'=> 'col-lg-2 col-xl-2 col-form-label',
                                    'text'=>'Keterangan *'
                                ],
                                'rows' => 2
                            ]);
                        ?>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table class="table table-detail table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>AKUN</th>
                                                <th width="150px">DEBIT</th>
                                                <th width="150px">KREDIT</th>
                                                <th width="50px" class="text-center no-padding">
                                                    <a href="#" class="btn btn-icon btn-sm btn-primary btn-add-account">
                                                        <i class="fa fa-plus"></i>
                                                    </a>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr class="nothing">
                                                <td colspan="4">TIDAK ADA DATA</td>
                                            </tr>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th class="text-right">
                                                    TOTAL
                                                </th>
                                                <th class="text-right">
                                                    <div class="total-debit">0</div>
                                                </th>
                                                <th class="text-right">
                                                    <div class="total-credit">0</div>
                                                </th>
                                                <th></th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
		            </div>
	            </div>
	            <div class="kt-portlet__foot">
					<div class="kt-form__actions">
						<div class="row">
							<div class="col-lg-12">
                                <button type="submit" class="btn btn-primary btn-submit">Submit</button>
								<button type="reset" class="btn btn-warning">Reset</button>
							</div>
						</div>
					</div>
				</div>
			<?=$this->Form->end();?>
			<!--end::Form-->
		</div>
    </div>
</div>

<?php $this->start('script');?>
    <script>
        
        var totalDebt   = 0;
        var totalCredit = 0;
        var countDetail = 0;
        $(".btn-add-account").on("click",function(e){
            e.preventDefault();
            
            var table = $(".table-detail");
            var tbody = table.find("tbody");
            var emptyRow = tbody.find(".nothing");
            if(emptyRow != undefined){
                emptyRow.remove()
            }
            var row = `<tr>
                        <td class="no-padding">
                            <select name="journals_details[${countDetail}][code_account_id]" style="width:100%" class="form-control m-input" data-name="code_account_id" required="required">
                                <option value="">Pilih COA</option>
                            </select>
                        </td>
                        <td class="no-padding">
                            <input type="text" name="journals_details[${countDetail}][debit]" data-name="debit" class="form-control m-input onlyNumber text-debit text-right" value="0" autocomplete="off">
                        </td>
                        <td class="no-padding">
                            <input type="text" name="journals_details[${countDetail}][credit]"  data-name="credit" class="form-control m-input  text-right text-credit onlyNumber" value="0" autocomplete="off">
                        </td>
                        <td class="text-center no-padding">
                            <a href="#" class="btn btn-icon btn-sm btn-danger btn-delete-account">
                                <i class="fa fa-times"></i>
                            </a>
                        </td>
                        </tr>`;
            var rowInitialize = $(row);
            initSelect2(rowInitialize.find('select'))
            rowInitialize.find('.onlyNumber').number( true, 2 );
            tbody.append(rowInitialize);
            countDetail = countDetail +1;
        })
        function initSelect2(el){
            el.select2({
                ajax: {
                    url: '<?=$this->Url->build(['controller'=>'Apis','action'=>'getAccounts']);?>',
                    dataType: 'json',
                    data: function (params) {
                        var query = {
                            search: params.term,
                        }
                        // Query parameters will be ?search=[term]&type=public
                        return query;
                    },
                    processResults: function (data) {
                        return {
                            results: data
                        };
                    }
                },
            });
        }
        $("body").on("keyup",".text-debit,.text-credit",function(e){
            calculateData()
        })
        function calculateData(){
            var table = $(".table-detail");
            var tbody = table.find("tbody");
            var tbodyTr = tbody.find("tr");
            totalDebt = 0;
            totalCredit = 0;
            $.each(tbodyTr,function(e,item){
                var debt = $(item).find(".text-debit").val() *1;
                var credit = $(item).find(".text-credit").val() *1;
                totalDebt   = (totalDebt *1) + debt;
                totalCredit = (totalCredit * 1) + credit;
            })
            $(".total-debit").html($.number(totalDebt,2));
            $(".total-credit").html($.number(totalCredit,2));
        }
        $("body").on("click",".btn-delete-account",function(e){
            e.preventDefault();
            var btn = $(this);
            var tr = btn.closest('tr');
            tr.remove();
            
            var table = $(".table-detail");
            var tbody = table.find("tbody");
            var tbodyTr = tbody.find("tr");

            countDetail = 0;
            $.each(tbodyTr,function(key,item){
                $(item).find('select').select2('destroy');
                var input = $(item).find('select,input');
                $.each(input,function(keyInput,itemInput){
                    console.log()
                    var name = $(itemInput).data('name');
                    $(itemInput).attr('name',`journals_details[${countDetail}][${name}]`);
                    if(name == "code_account_id"){
                        initSelect2($(itemInput))
                    }
                })
                countDetail = countDetail +1;
                
            })
            calculateData()

        })
        $(".kt-form").on("submit",function(e){
            return true;
        })
    </script>
<?php $this->end();?>