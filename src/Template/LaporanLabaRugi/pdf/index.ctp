<!DOCTYPE html>
    <head>
        <style>
            body{
                margin:0px;
                padding:0px;
                font-size:10px;
                font-family: Arial, Helvetica, sans-serif;
            }
            .text-left{
                text-align:left;
            }
            .text-center{
                text-align:center;
            }
            .text-right{
                text-align:right;
            }
            table{
                width:100%;
                margin:0px;
                padding:0px;
                border-collapse: collapse;
            }
            table td, table th{
            }
            table tbody td{
                vertical-align:top;
                padding:3px 5px;
            }
            thead { display: table-header-group; }
            tfoot { display: table-row-group; }
            tr { page-break-inside: avoid; }
            .page-break{
                page-break-after:always;
            }
            .header-optional{
                display:none;
            }
            @media print{
                .header-optional{
                    display:table-header-group;
                }
            }
        </style>
    </head>
    <body>
        <?php
            $header = '<tr><th colspan="2" class="text-left">LAPORAN LABA RUGI<br>';
            $header .= 'PERIODE : '.strtoupper($this->Utilities->indonesiaDateFormat($start)).' S.D '.strtoupper($this->Utilities->indonesiaDateFormat($end)).'<br>';
        ?>
        <table>
            <thead>
                <?=$header;?>
            </thead>
            <tbody>
                <!--PENDAPATAN-->
                <?php if(!empty($codeAccountsPendapatan)):?>
                    <?=$this->Utilities->createLabaRugi($codeAccountsPendapatan);?>
                <?php endif;?>
                <!--END PENDAPATAN-->
                <!--BIAYA MATERIAL-->
                <?php if(!empty($codeAccountsBiayaMaterial)):?>
                    <?=$this->Utilities->createLabaRugi($codeAccountsBiayaMaterial);?>
                <?php endif;?>
                <!--BIAYA ADUM-->
                <?php if(!empty($codeAccountsBiayaAdum)):?>
                    <?=$this->Utilities->createLabaRugi($codeAccountsBiayaAdum);?>
                <?php endif;?>
                <tr>
                    <td style="border-top:1px solid #000;border-bottom:1px solid #000;"><b>LABA KOTOR</b></td>
                    <td style="border-top:1px solid #000;border-bottom:1px solid #000;" class="text-right"><b><?=number_format($totalPendapatan - $totalBeban,2);?></b></td>
                </tr>
                <?php if(!empty($codeAccountsBiayaAdum) || !empty($codeAccountsBiayaMaterial)):?>
                    <!-- <tr>
                        <td style="border-top:1px solid #000;border-bottom:1px solid #000;"><b>TOTAL BEBAN</b></td>
                        <td  style="border-top:1px solid #000;border-bottom:1px solid #000;" class="text-right"><b><?=number_format($totalPendapatan - $totalBeban);?></b></td>
                    </tr> -->
                <?php endif;?>
                <!--PENDAPATAN-->
                <?php if(!empty($codeAccountsPendapatanLainnya)):?>
                    <?=$this->Utilities->createLabaRugi($codeAccountsPendapatanLainnya);?>
                <?php endif;?>
                <!--END PENDAPATAN-->
                <!--PENDAPATAN-->
                <?php if(!empty($codeAccountsBiayaLainnya)):?>
                    <?=$this->Utilities->createLabaRugi($codeAccountsBiayaLainnya);?>
                <?php endif;?>
                <!--END PENDAPATAN-->
                <tr>
                    <td style="border-top:1px solid #000;border-bottom:1px solid #000;"><b>TOTAL LABA</b></td>
                    <td style="border-top:1px solid #000;border-bottom:1px solid #000;" class="text-right"><b><?=number_format($totalLaba,2);?></b></td>
                </tr>
            </tbody>
        </table>
    </body>
</html>