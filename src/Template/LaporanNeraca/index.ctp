<?php
    $this->start('sub_header_toolbar');
?>
    <?php if($this->Acl->check(['action'=>'index']) == true):?>
        <a href="<?=$this->Url->build(['action'=>'index']);?>" class="btn btn-warning">
            <i class="la la-angle-double-left"></i> Kembali
        </a>
    <?php endif;?>
<?php
    $this->end();
?>
<div class="row">
    <div class="col-md-12">
        <div class="kt-portlet">
            <div class="kt-portlet__head">
				<div class="kt-portlet__head-label">
					<h3 class="kt-portlet__head-title">
						Laporan Neraca
					</h3>
				</div>
			</div>
            <!--begin::Form-->
			<?= $this->Form->create(null,['class'=>'kt-form','type'=>'file']) ?>
				<div class="kt-portlet__body">
					<div class="kt-section kt-section--first">
                        <div class="row">
                            <div class="col-md-6 col-lg-4">
                                <?php
                                    echo $this->Form->control('periode',[
                                        'class'=>'form-control m-input',
                                        'templateVars' => [
                                            'colsize' => 'col-lg-8 col-xl-8',
                                        ],
                                        'class'=>'datepicker-range-report form-control',
                                        'label' => [
                                            'class'=> 'col-lg-4 col-xl-4 col-form-label',
                                            'text'=>'Periode *'
                                        ],
                                    ]);
                                ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-lg-4">
                                <?php
                                    echo $this->Form->control('output',[
                                        'class'=>'form-control m-input',
                                        'templateVars' => [
                                            'colsize' => 'col-lg-8 col-xl-8',
                                        ],
                                        'options' => [
                                            'webview' => 'Webview',
                                            'pdf' => 'PDF',
                                            'excel' => 'Excel'
                                        ],
                                        'class'=>' form-control',
                                        'label' => [
                                            'class'=> 'col-lg-4 col-xl-4 col-form-label',
                                            'text'=>'Output *'
                                        ],
                                    ]);
                                ?>
                            </div>
                        </div>
		            </div>
	            </div>
	            <div class="kt-portlet__foot">
					<div class="kt-form__actions">
						<div class="row">
							<div class="col-lg-9">
                                <button type="submit" class="btn btn-primary btn-submit">Submit</button>
								<button type="reset" class="btn btn-warning">Reset</button>
							</div>
						</div>
					</div>
				</div>
			<?=$this->Form->end();?>
			<!--end::Form-->
		</div>
    </div>
</div>

<?php $this->start('script');?>
    <script>
        $('.datepicker-range-report').daterangepicker({
            buttonClasses: ' btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary',
            autoApply : true,
            locale:{
                format:'YYYY-MM-DD',
                "separator": " / ",
            }
        });
        $(".kt-form").validate({
            rules: {
                periode: {
                    required: true 
                }
            },
            messages : {
                periode: {
                    required: "Pilih periode" 
                }
            },
            errorPlacement: function ( error, element ) {

                if(element.parent().hasClass('input-group')){
                    error.insertAfter( element.parent() );
                }else{
                    error.insertAfter( element );
                }

            },
            submitHandler: function (form) {
                var periode = $("#periode").val();
                var businessTypeId    = $("#business-type-id").val();
                var output    = $("#output").val();
                var explode_periode = periode.split(" / ");
                var date_1 = explode_periode[0];
                var date_2 = explode_periode[1];
                var urlTarget = "<?=$this->Url->build(['action'=>'index']);?>?print=true&date_1="+date_1+"&date_2="+date_2+"&business-type-id=&output="+output;
                window.open(urlTarget);
            }
        })
        $(".kt-form").on('submit',function(e){
            e.preventDefault();
            
        })
    </script>
<?php $this->end();?>