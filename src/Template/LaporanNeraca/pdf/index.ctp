<!DOCTYPE html>
    <head>
        <style>
            body{
                margin:0px;
                padding:0px;
                font-size:10px;
                font-family: Arial, Helvetica, sans-serif;
            }
            .text-left{
                text-align:left;
            }
            .text-center{
                text-align:center;
            }
            .text-right{
                text-align:right;
            }
            table{
                width:100%;
                margin:0px;
                padding:0px;
                border-collapse: collapse;
            }
            table td, table th{
            }
            table tbody td{
                vertical-align:top;
                padding:3px 5px;
            }
            thead { display: table-header-group; }
            tfoot { display: table-row-group; }
            tr { page-break-inside: avoid; }
            .page-break{
                page-break-after:always;
            }
            .header-optional{
                display:none;
            }
            @media print{
                .header-optional{
                    display:table-header-group;
                }
            }
        </style>
    </head>
    <body>
        <?php
            $header = '<tr><th colspan="2" class="text-left">LAPORAN NERACA<br>';
            $header .= 'PERIODE : '.strtoupper($this->Utilities->indonesiaDateFormat($start)).' S.D '.strtoupper($this->Utilities->indonesiaDateFormat($end)).'<br>';
        ?>
        <table>
            <thead>
                <?=$header;?>
            </thead>
            <tbody>
                <!--ASSET-->
                <tr>
                    <td style="" colspan="2">
                        <b>ASSET</b>
                    </td>
                </tr>
                <!--ASSET AKTIVA LANCAR-->
                <?php if(!empty($codeAccountsAssetAktivaLancar)):?>
                    <?=$this->Utilities->createLabaRugi($codeAccountsAssetAktivaLancar);?>
                <?php endif;?>
                <!--ASSET TIDAK AKTIVA LANCAR-->
                <?php if(!empty($codeAccountsAssetAktivaTidakLancar)):?>
                    <?=$this->Utilities->createLabaRugi($codeAccountsAssetAktivaTidakLancar);?>
                <?php endif;?>
                <tr>
                    <td style="border-top:1px solid #000; border-bottom:1px solid #000;color:blue;">
                        <b>TOTAL ASSET</b>
                    </td>
                    <td  class="text-right" style="border-top:1px solid #000; border-bottom:1px solid #000;color:blue;" class="text-right">
                        <b><?=number_format($totalAsset);?></b>
                    </td>
                </tr>
                <!--KEWAJIBAN-->
                <tr>
                    <td style="" colspan="2">
                        <b>KEWAJIBAN</b>
                    </td>
                </tr>
                <?php if(!empty($codeAccountsKewajiban)):?>
                    <?=$this->Utilities->createLabaRugi($codeAccountsKewajiban);?>
                <?php endif;?>
                <tr>
                    <td style="border-top:1px solid #000; border-bottom:1px solid #000;color:green;">
                        <b>TOTAL KEWAJIBAN</b>
                    </td>
                    <td  class="text-right" style="border-top:1px solid #000; border-bottom:1px solid #000;color:green;">
                        <b><?=number_format($totalKewajiban);?></b>
                    </td>
                </tr>
                <!--EKUITAS-->
                <tr>
                    <td style="" colspan="2">
                        <b>EKUITAS</b>
                    </td>
                </tr>
                <?php if(!empty($codeAccountsEkuitasModal)):?>
                    <?=$this->Utilities->createLabaRugi($codeAccountsEkuitasModal);?>
                <?php endif;?>
                <?php if(!empty($codeAccountsEkuitasLabaDitahan)):?>
                    <?=$this->Utilities->createLabaRugi($codeAccountsEkuitasLabaDitahan);?>
                <?php endif;?>
                <tr>
                    <td style="border-top:1px solid #000; border-bottom:1px solid #000;color:red;">
                        <b>TOTAL EKUITAS</b>
                    </td>
                    <td class="text-right" style="border-top:1px solid #000; border-bottom:1px solid #000;color:red;">
                        <b><?=number_format($totalEkuitas);?></b>
                    </td>
                </tr>
                <tr>
                    <td style="border-top:1px solid #000; border-bottom:1px solid #000;color:blue;">
                        <b>TOTAL EKUITAS + KEWAJIBAN</b>
                    </td>
                    <td class="text-right" style="border-top:1px solid #000; border-bottom:1px solid #000;color:blue;">
                        <b><?=number_format($totalEkuitas + $totalKewajiban);?></b>
                    </td>
                </tr>
                <?php if($totalEkuitas + $totalKewajiban != $totalAsset):?>
                    <tr>
                    <td style="border-top:1px solid #000; border-bottom:1px solid #000;color:blue;">
                        <b>TOTAL SELISIH</b>
                    </td>
                    <td class="text-right" style="border-top:1px solid #000; border-bottom:1px solid #000;color:blue;">
                        <b><?=number_format($totalAsset - ($totalEkuitas + $totalKewajiban));?></b>
                    </td>
                </tr>
                <?php endif;?>
            </tbody>
        </table>
    </body>
</html>