<html>
    <head>
        <meta charset="utf-8"/>
        <title><?=$defaultAppSettings['App.Name'];?><?=(!empty($titleModule) ? ' | '.$titleModule : '');?></title>
        <style>
            html,body{
                padding:5px;
                margin:0px;
                font-family:sans-serif;
            }
            table{
                font-size:11px;
                line-height:1.3;
                border-collapse: collapse;
                width:100%;
            }
            .table-header{
                margin-bottom:15px;
            }
            .table-main{
                border-top:4px double #000;
                border-bottom:4px double #000;
            }
            .table-main th{
                vertical-align:top;
            }
            .table-header th,.table-main th{
                text-align:left;
            }
            .company-name{
                font-size:14px;
            }
            .document-type{
                border: 2px solid #000;
                padding:5px;
                text-align:center;
            }
            .document-name{
                border-bottom: 1px solid #000;
                
            }
            .table-content{
                margin-top:10px;
                border:1px solid #000;
            }
            .table-content th,.table-content td{
                border-right:1px solid #000;
                border-bottom:1px solid #000;
            }
            .table-content td{
                vertical-align:top;
            }

            .text-center{
                text-align:center;
            }
            .text-right{
                text-align:right;
            }
            .table-signature{
                width:600px;
                margin-top:15px;
                text-align:center;
                margin-left:auto;
                margin-right:auto;
            }
        </style>
    </head>
    <body>
        <table class="table-header">
            <thead>
                <tr>
                    <th width="400px">
                        <div class="company-name"><?=$defaultAppSettings['Company.Name'];?></div>
                        <div class="company-address"><?=$defaultAppSettings['Company.Address'];?></div>
                    </th>
                    <th></th>
                    <th width="150px">
                        <div class="document-type">
                            <div class="document-name">PEMBAYARAN INVOICE</div>
                            <div class="document-code"><?=$record->code;?></div>
                        </div>
                    </th>
                </tr>
            </thead>
        </table>
        <table class="table-main">
            <tbody>
                <tr>
                    <th width="15%">TANGGAL PEMBAYARAN</th>
                    <th width="2%">:</th>
                    <th width=""><?=$record->date->format('d-m-Y');?></th>
                </tr>
                <tr>
                    <th width="15%">NO. PO</th>
                    <th width="2%">:</th>
                    <th width=""><?=$record->invoicing->code;?></th>
                </tr>
                <tr>
                    <th>CUSTOMER</th>
                    <th>:</th>
                    <th>
                        <?=$record->invoicing->customer->name;?><br>
                        <?=$record->invoicing->customer->address;?>
                    </th>
                </tr>
                <tr>
                    <th>JUMLAH</th>
                    <th>:</th>
                    <th>Rp.<?=number_format($record->payment_amount);?></th>
                </tr>
            </tbody>
        </table>
        <table class="table-signature">
            <tbody>
                <tr>
                    <td width="33.3%">
                        USER PEMBUAT<br><br><br><br>
                        (<?=$record->created_user->name;?>)
                    </td>
                    <td width="33.3%">
                        MENGETAHUI<br><br><br><br>
                        (..................................)
                    </td>
                    <td width="33.3%">
                        PENERIMA<br><br><br><br>
                        (..................................)
                    </td>
                </tr>
            </tbody>
        </table>
    </body>
</html>