<?php
    $this->start('sub_header_toolbar');
?>
    <?php if($this->Acl->check(['action'=>'index']) == true):?>
        <a href="<?=$this->Url->build(['action'=>'index']);?>" class="btn btn-warning">
            <i class="la la-angle-double-left"></i> Kembali
        </a>
    <?php endif;?>
<?php
    $this->end();
?>
<div class="row">
    <div class="col-md-12">
        <div class="kt-portlet">
            <div class="kt-portlet__head">
				<div class="kt-portlet__head-label">
					<h3 class="kt-portlet__head-title">
						<?=$titlesubModule;?>
					</h3>
				</div>
			</div>
			<!--begin::Form-->
			<?= $this->Form->create($record,['class'=>'kt-form','type'=>'file']) ?>
				<div class="kt-portlet__body">
					<div class="kt-section kt-section--first">
                        <div class="row">
                            <div class="col-md-6 col-lg-4">
                                <?=$this->Form->control('customer_id',[
                                    'class'=>'form-control m-input',
                                    'type' => 'select',
                                    'empty' => 'PILIH CUSTOMER',
                                    'autocomplete' => 'off',
                                    'templateVars' => [
                                        'colsize' => 'col-lg-8 col-md-7',
                                    ],
                                    'label' => [
                                        'class'=> 'col-lg-4 col-md-5 col-form-label',
                                        'text'=>'Customer'
                                    ],
                                ]);?>
                            </div>
                            <div class="col-md-6 col-lg-4">
                                <?=$this->Form->control('expenditure_id',[
                                    'class'=>'form-control m-input',
                                    'type' => 'select',
                                    'empty' => 'PILIH NO. PENGELUARAN',
                                    'autocomplete' => 'off',
                                    'templateVars' => [
                                        'colsize' => 'col-lg-8 col-md-7',
                                    ],
                                    'label' => [
                                        'class'=> 'col-lg-4 col-md-5 col-form-label',
                                        'text'=>'No. Pengeluaran'
                                    ],
                                ]);?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-lg-4">
                                <?=$this->Form->control('date',[
                                    'class'=>'form-control m-input date-picker',
                                    'type' => 'text',
                                    'autocomplete' => 'off',
                                    'templateVars' => [
                                        'colsize' => 'col-lg-8 col-md-7',
                                    ],
                                    'label' => [
                                        'class'=> 'col-lg-4 col-md-5 col-form-label',
                                        'text'=>'Tgl. Surat Jalan *'
                                    ],
                                ]);?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-lg-4">
                                <?=$this->Form->control('driver_name',[
                                    'class'=>'form-control m-input',
                                    'type' => 'text',
                                    'autocomplete' => 'off',
                                    'templateVars' => [
                                        'colsize' => 'col-lg-8 col-md-7',
                                    ],
                                    'label' => [
                                        'class'=> 'col-lg-4 col-md-5 col-form-label',
                                        'text'=>'Nama Supir *'
                                    ],
                                ]);?>
                            </div>
                            <div class="col-md-6 col-lg-4">
                                <?=$this->Form->control('license_plate',[
                                    'class'=>'form-control m-input',
                                    'type' => 'text',
                                    'autocomplete' => 'off',
                                    'templateVars' => [
                                        'colsize' => 'col-lg-8 col-md-7',
                                    ],
                                    'label' => [
                                        'class'=> 'col-lg-4 col-md-5 col-form-label',
                                        'text'=>'Plat Mobil *'
                                    ],
                                ]);?>
                            </div>
                        </div>
                    </div>
                    <div class="kt-section">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-sm table-bordered table-input table-input-container">
                                    <thead>
                                        <tr>
                                            <th width="50px" class="text-center">
                                                <label class="kt-checkbox kt-checkbox--tick kt-checkbox--brand">
                                                    <input type="checkbox" class="checkall">
                                                    <span></span>
                                                </label>
                                            </th>
                                            <th>NAMA BARANG</th>
                                            <th width="10%">SATUAN</th>
                                            <th width="10%">SALDO</th>
                                            <th width="10%">QTY</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach($expenditureDetails as $key=> $expenditureDetail):?>
                                            <?php 
                                                $vwStocksLocation = $expenditureDetail->picking_lists_detail->vw_stocks_location;
                                            ?>
                                            <tr>
                                                <td class="text-center">
                                                    <input name="travel_documents_details[<?=$key;?>][expenditures_picking_lists_detail_id]" type="hidden" class="" value="0">
                                                    <?php 
                                                        $checked = '';
                                                        $qty = $expenditureDetail->qty;
                                                    ?>
                                                    <?php if(!empty($expenditureDetail->travel_document_detail_id)):?>
                                                        <?php 
                                                            $checked = 'checked="checked"';
                                                            $qty = $expenditureDetail->travel_document_detail_qty;
                                                        ?>
                                                        <input name="travel_documents_details[<?=$key;?>][id]" type="hidden" class="" value="<?=$expenditureDetail->travel_document_detail_id;?>">
                                                    <?php endif;?>
                                                    <label class="kt-checkbox kt-checkbox--tick kt-checkbox--brand">
                                                        <input name="travel_documents_details[<?=$key;?>][expenditures_picking_lists_detail_id]" type="checkbox" class="checkid" value="<?=$expenditureDetail->id;?>" <?=$checked;?>>
                                                        <span></span>
                                                    </label>
                                                </td>
                                                <td>[<?=$vwStocksLocation->ProductCode;?>] <?=$vwStocksLocation->ProductName;?></td>
                                                <td><?=$vwStocksLocation->ProductUnit;?></td>
                                                <td class="text-right"><?=$expenditureDetail->saldo;?></td>
                                                <td class="text-right td-input">
                                                    <input type="text" value="<?=$qty;?>" name="travel_documents_details[<?=$key;?>][qty]" class="form-control onlyNumber"  autocomplete="off">
                                                </td>
                                            </tr>
                                        <?php endforeach;?>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
	            </div>
	            <div class="kt-portlet__foot">
					<div class="kt-form__actions">
						<div class="row">
							<div class="col-lg-12">
                                <button type="submit" class="btn btn-primary btn-submit">Submit</button>
								<button type="reset" class="btn btn-warning">Reset</button>
							</div>
						</div>
					</div>
				</div>
			<?=$this->Form->end();?>
			<!--end::Form-->
		</div>
    </div>
</div>

<?php $this->start('script');?>
    <script>
        expSelected = {};
        $("#customer-id").select2({}).on("select2:select",function(e){
            if($(this).val() != ""){
                $.ajax({
                    url : `<?=$this->Url->build(['controller'=>'Apis','action'=>'getExpendituresByCustomer']);?>/${$("#customer-id").val()}`,
                    dataType : 'json',
                    beforeSend : function(){

                    },
                    success : function(response){
                        var dataResults = response;
                        expSelected = {};
                        $("#expenditure-id").select2('destroy');
                        $("#expenditure-id").select2({
                            data : dataResults,
                            width:'100%',
                        }).on("select2:select",function(x){
                            expSelected = x.params.data;
                            if(expSelected.id != ""){
                                var html = "";
                                $.ajax({
                                    url : `<?=$this->Url->build(['controller'=>'Apis','action'=>'getExpendituresDetails']);?>/${expSelected.id}`,
                                    dataType : 'json',
                                    beforeSend : function(){

                                    },
                                    success : function(response){
                                        html = "";
                                        var countData = 0;
                                        $.each(response, function(keyEplD,epld){
                                            var vwStocksLocation = epld.picking_lists_detail.vw_stocks_location;
                                            html += `<tr>
                                                <td class="text-center">
                                                    <input name="travel_documents_details[${countData}][expenditures_picking_lists_detail_id]" type="hidden" class="checkid" value="0">
                                                    <label class="kt-checkbox kt-checkbox--tick kt-checkbox--brand">
                                                        <input name="travel_documents_details[${countData}][expenditures_picking_lists_detail_id]" type="checkbox" class="checkid" value="${epld.id}">
                                                        <span></span>
                                                    </label>
                                                </td>
                                                <td>[${vwStocksLocation.ProductCode}] ${vwStocksLocation.ProductName}</td>
                                                <td>${vwStocksLocation.ProductUnit}</td>
                                                <td class="text-right">${$.number(epld.saldo)}</td>
                                                <td class="text-right td-input">
                                                    <input type="text" value="${$.number(epld.saldo)}" name="travel_documents_details[${countData}][qty]" class="form-control onlyNumber"  autocomplete="off">
                                                </td>
                                            </tr>`;
                                            countData = countData + 1;
                                        })
                                        
                                        if(html != ""){
                                            var html = $(html);
                                            // html.find(".onlyNumber").number( true, 2 )
                                            $(".table-input tbody").html(html)
                                        }
                                    }
                                })

                            }else{
                                var html ="";
                                $(".table-input tbody").html(html)
                            }
                            
                        });
                    }
                })
            }else{
                $("#expenditure-id").select2('destroy');
                $("#expenditure-id").select2({
                    data : null,
                    width:'100%',
                })
            }
        });

        $("#expenditure-id").select2({
            width:'100%'
        }).on("select2:select",function(x){
            expSelected = x.params.data;
            if(expSelected.id != ""){
                var html = "";
                $.ajax({
                    url : `<?=$this->Url->build(['controller'=>'Apis','action'=>'getExpendituresDetails']);?>/${expSelected.id}`,
                    dataType : 'json',
                    beforeSend : function(){

                    },
                    success : function(response){
                        html = "";
                        var countData = 0;
                        $.each(response, function(keyEplD,epld){
                            var vwStocksLocation = epld.picking_lists_detail.vw_stocks_location;
                            html += `<tr>
                                <td class="text-center">
                                    <input name="travel_documents_details[${countData}][expenditures_picking_lists_detail_id]" type="hidden" class="checkid" value="0">
                                    <label class="kt-checkbox kt-checkbox--tick kt-checkbox--brand">
                                        <input name="travel_documents_details[${countData}][expenditures_picking_lists_detail_id]" type="checkbox" class="checkid" value="${epld.id}">
                                        <span></span>
                                    </label>
                                </td>
                                <td>[${vwStocksLocation.ProductCode}] ${vwStocksLocation.ProductName}</td>
                                <td>${vwStocksLocation.ProductUnit}</td>
                                <td class="text-right">${$.number(epld.saldo)}</td>
                                <td class="text-right td-input">
                                    <input type="text" value="${$.number(epld.saldo)}" name="travel_documents_details[${countData}][qty]" class="form-control onlyNumber">
                                </td>
                            </tr>`;
                            countData = countData + 1;
                        })
                        
                        if(html != ""){
                            var html = $(html);
                            // html.find(".onlyNumber").number( true, 2 )
                            $(".table-input tbody").html(html)
                        }
                    }
                })

            }else{
                var html ="";
                $(".table-input tbody").html(html)
            }
            
        });


        
        var form = $('.kt-form');
        form.submit(function (e) {
			e.preventDefault();
            var btn = $('.btn-submit');
            var rulesValidate = {
                date: {
                    required: true
                },
                customer_id: {
                    required: true
                },
                expenditure_id: {
                    required: true
                },
                driver_name: {
                    required: true
                },
                license_plate: {
                    required: true
                },
            };
            // console.log(rulesValidate);

            form.validate({
                rules: rulesValidate,
            });

			if (!form.valid()) {
				return;
			}

			btn.addClass('kt-loader kt-loader--right kt-loader--light').attr('disabled', true);
            KTApp.block($("#kt_wrapper"))

			form.ajaxSubmit({
                dataType : 'json',
                beforeSend : function(){
                },
				success: function (response, status, xhr, $form) {
                    KTApp.unblock($("#kt_wrapper"))
                    btn.removeClass('kt-loader kt-loader--right kt-loader--light').attr('disabled', false);
                    if(response.code == 200){
                        window.location.href='<?=$this->Url->build(['action'=>'index']);?>'
                    }else{
                        Utils.showAlertMsg(form, 'danger', response.message,response.errors);
                        KTUtil.scrollTo("kt_wrapper",-200)
                    }
                    
                },
                error:function(){
                    KTApp.unblock($("#kt_wrapper"))
                    btn.removeClass('kt-loader kt-loader--right kt-loader--light').attr('disabled', false);
                    Utils.showAlertMsg(form, 'danger', 'TERJADI KESALAHAN',[]);
                    KTUtil.scrollTo("kt_wrapper",-200)
                }
			});
		});
    </script>
<?php $this->end();?>