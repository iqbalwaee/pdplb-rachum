<?php
    $this->start('sub_header_toolbar');
?>
    <?php if($this->Acl->check(['action'=>'index']) == true):?>
        <a href="<?=$this->Url->build(['action'=>'index']);?>" class="btn btn-warning">
            <i class="la la-angle-double-left"></i> Kembali
        </a>
    <?php endif;?>
<?php
    $this->end();
?>
<div class="row">
    <div class="col-md-12">
        <div class="kt-portlet">
            <div class="kt-portlet__head">
				<div class="kt-portlet__head-label">
					<h3 class="kt-portlet__head-title">
						<?=$titlesubModule;?>
					</h3>
				</div>
			</div>
			<!--begin::Form-->
			<?= $this->Form->create($record,['class'=>'kt-form','type'=>'file']) ?>
				<div class="kt-portlet__body">
					<div class="kt-section kt-section--first">
                        <div class="row">
                            <div class="col-md-6 col-lg-4">
                                <?=$this->Form->control('date',[
                                    'class'=>'form-control m-input date-picker',
                                    'type' => 'text',
                                    'autocomplete' => 'off',
                                    'templateVars' => [
                                        'colsize' => 'col-lg-8 col-md-7',
                                    ],
                                    'label' => [
                                        'class'=> 'col-lg-4 col-md-5 col-form-label',
                                        'text'=>'Tgl. Invoice *'
                                    ],
                                ]);?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-lg-4">
                                <?=$this->Form->control('customer_id',[
                                    'class'=>'form-control m-input',
                                    'type' => 'select',
                                    'empty' => 'PILIH CUSTOMER',
                                    'templateVars' => [
                                        'colsize' => 'col-lg-8 col-md-7',
                                    ],
                                    'label' => [
                                        'class'=> 'col-lg-4 col-md-5 col-form-label',
                                        'text'=>'Customer *'
                                    ],
                                ]);?>
                            </div>
                            <div class="col-md-6 col-lg-4">
                                <?=$this->Form->control('total_amount',[
                                    'class'=>'form-control m-input onlyNumber',
                                    'value' => 0,
                                    'type' => 'text',
                                    'readonly' => true,
                                    'templateVars' => [
                                        'colsize' => 'col-lg-8 col-md-7',
                                    ],
                                    'label' => [
                                        'class'=> 'col-lg-4 col-md-5 col-form-label',
                                        'text'=>'TOTAL INVOICE *'
                                    ],
                                ]);?>
                            </div>
                        </div>
                    </div>
                    <div class="kt-section">
                        <h3 class="kt-section__title">2. Data Barang:</h3>
                        <div class="row">
                            <div class="col-md-12 col-lg-6">
                                <div class="input-group flex-nowrap mb-3">
                                    <?=$this->Form->select('expenditure_id',[
                                        
                                    ],
                                    [
                                        'class'=>'form-control kt-select m-input',
                                        'type' => 'select',
                                        'id' => 'expenditure-id',
                                        'empty' => 'PILIH NO. PENGELUARAN',
                                        'label' => false
                                    ]);?>
                                    <div class="input-group-apend">
                                        <a href="#" class="btn btn-brand btn-add-expenditure">
                                             Tambah
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <div class="exp-data"></div>
                            </div>
                        </div>

                    </div>
	            </div>
	            <div class="kt-portlet__foot">
					<div class="kt-form__actions">
						<div class="row">
							<div class="col-lg-12">
                                <button type="submit" class="btn btn-primary btn-submit">Submit</button>
								<button type="reset" class="btn btn-warning">Reset</button>
							</div>
						</div>
					</div>
				</div>
			<?=$this->Form->end();?>
			<!--end::Form-->
		</div>
    </div>
</div>

<?php $this->start('script');?>
    <script>
        var counter = 0;
        var arraySelect = [];
        var selectedData = undefined;
        $("#customer-id").select2({}).on("select2:select",function(e){
            if($(this).val() != ""){
                $.ajax({
                    url : `<?=$this->Url->build(['controller'=>'Apis','action'=>'getExpendituresByCustomer']);?>/${$("#customer-id").val()}`,
                    dataType : 'json',
                    beforeSend : function(){

                    },
                    success : function(response){
                        var dataResults = response;
                        selectedData = undefined;
                        $("#expenditure-id").select2('destroy');
                        $("#expenditure-id").select2({
                            data : dataResults,
                            width:'100%',
                        }).on("select2:select",function(x){
                            selectedData = x.params.data;
                            
                        });
                    }
                })
            }else{
                $("#expenditure-id").select2('destroy');
                $("#expenditure-id").select2({
                    data : [],
                    width:'100%',
                })
            }
        });

        $(".btn-add-expenditure").on("click",function(e){
            e.preventDefault();
            
            if(selectedData != undefined){
                arraySelect.push(selectedData.id);
                $.ajax({
                    url : `<?=$this->Url->build([
                        'controller'=>'Apis',
                        'action' => 'getExpendituresDetails'
                    ]);?>/${selectedData.id}`,
                    dataType : 'json',
                    success : function(response){
                        var dataDetail = response;
                        var counterDetail = 0;
                        var detailHtml = ``;
                        $.each(dataDetail,function(key,item){
                            var vwStocksLocation = item.picking_lists_detail.vw_stocks_location;
                            var hpp = vwStocksLocation.PoDetailPriceIdr;
                            detailHtml += `<tr>
                                <td>[${vwStocksLocation.ProductCode}] ${vwStocksLocation.ProductName}</td>
                                <td>${vwStocksLocation.ProductUnit}</td>
                                <td class="text-right">${$.number(item.qty)}</td>
                                <td class="text-right td-input">
                                    <input type="text" name="invoicings_expenditures[${counter}][invoicings_expenditures_details][${key}][amount]" value="0" class="form-control m-input tamount onlyNumber">
                                    <input type="text" name="invoicings_expenditures[${counter}][invoicings_expenditures_details][${key}][hpp]" value="${hpp}" class="form-control m-input thpp">
                                    <input type="hidden" name="invoicings_expenditures[${counter}][invoicings_expenditures_details][${key}][qty]" value="${item.qty}" class="tqty">
                                    <input type="hidden" name="invoicings_expenditures[${counter}][invoicings_expenditures_details][${key}][expenditures_picking_lists_detail_id]" value="${item.id}" class="form-control m-input">
                                </td>
                                <td class="text-right td-input">
                                    <input type="text" name="invoicings_expenditures[${counter}][invoicings_expenditures_details][${key}][subtotal]" value="0" class="form-control m-input tsubtotal onlyNumber" readonly=readonly style="background:#fff !important;">
                                </td>
                            </tr>`;
                        })
                        var html = `<table class="table table-sm table-bordered table-input table-input-container">
                            <thead>
                                <tr>
                                    <th colspan="4">
                                        NO. PENGELUARAN BARANG : ${selectedData.code}
                                        <input type="hidden" name="invoicings_expenditures[${counter}][expenditure_id]" value="${selectedData.id}" class="form-control m-input">
                                    </th>
                                    <td class="td-input"><a href="#" class="btn btn-block btn-danger btn-sm btn-delete-expenditure"><i class="la la-trash"></i> HAPUS</a></td>
                                </tr>
                                <tr>
                                    <th>NAMA BARANG</th>
                                    <th width="10%">SATUAN</th>
                                    <th width="10%">QTY</th>
                                    <th width="15%">HARGA</th>
                                    <th width="15%">SUBTOTAL</th>
                                </tr>
                            </thead>
                            <tbody>
                            ${detailHtml}
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th class="text-right" colspan=4>TOTAL</th>
                                    <th class="text-right total_sub">0</th>
                                </tr>
                            </tfoot>
                        </table>`;
                        var html = $(html);
                        html.find(".onlyNumber").number( true, 2 );
                        html.find(".total_sub").number( true, 2 );
                        $(".exp-data").append(html);
                        counter = counter + 1;
                    }
                })
            }
        })

        $("body").on("click",".btn-delete-expenditure",function(e){
            e.preventDefault();
            var table = $(this).closest("table");
            table.remove()
            getTotalAmount()
        })

        $("body").on("focus",".onlyNumber",function(e){
            $(this).select();
        })
        $("body").on("keyup",".tamount",function(e){
            var table = $(this).closest("table");
            var tr = $(this).closest("tr");
            var tsubtotal = tr.find(".tsubtotal");
            var qty = tr.find(".tqty").val() * 1;
            var amount = tr.find(".tamount").val() *1;
            var subtotal = amount * qty;
            tsubtotal.val(subtotal);
            var tsubtotalTable = table.find(".tsubtotal");
            var totalSub = table.find(".total_sub");
            var subtotalTable = 0;
            $.each(tsubtotalTable,function(e,i){
                var valSub = $(this).val() * 1;
                subtotalTable = subtotalTable + valSub;
            })
            totalSub.html($.number(subtotalTable));
            getTotalAmount()
        })

        function getTotalAmount(){
            var subtotalTable = 0;
            $.each($(".tsubtotal"),function(e,i){
                var valSub = $(this).val() * 1;
                subtotalTable = subtotalTable + valSub;
            })
            $("#total-amount").val(subtotalTable);
        }
        

        $("#expenditure-id").select2({
            width:'100%'
        });
        
        var form = $('.kt-form');
        form.submit(function (e) {
			e.preventDefault();
            var btn = $('.btn-submit');
            var rulesValidate = {
                purchase_order_id: {
                    required: true
                },
                bc_code: {
                    required: true
                },
                bc_date: {
                    required: true
                },
                invoice_code: {
                    required: true
                },
                invoice_date: {
                    required: true
                },
                bl_code: {
                    required: true
                },
                bl_date: {
                    required: true
                }
            };
            // console.log(rulesValidate);

            form.validate({
                rules: rulesValidate,
            });

			if (!form.valid()) {
				return;
			}

			btn.addClass('kt-loader kt-loader--right kt-loader--light').attr('disabled', true);
            KTApp.block($("#kt_wrapper"))

			form.ajaxSubmit({
                dataType : 'json',
                beforeSend : function(){
                },
				success: function (response, status, xhr, $form) {
                    KTApp.unblock($("#kt_wrapper"))
                    btn.removeClass('kt-loader kt-loader--right kt-loader--light').attr('disabled', false);
                    if(response.code == 200){
                        window.location.href='<?=$this->Url->build(['action'=>'index']);?>'
                    }else{
                        Utils.showAlertMsg(form, 'danger', response.message,response.errors);
                        KTUtil.scrollTo("kt_wrapper",-200)
                    }
                    
                },
                error:function(){
                    KTApp.unblock($("#kt_wrapper"))
                    btn.removeClass('kt-loader kt-loader--right kt-loader--light').attr('disabled', false);
                    Utils.showAlertMsg(form, 'danger', 'TERJADI KESALAHAN',[]);
                    KTUtil.scrollTo("kt_wrapper",-200)
                }
			});
		});
    </script>
<?php $this->end();?>