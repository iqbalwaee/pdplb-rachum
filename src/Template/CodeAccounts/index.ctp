<?php
    $this->start('sub_header_toolbar');
?>
<?php if($this->Acl->check(['action'=>'add']) == true):?>
    <a href="<?=$this->Url->build(['action'=>'add']);?>" class="btn btn-primary">
        <i class="la la-plus-circle"></i> Tambah Data
    </a>
<?php endif;?>
    <?php if($this->Acl->check(['action'=>'print']) == true):?>
        <a href="<?=$this->Url->build(['action'=>'print']);?>" class="btn btn-primary">
            <i class="la la-print"></i> Print
        </a>
    <?php endif;?>
<?php
    $this->end();
?>
<?=$this->element('widget/datatable');?>
<?php $this->start('script');?>
    <script>
        <?php
            $deleteUrl    = $this->Url->build(['action'=>'delete'])."/";
            if($this->Acl->check(['action'=>'delete']) == false){
                $deleteUrl = "";
            }
            $editUrl      = $this->Url->build(['action'=>'edit'])."/";
            if($this->Acl->check(['action'=>'edit']) == false){
                $editUrl = "";
            }
            $viewUrl = $this->Url->build(['action'=>'view'])."/";
            if($this->Acl->check(['action'=>'view']) == false){
                $viewUrl = "";
            }
        ?>
        jQuery(document).ready(function() {
            var deleteUrl = "<?=$deleteUrl;?>";
            var editUrl = "<?=$editUrl;?>";
            var viewUrl = "<?=$viewUrl;?>";
            var options = {
                "columnDefs": [
                    { 
                        "title": "#", 
                        "name": "#", 
                        "targets": 0,
                        "orderable" : !1,
                        "searchable" : !1,
                        "className": "text-center",
                        width: '5%',
                        data : "id",
                        render:function(id, e, t, n) {
                            var listLink = "";
                            if(deleteUrl != ""){
                                listLink += '<a class="dropdown-item btn-delete-on-table" href="'+deleteUrl+id+'"><i class="la la-delete	la-trash"></i> Delete</a>\n ';
                            }
                            if(editUrl != ""){
                                listLink += '<a class="dropdown-item" href="'+editUrl+id+'"><i class="la la-edit"></i> Edit</a>\n ';
                            }
                            if(viewUrl != ""){
                                listLink += '<a class="dropdown-item" href="'+viewUrl+id+'"><i class="la la-search-plus"></i> View</a>\n ';
                            }
                            if(listLink != ""){
                                return'\n<span class="dropdown">\n'
                                + '\n<a href="#" class="btn btn-sm btn-primary btn-icon btn-icon-md\n'
                                + '" data-toggle="dropdown"'
                                + 'aria-expanded="true">\n<i class="la la-reorder"></i>\n'               
                                +'</a>\n'
                                +'<div class="dropdown-menu">\n '+listLink+'                    </div>\n</span>\n';
                            }
                            return "-";
                        },
                        responsivePriority: -1

                    },
                    { 
                        "title": "Kode Akun", 
                        "name": "CodeAccounts.code", 
                        "targets": 1,
                        "data" : 'code',
                        "width" : '100px'
                    },
                    { 
                        "title": "Nama Akun", 
                        "name": "CodeAccounts.name", 
                        "targets": 2,
                        "data" : 'name'
                    },
                    { 
                        "title": "Status", 
                        "name": "CodeAccounts.status",
                        "className": "text-center",
                        "searchable": !1,  
                        "targets": 3,
                        "data" : 'status',
                        width:'10%',
                        render:function(status) {
                            return Utils.statusLabelActive(status);
                        }
                    },
                    { 
                        "title": "Saldo Normal", 
                        "name": "CodeAccounts.normal_balance",
                        "className": "text-center",
                        "searchable": !1,  
                        "targets": 4,
                        "data" : 'normal_balance',
                        width:'10%',
                        render:function(normal_balance) {
                            if(normal_balance == 0){
                                return '<span class="kt-badge kt-badge--success  kt-badge--inline kt-badge--pill">DEBIT</span>';
                            }else{
                                return '<span class="kt-badge kt-badge--warning  kt-badge--inline kt-badge--pill">KREDIT</span>';
                            }
                        }
                    },
                    { 
                        "title": "Laba Rugi", 
                        "name": "CodeAccounts.laba_rugi",
                        "className": "text-center",
                        "searchable": !1,  
                        "targets": 5,
                        "data" : 'laba_rugi',
                        width:'5%',
                        render:function(laba_rugi) {
                            if(laba_rugi == 1){
                                return '<span class="kt-badge kt-badge--brand  kt-badge--inline kt-badge--pill"><i class="fa fa-check"></i></span>';
                            }else{
                                return '<span class="kt-badge kt-badge--danger  kt-badge--inline kt-badge--pill"><i class="fa fa-times"></i></span>';
                            }
                        }
                    },
                    { 
                        "title": "Neraca", 
                        "name": "CodeAccounts.neraca",
                        "className": "text-center",
                        "searchable": !1,  
                        "targets": 6,
                        "data" : 'neraca',
                        width:'5%',
                        render:function(neraca) {
                            if(neraca == 1){
                                return '<span class="kt-badge kt-badge--brand  kt-badge--inline kt-badge--pill"><i class="fa fa-check"></i></span>';
                            }else{
                                return '<span class="kt-badge kt-badge--danger  kt-badge--inline kt-badge--pill"><i class="fa fa-times"></i></span>';
                            }
                        }
                    },
                    { 
                        "title": "Arus Kas", 
                        "name": "CodeAccounts.arus_kas",
                        "className": "text-center",
                        "searchable": !1,  
                        "targets": 7,
                        "data" : 'arus_kas',
                        width:'5%',
                        render:function(arus_kas) {
                            if(arus_kas == 1){
                                return '<span class="kt-badge kt-badge--brand  kt-badge--inline kt-badge--pill"><i class="fa fa-check"></i></span>';
                            }else{
                                return '<span class="kt-badge kt-badge--danger  kt-badge--inline kt-badge--pill"><i class="fa fa-times"></i></span>';
                            }
                        }
                    },
                ],
                "order": [[ 1, "ASC" ]]
            }
            DatatableRemoteAjaxDemo.init("",options,"<?=$this->request->getParam('_csrfToken');?>")
        });
    </script>
<?php $this->end();?>