<html>
    <head>
        <title><?=$defaultAppSettings['App.Name'];?><?=(!empty($titleModule) ? ' | '.$titleModule : '');?></title>
        <style>
            body,html{
                font-size:11px;
                font-family:"Arial";
            }
            table{
                width:100%;
                border-collapse: collapse;
                border : 1px solid #000;
            }
            table thead tr th{
                border-bottom:4px double #000;
                padding: 10px 5px;
            }
            .text-center{
                text-align:center;
            }
            table tbody tr td{
                border-bottom:1px solid;
            }
        </style>
    </head>
    <body>
        <table class="table-main">
            <thead>
                <tr>
                    <th>NAMA AKUN</th>
                    <th>STATUS</th>
                    <th>SALDO NORMAL</th>
                    <th>NERACA</th>
                    <th>LABA RUGI</th>
                    <th>ARUS KAS</th>
                </tr>
            </thead>
            <tbody>
                <?=$this->Utilities->generateTableCoa($results);?>
            </tbody>
        </table>
    </body>
</html>