<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\CodeAccount $codeAccount
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Code Account'), ['action' => 'edit', $codeAccount->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Code Account'), ['action' => 'delete', $codeAccount->id], ['confirm' => __('Are you sure you want to delete # {0}?', $codeAccount->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Code Accounts'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Code Account'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Parent Code Accounts'), ['controller' => 'CodeAccounts', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Parent Code Account'), ['controller' => 'CodeAccounts', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Child Code Accounts'), ['controller' => 'CodeAccounts', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Child Code Account'), ['controller' => 'CodeAccounts', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="codeAccounts view large-9 medium-8 columns content">
    <h3><?= h($codeAccount->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Parent Code Account') ?></th>
            <td><?= $codeAccount->has('parent_code_account') ? $this->Html->link($codeAccount->parent_code_account->name, ['controller' => 'CodeAccounts', 'action' => 'view', $codeAccount->parent_code_account->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Code') ?></th>
            <td><?= h($codeAccount->code) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($codeAccount->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($codeAccount->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Lft') ?></th>
            <td><?= $this->Number->format($codeAccount->lft) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Rght') ?></th>
            <td><?= $this->Number->format($codeAccount->rght) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Sort') ?></th>
            <td><?= $this->Number->format($codeAccount->sort) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created By') ?></th>
            <td><?= $this->Number->format($codeAccount->created_by) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified By') ?></th>
            <td><?= $this->Number->format($codeAccount->modified_by) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($codeAccount->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($codeAccount->modified) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Normal Balance') ?></th>
            <td><?= $codeAccount->normal_balance ? __('Yes') : __('No'); ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Status') ?></th>
            <td><?= $codeAccount->status ? __('Yes') : __('No'); ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Code Accounts') ?></h4>
        <?php if (!empty($codeAccount->child_code_accounts)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Parent Id') ?></th>
                <th scope="col"><?= __('Code') ?></th>
                <th scope="col"><?= __('Name') ?></th>
                <th scope="col"><?= __('Normal Balance') ?></th>
                <th scope="col"><?= __('Lft') ?></th>
                <th scope="col"><?= __('Rght') ?></th>
                <th scope="col"><?= __('Status') ?></th>
                <th scope="col"><?= __('Sort') ?></th>
                <th scope="col"><?= __('Created By') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified By') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($codeAccount->child_code_accounts as $childCodeAccounts): ?>
            <tr>
                <td><?= h($childCodeAccounts->id) ?></td>
                <td><?= h($childCodeAccounts->parent_id) ?></td>
                <td><?= h($childCodeAccounts->code) ?></td>
                <td><?= h($childCodeAccounts->name) ?></td>
                <td><?= h($childCodeAccounts->normal_balance) ?></td>
                <td><?= h($childCodeAccounts->lft) ?></td>
                <td><?= h($childCodeAccounts->rght) ?></td>
                <td><?= h($childCodeAccounts->status) ?></td>
                <td><?= h($childCodeAccounts->sort) ?></td>
                <td><?= h($childCodeAccounts->created_by) ?></td>
                <td><?= h($childCodeAccounts->created) ?></td>
                <td><?= h($childCodeAccounts->modified_by) ?></td>
                <td><?= h($childCodeAccounts->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'CodeAccounts', 'action' => 'view', $childCodeAccounts->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'CodeAccounts', 'action' => 'edit', $childCodeAccounts->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'CodeAccounts', 'action' => 'delete', $childCodeAccounts->id], ['confirm' => __('Are you sure you want to delete # {0}?', $childCodeAccounts->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
