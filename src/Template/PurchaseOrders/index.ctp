<?php
    $this->start('sub_header_toolbar');
?>
    <?php if($this->Acl->check(['action'=>'add']) == true):?>
        <a href="<?=$this->Url->build(['action'=>'add']);?>" class="btn btn-primary">
            <i class="la la-plus-circle"></i> Tambah Data
        </a>
    <?php endif;?>
<?php
    $this->end();
?>
<?=$this->element('widget/datatable');?>
<?php $this->start('script');?>
    <script>
        <?php
            $deleteUrl    = $this->Url->build(['action'=>'delete'])."/";
            if($this->Acl->check(['action'=>'delete']) == false){
                $deleteUrl = "";
            }
            $editUrl      = $this->Url->build(['action'=>'edit'])."/";
            if($this->Acl->check(['action'=>'edit']) == false){
                $editUrl = "";
            }
            $viewUrl = $this->Url->build(['action'=>'view'])."/";
            if($this->Acl->check(['action'=>'view']) == false){
                $viewUrl = "";
            }
        ?>
        jQuery(document).ready(function() {
            var deleteUrl = "<?=$deleteUrl;?>";
            var editUrl = "<?=$editUrl;?>";
            var viewUrl = "<?=$viewUrl;?>";
            var noCol = 0;
            var options = {
                "columnDefs": [
                    { 
                        "title": "#", 
                        "name": "#", 
                        "targets": noCol++,
                        "orderable" : !1,
                        "searchable" : !1,
                        "className": "text-center",
                        width: '5%',
                        data : "id",
                        render:function(id, e, t, n) {
                            var listLink = "";
                            if(deleteUrl != ""){
                                listLink += '<a class="dropdown-item btn-delete-on-table" href="'+deleteUrl+id+'"><i class="la la-delete	la-trash"></i> Delete</a>\n ';
                            }
                            if(editUrl != ""){
                                listLink += '<a class="dropdown-item" href="'+editUrl+id+'"><i class="la la-edit"></i> Edit</a>\n ';
                            }
                            if(viewUrl != ""){
                                listLink += '<a class="dropdown-item" target="_BLANK" href="'+viewUrl+id+'"><i class="la la-search-plus"></i> View</a>\n ';
                            }
                            if(listLink != ""){
                                return'\n<span class="dropdown">\n'
                                + '\n<a href="#" class="btn btn-sm btn-primary btn-icon btn-icon-md\n'
                                + '" data-toggle="dropdown"'
                                + 'aria-expanded="true">\n<i class="la la-reorder"></i>\n'               
                                +'</a>\n'
                                +'<div class="dropdown-menu">\n '+listLink+'                    </div>\n</span>\n';
                            }
                            return "-";
                        },
                        responsivePriority: -1

                    },
                    { 
                        "title": "NO. PO", 
                        "name": "PurchaseOrders.code", 
                        "targets": noCol++,
                        "data" : 'code'
                    },
                    { 
                        "title": "TGL. PO", 
                        "name": "PurchaseOrders.date", 
                        "targets": noCol++,
                        "data" : 'date',
                        render : function(date){
                            return Utils.dateIndonesia(date,false,false)
                        }
                    },
                    { 
                        "title": "SUPLIER", 
                        "name": "Suppliers.name", 
                        "targets": noCol++,
                        "data" : 'supplier.name',
                    },
                    { 
                        "title": "TOTAL", 
                        "name": "PurchaseOrders.total_amount",
                        "searchable": !1,  
                        "targets": noCol++,
                        "data" : 'total_amount',
                        width:'10%',
                        render:function(total_amount) {
                            return `<div class="text-right">${$.number(total_amount)}</div>`;
                        }
                    },
                    { 
                        "title": "Status", 
                        "name": "PurchaseOrders.status",
                        "className": "text-center",
                        "searchable": !1,  
                        "targets": noCol++,
                        "data" : 'status',
                        width:'15%',
                        render:function(status) {
                            if(status == 0){
                                return `<div class="kt-badge kt-badge--danger  kt-badge--inline kt-badge--pill">BELUM LUNAS</div>`
                            }else if(status == 1){
                                return `<div class="kt-badge kt-badge--info  kt-badge--inline kt-badge--pill">DALAM PEMBAYARAN</div>`
                            }else{
                                return `<div class="kt-badge kt-badge--primary  kt-badge--inline kt-badge--pill">LUNAS</div>`
                            }
                        }
                    },
                    { 
                        "title": "TGL. DIBUAT", 
                        "name": "PurchaseOrders.created",
                        "searchable": !1,  
                        "targets": noCol++,
                        "data" : 'created',
                        width:'15%',
                        render:function(created) {
                            return Utils.dateIndonesia(created,true,true)
                        }
                    },
                ],
                "order": [[ 1, "DESC" ]]
            }
            DatatableRemoteAjaxDemo.init("",options,"<?=$this->request->getParam('_csrfToken');?>")
        });
    </script>
<?php $this->end();?>