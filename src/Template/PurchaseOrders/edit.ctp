<?php
    $this->start('sub_header_toolbar');
?>
    <?php if($this->Acl->check(['action'=>'index']) == true):?>
        <a href="<?=$this->Url->build(['action'=>'index']);?>" class="btn btn-warning">
            <i class="la la-angle-double-left"></i> Kembali
        </a>
    <?php endif;?>
<?php
    $this->end();
?>
<div class="row">
    <div class="col-md-12">
        <div class="kt-portlet">
            <div class="kt-portlet__head">
				<div class="kt-portlet__head-label">
					<h3 class="kt-portlet__head-title">
						<?=$titlesubModule;?>
					</h3>
				</div>
			</div>
			<!--begin::Form-->
            <?= $this->Form->create($record,['class'=>'kt-form','type'=>'file']) ?>
                <?=$this->Form->hidden('deleted_po_detail_id',['id'=>'deleted_po_detail_id']);?>
				<div class="kt-portlet__body">
					<div class="kt-section kt-section--first">
                        <div class="row">
                            <div class="col-md-6">
                                <?=$this->Form->control('date',[
                                    'class'=>'form-control m-input date-picker',
                                    'type' => 'text',
                                    'autocomplete' => 'off',
                                    'templateVars' => [
                                        'colsize' => 'col-lg-8 col-md-7',
                                    ],
                                    'label' => [
                                        'class'=> 'col-lg-4 col-md-5 col-form-label',
                                        'text'=>'TGL. PO *'
                                    ],
                                ]);?>
                            </div>
                            <div class="col-md-6">
                                <?=$this->Form->control('supplier_id',[
                                    'class'=>'form-control m-input',
                                    'empty' => 'PILIH SUPLIER',
                                    'templateVars' => [
                                        'colsize' => 'col-lg-8 col-md-7',
                                    ],
                                    'label' => [
                                        'class'=> 'col-lg-4 col-md-5 col-form-label',
                                        'text'=>'SUPLIER *'
                                    ],
                                ]);?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <?=$this->Form->control('currency_id',[
                                    'class'=>'form-control m-input',
                                    'empty' => 'PILIH MATA UANG',
                                    'templateVars' => [
                                        'colsize' => 'col-lg-8 col-md-7',
                                    ],
                                    'label' => [
                                        'class'=> 'col-lg-4 col-md-5 col-form-label',
                                        'text'=>'MATA UANG *'
                                    ],
                                ]);?>
                            </div>
                            <div class="col-md-6">
                                <?=$this->Form->control('kurs',[
                                    'class'=>'form-control m-input onlyNumber',
                                    'type' => 'text',
                                    'autocomplete' => 'off',
                                    'readonly' => true,
                                    'templateVars' => [
                                        'colsize' => 'col-lg-8 col-md-7',
                                    ],
                                    'label' => [
                                        'class'=> 'col-lg-4 col-md-5 col-form-label',
                                        'text'=>'KURS *'
                                    ],
                                ]);?>
                            </div>
                        </div>
		            </div>
                    <div class="kt-section kt-section--last">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table class="table table-sm table-bordered table-input">
                                        <thead>
                                            <tr>
                                                <th>NAMA BARANG</th>
                                                <th width="10%">SATUAN</th>
                                                <th width="10%">QTY</th>
                                                <th width="15%">HARGA</th>
                                                <th width="15%">SUBTOTAL</th>
                                                <th width="10%" class="text-center">#</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $count = count($record->purchase_orders_details);?>
                                            <?php foreach($record->purchase_orders_details as $key => $purchaseOrdersDetails):?>
                                                <tr class="tr-input">
                                                    <td class="td-input">
                                                        <?=$this->Form->hidden('purchase_orders_details.'.$key.'.id',[
                                                            'class'=>'form-control m-input id_po_detail',
                                                            'data-name' => 'id_po_detail',
                                                        ]);?>
                                                        <?=$this->Form->select('purchase_orders_details.'.$key.'.product_id',[],[
                                                            'class'=>'form-control m-input select2-products',
                                                            'empty' => [
                                                                $purchaseOrdersDetails->product_id => "[".$purchaseOrdersDetails->product->code."] ".$purchaseOrdersDetails->product->name
                                                            ],
                                                            'style' => 'width:100%',
                                                            'label' => false,
                                                            'data-name' => 'product_id',
                                                            'required' => true
                                                        ]);?>
                                                    </td>
                                                    <td class="td-input">
                                                        <?=$this->Form->text('purchase_orders_details'.$key.'0.unit',[
                                                            'class'=>'form-control m-input unittag',
                                                            'label' => false,
                                                            'data-name' => 'unit',
                                                            'value' => $purchaseOrdersDetails->product->unit,
                                                            'readonly' => true
                                                        ]);?>
                                                    </td>
                                                    <td class="td-input">
                                                        <?=$this->Form->text('purchase_orders_details'.$key.'0.qty',[
                                                            'class'=>'form-control m-input qtytag onlyNumber',
                                                            'label' => false,
                                                            'data-name' => 'qty',
                                                            'value' => $purchaseOrdersDetails->qty,
                                                        ]);?>
                                                    </td>
                                                    <td class="td-input">
                                                        <?=$this->Form->text('purchase_orders_details'.$key.'0.price',[
                                                            'class'=>'form-control m-input pricetag onlyNumber',
                                                            'label' => false,
                                                            'data-name' => 'price',
                                                            'value' => $purchaseOrdersDetails->price,
                                                        ]);?>
                                                    </td>
                                                    <td class="td-input">
                                                        <?=$this->Form->text('purchase_orders_details'.$key.'0.subtotal',[
                                                            'class'=>'form-control m-input subtotaltag onlyNumber',
                                                            'label' => false,
                                                            'data-name' => 'subtotal',
                                                            'readonly' => true,
                                                            'value' => $purchaseOrdersDetails->subtotal,
                                                        ]);?>
                                                    </td>
                                                    <td class="text-center">
                                                        <?php if($key == 1 && $count == 1):
                                                        ?>
                                                        <a href="#" class="btn btn-brand btn-sm btn-icon btn-add-item">
                                                            <i class="la la-plus"></i>
                                                        </a>
                                                        <?php elseif($key == $count - 1):?>
                                                        <a href="#" class="btn btn-brand btn-sm btn-icon btn-add-item">
                                                            <i class="la la-plus"></i>
                                                        </a>
                                                        <a href="#" class="btn btn-danger btn-sm btn-icon btn-remove-item">
                                                            <i class="la la-trash"></i>
                                                        </a>
                                                        <?php else:?>
                                                            <a href="#" class="btn btn-danger btn-sm btn-icon btn-remove-item">
                                                            <i class="la la-trash"></i>
                                                            </a>
                                                        <?php endif;?>
                                                    </td>
                                                </tr>
                                            <?php endforeach;?>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th class="text-right" colspan="4">TOTAL</th>
                                                <th class="td-input">
                                                    <?=$this->Form->text('total_amount',[
                                                        'class'=>'form-control m-input onlyNumber total_amount',
                                                        'label' => false,
                                                        'readonly' => false,
                                                    ]);?>
                                                </th>
                                                <th></th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
	            </div>
	            <div class="kt-portlet__foot">
					<div class="kt-form__actions">
						<div class="row">
							<div class="col-lg-12">
                                <button type="submit" class="btn btn-primary btn-submit">Submit</button>
								<button type="reset" class="btn btn-warning">Reset</button>
							</div>
						</div>
					</div>
				</div>
			<?=$this->Form->end();?>
			<!--end::Form-->
		</div>
    </div>
</div>

<?php $this->start('script');?>
    <script>
        var counter = <?=$count-1;?>;
        var currenciesJson = <?=json_encode($currenciesKurs);?>;
        $("#supplier-id").select2({});
        $("#currency-id").select2({}).on("select2:select",function(e){
            $("#kurs").val(currenciesJson[$(this).val()]);
        });
        
        function selectProductsInit(el){
            el.select2({
                ajax:{
                    url:'<?= $this->url->build(['controller'=>'Apis','action'=>'getProducts'])?>',
                    dataType:'json',
                    data: function(params){
                        var query = {
                            search: params.term,
                            type:'public',
                        }
                        return query;
                    }
                }
            }).on("select2:select",function(res){
                var data = res.params.data;
                $(this).closest("tr").find(".unittag").val(data.unit);
                $(this).closest("tr").find(".qtytag").focus();
            });
        }

        function totalAmount(){
            var subtotalEl = $(".subtotaltag");
            var subtotal = 0;
            $.each(subtotalEl,function(e){
                subtotal = subtotal + ($(this).val() * 1);
            })
            $(".total_amount").val(subtotal);
        }

        selectProductsInit($(".select2-products"))
        
        $("body").on("focus",".pricetag,.qtytag",function(){
            $(this).select();
        })
        var sumSubTotal = 0;
        $("body").on("keyup",".qtytag,.pricetag",function(e){
            if(e.keyCode == 13){
                return false;
            }
            var tr          = $(this).closest("tr");
            var qty         = tr.find(".qtytag").val() * 1;
            var price       = tr.find(".pricetag").val() * 1;
            var subtotal    = qty * price;
            tr.find(".subtotaltag").val(subtotal);
            totalAmount();
        })

        $("body").on("click",".btn-add-item",function(e){
            e.preventDefault();
            var btn = $(this);
            var tr  = btn.closest("tr");
            var btnRemove = tr.find(".btn-remove-item");
            if(btnRemove.length == 0){
                btn.attr("class","btn btn-danger btn-sm btn-icon btn-remove-item").html('<i class="la la-trash"></i>');
            }else{
                btn.remove();
            }
            
            counter = counter +1;
            var newRow = `<tr class="tr-input">
                            <td class="td-input">
                                <select name="purchase_orders_details[${counter}][product_id]" class="form-control m-input select2-products" style="width:100%;" data-name="product_id" required="true" ><option value="">PILIH BARANG</option></select>
                            </td>
                            <td class="td-input">
                                <input type="text" name="purchase_orders_details[${counter}][unit]" class="form-control m-input unittag" data-name="unit" value="">
                            </td>
                            <td class="td-input">
                                <input type="text" name="purchase_orders_details[${counter}][qty]" class="form-control m-input qtytag onlyNumber" data-name="qty" value="0">
                            </td>
                            <td class="td-input">
                                <input type="text" name="purchase_orders_details[${counter}][price]" class="form-control m-input pricetag onlyNumber" data-name="price" value="0">
                            </td>
                            <td class="td-input">
                                <input type="text" name="purchase_orders_details[${counter}][subtotal]" class="form-control m-input subtotaltag onlyNumber" data-name="subtotal" readonly="readonly" value="0">
                            </td>
                            <td class="text-center">
                                <a href="#" class="btn btn-brand btn-sm btn-icon btn-add-item">
                                    <i class="la la-plus"></i>
                                </a>
                                <a href="#" class="btn btn-danger btn-sm btn-icon btn-remove-item">
                                    <i class="la la-trash"></i>
                                </a>
                            </td>
                        </tr>`;
            var newRow = $(newRow);
            var select2ProductEl = newRow.find(".select2-products");
            var onlyNumberInput = newRow.find(".onlyNumber");
            onlyNumberInput.number( true, 2 );
            selectProductsInit(select2ProductEl)

            $(".table-input tbody").append(newRow);
            
        })

        $("body").on("click",".btn-remove-item",function(e){
            e.preventDefault();
            var tr = $(this).closest("tr");
            var poDetailId = tr.find(".id_po_detail");
            if(poDetailId != undefined){
                var deletedPoDetailId = $("#deleted_po_detail_id").val();
                if(deletedPoDetailId == ""){
                    $("#deleted_po_detail_id").val(poDetailId.val())
                }else{
                    $("#deleted_po_detail_id").val(`${$("#deleted_po_detail_id").val()};${poDetailId.val()}`)
                }
            }
            if(tr.is(":last-child")){
                var prevTr = tr.prev();
                var lastTd = prevTr.find('td:last-child');
                if(prevTr.is(":first-child")){
                    lastTd.html(`<a href="#" class="btn btn-brand btn-sm btn-icon btn-add-item">
                                    <i class="la la-plus"></i>
                                </a>`);
                }else{
                    lastTd.html(`
                                <a href="#" class="btn btn-brand btn-sm btn-icon btn-add-item">
                                    <i class="la la-plus"></i>
                                </a>
                                <a href="#" class="btn btn-danger btn-sm btn-icon btn-remove-item">
                                    <i class="la la-trash"></i>
                                </a>`);
                }

                tr.remove();
                
            }else if(tr.is(":first-child")){
                var nextTr = tr.next();
                var lastTd = nextTr.find('td:last-child');
                if(nextTr.is(":last-child")){
                    lastTd.html(`<a href="#" class="btn btn-brand btn-sm btn-icon btn-add-item">
                                    <i class="la la-plus"></i>
                                </a>`);
                }
                tr.remove();
            }else{
                tr.remove();
            }
        })
        var form = $('.kt-form');
        

        form.submit(function (e) {
			e.preventDefault();
            var btn = $('.btn-submit');
            var rulesValidate = {
                date: {
                    required: true
                },
                supplier_id: {
                    required: true
                },
                currency_id: {
                    required: true
                },
                kurs: {
                    required: true
                }
            };
            // console.log(rulesValidate);

            form.validate({
                rules: rulesValidate,
            });

			if (!form.valid()) {
				return;
			}

			btn.addClass('kt-loader kt-loader--right kt-loader--light').attr('disabled', true);
            KTApp.block($("#kt_wrapper"))

			form.ajaxSubmit({
                dataType : 'json',
                beforeSend : function(){
                },
				success: function (response, status, xhr, $form) {
                    KTApp.unblock($("#kt_wrapper"))
                    btn.removeClass('kt-loader kt-loader--right kt-loader--light').attr('disabled', false);
                    if(response.code == 200){
                        window.location.href='<?=$this->Url->build(['action'=>'index']);?>'
                    }else{
                        Utils.showAlertMsg(form, 'danger', response.message,response.errors);
                        KTUtil.scrollTo("kt_wrapper",-200)
                    }
                    
				}
			});
		});
    </script>
<?php $this->end();?>