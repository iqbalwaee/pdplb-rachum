<?php
    $this->start('sub_header_toolbar');
?>
    <?php if($this->Acl->check(['action'=>'index']) == true):?>
        <a href="<?=$this->Url->build(['action'=>'index']);?>" class="btn btn-warning">
            <i class="la la-angle-double-left"></i> Kembali
        </a>
    <?php endif;?>
<?php
    $this->end();
?>
<div class="row">
    <div class="col-md-12">
        <div class="kt-portlet">
            <div class="kt-portlet__head">
				<div class="kt-portlet__head-label">
					<h3 class="kt-portlet__head-title">
						<?=$titlesubModule;?>
					</h3>
				</div>
			</div>
			<!--begin::Form-->
			<?= $this->Form->create($record,['class'=>'kt-form','type'=>'file']) ?>
				<div class="kt-portlet__body">
					<div class="kt-section kt-section--first">
                        <div class="row">
                            <div class="col-md-6">
                                <?=$this->Form->control('date',[
                                    'class'=>'form-control m-input date-picker',
                                    'type' => 'text',
                                    'autocomplete' => 'off',
                                    'templateVars' => [
                                        'colsize' => 'col-lg-8 col-md-7',
                                    ],
                                    'label' => [
                                        'class'=> 'col-lg-4 col-md-5 col-form-label',
                                        'text'=>'TGL. PEMBAYARAN *'
                                    ],
                                ]);?>
                            </div>
                            <div class="col-md-6">
                                <?=$this->Form->control('purchase_order_id',[
                                    'class'=>'form-control m-input',
                                    'empty' => 'PILIH PO',
                                    'templateVars' => [
                                        'colsize' => 'col-lg-8 col-md-7',
                                    ],
                                    'label' => [
                                        'class'=> 'col-lg-4 col-md-5 col-form-label',
                                        'text'=>'PURCHASE ORDER *'
                                    ],
                                ]);?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <?=$this->Form->control('total_hutang',[
                                    'class'=>'form-control m-input onlyNumber',
                                    'type' => 'text',
                                    'autocomplete' => 'off',
                                    'readonly' => true,
                                    'templateVars' => [
                                        'colsize' => 'col-lg-8 col-md-7',
                                    ],
                                    'label' => [
                                        'class'=> 'col-lg-4 col-md-5 col-form-label',
                                        'text'=>'TOTAL HUTANG IDR'
                                    ],
                                ]);?>
                            </div>
                            <div class="col-md-6">
                                <?=$this->Form->control('saldo_hutang',[
                                    'class'=>'form-control m-input onlyNumber',
                                    'type' => 'text',
                                    'autocomplete' => 'off',
                                    'readonly' => true,
                                    'templateVars' => [
                                        'colsize' => 'col-lg-8 col-md-7',
                                    ],
                                    'label' => [
                                        'class'=> 'col-lg-4 col-md-5 col-form-label',
                                        'text'=>'SALDO HUTANG'
                                    ],
                                ]);?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <?=$this->Form->control('payment_amount_idr',[
                                    'class'=>'form-control m-input onlyNumber',
                                    'type' => 'text',
                                    'autocomplete' => 'off',
                                    'templateVars' => [
                                        'colsize' => 'col-lg-8 col-md-7',
                                    ],
                                    'label' => [
                                        'class'=> 'col-lg-4 col-md-5 col-form-label',
                                        'text'=>'NOMINAL PEMBAYARAN'
                                    ],
                                ]);?>
                            </div>
                        </div>
                    </div>
	            </div>
	            <div class="kt-portlet__foot">
					<div class="kt-form__actions">
						<div class="row">
							<div class="col-lg-12">
                                <button type="submit" class="btn btn-primary btn-submit">Submit</button>
								<button type="reset" class="btn btn-warning">Reset</button>
							</div>
						</div>
					</div>
				</div>
			<?=$this->Form->end();?>
			<!--end::Form-->
		</div>
    </div>
</div>

<?php $this->start('script');?>
    <script>
        $("#purchase-order-id").select2({}).on("select2:select",function(e){
            if($(this).val() != ""){
                $.ajax({
                    url : `<?=$this->Url->build(['controller'=>'Apis','action'=>'getPurchaseOrder']);?>/${$("#purchase-order-id").val()}`,
                    dataType : 'json',
                    beforeSend : function(){

                    },
                    success : function(response){
                        var data = response.result;
                        $("#total-hutang").val(data.total_amount_idr);
                        $("#saldo-hutang").val(data.saldo_hutang);
                    }
                })
            }
            
        });
        var form = $('.kt-form');

        form.submit(function (e) {
			e.preventDefault();
            var btn = $('.btn-submit');
            var rulesValidate = {
                date: {
                    required: true
                },
                purchase_order_id: {
                    required: true
                },
                payment_amount_idr: {
                    required: true
                }
            };
            // console.log(rulesValidate);

            form.validate({
                rules: rulesValidate,
            });

			if (!form.valid()) {
				return;
			}

			btn.addClass('kt-loader kt-loader--right kt-loader--light').attr('disabled', true);
            KTApp.block($("#kt_wrapper"))

			form.ajaxSubmit({
                dataType : 'json',
                beforeSend : function(){
                },
				success: function (response, status, xhr, $form) {
                    KTApp.unblock($("#kt_wrapper"))
                    btn.removeClass('kt-loader kt-loader--right kt-loader--light').attr('disabled', false);
                    if(response.code == 200){
                        window.location.href='<?=$this->Url->build(['action'=>'index']);?>'
                    }else{
                        console.log(response)
                        Utils.showAlertMsg(form, 'danger', response.message,response.errors);
                        KTUtil.scrollTo("kt_wrapper",-200)
                    }
                    
				}
			});
		});
    </script>
<?php $this->end();?>