<html>
    <head>
        <meta charset="utf-8"/>
        <title><?=$defaultAppSettings['App.Name'];?><?=(!empty($titleModule) ? ' | '.$titleModule : '');?></title>
        <style>
            html,body{
                padding:5px;
                margin:0px;
                font-family:sans-serif;
            }
            table{
                font-size:11px;
                line-height:1.3;
                border-collapse: collapse;
                width:100%;
            }
            .table-header{
                margin-bottom:15px;
            }
            .table-main{
                border-top:4px double #000;
                border-bottom:4px double #000;
            }
            .table-main th{
                vertical-align:top;
            }
            .table-header th,.table-main th{
                text-align:left;
            }
            .company-name{
                font-size:14px;
            }
            .document-type{
                border: 2px solid #000;
                padding:5px;
                text-align:center;
            }
            .document-name{
                border-bottom: 1px solid #000;
                
            }
            .table-content{
                margin-top:10px;
                border:1px solid #000;
            }
            .table-content th,.table-content td{
                border-right:1px solid #000;
                border-bottom:1px solid #000;
            }
            .table-content td{
                vertical-align:top;
            }

            .text-center{
                text-align:center;
            }
            .text-right{
                text-align:right;
            }
            .table-signature{
                width:400px;
                margin-top:15px;
                text-align:center;
            }
        </style>
    </head>
    <body>
        <table class="table-header">
            <thead>
                <tr>
                    <th width="400px">
                        <div class="company-name"><?=$defaultAppSettings['Company.Name'];?></div>
                        <div class="company-address"><?=$defaultAppSettings['Company.Address'];?></div>
                    </th>
                    <th></th>
                    <th width="150px">
                        <div class="document-type">
                            <div class="document-name">PUT AWAY</div>
                            <div class="document-code"><?=$record->code;?></div>
                        </div>
                    </th>
                </tr>
            </thead>
        </table>
        <table class="table-main">
            <tbody>
                <tr>
                    <th width="15%">TANGGAL PUT AWAY</th>
                    <th width="2%">:</th>
                    <th width=""><?=$record->date->format('d-m-Y');?></th>
                </tr>
                <tr>
                    <th>LOKASI PENYIMPANAN</th>
                    <th>:</th>
                    <th>
                        <?=$record->location->name;?>
                    </th>
                </tr>
            </tbody>
        </table>
        <table class="table-content">
            <thead>
                <tr>
                    <th width="15%">NO. PENGAJUAN</th>
                    <th width="15%">NO. PENERIMAAN</th>
                    <th>NAMA BARANG</th>
                    <th width="5%">SATUAN</th>
                    <th width="5%">QTY</th>
                </tr>
            </thead>
            <tbody>
                <?php $totalQty = 0;?>
                <?php foreach($record->vw_stocks_locations as $key => $vwStocks):?>
                    <tr>
                        <td><?=$vwStocks->RBCCode;?></td>
                        <td><?=$vwStocks->RCode;?></td>
                        <td><?="[".$vwStocks->ProductCode."] ".$vwStocks->ProductName;?></td>
                        <td class="text-center"><?=$vwStocks->ProductUnit;?></td>
                        <td class="text-right"><?=number_format($vwStocks->StocksLocationQtyStock);?></td>
                    </tr>
                <?php endforeach;?>
            </tbody>
        </table>
        <table class="table-signature">
            <tbody>
                <tr>
                    <td width="50%">
                        USER PEMBUAT<br><br><br><br>
                        (<?=$record->created_user->name;?>)
                    </td>
                    <td width="50%">
                        MENGETAHUI<br><br><br><br>
                        (..................................)
                    </td>
                </tr>
            </tbody>
        </table>
    </body>
</html>