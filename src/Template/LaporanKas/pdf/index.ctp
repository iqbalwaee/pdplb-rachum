<!DOCTYPE html>
    <head>
        <style>
            body{
                margin:0px;
                padding:0px;
                font-size:11px;
                font-family: Arial, Helvetica, sans-serif;
            }
            .text-left{
                text-align:left;
            }
            .text-center{
                text-align:center;
            }
            .text-right{
                text-align:right;
            }
            table{
                width:100%;
                margin:0px;
                padding:0px;
                border:1px solid #000;
                border-collapse: collapse;
            }
            table td, table th{
                border-bottom :1px solid #000;
                border-left :1px solid #000;
            }
            table thead th{
                padding:5px;
                height:20px;
            }
            table tbody td{
                vertical-align:top;
                padding:5px;
                height:40px;
            }
            table tbody tr.summary td{
                vertical-align:middle;
                padding:5px;
                height:20px !important;
                font-weight:bold;
            }
            thead { display: table-header-group; }
            tfoot { display: table-row-group; }
            tr { page-break-inside: avoid; }
            .page-break{
                page-break-after:always;
            }
            .header-optional{
                display:none;
            }
            @media print{
                .header-optional{
                    display:table-header-group;
                }
            }
        </style>
    </head>
    <body>
        <?php
            $header = '<tr><th colspan="8">LAPORAN ARUS KAS<br>';
            $header .= 'PERIODE : '.strtoupper($this->Utilities->indonesiaDateFormat($start)).' S.D '.strtoupper($this->Utilities->indonesiaDateFormat($end)).'<br>';
            if(!empty($businessType)):
                $header .= 'JENIS USAHA : '.$this->Utilities->businessTypeVal($businessType->type).' - '.$businessType->name.'</th></tr>';
            else:
                $header .= 'SEMUA JENIS USAHA</th></tr>';
            endif;
            $header .= '<tr>
                <th >No.</th>
                <th>No. Jurnal</th>
                <th>Tanggal</th>
                <th>Kode Akun</th>
                <th>Nama Akun</th>
                <th>Remark</th>
                <th>Debit</th>
                <th>Kredit</th>
            </tr>';
        ?>
        <table>
            <thead>
                <?=$header;?>
            </thead>
            <tbody>
                <?php $totalDebit = 0; $totalKredit = 0;?>
                <?php foreach($results as $key => $result):?>
                    <?php $totalDebit += $result->debit; $totalKredit += $result->credit;?>
                        <tr>
                            <td width="2%"><?=$key+1;?></td>
                            <td width="5%" class="text-center"><?=$result->journal->code;?></td>
                            <td width="10%" class="text-center"><?=$result->journal->date->format('d-m-Y');?></td>
                            <td width="10%"><?=$result->code_account->code;?></td>
                            <td width="15%"><?=$result->code_account->name;?></td>
                            <td><?=$result->journal->description ;?></td>
                            <td width="10%" class="text-right"><?=number_format($result->debit);?></td>
                            <td width="10%" class="text-right"><?=number_format($result->credit);?></td>
                        </tr>
                <?php endforeach;?>
                <tr class="summary">
                    <td colspan="6" style="background:#ddd;" class="text-right">&nbsp;</td>
                    <td width="10%" style="background:#ddd;" class="text-right"><?=number_format($totalDebit);?></td>
                    <td width="10%" style="background:#ddd;" class="text-right"><?=number_format($totalKredit);?></td>
                </tr>
            </tbody>
        </table>
    </body>
</html>