<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Routing\Router;
use Cake\Core\Configure;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
/**
 * Laporan Jurnal Controller
 *
 * @property \App\Model\Table\JournalsTable $Journals
 *
 * @method \App\Model\Entity\BusinessType[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class LaporanJurnalController extends AppController
{

    private $titleModule = "Laporan Jurnal";
    private $primaryModel = "Journals";

    public function initialize()
    {
        parent::initialize();
        $this->set([
            'titleModule' => $this->titleModule,
            'primaryModel' => $this->primaryModel,
        ]);
    }

    function beforeFilter(\Cake\Event\Event $event){
        parent::beforeFilter($event);
    
        if(isset($this->Security) && $this->request->isAjax() && ($this->action = 'index' || $this->action = 'delete')){
    
            $this->Security->config('validatePost',false);
            //$this->getEventManager()->off($this->Csrf);
    
        }
    
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->loadModel('Journals');
        // JIKA FILTER QUERY ADA
        if(!empty($this->request->getQuery())){
            $query              = $this->request->getQuery();
            $start              = $query['date_1'];
            $end                = $query['date_2'];
            $output             = $query['output'];
            $results            = $this->Journals->find()
                                    ->contain([
                                        'JournalsDetails',
                                        'JournalsDetails.CodeAccounts'
                                    ])
                                    ->where([
                                        '`Journals`.`date` BETWEEN :start AND :end'
                                    ])
                                    ->bind(':start', $start, 'date')
                                    ->bind(':end',   $end, 'date');

            $this->set(compact('results','start','end'));
            $this->viewBuilder()->setLayout(false);
            if($output == "pdf"){
                Configure::write('CakePdf', [
                    'engine' => [
                        'className' => 'CakePdf.WkHtmlToPdf',
                        'binary' => env("WKHTML_PATH","C:\\wkhtmltopdf\\bin\\wkhtmltopdf.exe"),
                        'options' => [
                            'print-media-type' => false,
                            'orientation' => 'landscape',
                            'outline' => true,
                            'dpi' => 96
                        ],
                    ],
                ]);
                $this->viewBuilder()->setClassName('CakePdf.Pdf');
            }elseif($output == "excel"){
                $this->autoRender = false;
                $spreadsheet = new Spreadsheet();
                $sheet = $spreadsheet->getActiveSheet();
                $sheet->getColumnDimension('A')->setAutoSize(true);
                $sheet->getColumnDimension('B')->setAutoSize(true);
                $sheet->getColumnDimension('C')->setAutoSize(true);
                $sheet->getColumnDimension('D')->setAutoSize(true);
                $sheet->getColumnDimension('E')->setAutoSize(true);
                $sheet->getColumnDimension('F')->setAutoSize(true);
                $sheet->getColumnDimension('G')->setAutoSize(true);
                $sheet->getColumnDimension('H')->setAutoSize(true);

                $sheet->mergeCells('A1:I1');
                $sheet->mergeCells('A2:I2');

                $sheet->setCellValue('A1', 'LAPORAN JURNAL');
                $sheet->setCellValue('A2', 'PERIODE : '.strtoupper($this->Utilities->indonesiaDateFormat($start)).' S.D '.strtoupper($this->Utilities->indonesiaDateFormat($end)));
                

                $sheet->getStyle('A1:I3')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $sheet->getStyle('A1:I3')->getFont()->setBold(true);

                $styleArray = [
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            'color' => ['argb' => '333333'],
                        ],
                    ],
                ];

                $start = 4;

                $sheet->setCellValue('A' . $start, 'No.');
                $sheet->setCellValue('B' . $start, 'No. Jurnal');
                $sheet->setCellValue('C' . $start, 'Tanggal');
                $sheet->setCellValue('D' . $start, 'Kode Akun');
                $sheet->setCellValue('E' . $start, 'Nama Akun');
                $sheet->setCellValue('F' . $start, 'Remark');
                $sheet->setCellValue('G' . $start, 'Debit');
                $sheet->setCellValue('H' . $start, 'Kredit');

                $sheet->getStyle('A'.$start)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $sheet->getStyle('B'.$start)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $sheet->getStyle('C'.$start)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $sheet->getStyle('D'.$start)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $sheet->getStyle('E'.$start)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $sheet->getStyle('F'.$start)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $sheet->getStyle('G'.$start)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $sheet->getStyle('H'.$start)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                
                $rrow = $start + 1;
                $row = 1;
                $oldData = -1;
                $batas = 16;
                foreach($results as $key => $result){
                    $rowspan = count($result->journals_details);
                    $totalDebit=0;
                    $totalKredit=0;

                    foreach($result->journals_details as $jd_key => $result_detail){
                        if($oldData != $key){
                            $sheet->setCellValue('A' . $rrow, $key + 1);
                            $sheet->setCellValue('B' . $rrow, $result->code);
                            $sheet->setCellValue('C' . $rrow, $result->date->format('d-m-Y'));
                            $sheet->setCellValue('D' . $rrow, $result_detail->code_account->code);
                            $sheet->setCellValue('E' . $rrow, $result_detail->code_account->name);
                            $sheet->setCellValue('F' . $rrow, $result->description);
                            $sheet->setCellValue('G' . $rrow, number_format($result_detail->debit));
                            $sheet->setCellValue('H' . $rrow, number_format($result_detail->credit));
                            $sheet->mergeCells('A'.$rrow.':A'.($rrow + $rowspan));
                            $sheet->mergeCells('B'.$rrow.':B'.($rrow + $rowspan));
                            $sheet->mergeCells('C'.$rrow.':C'.($rrow + $rowspan));
                            $sheet->mergeCells('F'.$rrow.':F'.($rrow + $rowspan));
                        }else{
                            $sheet->setCellValue('D' . $rrow, $result_detail->code_account->code);
                            $sheet->setCellValue('E' . $rrow, $result_detail->code_account->name);
                            $sheet->setCellValue('G' . $rrow, number_format($result_detail->debit));
                            $sheet->setCellValue('H' . $rrow, number_format($result_detail->credit));
                        }
                        $sheet->getStyle('A'.$rrow)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                        $sheet->getStyle('B'.$rrow)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                        $sheet->getStyle('C'.$rrow)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                        $sheet->getStyle('D'.$rrow)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                        $sheet->getStyle('F'.$rrow.':G'.$rrow)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);

                        $row++;
                        $totalDebit += $result_detail->debit;
                        $totalKredit += $result_detail->credit;
                        $rrow++;
                        $oldData = $key;
                    }
                   
                    $sheet->setCellValue('G' . $rrow, number_format($totalDebit));
                    $sheet->setCellValue('H' . $rrow, number_format($totalKredit));

                    $sheet->getStyle('G'.$rrow.':H'.$rrow)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
                    
                    $rrow++;
                }
                $sheet->getStyle('A1'.':A'.($rrow - 1))->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP);
                $sheet->getStyle('B1'.':B'.($rrow - 1))->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP);
                $sheet->getStyle('C1'.':C'.($rrow - 1))->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP);
                $sheet->getStyle('D1'.':D'.($rrow - 1))->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP);
                $sheet->getStyle('F1'.':G'.($rrow - 1))->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP);
                $sheet->getStyle('A' . $start . ':H' . --$rrow)->applyFromArray($styleArray);

                $writer = new Xlsx($spreadsheet);
                header('Content-Type: application/vnd.ms-excel');
                header('Content-Disposition: attachment;filename="LAPORAN_BUKU_BESAR-' . time() . '.xlsx"');
                header('Cache-Control: max-age=0');
                $writer->save('php://output');
                exit();
            }else{
                $this->render('./pdf/index');
            }
        }
            
        
    }

}
