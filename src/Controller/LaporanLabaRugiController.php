<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Routing\Router;
use Cake\Core\Configure;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
/**
 * Laporan Laba Rugi Controller
 *
 * @property \App\Model\Table\JournalsTable $Journals
 *
 * @method \App\Model\Entity\BusinessType[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class LaporanLabaRugiController extends AppController
{

    private $titleModule = "Laporan Laba Rugi";
    private $primaryModel = "Journals";

    public function initialize()
    {
        parent::initialize();
        $this->set([
            'titleModule' => $this->titleModule,
            'primaryModel' => $this->primaryModel,
        ]);
        $this->Auth->allow();
    }

    function beforeFilter(\Cake\Event\Event $event){
        parent::beforeFilter($event);
    
        if(isset($this->Security) && $this->request->isAjax() && ($this->action = 'index' || $this->action = 'delete')){
    
            $this->Security->config('validatePost',false);
            //$this->getEventManager()->off($this->Csrf);
    
        }
    
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->loadModel('Journals');
        $this->loadModel('CodeAccounts');
        // JIKA FILTER QUERY ADA
        if(!empty($this->request->getQuery())){
            $query              = $this->request->getQuery();
            $start              = $query['date_1'];
            $end                = $query['date_2'];
            $output             = $query['output'];
            // JIKA KONDISI PER PERUSAHAAN//
            $addWhere = "";
            $codeAccountsPendapatan                 = $this->CodeAccounts->find()
                                            ->where([
                                                'CodeAccounts.laba_rugi' => 1,
                                                'CodeAccounts.code LIKE "4%"'
                                            ])
                                            ->order([
                                                'CodeAccounts.code ASC'
                                            ])
                                            ->select($this->CodeAccounts)
                                            ->select([
                                                'amount' => '(
                                                    CASE
                                                        WHEN ((CodeAccounts.lft - CodeAccounts.rght) = -1) THEN
                                                            (
                                                                SELECT 
                                                                    (
                                                                        CASE WHEN (`CodeAccounts`.`normal_balance` = 0) THEN
                                                                            IFNULL(SUM(journals_details.debit - journals_details.credit),0)
                                                                        ELSE 
                                                                            IFNULL(SUM(journals_details.credit - journals_details.debit),0)
                                                                        END
                                                                    ) as AMT 
                                                                FROM journals_details
                                                                INNER JOIN journals on journals.id = journals_details.journal_id
                                                                WHERE 
                                                                journals_details.code_account_id = CodeAccounts.id
                                                                AND
                                                                (
                                                                    journals.date BETWEEN "'.$start.'" AND "'.$end.'"
                                                                    OR
                                                                    journals.date < "'.$start.'"
                                                                )'.
                                                                $addWhere
                                                                .'
                                                            )
                                                        ELSE
                                                            (
                                                                SELECT 
                                                                (
                                                                    CASE WHEN (`CDA`.`normal_balance` = 0) THEN
                                                                        IFNULL(SUM(journals_details.debit - journals_details.credit),0)
                                                                    ELSE 
                                                                        IFNULL(SUM(journals_details.credit - journals_details.debit),0)
                                                                    END
                                                                ) as AMT
                                                                FROM journals_details
                                                                INNER JOIN
                                                                journals on journals.id = journals_details.journal_id
                                                                INNER JOIN
                                                                code_accounts as CDA on CDA.id = journals_details.code_account_id 
                                                                WHERE
                                                                `CDA`.`lft` > `CodeAccounts`.`lft` AND CDA.rght < `CodeAccounts`.`rght`
                                                                AND (CDA.lft - CDA.rght) = -1
                                                                AND
                                                                (
                                                                    journals.date BETWEEN "'.$start.'" AND "'.$end.'"
                                                                    OR
                                                                    journals.date < "'.$start.'"
                                                                )'.
                                                                $addWhere
                                                                .'
                                                            )
                                                    END
                                                )'
                                            ]);

            
            $codeAccountsBiayaMaterial              = $this->CodeAccounts->find()
                                                ->where([
                                                    'CodeAccounts.laba_rugi' => 1,
                                                    '(CodeAccounts.lft - CodeAccounts.rght) != -1',
                                                    'CodeAccounts.code LIKE "5%"'
                                                ])
                                                ->order([
                                                    'CodeAccounts.code ASC'
                                                ])
                                                ->select($this->CodeAccounts)
                                                ->select([
                                                    'amount' => '(
                                                        CASE
                                                            WHEN ((CodeAccounts.lft - CodeAccounts.rght) = -1) THEN
                                                                (
                                                                    SELECT 
                                                                        (
                                                                            CASE WHEN (`CodeAccounts`.`normal_balance` = 0) THEN
                                                                                IFNULL(SUM(journals_details.debit - journals_details.credit),0)
                                                                            ELSE 
                                                                                IFNULL(SUM(journals_details.credit - journals_details.debit),0)
                                                                            END
                                                                        ) as AMT 
                                                                    FROM journals_details
                                                                    INNER JOIN journals on journals.id = journals_details.journal_id
                                                                    WHERE 
                                                                    journals_details.code_account_id = CodeAccounts.id
                                                                    AND
                                                                    (
                                                                        journals.date BETWEEN "'.$start.'" AND "'.$end.'"
                                                                        OR
                                                                        journals.date < "'.$start.'"
                                                                    )'.
                                                                    $addWhere
                                                                    .'
                                                                )
                                                            ELSE
                                                                (
                                                                    SELECT 
                                                                    (
                                                                        CASE WHEN (`CDA`.`normal_balance` = 0) THEN
                                                                            IFNULL(SUM(journals_details.debit - journals_details.credit),0)
                                                                        ELSE 
                                                                            IFNULL(SUM(journals_details.credit - journals_details.debit),0)
                                                                        END
                                                                    ) as AMT
                                                                    FROM journals_details
                                                                    INNER JOIN
                                                                    journals on journals.id = journals_details.journal_id
                                                                    INNER JOIN
                                                                    code_accounts as CDA on CDA.id = journals_details.code_account_id 
                                                                    WHERE
                                                                    `CDA`.`lft` > `CodeAccounts`.`lft` AND CDA.rght < `CodeAccounts`.`rght`
                                                                    AND (CDA.lft - CDA.rght) = -1
                                                                    AND
                                                                    (
                                                                        journals.date BETWEEN "'.$start.'" AND "'.$end.'"
                                                                        OR
                                                                        journals.date < "'.$start.'"
                                                                    )'.
                                                                    $addWhere
                                                                    .'
                                                                )
                                                        END
                                                    )'
                                                ]);

            
            $codeAccountsBiayaAdum                  = $this->CodeAccounts->find()
                                            ->where([
                                                'CodeAccounts.laba_rugi' => 1,
                                                '(CodeAccounts.lft - CodeAccounts.rght) != -1',
                                                'CodeAccounts.code LIKE "6%"'
                                            ])
                                            ->order([
                                                'CodeAccounts.code ASC'
                                            ])
                                            ->select($this->CodeAccounts)
                                            ->select([
                                                'amount' => '(
                                                    CASE
                                                        WHEN ((CodeAccounts.lft - CodeAccounts.rght) = -1) THEN
                                                            (
                                                                SELECT 
                                                                    (
                                                                        CASE WHEN (`CodeAccounts`.`normal_balance` = 0) THEN
                                                                            IFNULL(SUM(journals_details.debit - journals_details.credit),0)
                                                                        ELSE 
                                                                            IFNULL(SUM(journals_details.credit - journals_details.debit),0)
                                                                        END
                                                                    ) as AMT 
                                                                FROM journals_details
                                                                INNER JOIN journals on journals.id = journals_details.journal_id
                                                                WHERE 
                                                                journals_details.code_account_id = CodeAccounts.id
                                                                AND
                                                                (
                                                                    journals.date BETWEEN "'.$start.'" AND "'.$end.'"
                                                                    OR
                                                                    journals.date < "'.$start.'"
                                                                )'.
                                                                $addWhere
                                                                .'
                                                            )
                                                        ELSE
                                                            (
                                                                SELECT 
                                                                (
                                                                    CASE WHEN (`CDA`.`normal_balance` = 0) THEN
                                                                        IFNULL(SUM(journals_details.debit - journals_details.credit),0)
                                                                    ELSE 
                                                                        IFNULL(SUM(journals_details.credit - journals_details.debit),0)
                                                                    END
                                                                ) as AMT
                                                                FROM journals_details
                                                                INNER JOIN
                                                                journals on journals.id = journals_details.journal_id
                                                                INNER JOIN
                                                                code_accounts as CDA on CDA.id = journals_details.code_account_id 
                                                                WHERE
                                                                `CDA`.`lft` > `CodeAccounts`.`lft` AND CDA.rght < `CodeAccounts`.`rght`
                                                                AND (CDA.lft - CDA.rght) = -1
                                                                AND
                                                                (
                                                                    journals.date BETWEEN "'.$start.'" AND "'.$end.'"
                                                                    OR
                                                                    journals.date < "'.$start.'"
                                                                )'.
                                                                $addWhere
                                                                .'
                                                            )
                                                    END
                                                )'
                                            ]);
            
            $codeAccountsPendapatanLainnya          = $this->CodeAccounts->find()
                                    ->where([
                                        'CodeAccounts.laba_rugi' => 1,
                                        // '(CodeAccounts.lft - CodeAccounts.rght) != -1',
                                        'CodeAccounts.code LIKE "7%"'
                                    ])
                                    ->order([
                                        'CodeAccounts.code ASC'
                                    ])
                                    ->select($this->CodeAccounts)
                                    ->select([
                                        'amount' => '(
                                            CASE
                                                WHEN ((CodeAccounts.lft - CodeAccounts.rght) = -1) THEN
                                                    (
                                                        SELECT 
                                                            (
                                                                CASE WHEN (`CodeAccounts`.`normal_balance` = 0) THEN
                                                                    IFNULL(SUM(journals_details.debit - journals_details.credit),0)
                                                                ELSE 
                                                                    IFNULL(SUM(journals_details.credit - journals_details.debit),0)
                                                                END
                                                            ) as AMT 
                                                        FROM journals_details
                                                        INNER JOIN journals on journals.id = journals_details.journal_id
                                                        WHERE 
                                                        journals_details.code_account_id = CodeAccounts.id
                                                        AND
                                                        (
                                                            journals.date BETWEEN "'.$start.'" AND "'.$end.'"
                                                            OR
                                                            journals.date < "'.$start.'"
                                                        )'.
                                                        $addWhere
                                                        .'
                                                    )
                                                ELSE
                                                    (
                                                        SELECT 
                                                        (
                                                            CASE WHEN (`CDA`.`normal_balance` = 0) THEN
                                                                IFNULL(SUM(journals_details.debit - journals_details.credit),0)
                                                            ELSE 
                                                                IFNULL(SUM(journals_details.credit - journals_details.debit),0)
                                                            END
                                                        ) as AMT
                                                        FROM journals_details
                                                        INNER JOIN
                                                        journals on journals.id = journals_details.journal_id
                                                        INNER JOIN
                                                        code_accounts as CDA on CDA.id = journals_details.code_account_id 
                                                        WHERE
                                                        `CDA`.`lft` > `CodeAccounts`.`lft` AND CDA.rght < `CodeAccounts`.`rght`
                                                        AND (CDA.lft - CDA.rght) = -1
                                                        AND
                                                        (
                                                            journals.date BETWEEN "'.$start.'" AND "'.$end.'"
                                                            OR
                                                            journals.date < "'.$start.'"
                                                        )'.
                                                        $addWhere
                                                        .'
                                                    )
                                            END
                                        )'
                                    ]);
           
            $codeAccountsBiayaLainnya               = $this->CodeAccounts->find()
                                    ->where([
                                        'CodeAccounts.laba_rugi' => 1,
                                        // '(CodeAccounts.lft - CodeAccounts.rght) != -1',
                                        'CodeAccounts.code LIKE "8%"'
                                    ])
                                    ->order([
                                        'CodeAccounts.code ASC'
                                    ])
                                    ->select($this->CodeAccounts)
                                    ->select([
                                        'amount' => '(
                                            CASE
                                                WHEN ((CodeAccounts.lft - CodeAccounts.rght) = -1) THEN
                                                    (
                                                        SELECT 
                                                            (
                                                                CASE WHEN (`CodeAccounts`.`normal_balance` = 0) THEN
                                                                    IFNULL(SUM(journals_details.debit - journals_details.credit),0)
                                                                ELSE 
                                                                    IFNULL(SUM(journals_details.credit - journals_details.debit),0)
                                                                END
                                                            ) as AMT 
                                                        FROM journals_details
                                                        INNER JOIN journals on journals.id = journals_details.journal_id
                                                        WHERE 
                                                        journals_details.code_account_id = CodeAccounts.id
                                                        AND
                                                        (
                                                            journals.date BETWEEN "'.$start.'" AND "'.$end.'"
                                                            OR
                                                            journals.date < "'.$start.'"
                                                        )'.
                                                        $addWhere
                                                        .'
                                                    )
                                                ELSE
                                                    (
                                                        SELECT 
                                                        (
                                                            CASE WHEN (`CDA`.`normal_balance` = 0) THEN
                                                                IFNULL(SUM(journals_details.debit - journals_details.credit),0)
                                                            ELSE 
                                                                IFNULL(SUM(journals_details.credit - journals_details.debit),0)
                                                            END
                                                        ) as AMT
                                                        FROM journals_details
                                                        INNER JOIN
                                                        journals on journals.id = journals_details.journal_id
                                                        INNER JOIN
                                                        code_accounts as CDA on CDA.id = journals_details.code_account_id 
                                                        WHERE
                                                        `CDA`.`lft` > `CodeAccounts`.`lft` AND CDA.rght < `CodeAccounts`.`rght`
                                                        AND (CDA.lft - CDA.rght) = -1
                                                        AND
                                                        (
                                                            journals.date BETWEEN "'.$start.'" AND "'.$end.'"
                                                            OR
                                                            journals.date < "'.$start.'"
                                                        )'.
                                                        $addWhere
                                                        .'
                                                    )
                                            END
                                        )'
                                    ]);
            
            //PENDAPATAN//
            $codeAccountsPendapatan = $codeAccountsPendapatan->find('threaded')->toArray();
            $totalPendapatan = 0;
            if(!empty($codeAccountsPendapatan)){
                $totalPendapatan = $codeAccountsPendapatan[0]->amount;
            }                  
            //END PENDAPATAN//
            
            //BIAYA MATERIAL//
            $codeAccountsBiayaMaterial = $codeAccountsBiayaMaterial->find('threaded')->toArray();
            $totalBiayaMaterial = 0;
            if(!empty($codeAccountsBiayaMaterial)){
                $totalBiayaMaterial = $codeAccountsBiayaMaterial[0]->amount;
            }
            //END BIAYA MATERIAL//


            //BIAYA ADUM//
            $codeAccountsBiayaAdum = $codeAccountsBiayaAdum->find('threaded')->toArray();

            $totalBiayaAdum = 0;
            if(!empty($codeAccountsBiayaAdum)){
                $totalBiayaAdum = $codeAccountsBiayaAdum[0]->amount;
            }
            $totalBeban = $totalBiayaAdum + $totalBiayaMaterial;
            //END BIAYA ADUM
            
            //PENDAPATAN LAINNYA//
            $codeAccountsPendapatanLainnya = $codeAccountsPendapatanLainnya->find('threaded')->toArray();
            $totalPendapatanLainnya = 0;
            if(!empty($codeAccountsPendapatanLainnya)){
                $totalPendapatanLainnya = $codeAccountsPendapatanLainnya[0]->amount;
            }
            //END PENDAPATAN LAINNYA//

            //BIAYA LAINNYA//
            $codeAccountsBiayaLainnya = $codeAccountsBiayaLainnya->find('threaded')->toArray();      
            $totalBiayaLainnya = 0;
            if(!empty($codeAccountsBiayaLainnya)){
                $totalBiayaLainnya = $codeAccountsBiayaLainnya[0]->amount;
            }
            //BIAYA LAINNYA//

            $totalLaba = ($totalPendapatan + $totalPendapatanLainnya) - ($totalBeban + $totalBiayaLainnya);
            $this->set(compact(
                'start',
                'end',
                'businessType',
                'codeAccountsPendapatan',
                'totalPendapatan',
                'codeAccountsBiayaMaterial',
                'totalBiayaMaterial',
                'codeAccountsBiayaAdum',
                'totalBiayaAdum',
                'totalBeban',
                'codeAccountsPendapatanLainnya',
                'totalPendapatanLainnya',
                'codeAccountsBiayaLainnya',
                'totalBiayaLainnya',
                'totalLaba'
            ));
            $this->viewBuilder()->setLayout(false);
            if($output == "pdf"){
                    Configure::write('CakePdf', [
                        'engine' => [
                            'className' => 'CakePdf.WkHtmlToPdf',
                            'binary' => env("WKHTML_PATH","C:\\wkhtmltopdf\\bin\\wkhtmltopdf.exe"),
                            'options' => [
                                'print-media-type' => false,
                                'orientation' => 'potrait',
                                'outline' => true,
                                'dpi' => 96
                            ],
                        ],
                    ]);
                    $this->viewBuilder()->setClassName('CakePdf.Pdf');
            }elseif($output == "excel"){
                $this->autoRender = false;
                $spreadsheet = new Spreadsheet();
                $sheet = $spreadsheet->getActiveSheet();
                $sheet->getColumnDimension('A')->setAutoSize(true);
                $sheet->getColumnDimension('B')->setAutoSize(true);

                $sheet->mergeCells('A1:B1');
                $sheet->mergeCells('A2:B2');
                $sheet->mergeCells('A3:B3');

                $sheet->setCellValue('A1', 'LAPORAN LABA RUGI');
                $sheet->setCellValue('A2', 'PERIODE : '.strtoupper($this->Utilities->indonesiaDateFormat($start)).' S.D '.strtoupper($this->Utilities->indonesiaDateFormat($end)));

                // $sheet->getStyle('A1:B3')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $sheet->getStyle('A1:B3')->getFont()->setBold(true);

                $styleArray = [
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            'color' => ['argb' => '333333'],
                        ]
                    ],
                ];

                $row = 4;
                // START PENDAPATAN
                if(!empty($codeAccountsPendapatan)){
                    $row = $this->Utilities->createLabaRugiExcel($codeAccountsPendapatan, $sheet, $row);
                }
                // END PENDAPATAN
                // START BIAYA MATERIAL
                if(!empty($codeAccountsBiayaMaterial)){
                    $row = $this->Utilities->createLabaRugiExcel($codeAccountsBiayaMaterial, $sheet, $row);
                }// BIAYA ADUM
                if(!empty($codeAccountsBiayaAdum)){
                    $row = $this->Utilities->createLabaRugiExcel($codeAccountsBiayaAdum, $sheet, $row);
                }
                $sheet->getStyle('B'.$row)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
                $sheet->getStyle('A'.$row.':B'.$row)->applyFromArray($styleArray);
                $sheet->setCellValue('A'.$row, 'LABA KOTOR');
                $sheet->setCellValue('B'.$row++, number_format($totalPendapatan - $totalBeban));
                // END BIAYA MATERIAL
                
                // PENDAPATAN
                if(!empty($codeAccountsPendapatanLainnya)){
                    $row = $this->Utilities->createLabaRugiExcel($codeAccountsPendapatanLainnya, $sheet, $row);
                }
                // END PENDAPATAN
                // PENDAPATAN
                if(!empty($codeAccountsBiayaLainnya)){
                    $row = $this->Utilities->createLabaRugiExcel($codeAccountsBiayaLainnya, $sheet, $row);
                }
                // END PENDAPATAN
                $sheet->getStyle('B'.$row)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
                $sheet->getStyle('A'.$row.':B'.$row)->applyFromArray($styleArray);
                $sheet->setCellValue('A'.$row, 'TOTAL LABA');
                $sheet->setCellValue('B'.$row++, number_format($totalLaba));

                $writer = new Xlsx($spreadsheet);
                header('Content-Type: application/vnd.ms-excel');
                header('Content-Disposition: attachment;filename="LAPORAN_LABA_RUGI-' . time() . '.xlsx"');
                header('Cache-Control: max-age=0');
                $writer->save('php://output');
                exit();
            }else{
                $this->render('./pdf/index');
            }
            
        }else{
            
        }
           
    }

}
