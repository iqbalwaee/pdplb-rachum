<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Routing\Router;

/**
 * APIS Controller
 */
class ApisController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow();
    }
    
    public function getProducts(){
        $search = "";
        if($this->request->query('search')){
            $search = $this->request->query('search');
        }
        $this->loadModel('Products');
        $results = $this->Products->find()
        ->where([
            'OR' =>[
                'Products.name LIKE' => $search.'%',
                'Products.code LIKE' => $search.'%'
            ]
        ])
        ->order([
            'Products.name' => 'ASC'
        ]);
        foreach($results as $key => $product){
            $product->text = "[".$product->code."] ".$product->name; 
        }
        $this->set(compact('results'));
        $this->set('_serialize',['results']);
    }

    public function getPurchaseOrder($id = null){
        if($id != null){
            $this->loadModel('PurchaseOrders');
            $result = $this->PurchaseOrders->find()
            ->select($this->PurchaseOrders)
            ->select([
                'total_dibayar' => $this->PurchaseOrders->PurchaseOrdersPayments->find()->select([
                    'saldo'=>'(IFNULL(SUM(`payment_amount_idr`),0))'
                ])->where([
                    '`purchase_order_id`' => $id
                ])
            ])
            ->where([
                '`PurchaseOrders`.`id`' => $id
            ])
            ->first();
            $result->saldo_hutang = $result->total_amount_idr - $result->total_dibayar;
        }else{
            $result = [];
        }
        $this->set(compact('result'));
        $this->set('_serialize',['result']);
    }

    public function getPurchaseOrderFull($id = null){
        if($id != null){
            $this->loadModel('PurchaseOrders');
            $result = $this->PurchaseOrders->get($id,[
                'contain' => [
                    'PurchaseOrdersDetails',
                    'PurchaseOrdersDetails.Products',
                    'Suppliers',
                    'Currencies'
                ]
            ]);
        }else{
            $result = [];
        }
        $this->set(compact('result'));
        $this->set('_serialize',['result']);
    }

    public function getAccounts(){
        $this->loadModel('BusinessTypes');
        $this->loadModel('CodeAccounts');
        $search = "";
        if(!empty($this->request->query('search'))){
            $search = $this->request->query('search');
        }
        // $results = $this->BusinessTypes->get($business_type_id);
        $results = $this->CodeAccounts->find()
            ->where([
                'OR' => [
                    'name LIKE' => $search.'%',
                    'code LIKE' => $search.'%',
                ]
            ])
            ->where([
                '(`CodeAccounts`.`rght` - `CodeAccounts`.`lft`) = 1',
            ])
            ->select([
                'id' => '`CodeAccounts`.`id`',
                'text' => 'CONCAT(`CodeAccounts`.`code`," - ",`CodeAccounts`.`name`)',
            ])
            ->order('`CodeAccounts`.`code` ASC')
            ->limit(200);
            

        $this->set('results',$results);
        $this->set('_serialize','results');
        $this->RequestHandler->renderAs($this, 'json');
    }

    public function getPickingListsByCustomer($customerId){
        $this->loadModel('PickingLists');
        $results = $this->PickingLists->find()
            ->contain(['PickingListsDetails.VwStocksLocations'])
            ->select($this->PickingLists)
            ->select([
                'text' => 'PickingLists.code',
                'value' => 'PickingLists.id',
            ])
            ->where([
                'PickingLists.customer_id' => $customerId,
                'PickingLists.status' => 0,
            ])
            ->order('`PickingLists`.`date` ASC');

        $this->set('results',$results);
        $this->set('_serialize','results');
        $this->RequestHandler->renderAs($this, 'json');
    }

    public function getExpendituresByCustomer($customerId){
        $this->loadModel('Expenditures');
        $this->loadModel('TravelDocumentsDetails');
        $results = $this->Expenditures->find()
            ->select($this->Expenditures)
            ->select([
                'text' => 'Expenditures.code',
                // 'value' => 'Expenditures.id',
            ])
            ->where([
                'Expenditures.customer_id' => $customerId,
            ])
            ->order('`Expenditures`.`date` ASC');

        $this->set('results',$results);
        $this->set('_serialize','results');
        $this->RequestHandler->renderAs($this, 'json');
    }

    public function getExpendituresDetails($expenditure_id){
        $this->loadModel('ExpendituresPickingListsDetails');
        $this->loadModel('TravelDocumentsDetails');
        $results = $this->ExpendituresPickingListsDetails->find()
            ->contain([
                'ExpendituresPickingLists',
                'ExpendituresPickingLists.Expenditures',
                'PickingListsDetails',
                'PickingListsDetails.VwStocksLocations'
            ])
            ->select($this->ExpendituresPickingListsDetails)
            ->select($this->ExpendituresPickingListsDetails->PickingListsDetails)
            ->select($this->ExpendituresPickingListsDetails->PickingListsDetails->VwStocksLocations)
            ->select([
                'saldo' => '(`ExpendituresPickingListsDetails`.`qty`) - (
                    '.
                    $this->TravelDocumentsDetails->find()
                    ->select([
                        'SUMARY' => '(IFNULL(SUM(`TravelDocumentsDetails`.`qty`),0))'
                    ])
                    ->where([
                        '`TravelDocumentsDetails`.expenditures_picking_lists_detail_id = `ExpendituresPickingListsDetails`.`id`'
                    ])
                    .'
                )'
            ])
            ->where([
                'ExpendituresPickingLists.expenditure_id' => $expenditure_id,
            ])
            ->having([
                'saldo > 0'
            ])
            ->order([
                '`Expenditures`.`date` ASC',
                '`VwStocksLocations`.`ProductName`' => 'ASC',
            ]); 

        $this->set('results',$results);
        $this->set('_serialize','results');
        $this->RequestHandler->renderAs($this, 'json');
    }

    public function getInvoicing($id = null){
        if($id != null){
            $this->loadModel('Invoicings');
            $result = $this->Invoicings->find()
            ->select($this->Invoicings)
            ->select([
                'total_dibayar' => $this->Invoicings->InvoicingsPayments->find()->select([
                    'saldo'=>'(IFNULL(SUM(`payment_amount`),0))'
                ])->where([
                    '`invoicing_id`' => $id
                ])
            ])
            ->where([
                '`Invoicings`.`id`' => $id
            ])
            ->first();
            $result->saldo_piutang = $result->total_amount - $result->total_dibayar;
        }else{
            $result = [];
        }
        $this->set(compact('result'));
        $this->set('_serialize',['result']);
    }



}
