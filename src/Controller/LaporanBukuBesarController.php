<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Routing\Router;
use Cake\Core\Configure;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
/**
 * Laporan Buku Besar Controller
 *
 * @property \App\Model\Table\JournalsTable $Journals
 *
 * @method \App\Model\Entity\BusinessType[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class LaporanBukuBesarController extends AppController
{

    private $titleModule = "Laporan BukuBesar";
    private $primaryModel = "Journals";

    public function initialize()
    {
        parent::initialize();
        $this->set([
            'titleModule' => $this->titleModule,
            'primaryModel' => $this->primaryModel,
        ]);
        $this->Auth->allow();
    }

    function beforeFilter(\Cake\Event\Event $event){
        parent::beforeFilter($event);
    
        if(isset($this->Security) && $this->request->isAjax() && ($this->action = 'index' || $this->action = 'delete')){
    
            $this->Security->config('validatePost',false);
            //$this->getEventManager()->off($this->Csrf);
    
        }
    
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->loadModel('CodeAccounts');
        $this->loadModel('JournalsDetails');
        $this->loadModel('BusinessTypes');
        // JIKA FILTER QUERY ADA
        if(!empty($this->request->getQuery())){
            $query              = $this->request->getQuery();
            $codeAccountId      = $query['code-account-id'];
            $start              = $query['date_1'];
            $end                = $query['date_2'];
            $output             = $query['output'];
            $codeAccounts       = $this->CodeAccounts->find()->where([
                '(`CodeAccounts`.`lft` - `CodeAccounts`.`rght`) = -1'
            ]);
            if(!empty($codeAccountId)){
                $codeAccounts->where([
                    'CodeAccounts.id IN' => explode(",",$codeAccountId)
                ]);
            }
            $whereAdd = "";
            $codeAccounts       = $codeAccounts->select([
                'id' => 'CodeAccounts.id',
                'code' => 'CodeAccounts.code',
                'name' => 'CodeAccounts.name',
                'normal_balance' => 'CodeAccounts.normal_balance',
                'saldoAwal' => '(
                    CASE 
                        WHEN CodeAccounts.normal_balance = 0 THEN
                            (
                                SELECT IFNULL(SUM(journals_details.debit - journals_details.credit),0) as total FROM journals_details
                                INNER JOIN journals ON journals.id = journals_details.journal_id 
                                WHERE 
                                DATE(journals.date) < "'.$start.'" 
                                AND
                                journals_details.code_account_id = CodeAccounts.id '.$whereAdd.'
                            )
                        ELSE
                            (
                                SELECT IFNULL(SUM(journals_details.credit - journals_details.debit),0) as total FROM journals_details
                                INNER JOIN journals ON journals.id = journals_details.journal_id 
                                WHERE
                                DATE(journals.date) <  "'.$start.'" 
                                AND
                                journals_details.code_account_id = CodeAccounts.id '.$whereAdd.'
                            )
                    END
                )'
            ])->order([
                'CodeAccounts.code ASC'
            ])->toArray();
            foreach($codeAccounts as $key => $codeAccount){
                $results            = $this->JournalsDetails->find()
                                    ->contain([
                                        'Journals',
                                    ])
                                    ->where([
                                        '`Journals`.`date` BETWEEN :start AND :end',
                                        '`JournalsDetails`.`code_account_id`' => $codeAccount->id,
                                    ])
                                    ->order([
                                        '`Journals`.`date`' => 'ASC'
                                    ])
                                    ->bind(':start', $start, 'date')
                                    ->bind(':end',   $end, 'date');

                $codeAccount->journals = $results;
            }
            
            
            
            $this->set(compact('codeAccounts','start','end','businessType'));
            $this->viewBuilder()->setLayout(false);
            if($output == "pdf"){
                    Configure::write('CakePdf', [
                        'engine' => [
                            'className' => 'CakePdf.WkHtmlToPdf',
                            'binary' => env("WKHTML_PATH","C:\\wkhtmltopdf\\bin\\wkhtmltopdf.exe"),
                            'options' => [
                                'print-media-type' => false,
                                'orientation' => 'landscape',
                                'outline' => true,
                                'dpi' => 96
                            ],
                        ],
                    ]);
                    $this->viewBuilder()->setClassName('CakePdf.Pdf');
            }elseif($output == "excel"){
                $this->autoRender = false;
                $spreadsheet = new Spreadsheet();
                $sheet = $spreadsheet->getActiveSheet();
                $sheet->getColumnDimension('A')->setAutoSize(true);
                $sheet->getColumnDimension('B')->setAutoSize(true);
                $sheet->getColumnDimension('C')->setAutoSize(true);
                $sheet->getColumnDimension('D')->setAutoSize(true);
                $sheet->getColumnDimension('E')->setAutoSize(true);
                $sheet->getColumnDimension('F')->setAutoSize(true);
                $sheet->getColumnDimension('G')->setAutoSize(true);

                $styleArray = [
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            'color' => ['argb' => '333333'],
                        ],
                    ],
                ];

                $row = 1;

                foreach($codeAccounts as $ckey => $codeAccount){
                    $startBold = $row;

                    $sheet->mergeCells('A'.$row.':G'.$row);
                    $sheet->setCellValue('A'.$row++, 'BUKU BESAR');
                    $sheet->mergeCells('A'.$row.':G'.$row);
                    $sheet->setCellValue('A'.$row++, 'PERIODE : '.strtoupper($this->Utilities->indonesiaDateFormat($start)).' S.D '.strtoupper($this->Utilities->indonesiaDateFormat($end)));
                    $sheet->mergeCells('A'.$row.':G'.$row);
                    $sheet->setCellValue('A'.$row, 'COA : ['.$codeAccount->code.'] '.$codeAccount->name);

                    $sheet->getStyle('A'.$startBold.':G'.$row++)->getFont()->setBold(true);

                    $totalDebit     = 0;
                    $totalKredit    = 0;
                    $totalSaldo     = $codeAccount->saldoAwal;
                    

                    $startRow = $row;

                    $sheet->getStyle('A'.$row.':H'.$row)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

                    $sheet->setCellValue('A'.$row, 'No.');
                    $sheet->setCellValue('B'.$row, 'No. Jurnal');
                    $sheet->setCellValue('C'.$row, 'Tanggal');
                    $sheet->setCellValue('D'.$row, 'Remark');
                    $sheet->setCellValue('E'.$row, 'Debit');
                    $sheet->setCellValue('F'.$row, 'Kredit');
                    $sheet->setCellValue('G'.$row++, 'Balance');

                    $sheet->getStyle('A'.$row.':C'.$row)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                    $sheet->getStyle('D'.$row.':H'.$row)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);

                    $sheet->setCellValue('D'.$row, $start);
                    $sheet->setCellValue('E'.$row, 'SALDO AWAL');
                    $sheet->setCellValue('F'.$row, number_format($totalDebit));
                    $sheet->setCellValue('G'.$row, number_format($totalKredit));
                    $sheet->setCellValue('H'.$row++, number_format($totalSaldo));

                    $results = $codeAccount->journals;

                    foreach($results as $key => $result){
                        $totalDebit += $result->debit;
                        $totalKredit += $result->credit;
                        if($codeAccount->normal_balance == 0){
                            $totalSaldo    += ($result->debit - $result->credit);
                        }else{
                            $totalSaldo    += ($result->credit - $result->debit);
                        }

                        $sheet->getStyle('A'.$row.':D'.$row)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                        $sheet->getStyle('F'.$row.':H'.$row)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);

                        $sheet->setCellValue('A'.$row, $key + 1);
                        $sheet->setCellValue('B'.$row, $result->journal->code);
                        $sheet->setCellValue('C'.$row, $result->journal->date->format('d-m-Y'));
                        $sheet->setCellValue('D'.$row, $result->journal->description);
                        $sheet->setCellValue('E'.$row, number_format($result->debit));
                        $sheet->setCellValue('F'.$row, number_format($result->credit));
                        $sheet->setCellValue('G'.$row++, number_format($totalSaldo));
                    }
                    $sheet->mergeCells('A'.$row.':C'.$row);
                    $sheet->getStyle('D'.$row.':H'.$row)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);

                    $sheet->setCellValue('E'.$row, number_format($totalDebit));
                    $sheet->setCellValue('F'.$row++, number_format($totalKredit));
                    $sheet->setCellValue('G'.$row, number_format($totalSaldo));

                    $sheet->getStyle('A'.$startRow.':H'.($row - 1))->applyFromArray($styleArray);
                }

                $writer = new Xlsx($spreadsheet);
                header('Content-Type: application/vnd.ms-excel');
                header('Content-Disposition: attachment;filename="LAPORAN_BUKU_BESAR-' . time() . '.xlsx"');
                header('Cache-Control: max-age=0');
                $writer->save('php://output');
                exit();
            }else{
                $this->render('./pdf/index');
            }
            
        }else{
            $this->set(compact('businessTypes'));
        }
    }

}
