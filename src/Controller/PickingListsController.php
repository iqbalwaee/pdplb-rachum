<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Routing\Router;
/**
 * PickingLists Controller
 *
 * @property \App\Model\Table\PickingListsTable $PickingLists
 *
 * @method \App\Model\Entity\Currency[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PickingListsController extends AppController
{

    private $titleModule = "Picking List";
    private $primaryModel = "PickingLists";


    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow(['configure']);
        $this->set([
            'titleModule' => $this->titleModule,
            'primaryModel' => $this->primaryModel,
        ]);
    }

    function beforeFilter(\Cake\Event\Event $event){
        parent::beforeFilter($event);
    
            $this->Security->config('validatePost',false);
            //$this->getEventManager()->off($this->Csrf);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        if($this->request->is('ajax')){
            $source = $this->{$this->primaryModel};
            $searchAble = [
                $this->primaryModel.'.id',
                $this->primaryModel.'.name'
            ];
            $data = [
                'source'=>$source,
                'searchAble' => $searchAble,
                'defaultField' => $this->primaryModel.'.id',
                'defaultSort' => 'desc',
                'contain' => [
                    'Customers'
                ]
                    
            ];
            $dataTable   = $this->Datatables->make($data);  
            $this->set('aaData',$dataTable['aaData']);
            $this->set('iTotalDisplayRecords',$dataTable['iTotalDisplayRecords']);
            $this->set('iTotalRecords',$dataTable['iTotalRecords']);
            $this->set('sColumns',$dataTable['sColumns']);
            $this->set('sEcho',$dataTable['sEcho']);
            $this->set('_serialize',['aaData','iTotalDisplayRecords','iTotalRecords','sColumns','sEcho']);
        }else{
            $titlesubModule = "List ".$this->titleModule;
            $breadCrumbs = [
                Router::url(['action' => 'index']) => $titlesubModule
            ];
            $this->set(compact('breadCrumbs','titlesubModule'));
        }
        
        
    }

    /**
     * View method
     *
     * @param string|null $id Country id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $record = $this->{$this->primaryModel}->get($id, [
            'contain' => [
                'CreatedUsers',
                'Customers',
                'PickingListsDetails',
                'PickingListsDetails.VwStocksLocations'
            ]
        ]);
        $this->set('record', $record);
        $this->set('_serialize',['record']);
        $titlesubModule = "View ".$this->titleModule;
        $breadCrumbs = [
            Router::url(['action' => 'index']) => "List ".$this->titleModule,
            Router::url(['action' => 'view',$id]) => $titlesubModule
        ];
        $this->set(compact('breadCrumbs','titlesubModule'));
        $this->layout = false;
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $record = $this->{$this->primaryModel}->newEntity();
        if ($this->request->is('post')) {
            $data = $this->request->getData();
            $data['date'] = $this->Utilities->generalitationDateFormat($data['date']);
            $data['status'] = 0;
            foreach($data['picking_lists_details'] as $key => $ptd){
                if(empty($ptd['stocks_location_id'])){
                    unset($data['picking_lists_details'][$key]);
                }
            }
            $record = $this->{$this->primaryModel}->patchEntity($record, $data);
            if ($this->{$this->primaryModel}->save($record)) {
                $this->Flash->success(__($this->Utilities->message_alert($this->titleModule,1)));
                $status = "SUCCESS";
                $message = "DATA BERHASIL DISIMPAN";
                $code   = 200;
                $errors = [];
            }else{
                $status = "ERROR";
                $message = "DATA GAGAL DISIMPAN";
                $code   = 50;
                $errors = call_user_func_array('array_merge', $record->errors());
            }
            $this->set(compact('status','code','message','errors'));
            $this->set('_serialize',['status','code','message','errors']);
            // $this->Flash->error(__($this->Utilities->message_alert($this->titleModule,2)));
        }
        $this->loadModel('VwStocksLocations');
        $this->loadModel('StocksLocations');
        $stocksLocations = $this->VwStocksLocations->find()
        ->select($this->VwStocksLocations)
        ->select([
            'StocksLocationTemporaryStock' => '(`StocksLocationActualStock`) - ('.
                $this->{$this->primaryModel}->PickingListsDetails->find()->select([
                    'PTDQTY' => '(IFNULL(SUM(`PickingListsDetails`.`qty`),0))' 
                ])->where([
                    '`PickingListsDetails`.`stocks_location_id` = VwStocksLocations.StocksLocationID'
                ])
            .')'
        ])
        ->order([
            '`VwStocksLocations`.`RDate`' => 'ASC',
        ])->having([
            '`StocksLocationTemporaryStock` >' => 0
        ]);
        $customers = $this->{$this->primaryModel}->Customers->find('list')
                    ->order([
                        'Customers.name' => 'ASC'
                    ]);
        $this->set(compact('record','stocksLocations','customers'));
        $titlesubModule = "Tambah ".$this->titleModule;
        $breadCrumbs = [
            Router::url(['action' => 'index']) => "List ".$this->titleModule,
            Router::url(['action' => 'add']) => $titlesubModule
        ];
        $this->set(compact('breadCrumbs','titlesubModule'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Country id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $record = $this->{$this->primaryModel}->get($id, [
            'contain' => [
                
            ]
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $ptdOldId = [];
            $data = $this->request->getData();
            $data['date'] = $this->Utilities->generalitationDateFormat($data['date']);
            foreach($data['picking_lists_details'] as $key => $ptd){
                if(empty($ptd['stock_id'])){
                    unset($data['picking_lists_details'][$key]);
                    if(!empty($ptd['id'])){
                        array_push($ptdOldId,$ptd['id']);
                    }
                }
            }
            $record = $this->{$this->primaryModel}->patchEntity($record, $data);
            if (empty($record->errors())) {
                foreach($ptdOldId as $key => $idPtd){
                   $ptd = $this->{$this->primaryModel}->PickingListsDetails->get($idPtd);
                   $this->{$this->primaryModel}->PickingListsDetails->delete($ptd);
                }
                $this->{$this->primaryModel}->save($record);
                $this->Flash->success(__($this->Utilities->message_alert($this->titleModule,3)));
                $status = "SUCCESS";
                $message = "DATA BERHASIL DISIMPAN";
                $code   = 200;
                $errors = [];
            }else{
                $status = "ERROR";
                $message = "DATA GAGAL DISIMPAN";
                $code   = 50;
                $errors = call_user_func_array('array_merge', $record->errors());
            }
            $this->set(compact('status','code','message','errors'));
            $this->set('_serialize',['status','code','message','errors']);
        }
        $this->loadModel('VwStocksLocations');
        $this->loadModel('StocksLocations');
        $stocksLocations = $this->VwStocksLocations->find()
        ->select($this->VwStocksLocations)
        ->order([
            '`VwStocksLocations`.`RDate`' => 'ASC',
        ])->having([
            '`VwStocksLocations`.`StocksLocationActualStock` >' => 0
        ]);
        $customers = $this->{$this->primaryModel}->Customers->find('list')
                    ->order([
                        'Customers.name' => 'ASC'
                    ]);
        $record->date = $record->date->format('d-m-Y');
        $this->set(compact('record','stocksLocations','customers'));
        $titlesubModule = "Edit ".$this->titleModule;
        $breadCrumbs = [
            Router::url(['action' => 'index']) => "List ".$this->titleModule,
            Router::url(['action' => 'edit',$id]) => $titlesubModule
        ];
        $this->set(compact('breadCrumbs','titlesubModule'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Country id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $record = $this->{$this->primaryModel}->get($id);
        if ($this->{$this->primaryModel}->delete($record)) {
            $code = 200;
            $message = __($this->Utilities->message_alert($this->titleModule,5));
            $status = 'success';
        } else {
            $code = 99;
            $message = __($this->Utilities->message_alert($this->titleModule,6));
            $status = 'error';
        }
        if($this->request->is('ajax')){
            $this->set('code',$code);
            $this->set('message',$message);
            $this->set('_serialize',['code','message']);
        }else{
            $this->Flash->{$status}($message);
            return $this->redirect(['action' => 'index']);
        }
    }

    
}
