<?php
// src/Controller/Component/UtilitiesComponent.php
namespace App\Controller\Component;
use Cake\ORM\TableRegistry;
use Cake\Controller\Component;
use Cake\Cache\Cache;

class UtilitiesComponent extends Component
{
    public $components = ['Auth','Acl.Acl'];
    

    public function message_alert($title,$no = 1){
        $array = [
            1 => 'Data '.$title.' berhasil disimpan',
            'Data '.$title.' gagal disimpan',
            'Data '.$title.' berhasil diedit',
            'Data '.$title.' gagal diedit',
            'Data '.$title.' berhasil dihapus',
            'Data '.$title.' gagal dihapus'
        ];
        return $array[$no];
    }

    public function monthArray()
    {
        $array = array (1 =>   'Januari',
                    'Februari',
                    'Maret',
                    'April',
                    'Mei',
                    'Juni',
                    'Juli',
                    'Agustus',
                    'September',
                    'Oktober',
                    'November',
                    'Desember'
                );
        return $array;
    }
    
    public function indonesiaDateFormat($tanggal,$time = false,$toIndonesia = true)
    {
        $bulan = $this->monthArray();
        $datepick = explode(" ", $tanggal);
        $split = explode("-", $datepick[0]);
        if($toIndonesia == true){
            if($time == false){
                return $split[2] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[0];
            }else{
                return $split[2] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[0]. ' ' . $datepick[1];
            }
        }else{
            if($time == false){
                return $split[2] . '-' . $split[1]. '-' . $split[0];
            }else{
                return $split[2] . '-' . $split[1] . '-' . $split[0].' '.$datepick[1];
            }
        }
        
    }  

    public function generalitationDateFormat($tanggal,$time = false)
    {
        $datepick = explode(" ", $tanggal);
        $split = explode("-", $datepick[0]);
        if($time){
            return $split[2] . '-' . $split[1] . '-' . $split[0]. " " . $datepick[1]; 
        }else{
            return $split[2] . '-' . $split[1] . '-' . $split[0]; 
        }
           
    }
    
	public function generalitationNumber($number){
        return str_replace(",","",$number);
    }
    
    public function businessTypeVal ($type = null){
        $array = [
            1 => "NON PLB",
            2 => "PLB",
        ];
        if($type == null){
            return $array;
        }else{
            return $array[$type];
        }
    }

    public function journalType ($type = null){
        $array = [
            1 => "UMUM",
            2 => "PENJUALAN",
            3 => "PENERIMAAN KAS",
            4 => "PENGELUARAN KAS",
            5 => "PEMBELIAN",
            6 => "SALDO AWAL",
        ];
        if($type == null){
            return $array;
        }else{
            return $array[$type];
        }
    }

    public function createLabaRugiExcel($codeAccounts, $sheet, $row){
        foreach($codeAccounts as $key => $codeAccount){
            $space = '';
            $aSpace = '           ';
            for ($i = 0; $i < $codeAccount->level; $i++) { 
                $space .= $aSpace;
            }
            if(!empty($codeAccount->children)){
                $sheet->mergeCells('A'.$row.':B'.$row);
                $sheet->setCellValue('A'.$row++, $space.$codeAccount->name);
            }else{
                $sheet->getStyle('B'.$row)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
                $sheet->setCellValue('A'.$row, $space.$codeAccount->name);
                $sheet->setCellValue('B'.$row++, number_format($codeAccount->amount));
            }

            if(!empty($codeAccount->children)){
                $row = $this->createLabaRugiExcel($codeAccount->children, $sheet, $row);
                $sheet->getStyle('B'.$row)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
                $sheet->setCellValue('A'.$row, $space.'TOTAL '.$codeAccount->name);
                $sheet->setCellValue('B'.$row++, number_format($codeAccount->amount));
            }
        }
        return $row;
    }

}

?>