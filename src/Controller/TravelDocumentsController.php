<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Routing\Router;
/**
 * TravelDocuments Controller
 *
 * @property \App\Model\Table\TravelDocumentsTable $TravelDocuments
 *
 * @method \App\Model\Entity\Currency[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class TravelDocumentsController extends AppController
{

    private $titleModule = "Surat Jalan";
    private $primaryModel = "TravelDocuments";


    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow(['configure']);
        $this->set([
            'titleModule' => $this->titleModule,
            'primaryModel' => $this->primaryModel,
        ]);
    }

    function beforeFilter(\Cake\Event\Event $event){
        parent::beforeFilter($event);
    
            $this->Security->config('validatePost',false);
            //$this->getEventManager()->off($this->Csrf);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        if($this->request->is('ajax')){
            $source = $this->{$this->primaryModel};
            $searchAble = [
                $this->primaryModel.'.id',
                $this->primaryModel.'.name'
            ];
            $data = [
                'source'=>$source,
                'searchAble' => $searchAble,
                'defaultField' => $this->primaryModel.'.id',
                'defaultSort' => 'desc',
                'contain' => [
                    'Expenditures',
                    'Expenditures.Customers'
                ]
                    
            ];
            $dataTable   = $this->Datatables->make($data);  
            $this->set('aaData',$dataTable['aaData']);
            $this->set('iTotalDisplayRecords',$dataTable['iTotalDisplayRecords']);
            $this->set('iTotalRecords',$dataTable['iTotalRecords']);
            $this->set('sColumns',$dataTable['sColumns']);
            $this->set('sEcho',$dataTable['sEcho']);
            $this->set('_serialize',['aaData','iTotalDisplayRecords','iTotalRecords','sColumns','sEcho']);
        }else{
            $titlesubModule = "List ".$this->titleModule;
            $breadCrumbs = [
                Router::url(['action' => 'index']) => $titlesubModule
            ];
            $this->set(compact('breadCrumbs','titlesubModule'));
        }
        
        
    }

    /**
     * View method
     *
     * @param string|null $id Country id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $record = $this->{$this->primaryModel}->get($id, [
            'contain' => [
                'CreatedUsers',
                'Customers',
                'Expenditures',
                'TravelDocumentsDetails',
                'TravelDocumentsDetails.ExpendituresPickingListsDetails.PickingListsDetails.VwStocksLocations',
            ]
        ]);
        $this->set('record', $record);
        $this->set('_serialize',['record']);
        $titlesubModule = "View ".$this->titleModule;
        $breadCrumbs = [
            Router::url(['action' => 'index']) => "List ".$this->titleModule,
            Router::url(['action' => 'view',$id]) => $titlesubModule
        ];
        $this->set(compact('breadCrumbs','titlesubModule'));
        $this->layout = false;
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $record = $this->{$this->primaryModel}->newEntity();
        if ($this->request->is('post')) {
            $data = $this->request->getData();
            $data['date'] = $this->Utilities->generalitationDateFormat($data['date']);
            if(!empty($data['travel_documents_details'])){
                foreach($data['travel_documents_details'] as $key => $tdd){
                    if(empty($tdd['expenditures_picking_lists_detail_id'])){
                        unset($data['travel_documents_details'][$key]);
                    }
                }
            }
            $record = $this->{$this->primaryModel}->patchEntity($record, $data,[
            ]);
            if ($this->{$this->primaryModel}->save($record)) {
                $this->Flash->success(__($this->Utilities->message_alert($this->titleModule,1)));
                $status = "SUCCESS";
                $message = "DATA BERHASIL DISIMPAN";
                $code   = 200;
                $errors = [];
            }else{
                $status = "ERROR";
                $message = "DATA GAGAL DISIMPAN";
                $code   = 50;
                $errors = call_user_func_array('array_merge', $record->errors());
            }
            $this->set(compact('status','code','message','errors'));
            $this->set('_serialize',['status','code','message','errors']);
            // $this->Flash->error(__($this->Utilities->message_alert($this->titleModule,2)));
        }
        $customers = $this->{$this->primaryModel}->Customers->find('list')
                    ->order([
                        'Customers.name' => 'ASC'
                    ]);
        $expenditures = [];
        $this->set(compact('record','customers','expenditures'));
        $titlesubModule = "Tambah ".$this->titleModule;
        $breadCrumbs = [
            Router::url(['action' => 'index']) => "List ".$this->titleModule,
            Router::url(['action' => 'add']) => $titlesubModule
        ];
        $this->set(compact('breadCrumbs','titlesubModule'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Country id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $record = $this->{$this->primaryModel}->get($id, [
            'contain' => [
                'TravelDocumentsDetails',
            ]
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            $data['date'] = $this->Utilities->generalitationDateFormat($data['date']);
            $tddOldId = [];
            if(!empty($data['travel_documents_details'])){
                foreach($data['travel_documents_details'] as $key => $tdd){
                    if(empty($tdd['expenditures_picking_lists_detail_id'])){
                        unset($data['travel_documents_details'][$key]);
                        if(!empty($tdd['id'])){
                            array_push($tddOldId,$tdd['id']);
                        }
                    }
                }
            }
            $record = $this->{$this->primaryModel}->patchEntity($record, $data,[
                
            ]);
            // dd($record);
            if (empty($record->errors())) {
                foreach($tddOldId as $key => $idPtd){
                    $ptd = $this->{$this->primaryModel}->TravelDocumentsDetails->get($idPtd);
                    $this->{$this->primaryModel}->TravelDocumentsDetails->delete($ptd);
                 }
                $this->{$this->primaryModel}->save($record);
                $this->Flash->success(__($this->Utilities->message_alert($this->titleModule,3)));
                $status = "SUCCESS";
                $message = "DATA BERHASIL DISIMPAN";
                $code   = 200;
                $errors = [];
            }else{
                $status = "ERROR";
                $message = "DATA GAGAL DISIMPAN";
                $code   = 50;
                $errors = call_user_func_array('array_merge', $record->errors());
            }
            $this->set(compact('status','code','message','errors'));
            $this->set('_serialize',['status','code','message','errors']);
        }
        $customers = $this->{$this->primaryModel}->Customers->find('list')
                    ->order([
                        'Customers.name' => 'ASC'
                    ]);
        $expenditures = $this->{$this->primaryModel}->Expenditures->find('list')
            ->where([
                'Expenditures.customer_id' => $record->customer_id,
            ])
            ->order([
                '`Expenditures`.`code` ASC',
            ]);

        $this->loadModel('ExpendituresPickingListsDetails');
        $this->loadModel('TravelDocumentsDetails');
        $expenditureDetails = $this->ExpendituresPickingListsDetails->find()
            ->contain([
                'ExpendituresPickingLists',
                'ExpendituresPickingLists.Expenditures',
                'PickingListsDetails',
                'PickingListsDetails.VwStocksLocations'
            ])
            ->select($this->ExpendituresPickingListsDetails)
            ->select($this->ExpendituresPickingListsDetails->PickingListsDetails)
            ->select($this->ExpendituresPickingListsDetails->PickingListsDetails->VwStocksLocations)
            ->select([
                'saldo' => '(`ExpendituresPickingListsDetails`.`qty`) - (
                    '.
                    $this->TravelDocumentsDetails->find()
                    ->select([
                        'SUMARY' => '(IFNULL(SUM(`TravelDocumentsDetails`.`qty`),0))'
                    ])
                    ->where([
                        '`TravelDocumentsDetails`.expenditures_picking_lists_detail_id = `ExpendituresPickingListsDetails`.`id`',
                        '`TravelDocumentsDetails`.`travel_document_id` !=' => $id
                    ])
                    .'
                )'
            ])
            ->where([
                'ExpendituresPickingLists.expenditure_id' => $record->expenditure_id,
            ])
            ->join([
                [
                    'table' => 'travel_documents_details',
                    'alias' => 'TravelDocumentsDetails',
                    'type' => 'LEFT',
                    'conditions' => [
                        'TravelDocumentsDetails.travel_document_id = '.$id,
                        'TravelDocumentsDetails.expenditures_picking_lists_detail_id = `ExpendituresPickingListsDetails`.`id`'
                    ],
                ]
            ])
            ->select([
                'travel_document_detail_id' => 'TravelDocumentsDetails.id',
                'travel_document_detail_qty' => 'TravelDocumentsDetails.qty',
            ])
            ->having([
                'saldo > 0'
            ])
            ->order([
                '`Expenditures`.`date` ASC',
                '`VwStocksLocations`.`ProductName`' => 'ASC',
            ]);
        $record->date = $record->date->format('d-m-Y');
        $this->set(compact('record','customers','expenditures','expenditureDetails'));
        $titlesubModule = "Edit ".$this->titleModule;
        $breadCrumbs = [
            Router::url(['action' => 'index']) => "List ".$this->titleModule,
            Router::url(['action' => 'edit',$id]) => $titlesubModule
        ];
        $this->set(compact('breadCrumbs','titlesubModule'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Country id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $record = $this->{$this->primaryModel}->get($id);
        if ($this->{$this->primaryModel}->delete($record)) {
            $code = 200;
            $message = __($this->Utilities->message_alert($this->titleModule,5));
            $status = 'success';
        } else {
            $code = 99;
            $message = __($this->Utilities->message_alert($this->titleModule,6));
            $status = 'error';
        }
        if($this->request->is('ajax')){
            $this->set('code',$code);
            $this->set('message',$message);
            $this->set('_serialize',['code','message']);
        }else{
            $this->Flash->{$status}($message);
            return $this->redirect(['action' => 'index']);
        }
    }

    
}
