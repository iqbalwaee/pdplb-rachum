<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Routing\Router;

/**
 * SaldoAwal Controller
 *
 *
 * @method \App\Model\Entity\SaldoAwal[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class SaldoAwalController extends AppController
{

    private $titleModule = "Saldo Awal";
    private $primaryModel = "Journals";

    public function initialize()
    {
        parent::initialize();
        $this->set([
            'titleModule' => $this->titleModule,
            'primaryModel' => $this->primaryModel,
        ]);
        $this->loadModel('Journals');
    }

    function beforeFilter(\Cake\Event\Event $event){
        parent::beforeFilter($event);
    
        if(isset($this->Security) && $this->request->isAjax() && ($this->action = 'index' || $this->action = 'delete')){
    
            $this->Security->config('validatePost',false);
            //$this->getEventManager()->off($this->Csrf);
    
        }else{
            $this->Security->config('unlockedFields',['journals_details','data_to_delete']);
            // dd($this->Security);
        }
    
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        
        if($this->request->is('ajax')){
            $source = $this->{$this->primaryModel};
            $searchAble = [
                $this->primaryModel.'.id',
                'BusinessTypes.name',
                $this->primaryModel.'.code',
                $this->primaryModel.'.description',
            ];
            $data = [
                'source'=>$source,
                'searchAble' => $searchAble,
                'defaultField' => $this->primaryModel.'.id',
                'defaultSort' => 'desc',
                'defaultSearch' => [
                    [    
                        'keyField' => 'Journals.ref_table',
                        'condition' => '=',
                        'value' => 'SALDO_AWAL'
                    ]
                ],
                    
            ];
            $dataTable   = $this->Datatables->make($data);  
            $this->set('aaData',$dataTable['aaData']);
            $this->set('iTotalDisplayRecords',$dataTable['iTotalDisplayRecords']);
            $this->set('iTotalRecords',$dataTable['iTotalRecords']);
            $this->set('sColumns',$dataTable['sColumns']);
            $this->set('sEcho',$dataTable['sEcho']);
            $this->set('_serialize',['aaData','iTotalDisplayRecords','iTotalRecords','sColumns','sEcho']);
        }else{
            $titlesubModule = "Daftar ".$this->titleModule;
            $breadCrumbs = [
                Router::url(['action' => 'index']) => $titlesubModule
            ];
            $this->set(compact('titleModule','breadCrumbs','titlesubModule'));
        }
        
        
    }

    /**
     * View method
     *
     * @param string|null $id BusinessType id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        
        $record = $this->{$this->primaryModel}->get($id, [
            'contain'=> [
                    'JournalsDetails',
                    'JournalsDetails.CodeAccounts'
                ]
        ]);
        $titlesubModule = "View ".$this->titleModule;
        $breadCrumbs = [
            Router::url(['action' => 'index']) => "List ".$this->titleModule,
            Router::url(['action' => 'view',$id]) => $titlesubModule
        ];
        $this->set(compact('titleModule','breadCrumbs','titlesubModule'));
        $this->set('record', $record);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $record = $this->{$this->primaryModel}->newEntity();
        if ($this->request->is('post')) {
            $data =  $this->request->getData();
            $data['debit']  = 0;
            $data['credit'] = 0;
            $data['ref_table'] = 'SALDO_AWAL';
            $data['date'] = $this->Utilities->generalitationDateFormat($data['date']);
            if(!empty($data['journals_details'])){
                foreach($data['journals_details'] as $key => $r){
                    $r['debit'] = $this->Utilities->generalitationNumber($r['debit']);
                    $r['credit'] = $this->Utilities->generalitationNumber($r['credit']);
                    $data['journals_details'][$key] = $r;
                    $data['debit']  += $r['debit'];
                    $data['credit'] += $r['credit'];
                }
            }
            $record = $this->{$this->primaryModel}->patchEntity($record,$data);
            if ($this->{$this->primaryModel}->save($record)) {
                $this->Flash->success(__($this->Utilities->message_alert($this->titleModule,1)));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__($this->Utilities->message_alert($this->titleModule,2)));
        }
        $this->set(compact('record'));
        $titlesubModule = "Tambah ".$this->titleModule;
        $breadCrumbs = [
            Router::url(['action' => 'index']) => "List ".$this->titleModule,
            Router::url(['action' => 'add']) => $titlesubModule
        ];
        $this->set(compact('titleModule','breadCrumbs','titlesubModule'));
    }

    /**
     * Edit method
     *
     * @param string|null $id BusinessType id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $record = $this->{$this->primaryModel}->get($id,[
            'contain' => [
                'JournalsDetails',
                'JournalsDetails.CodeAccounts'
            ]
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data =  $this->request->getData();
            $data['debit']  = 0;
            $data['credit'] = 0;
            $data['date'] = $this->Utilities->generalitationDateFormat($data['date']);
            if(!empty($data['journals_details'])){
                foreach($data['journals_details'] as $key => $r){
                    $r['debit'] = $this->Utilities->generalitationNumber($r['debit']);
                    $r['credit'] = $this->Utilities->generalitationNumber($r['credit']);
                    $data['journals_details'][$key] = $r;
                    $data['debit']  += $r['debit'];
                    $data['credit'] += $r['credit'];
                }
            }
            $record = $this->{$this->primaryModel}->patchEntity($record,$data);
            // dd($data);
            if(empty($record->errors())){
                if(!empty($data['data_to_delete'])){
                    $dataToDelete = $data['data_to_delete'];
                    $this->{$this->primaryModel}->JournalsDetails->deleteAll(['id'=>$dataToDelete]);
                }
            }
            if ($this->{$this->primaryModel}->save($record)) {
                
                
                $this->Flash->success(__($this->Utilities->message_alert($this->titleModule,3)));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__($this->Utilities->message_alert($this->titleModule,4)));
        }
        $record->date = $record->date->format('d-m-Y');
        
        $this->set(compact('record'));
        $titlesubModule = "Edit ".$this->titleModule;
        $breadCrumbs = [
            Router::url(['action' => 'index']) => "List ".$this->titleModule,
            Router::url(['action' => 'edit',$id]) => $titlesubModule
        ];
        $this->set(compact('titleModule','breadCrumbs','titlesubModule'));
        $this->set('record', $record);
    }

    /**
     * Delete method
     *
     * @param string|null $id BusinessType id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $record = $this->{$this->primaryModel}->get($id);
        if ($this->{$this->primaryModel}->delete($record)) {
            $code = 200;
            $message = __($this->Utilities->message_alert($this->titleModule,5));
            $status = 'success';
        } else {
            $code = 99;
            $message = __($this->Utilities->message_alert($this->titleModule,6));
            $status = 'error';
        }
        if($this->request->is('ajax')){
            $this->set('code',$code);
            $this->set('message',$message);
            $this->set('_serialize',['code','message']);
        }else{
            $this->Flash->{$status}($message);
            return $this->redirect(['action' => 'index']);
        }
    }
}
