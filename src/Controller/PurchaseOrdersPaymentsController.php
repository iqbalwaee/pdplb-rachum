<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Routing\Router;
/**
 * PurchaseOrdersPayments Controller
 *
 * @property \App\Model\Table\PurchaseOrdersPaymentsTable $PurchaseOrdersPayments
 *
 * @method \App\Model\Entity\Currency[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PurchaseOrdersPaymentsController extends AppController
{

    private $titleModule = "Pembayaran PO";
    private $primaryModel = "PurchaseOrdersPayments";


    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow(['configure']);
        $this->set([
            'titleModule' => $this->titleModule,
            'primaryModel' => $this->primaryModel,
        ]);
    }

    function beforeFilter(\Cake\Event\Event $event){
        parent::beforeFilter($event);
    
            $this->Security->config('validatePost',false);
            //$this->getEventManager()->off($this->Csrf);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        if($this->request->is('ajax')){
            $source = $this->{$this->primaryModel};
            $searchAble = [
                $this->primaryModel.'.id',
                $this->primaryModel.'.name'
            ];
            $data = [
                'source'=>$source,
                'searchAble' => $searchAble,
                'defaultField' => $this->primaryModel.'.id',
                'defaultSort' => 'desc',
                'contain' => [
                    'PurchaseOrders',
                    'PurchaseOrders.Suppliers',
                    'Currencies'
                ]
                    
            ];
            $dataTable   = $this->Datatables->make($data);  
            $this->set('aaData',$dataTable['aaData']);
            $this->set('iTotalDisplayRecords',$dataTable['iTotalDisplayRecords']);
            $this->set('iTotalRecords',$dataTable['iTotalRecords']);
            $this->set('sColumns',$dataTable['sColumns']);
            $this->set('sEcho',$dataTable['sEcho']);
            $this->set('_serialize',['aaData','iTotalDisplayRecords','iTotalRecords','sColumns','sEcho']);
        }else{
            $titlesubModule = "List ".$this->titleModule;
            $breadCrumbs = [
                Router::url(['action' => 'index']) => $titlesubModule
            ];
            $this->set(compact('breadCrumbs','titlesubModule'));
        }
        
        
    }

    /**
     * View method
     *
     * @param string|null $id Country id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $record = $this->{$this->primaryModel}->get($id, [
            'contain' => [
                'PurchaseOrders',
                'PurchaseOrders.Suppliers',
                'Currencies',
                'CreatedUsers'
            ]
        ]);
        $this->set('record', $record);
        $this->set('_serialize',['record']);
        $titlesubModule = "View ".$this->titleModule;
        $breadCrumbs = [
            Router::url(['action' => 'index']) => "List ".$this->titleModule,
            Router::url(['action' => 'view',$id]) => $titlesubModule
        ];
        $this->set(compact('breadCrumbs','titlesubModule'));
        $this->layout = false;
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $record = $this->{$this->primaryModel}->newEntity();
        if ($this->request->is('post')) {
            $data = $this->request->getData();
            $currency = $this->{$this->primaryModel}->Currencies->get(28);
            $data['payment_amount_idr'] = $this->Utilities->generalitationNumber($data['payment_amount_idr']);
            $data['saldo_hutang'] = $this->Utilities->generalitationNumber($data['saldo_hutang']);
            $data['total_hutang'] = $this->Utilities->generalitationNumber($data['total_hutang']);
            
            $data['kurs'] = $currency->kurs_idr;
            $data['currency_id'] = 28;
            $data['payment_amount'] = $data['payment_amount_idr'] / $data['kurs'];
            $data['date'] = $this->Utilities->generalitationDateFormat($data['date']);
            // $data['status'] = 0;
            // dd($data);
            $record = $this->{$this->primaryModel}->patchEntity($record, $data);
            if ($this->{$this->primaryModel}->save($record)) {
                $this->Flash->success(__($this->Utilities->message_alert($this->titleModule,1)));
                $status = "SUCCESS";
                $message = "DATA BERHASIL DISIMPAN";
                $code   = 200;
                $errors = [];
                $listErrors = [];
            }else{
                $status = "ERROR";
                $message = "DATA GAGAL DISIMPAN";
                $code   = 50;
                $listErrors = $record;
                $errors = call_user_func_array('array_merge', $record->errors());
            }
            $this->set(compact('status','code','message','errors','listErrors'));
            $this->set('_serialize',['status','code','message','errors','listErrors']);
            // $this->Flash->error(__($this->Utilities->message_alert($this->titleModule,2)));
        }
        $purchaseOrders = $this->{$this->primaryModel}->PurchaseOrders->find('list')->order([
            'PurchaseOrders.code' => 'ASC'
        ])->where([
            'PurchaseOrders.status !=' => 2
        ]);
        $this->set(compact('record','purchaseOrders'));
        $titlesubModule = "Tambah ".$this->titleModule;
        $breadCrumbs = [
            Router::url(['action' => 'index']) => "List ".$this->titleModule,
            Router::url(['action' => 'add']) => $titlesubModule
        ];
        $this->set(compact('breadCrumbs','titlesubModule'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Country id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $record = $this->{$this->primaryModel}->get($id, [
            'contain' => [
            ]
        ]);
        $po = $this->{$this->primaryModel}->PurchaseOrders->find()
        ->select($this->{$this->primaryModel}->PurchaseOrders)
        ->select([
            'total_dibayar' => $this->{$this->primaryModel}->find()->select([
                'saldo'=>'(IFNULL(SUM(`payment_amount_idr`),0))'
            ])->where([
                '`purchase_order_id`' => $record->purchase_order_id
            ])
        ])
        ->where([
            '`PurchaseOrders`.`id`' => $record->purchase_order_id
        ])
        ->first();
        $record->total_hutang = $po->total_amount_idr;
        $record->saldo_hutang = $po->total_amount_idr - $po->total_bayar;
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            $oldId = $record->purchase_order_id;
            $currency = $this->{$this->primaryModel}->Currencies->get(28);
            $data['payment_amount_idr'] = $this->Utilities->generalitationNumber($data['payment_amount_idr']);
            $data['saldo_hutang'] = $this->Utilities->generalitationNumber($data['saldo_hutang']);
            $data['total_hutang'] = $this->Utilities->generalitationNumber($data['total_hutang']);
            
            $data['kurs'] = $currency->kurs_idr;
            $data['currency_id'] = 28;
            $data['payment_amount'] = $data['payment_amount_idr'] / $data['kurs'];
            $data['date'] = $this->Utilities->generalitationDateFormat($data['date']);

            $record = $this->{$this->primaryModel}->patchEntity($record, $data);
            if (empty($record->errors())) {
                $this->{$this->primaryModel}->save($record);
                if($oldId != $record->purchase_order_id){
                    $this->{$this->primaryModel}->updatePoPayment($oldId);
                }
                $this->Flash->success(__($this->Utilities->message_alert($this->titleModule,3)));
                $status = "SUCCESS";
                $message = "DATA BERHASIL DISIMPAN";
                $code   = 200;
                $errors = [];
            }else{
                $status = "ERROR";
                $message = "DATA GAGAL DISIMPAN";
                $code   = 50;
                $errors = call_user_func_array('array_merge', $record->errors());
            }
            $this->set(compact('status','code','message','errors'));
            $this->set('_serialize',['status','code','message','errors']);
        }
        $purchaseOrders = $this->{$this->primaryModel}->PurchaseOrders->find('list')->order([
            'PurchaseOrders.code' => 'ASC'
        ])->where([
            'PurchaseOrders.status !=' => 2
        ]);
        $this->set(compact('record','purchaseOrders'));
        $titlesubModule = "Edit ".$this->titleModule;
        $breadCrumbs = [
            Router::url(['action' => 'index']) => "List ".$this->titleModule,
            Router::url(['action' => 'edit',$id]) => $titlesubModule
        ];
        $this->set(compact('breadCrumbs','titlesubModule'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Country id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $record = $this->{$this->primaryModel}->get($id);
        if ($this->{$this->primaryModel}->delete($record)) {
            $this->{$this->primaryModel}->updatePoPayment($record->purchase_order_id);
            $code = 200;
            $message = __($this->Utilities->message_alert($this->titleModule,5));
            $status = 'success';
        } else {
            $code = 99;
            $message = __($this->Utilities->message_alert($this->titleModule,6));
            $status = 'error';
        }
        if($this->request->is('ajax')){
            $this->set('code',$code);
            $this->set('message',$message);
            $this->set('_serialize',['code','message']);
        }else{
            $this->Flash->{$status}($message);
            return $this->redirect(['action' => 'index']);
        }
    }

    
}
