<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Routing\Router;
use Cake\Core\Configure;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
/**
 * Laporan Kas Controller
 *
 * @property \App\Model\Table\JournalsTable $Journals
 *
 * @method \App\Model\Entity\BusinessType[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class LaporanKasController extends AppController
{

    private $titleModule = "Laporan Kas";
    private $primaryModel = "Journals";

    public function initialize()
    {
        parent::initialize();
        $this->set([
            'titleModule' => $this->titleModule,
            'primaryModel' => $this->primaryModel,
        ]);
    }

    function beforeFilter(\Cake\Event\Event $event){
        parent::beforeFilter($event);
    
        if(isset($this->Security) && $this->request->isAjax() && ($this->action = 'index' || $this->action = 'delete')){
    
            $this->Security->config('validatePost',false);
            //$this->getEventManager()->off($this->Csrf);
    
        }
    
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->loadModel('JournalsDetails');
        // JIKA FILTER QUERY ADA
        if(!empty($this->request->getQuery())){
            $query              = $this->request->getQuery();
            $businessTypeId     = null;
            $start              = $query['date_1'];
            $end                = $query['date_2'];
            $output             = $query['output'];
            $results            = $this->JournalsDetails->find()
                                    ->contain([
                                        'Journals',
                                        'CodeAccounts'
                                    ])
                                    ->where([
                                        '`Journals`.`date` BETWEEN :start AND :end',
                                        '`CodeAccounts`.`arus_kas`' => 1,
                                    ])
                                    ->bind(':start', $start, 'date')
                                    ->bind(':end',   $end, 'date');

            // JIKA KONDISI PER PERUSAHAAN//
            // if(!empty($businessTypeId)){
            //     $businessType = $this->BusinessTypes->get($businessTypeId);
            //     $results->where([
            //         'Journals.business_type_id' => $businessTypeId
            //     ]);
            // }
            
            $this->set(compact('results','start','end','businessType'));
            $this->viewBuilder()->setLayout(false);
            if($output == "pdf"){
                    Configure::write('CakePdf', [
                        'engine' => [
                            'className' => 'CakePdf.WkHtmlToPdf',
                            'binary' => env("WKHTML_PATH","C:\\wkhtmltopdf\\bin\\wkhtmltopdf.exe"),
                            'options' => [
                                'print-media-type' => false,
                                'orientation' => 'landscape',
                                'outline' => true,
                                'dpi' => 96
                            ],
                        ],
                    ]);
                    $this->viewBuilder()->setClassName('CakePdf.Pdf');
            }elseif($output == "excel"){
                $this->autoRender = false;
                $spreadsheet = new Spreadsheet();
                $sheet = $spreadsheet->getActiveSheet();
                $sheet->getColumnDimension('A')->setAutoSize(true);
                $sheet->getColumnDimension('B')->setAutoSize(true);
                $sheet->getColumnDimension('C')->setAutoSize(true);
                $sheet->getColumnDimension('D')->setAutoSize(true);
                $sheet->getColumnDimension('E')->setAutoSize(true);
                $sheet->getColumnDimension('F')->setAutoSize(true);
                $sheet->getColumnDimension('G')->setAutoSize(true);
                $sheet->getColumnDimension('H')->setAutoSize(true);
                $sheet->getColumnDimension('I')->setAutoSize(true);

                $sheet->mergeCells('A1:I1');
                $sheet->mergeCells('A2:I2');
                $sheet->mergeCells('A3:I3');

                $sheet->setCellValue('A1', 'LAPORAN ARUS KAS');
                $sheet->setCellValue('A2', 'PERIODE : '.strtoupper($this->Utilities->indonesiaDateFormat($start)).' S.D '.strtoupper($this->Utilities->indonesiaDateFormat($end)));
                if(!empty($businessType)){
                    $sheet->setCellValue('A3', 'JENIS USAHA : '.$this->Utilities->businessTypeVal($businessType->type).' - '.$businessType->name);
                }else{
                    $sheet->setCellValue('A3', 'SEMUA JENIS USAHA');
                }

                $sheet->getStyle('A1:I3')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $sheet->getStyle('A1:I3')->getFont()->setBold(true);

                $styleArray = [
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            'color' => ['argb' => '333333'],
                        ],
                    ],
                ];

                $start = 4;

                $sheet->setCellValue('A' . $start, 'No.');
                $sheet->setCellValue('B' . $start, 'No. Jurnal');
                $sheet->setCellValue('C' . $start, 'Tanggal');
                $sheet->setCellValue('D' . $start, 'Kode Akun');
                $sheet->setCellValue('E' . $start, 'Nama Akun');
                $sheet->setCellValue('F' . $start, 'Remark');
                $sheet->setCellValue('G' . $start, 'Debit');
                $sheet->setCellValue('H' . $start, 'Kredit');

                $sheet->getStyle('A'.$start)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $sheet->getStyle('B'.$start)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $sheet->getStyle('C'.$start)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $sheet->getStyle('D'.$start)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $sheet->getStyle('E'.$start)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $sheet->getStyle('F'.$start)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $sheet->getStyle('G'.$start)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $sheet->getStyle('H'.$start)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

                $row = $start + 1;
                $totalDebit=0;
                $totalKredit=0;
                foreach($results as $key => $result){
                    $totalDebit += $result->debit;
                    $totalKredit += $result->credit;

                    $sheet->setCellValue('A' . $row, $key + 1);
                    $sheet->setCellValue('B' . $row, $result->journal->code);
                    $sheet->setCellValue('C' . $row, $result->journal->date->format('d-m-Y'));
                    $sheet->setCellValue('D' . $row, $result->code_account->code);
                    $sheet->setCellValue('E' . $row, $result->code_account->name);
                    $sheet->setCellValue('F' . $row, $result->journal->description);
                    $sheet->setCellValue('G' . $row, number_format($result->debit));
                    $sheet->setCellValue('H' . $row, number_format($result->credit));

                    $sheet->getStyle('A'.$row)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                    $sheet->getStyle('B'.$row)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                    $sheet->getStyle('C'.$row)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                    $sheet->getStyle('D'.$row)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
                    
                    $row++;
                }
                $sheet->mergeCells('A'.$row.':F'.$row);
                $sheet->setCellValue('G' . $row, number_format($totalDebit));
                $sheet->setCellValue('H' . $row, number_format($totalKredit));

                $sheet->getStyle('G'.($start + 1).':H'.$row)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
                $sheet->getStyle('A' . $start . ':H' . $row)->applyFromArray($styleArray);

                $writer = new Xlsx($spreadsheet);
                header('Content-Type: application/vnd.ms-excel');
                header('Content-Disposition: attachment;filename="LAPORAN_ARUS_KAS-' . time() . '.xlsx"');
                header('Cache-Control: max-age=0');
                $writer->save('php://output');
            }else{
                $this->render('./pdf/index');
            }
            
        }else{
            // dont do anything
        }
    }

}
