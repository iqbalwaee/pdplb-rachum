<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Routing\Router;
/**
 * Receiptions Controller
 *
 * @property \App\Model\Table\ReceiptionsTable $Receiptions
 *
 * @method \App\Model\Entity\Currency[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ReceptionsController extends AppController
{

    private $titleModule = "Penerimaan Barang";
    private $primaryModel = "Receptions";


    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow(['configure']);
        $this->set([
            'titleModule' => $this->titleModule,
            'primaryModel' => $this->primaryModel,
        ]);
    }

    function beforeFilter(\Cake\Event\Event $event){
        parent::beforeFilter($event);
    
            $this->Security->config('validatePost',false);
            //$this->getEventManager()->off($this->Csrf);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        if($this->request->is('ajax')){
            $source = $this->{$this->primaryModel};
            $searchAble = [
                $this->primaryModel.'.id',
                $this->primaryModel.'.name'
            ];
            $data = [
                'source'=>$source,
                'searchAble' => $searchAble,
                'defaultField' => $this->primaryModel.'.id',
                'defaultSort' => 'desc',
                'contain' => [
                    'PurchaseOrders'
                ]
                    
            ];
            $dataTable   = $this->Datatables->make($data);  
            $this->set('aaData',$dataTable['aaData']);
            $this->set('iTotalDisplayRecords',$dataTable['iTotalDisplayRecords']);
            $this->set('iTotalRecords',$dataTable['iTotalRecords']);
            $this->set('sColumns',$dataTable['sColumns']);
            $this->set('sEcho',$dataTable['sEcho']);
            $this->set('_serialize',['aaData','iTotalDisplayRecords','iTotalRecords','sColumns','sEcho']);
        }else{
            $titlesubModule = "List ".$this->titleModule;
            $breadCrumbs = [
                Router::url(['action' => 'index']) => $titlesubModule
            ];
            $this->set(compact('breadCrumbs','titlesubModule'));
        }
        
        
    }

    /**
     * View method
     *
     * @param string|null $id Country id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $record = $this->{$this->primaryModel}->get($id, [
            'contain' => [
                'CreatedUsers',
                'PurchaseOrders.Suppliers',
                'Importers',
                'Senders',
                'Currencies',
                'ReceptionsContainers.ReceptionsContainersDetails.Products',
                'ReceptionsContainers.ReceptionsContainersDetails.Countries',
            ]
        ]);
        $this->set('record', $record);
        $this->set('_serialize',['record']);
        $titlesubModule = "View ".$this->titleModule;
        $breadCrumbs = [
            Router::url(['action' => 'index']) => "List ".$this->titleModule,
            Router::url(['action' => 'view',$id]) => $titlesubModule
        ];
        $this->set(compact('breadCrumbs','titlesubModule'));
        $this->layout = false;
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $record = $this->{$this->primaryModel}->newEntity();
        if ($this->request->is('post')) {
            $data = $this->request->getData();
            $data['date'] = $this->Utilities->generalitationDateFormat($data['date']);
            $data['bc_date'] = $this->Utilities->generalitationDateFormat($data['bc_date']);
            $data['invoice_date'] = $this->Utilities->generalitationDateFormat($data['invoice_date']);
            $data['bl_date'] = $this->Utilities->generalitationDateFormat($data['bl_date']);
            $data['register_date'] = $this->Utilities->generalitationDateFormat($data['register_date']);
            $record = $this->{$this->primaryModel}->patchEntity($record, $data,[
                'associated'=>[
                    'ReceptionsContainers',
                    'ReceptionsContainers.ReceptionsContainersDetails',
                ]
            ]);
            if ($this->{$this->primaryModel}->save($record)) {
                $this->Flash->success(__($this->Utilities->message_alert($this->titleModule,1)));
                $status = "SUCCESS";
                $message = "DATA BERHASIL DISIMPAN";
                $code   = 200;
                $errors = [];
            }else{
                $status = "ERROR";
                $message = "DATA GAGAL DISIMPAN";
                $code   = 50;
                $errors = call_user_func_array('array_merge', $record->errors());
            }
            $this->set(compact('status','code','message','errors'));
            $this->set('_serialize',['status','code','message','errors']);
            // $this->Flash->error(__($this->Utilities->message_alert($this->titleModule,2)));
        }
        $purchaseOrders = $this->{$this->primaryModel}->PurchaseOrders->find('list')->order([
            'PurchaseOrders.code' => 'ASC'
        ]);
        $importers = $this->{$this->primaryModel}->Importers->find('list')->order([
            'Importers.name' => 'ASC'
        ]);
        $senders = $this->{$this->primaryModel}->Senders->find('list')->order([
            'Senders.name' => 'ASC'
        ]);
        $this->loadModel('Countries');
        $countries = $this->Countries->find('list')->order([
            'Countries.name' => 'ASC'
        ]);
        $this->set(compact('record','purchaseOrders','senders','importers','countries'));
        $titlesubModule = "Tambah ".$this->titleModule;
        $breadCrumbs = [
            Router::url(['action' => 'index']) => "List ".$this->titleModule,
            Router::url(['action' => 'add']) => $titlesubModule
        ];
        $this->set(compact('breadCrumbs','titlesubModule'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Country id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $record = $this->{$this->primaryModel}->get($id, [
            'contain' => [
                'ReceptionsContainers.ReceptionsContainersDetails.Products',
                'ReceptionsContainers.ReceptionsContainersDetails.Countries',
            ]
        ]);
        $po = $this->{$this->primaryModel}->PurchaseOrders->get($record->purchase_order_id,[
            'contain' => [
                'Suppliers',
                'Currencies',
                'PurchaseOrdersDetails' => function($q){
                    return $q->contain([
                        'Products'
                    ])->select($this->{$this->primaryModel}->PurchaseOrders->PurchaseOrdersDetails)
                    ->select($this->{$this->primaryModel}->PurchaseOrders->PurchaseOrdersDetails->Products)
                    ->select([
                        'text' => '(CONCAT("[",`Products`.`code`,"] ",`Products`.`name`))'
                    ]);
                }
            ]
        ]);
        $record->purchase_order_date    = $po->date->format('d-m-Y');
        $record->supplier_po            = $po->supplier->name;
        $record->currency_name          = $po->currency->name;
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            $data['date'] = $this->Utilities->generalitationDateFormat($data['date']);
            $data['bc_date'] = $this->Utilities->generalitationDateFormat($data['bc_date']);
            $data['invoice_date'] = $this->Utilities->generalitationDateFormat($data['invoice_date']);
            $data['bl_date'] = $this->Utilities->generalitationDateFormat($data['bl_date']);
            $data['register_date'] = $this->Utilities->generalitationDateFormat($data['register_date']);
            if(empty($data['file'])){
                unset($data['file']);
            }
            $record = $this->{$this->primaryModel}->patchEntity($record, $data);
            if (empty($record->errors())) {
                if(!empty($data['rcd-del-id'])){
                    $rcDelArray = explode(";",$data['rcd-del-id']);
                    foreach($rcDelArray as $key => $rcDelId){
                        $rc = $this->{$this->primaryModel}->ReceptionsContainers->ReceptionsContainersDetails->get($rcDelId);
                        $this->{$this->primaryModel}->ReceptionsContainers->ReceptionsContainersDetails->delete($rc);
                    }
                }
                if(!empty($data['rc-del-id'])){
                    $rcDelArray = explode(";",$data['rc-del-id']);
                    foreach($rcDelArray as $key => $rcDelId){
                        $rc = $this->{$this->primaryModel}->ReceptionsContainers->get($rcDelId);
                        $this->{$this->primaryModel}->ReceptionsContainers->delete($rc);
                    }
                }
                $this->{$this->primaryModel}->save($record);
                $this->Flash->success(__($this->Utilities->message_alert($this->titleModule,3)));
                $status = "SUCCESS";
                $message = "DATA BERHASIL DISIMPAN";
                $code   = 200;
                $errors = [];
            }else{
                $status = "ERROR";
                $message = "DATA GAGAL DISIMPAN";
                $code   = 50;
                $errors = call_user_func_array('array_merge', $record->errors());
            }
            $this->set(compact('status','code','message','errors'));
            $this->set('_serialize',['status','code','message','errors']);
        }
        $record->date = $record->date->format('d-m-Y');
        $record->bc_date = $record->bc_date->format('d-m-Y');
        $record->bl_date = $record->bl_date->format('d-m-Y');
        $record->invoice_date = $record->invoice_date->format('d-m-Y');
        $record->register_date = $record->register_date->format('d-m-Y');
        $purchaseOrders = $this->{$this->primaryModel}->PurchaseOrders->find('list')->order([
            'PurchaseOrders.code' => 'ASC'
        ]);
        $importers = $this->{$this->primaryModel}->Importers->find('list')->order([
            'Importers.name' => 'ASC'
        ]);
        $senders = $this->{$this->primaryModel}->Senders->find('list')->order([
            'Senders.name' => 'ASC'
        ]);
        $this->loadModel('Countries');
        $countries = $this->Countries->find('list')->order([
            'Countries.name' => 'ASC'
        ]);
        $this->set(compact('record','purchaseOrders','senders','importers','countries','po'));
        $titlesubModule = "Edit ".$this->titleModule;
        $breadCrumbs = [
            Router::url(['action' => 'index']) => "List ".$this->titleModule,
            Router::url(['action' => 'edit',$id]) => $titlesubModule
        ];
        $this->set(compact('breadCrumbs','titlesubModule'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Country id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $record = $this->{$this->primaryModel}->get($id);
        if ($this->{$this->primaryModel}->delete($record)) {
            $code = 200;
            $message = __($this->Utilities->message_alert($this->titleModule,5));
            $status = 'success';
        } else {
            $code = 99;
            $message = __($this->Utilities->message_alert($this->titleModule,6));
            $status = 'error';
        }
        if($this->request->is('ajax')){
            $this->set('code',$code);
            $this->set('message',$message);
            $this->set('_serialize',['code','message']);
        }else{
            $this->Flash->{$status}($message);
            return $this->redirect(['action' => 'index']);
        }
    }

    
}
