<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Routing\Router;
use Cake\Core\Configure;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
/**
 * Laporan Neraca Controller
 *
 * @property \App\Model\Table\JournalsTable $Journals
 *
 * @method \App\Model\Entity\BusinessType[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class LaporanNeracaController extends AppController
{

    private $titleModule = "Laporan Neraca";
    private $primaryModel = "Journals";

    public function initialize()
    {
        parent::initialize();
        $this->set([
            'titleModule' => $this->titleModule,
            'primaryModel' => $this->primaryModel,
        ]);
        $this->Auth->allow();
    }

    function beforeFilter(\Cake\Event\Event $event){
        parent::beforeFilter($event);
    
        if(isset($this->Security) && $this->request->isAjax() && ($this->action = 'index' || $this->action = 'delete')){
    
            $this->Security->config('validatePost',false);
            //$this->getEventManager()->off($this->Csrf);
    
        }
    
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->loadModel('Journals');
        $this->loadModel('CodeAccounts');
        // JIKA FILTER QUERY ADA
        if(!empty($this->request->getQuery())){
            $query              = $this->request->getQuery();
            $start              = $query['date_1'];
            $end                = $query['date_2'];
            $output             = $query['output'];
            // JIKA KONDISI PER PERUSAHAAN//
            $addWhere = "";
            $codeAccountsAssetAktivaLancar = $this->CodeAccounts->find()
            ->where([
                'CodeAccounts.neraca' => 1,
                'CodeAccounts.code LIKE "1.01%"',
                // '(`CodeAccounts`.`lft` - `CodeAccounts`.`rght`) != -1'
            ])
            ->order([
                'CodeAccounts.code ASC'
            ])
            ->select($this->CodeAccounts)
            ->select([
                'amount' => '(
                    CASE
                        WHEN ((CodeAccounts.lft - CodeAccounts.rght) = -1) THEN
                            (
                                SELECT 
                                    (
                                        CASE WHEN (`CodeAccounts`.`normal_balance` = 0) THEN
                                            IFNULL(SUM(journals_details.debit - journals_details.credit),0)
                                        ELSE 
                                            IFNULL(SUM(journals_details.credit - journals_details.debit),0)
                                        END
                                    ) as AMT 
                                FROM journals_details
                                INNER JOIN journals on journals.id = journals_details.journal_id
                                WHERE 
                                journals_details.code_account_id = CodeAccounts.id
                                AND
                                (
                                    journals.date BETWEEN "'.$start.'" AND "'.$end.'"
                                    OR
                                    journals.date < "'.$start.'"
                                )'.
                                $addWhere
                                .'
                            )
                        ELSE
                            (
                                SELECT 
                                (
                                    CASE WHEN (`CDA`.`normal_balance` = 0) THEN
                                        IFNULL(SUM(journals_details.debit - journals_details.credit),0)
                                    ELSE 
                                        IFNULL(SUM(journals_details.credit - journals_details.debit),0)
                                    END
                                ) as AMT
                                FROM journals_details
                                INNER JOIN
                                journals on journals.id = journals_details.journal_id
                                INNER JOIN
                                code_accounts as CDA on CDA.id = journals_details.code_account_id 
                                WHERE
                                `CDA`.`lft` > `CodeAccounts`.`lft` AND CDA.rght < `CodeAccounts`.`rght`
                                AND (CDA.lft - CDA.rght) = -1
                                AND
                                (
                                    journals.date BETWEEN "'.$start.'" AND "'.$end.'"
                                    OR
                                    journals.date < "'.$start.'"
                                )'.
                                $addWhere
                                .'
                            )
                    END
                )'
            ]);

            
            $labaTahunBerjalan = $this->CodeAccounts->getLabaRugi($end);
            
            $codeAccountsAssetAktivaTidakLancar = $this->CodeAccounts->find()
            ->where([
                'CodeAccounts.neraca' => 1,
                'CodeAccounts.code LIKE "1.02%"',
                '(`CodeAccounts`.`lft` - `CodeAccounts`.`rght`) != -1'
            ])
            ->order([
                'CodeAccounts.code ASC'
            ])
            ->select($this->CodeAccounts)
            ->select([
                'amount' => '(
                    CASE
                        WHEN ((CodeAccounts.lft - CodeAccounts.rght) = -1) THEN
                            (
                                SELECT 
                                    (
                                        CASE WHEN (`CodeAccounts`.`normal_balance` = 0) THEN
                                            IFNULL(SUM(journals_details.debit - journals_details.credit),0)
                                        ELSE 
                                            IFNULL(SUM(journals_details.credit - journals_details.debit),0)
                                        END
                                    ) as AMT 
                                FROM journals_details
                                INNER JOIN journals on journals.id = journals_details.journal_id
                                WHERE 
                                journals_details.code_account_id = CodeAccounts.id
                                AND
                                (
                                    journals.date BETWEEN "'.$start.'" AND "'.$end.'"
                                    OR
                                    journals.date < "'.$start.'"
                                )'.
                                $addWhere
                                .'
                            )
                        ELSE
                            (
                                SELECT 
                                (
                                    CASE WHEN (`CDA`.`normal_balance` = 0) THEN
                                        IFNULL(SUM(journals_details.debit - journals_details.credit),0)
                                    ELSE 
                                        IFNULL(SUM(journals_details.credit - journals_details.debit),0)
                                    END
                                ) as AMT
                                FROM journals_details
                                INNER JOIN
                                journals on journals.id = journals_details.journal_id
                                INNER JOIN
                                code_accounts as CDA on CDA.id = journals_details.code_account_id 
                                WHERE
                                `CDA`.`lft` > `CodeAccounts`.`lft` AND CDA.rght < `CodeAccounts`.`rght`
                                AND (CDA.lft - CDA.rght) = -1
                                AND
                                (
                                    journals.date BETWEEN "'.$start.'" AND "'.$end.'"
                                    OR
                                    journals.date < "'.$start.'"
                                )'.
                                $addWhere
                                .'
                            )
                    END
                )'
            ]);

            $codeAccountsKewajiban = $this->CodeAccounts->find()
            ->where([
                'CodeAccounts.neraca' => 1,
                'CodeAccounts.code LIKE "2.%"',
                '(`CodeAccounts`.`lft` - `CodeAccounts`.`rght`) != -1'
            ])
            ->order([
                'CodeAccounts.code ASC'
            ])
            ->select($this->CodeAccounts)
            ->select([
                'amount' => '(
                    CASE
                        WHEN ((CodeAccounts.lft - CodeAccounts.rght) = -1) THEN
                            (
                                SELECT 
                                    (
                                        CASE WHEN (`CodeAccounts`.`normal_balance` = 0) THEN
                                            IFNULL(SUM(journals_details.debit - journals_details.credit),0)
                                        ELSE 
                                            IFNULL(SUM(journals_details.credit - journals_details.debit),0)
                                        END
                                    ) as AMT 
                                FROM journals_details
                                INNER JOIN journals on journals.id = journals_details.journal_id
                                WHERE 
                                journals_details.code_account_id = CodeAccounts.id
                                AND
                                (
                                    journals.date BETWEEN "'.$start.'" AND "'.$end.'"
                                    OR
                                    journals.date < "'.$start.'"
                                )'.
                                $addWhere
                                .'
                            )
                        ELSE
                            (
                                SELECT 
                                (
                                    CASE WHEN (`CDA`.`normal_balance` = 0) THEN
                                        IFNULL(SUM(journals_details.debit - journals_details.credit),0)
                                    ELSE 
                                        IFNULL(SUM(journals_details.credit - journals_details.debit),0)
                                    END
                                ) as AMT
                                FROM journals_details
                                INNER JOIN
                                journals on journals.id = journals_details.journal_id
                                INNER JOIN
                                code_accounts as CDA on CDA.id = journals_details.code_account_id 
                                WHERE
                                `CDA`.`lft` > `CodeAccounts`.`lft` AND CDA.rght < `CodeAccounts`.`rght`
                                AND (CDA.lft - CDA.rght) = -1
                                AND
                                (
                                    journals.date BETWEEN "'.$start.'" AND "'.$end.'"
                                    OR
                                    journals.date < "'.$start.'"
                                )'.
                                $addWhere
                                .'
                            )
                    END
                )'
            ]);

            $codeAccountsEkuitasModal = $this->CodeAccounts->find()
                ->where([
                    'CodeAccounts.neraca' => 1,
                    'CodeAccounts.code LIKE "3.01%"',
                ])
                ->order([
                    'CodeAccounts.code ASC'
                ])
                ->select($this->CodeAccounts)
                ->select([
                    'amount' => '(
                        CASE
                            WHEN ((CodeAccounts.lft - CodeAccounts.rght) = -1) THEN
                                (
                                    SELECT 
                                        (
                                            CASE WHEN (`CodeAccounts`.`normal_balance` = 0) THEN
                                                IFNULL(SUM(journals_details.debit - journals_details.credit),0)
                                            ELSE 
                                                IFNULL(SUM(journals_details.credit - journals_details.debit),0)
                                            END
                                        ) as AMT 
                                    FROM journals_details
                                    INNER JOIN journals on journals.id = journals_details.journal_id
                                    WHERE 
                                    journals_details.code_account_id = CodeAccounts.id
                                    AND
                                    (
                                        journals.date BETWEEN "'.$start.'" AND "'.$end.'"
                                        OR
                                        journals.date < "'.$start.'"
                                    )'.
                                    $addWhere
                                    .'
                                )
                            ELSE
                                (
                                    SELECT 
                                    (
                                        CASE WHEN (`CDA`.`normal_balance` = 0) THEN
                                            IFNULL(SUM(journals_details.debit - journals_details.credit),0)
                                        ELSE 
                                            IFNULL(SUM(journals_details.credit - journals_details.debit),0)
                                        END
                                    ) as AMT
                                    FROM journals_details
                                    INNER JOIN
                                    journals on journals.id = journals_details.journal_id
                                    INNER JOIN
                                    code_accounts as CDA on CDA.id = journals_details.code_account_id 
                                    WHERE
                                    `CDA`.`lft` > `CodeAccounts`.`lft` AND CDA.rght < `CodeAccounts`.`rght`
                                    AND (CDA.lft - CDA.rght) = -1
                                    AND
                                    (
                                        journals.date BETWEEN "'.$start.'" AND "'.$end.'"
                                        OR
                                        journals.date < "'.$start.'"
                                    )'.
                                    $addWhere
                                    .'
                                )
                        END
                    )'
            ]);

            $codeAccountsEkuitasLabaDitahan = $this->CodeAccounts->find()
                ->where([
                    'CodeAccounts.neraca' => 1,
                    'CodeAccounts.code LIKE "3.02%"',
                ])
                ->order([
                    'CodeAccounts.code ASC'
                ])
                ->select($this->CodeAccounts)
                ->select([
                    'amount' => '(
                        CASE
                            WHEN ((CodeAccounts.lft - CodeAccounts.rght) = -1) THEN
                                (CASE
                                    WHEN `CodeAccounts`.`code` = "3.02.002"  THEN 
                                        (('.$labaTahunBerjalan.') -
                                        (
                                            SELECT 
                                                (
                                                    CASE WHEN (`CodeAccounts`.`normal_balance` = 0) THEN
                                                        IFNULL(SUM(journals_details.debit - journals_details.credit),0)
                                                    ELSE 
                                                        IFNULL(SUM(journals_details.credit - journals_details.debit),0)
                                                    END
                                                ) as AMT 
                                            FROM journals_details
                                            INNER JOIN journals on journals.id = journals_details.journal_id
                                            WHERE 
                                            journals_details.code_account_id = CodeAccounts.id
                                            AND
                                            (
                                                journals.date BETWEEN "'.$start.'" AND "'.$end.'"
                                                OR
                                                journals.date < "'.$start.'"
                                            )'.
                                            $addWhere
                                            .'
                                        ))
                                    ELSE
                                        (
                                            SELECT 
                                                (
                                                    CASE WHEN (`CodeAccounts`.`normal_balance` = 0) THEN
                                                        IFNULL(SUM(journals_details.debit - journals_details.credit),0)
                                                    ELSE 
                                                        IFNULL(SUM(journals_details.credit - journals_details.debit),0)
                                                    END
                                                ) as AMT 
                                            FROM journals_details
                                            INNER JOIN journals on journals.id = journals_details.journal_id
                                            WHERE 
                                            journals_details.code_account_id = CodeAccounts.id
                                            AND
                                            (
                                                journals.date BETWEEN "'.$start.'" AND "'.$end.'"
                                                OR
                                                journals.date < "'.$start.'"
                                            )'.
                                            $addWhere
                                            .'
                                        )
                                END)
                                
                            ELSE
                                (
                                    
                                    (SELECT 
                                    (
                                        CASE WHEN (`CDA`.`normal_balance` = 0) THEN
                                            IFNULL(SUM(journals_details.debit - journals_details.credit),0)
                                        ELSE 
                                            IFNULL(SUM(journals_details.credit - journals_details.debit),0)
                                        END
                                    ) as AMT
                                    FROM journals_details
                                    INNER JOIN
                                    journals on journals.id = journals_details.journal_id
                                    INNER JOIN
                                    code_accounts as CDA on CDA.id = journals_details.code_account_id 
                                    WHERE
                                    `CDA`.`lft` > `CodeAccounts`.`lft` AND CDA.rght < `CodeAccounts`.`rght`
                                    AND (CDA.lft - CDA.rght) = -1
                                    AND
                                    (
                                        journals.date BETWEEN "'.$start.'" AND "'.$end.'"
                                        OR
                                        journals.date < "'.$start.'"
                                    )'.
                                    $addWhere
                                    .') - ('.($labaTahunBerjalan < 0 ? $labaTahunBerjalan * -1 : $labaTahunBerjalan).')
                                )
                        END
                    )'
            ]);

            
            //AKTIVA LANCAR//
            $codeAccountsAssetAktivaLancar = $codeAccountsAssetAktivaLancar->find('threaded')->toArray();
            if(!empty($codeAccountsAssetAktivaLancar)){
                $totalAssetAktivaLancar = $codeAccountsAssetAktivaLancar[0]->amount;
            }                  
            //END AKTIVA LANCAR//
            
            //AKTIVA TIDAK LANCAR//
            $codeAccountsAssetAktivaTidakLancar = $codeAccountsAssetAktivaTidakLancar->find('threaded')->toArray();
            if(!empty($codeAccountsAssetAktivaTidakLancar)){
                $totalAssetAktivaTidakLancar = $codeAccountsAssetAktivaTidakLancar[0]->amount;
            }                  
            //END AKTIVA TIDAK LANCAR//
            $totalAsset = $totalAssetAktivaLancar + $totalAssetAktivaTidakLancar;

            
            //KEWAJIBAN//
            $codeAccountsKewajiban = $codeAccountsKewajiban->find('threaded')->toArray();
            if(!empty($codeAccountsKewajiban)){
                $totalKewajiban = $codeAccountsKewajiban[0]->amount;
            }    
            
            //EKUITAS MODAL//
            $codeAccountsEkuitasModal = $codeAccountsEkuitasModal->find('threaded')->toArray();
            if(!empty($codeAccountsEkuitasModal)){
                $totalEkuitasModal = $codeAccountsEkuitasModal[0]->amount;
            }   
            
            //EKUITAS LABA DITAHAN//
            $codeAccountsEkuitasLabaDitahan = $codeAccountsEkuitasLabaDitahan->find('threaded')->toArray();
            if(!empty($codeAccountsEkuitasLabaDitahan)){
                $totalEkuitasLabaDitahan = $codeAccountsEkuitasLabaDitahan[0]->amount;
            } 
            $totalEkuitas = $totalEkuitasModal + $totalEkuitasLabaDitahan;
            
            
            $this->set(compact(
                'start',
                'end',
                'businessType',
                'codeAccountsAssetAktivaLancar',
                'totalAssetAktivaLancar',
                'codeAccountsAssetAktivaTidakLancar',
                'totalAssetAktivaTidakLancar',
                'totalAsset',
                'codeAccountsKewajiban',
                'totalKewajiban',
                'codeAccountsEkuitasModal',
                'totalEkuitasLabaDitahan',
                'codeAccountsEkuitasLabaDitahan',
                'totalEkuitasModal',
                'totalEkuitas'
            ));
            $this->viewBuilder()->setLayout(false);
            if($output == "pdf"){
                    Configure::write('CakePdf', [
                        'engine' => [
                            'className' => 'CakePdf.WkHtmlToPdf',
                            'binary' => env("WKHTML_PATH","C:\\wkhtmltopdf\\bin\\wkhtmltopdf.exe"),
                            'options' => [
                                'print-media-type' => false,
                                'orientation' => 'potrait',
                                'outline' => true,
                                'dpi' => 96
                            ],
                        ],
                    ]);
                    $this->viewBuilder()->setClassName('CakePdf.Pdf');
            }elseif($output == "excel"){
                $this->autoRender = false;
                $spreadsheet = new Spreadsheet();
                $sheet = $spreadsheet->getActiveSheet();
                $sheet->getColumnDimension('A')->setAutoSize(true);
                $sheet->getColumnDimension('B')->setAutoSize(true);

                $sheet->mergeCells('A1:B1');
                $sheet->mergeCells('A2:B2');
                $sheet->mergeCells('A3:B3');

                $sheet->setCellValue('A1', 'LAPORAN NERACA');
                $sheet->setCellValue('A2', 'PERIODE : '.strtoupper($this->Utilities->indonesiaDateFormat($start)).' S.D '.strtoupper($this->Utilities->indonesiaDateFormat($end)));
                $sheet->setCellValue('A3', !empty($businessType) ? 'JENIS USAHA : '.$this->Utilities->businessTypeVal($businessType->type).' - '.$businessType->name : 'SEMUA JENIS USAHA');

                // $sheet->getStyle('A1:B3')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $sheet->getStyle('A1:B3')->getFont()->setBold(true);

                $styleArray = [
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            'color' => ['argb' => '333333'],
                        ]
                    ],
                ];

                $row = 4;

                // ASSET-->
                $sheet->mergeCells('A'.$row.':B'.$row);
                $sheet->setCellValue('A'.$row++, 'ASSET');
                // ASSET AKTIVA LANCAR-->
                if(!empty($codeAccountsAssetAktivaLancar)):
                    $row = $this->Utilities->createLabaRugiExcel($codeAccountsAssetAktivaLancar, $sheet, $row);
                endif;
                // ASSET TIDAK AKTIVA LANCAR-->
                if(!empty($codeAccountsAssetAktivaTidakLancar)):
                    $row = $this->Utilities->createLabaRugiExcel($codeAccountsAssetAktivaTidakLancar, $sheet, $row);
                endif;
                $sheet->getStyle('B'.$row)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
                $sheet->setCellValue('A'.$row, 'TOTAL ASSET');
                $sheet->setCellValue('B'.$row++, number_format($totalAsset));
                // KEWAJIBAN-->
                $sheet->getStyle('B'.$row)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
                $sheet->mergeCells('A'.$row.':B'.$row);
                $sheet->setCellValue('A'.$row++, 'KEWAJIBAN');
                if(!empty($codeAccountsKewajiban)):
                    $row = $this->Utilities->createLabaRugiExcel($codeAccountsKewajiban, $sheet, $row);
                endif;
                $sheet->getStyle('B'.$row)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
                $sheet->setCellValue('A'.$row, 'TOTAL KEWAJIBAN');
                $sheet->setCellValue('B'.$row++, number_format($totalKewajiban));
                // EKUITAS-->
                $sheet->getStyle('B'.$row)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
                $sheet->mergeCells('A'.$row.':B'.$row);
                $sheet->setCellValue('A'.$row++, 'EKUITAS');
                if(!empty($codeAccountsEkuitasModal)):
                    $row = $this->Utilities->createLabaRugiExcel($codeAccountsEkuitasModal, $sheet, $row);
                endif;
                if(!empty($codeAccountsEkuitasLabaDitahan)):
                    $row = $this->Utilities->createLabaRugiExcel($codeAccountsEkuitasLabaDitahan, $sheet, $row);
                endif;
                $sheet->getStyle('B'.$row)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
                $sheet->setCellValue('A'.$row, 'TOTAL EKUITAS');
                $sheet->setCellValue('B'.$row++, number_format($totalEkuitas));
                $sheet->getStyle('B'.$row)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
                $sheet->setCellValue('A'.$row, 'TOTAL EKUITAS + KEWAJIBAN');
                $sheet->setCellValue('B'.$row++, number_format($totalEkuitas+$totalKewajiban));

                $writer = new Xlsx($spreadsheet);
                header('Content-Type: application/vnd.ms-excel');
                header('Content-Disposition: attachment;filename="LAPORAN_NERACA-' . time() . '.xlsx"');
                header('Cache-Control: max-age=0');
                $writer->save('php://output');
                exit();
            }else{
                $this->render('./pdf/index');
            }
            
        }else{
        }
           
    }

}
