SELECT
  provinsi,
   kota,
  IFNULL(laki_standard,0) as laki_standard,
  IFNULL(perempuan_standard,0) as perempuan_standard,
  IFNULL(laki_standard+perempuan_standard,0) as jumlah_standard,

  IFNULL(laki_resiko,0) as laki_resiko,
  IFNULL(perempuan_resiko,0) as perempuan_resiko,
  IFNULL(laki_resiko+perempuan_resiko,0) as jumlah_resiko
  
FROM (SELECT
  `Provinces`.`name` AS `provinsi`, 
   `Cities`.`name` AS `kota`,
  (
    COUNT(
      DISTINCT (
        CASE WHEN 
          ((sistol != 0 OR sistol IS NOT NULL) AND (diastol != 0 OR diastol IS NOT NULL)) AND
          ((tinggi_badan != 0.00 OR tinggi_badan IS NOT NULL) AND (berat_badan != 0.00 OR berat_badan IS NOT NULL)) AND
          (lingkar_perut != 0.00 OR lingkar_perut IS NOT NULL) AND
          (kolesterol != 0.00 OR kolesterol IS NOT NULL)
          AND (
            YEAR(date) - YEAR(birthdate) >= 15 
          )
          AND gender = 'M' 
        THEN patient_id END
      )
    )
  ) AS `laki_standard`,
  (
    COUNT(
      DISTINCT (
        CASE WHEN 
          ((sistol != 0 OR sistol IS NOT NULL) AND (diastol != 0 OR diastol IS NOT NULL)) AND
          ((tinggi_badan != 0.00 OR tinggi_badan IS NOT NULL) AND (berat_badan != 0.00 OR berat_badan IS NOT NULL)) AND
          (lingkar_perut != 0.00 OR lingkar_perut IS NOT NULL) AND
          (kolesterol != 0.00 OR kolesterol IS NOT NULL)
          AND (
            YEAR(date) - YEAR(birthdate) >= 15 
          )
          AND gender = 'F' 
        THEN patient_id END
      )
    )
  ) AS `perempuan_standard`,


  (
    COUNT(
      DISTINCT (
        CASE WHEN 
          ((merokok = 1) OR
                    (gula >= 200) OR
                    (sistol >= 140 OR diastol >= 90) OR
                    (iva = 1) OR
                    ((berat_badan / (tinggi_badan * tinggi_badan / 10000)) >= 27) OR
                    (lingkar_perut > 90))
          AND (
            YEAR(date) - YEAR(birthdate) >= 15 
          )
          AND gender = 'M' 
        THEN patient_id END
      )
    )
  ) AS `laki_resiko`,

  (
    COUNT(
      DISTINCT (
        CASE WHEN 
          ((merokok = 1) OR
          (gula >= 200) OR
          (sistol >= 140 OR diastol >= 90) OR
          (iva = 1) OR
          ((berat_badan / (tinggi_badan * tinggi_badan / 10000)) >= 27) OR
          (lingkar_perut > 90))
          AND (
            YEAR(date) - YEAR(birthdate) >= 15 
          )
          AND gender = 'F' 
        THEN patient_id END
      )
    )
  ) AS `perempuan_resiko`
FROM 
  `registrations`
  INNER JOIN patients ON patients.id = registrations.patient_id
  RIGHT JOIN `cities` `Cities` ON Cities.id = registrations.city_id 
  AND year(registrations.date) = 2019 
  AND registrations.id IN (
    SELECT 
      MIN(R.id) 
    FROM 
      `registrations` AS R 
    WHERE 
      year(R.date) = 2019
    GROUP BY 
      patient_id
  ) 
  RIGHT JOIN `provinces` `Provinces` ON Provinces.id = Cities.province_id 

GROUP BY 
  `Cities`.`id`
) as coba