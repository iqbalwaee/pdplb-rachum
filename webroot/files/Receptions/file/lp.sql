SELECT
  provinsi,
  kota,
  IFNULL(lp_ya,0),
  IFNULL(lp_tidak,0),
  IFNULL(lp_ya+lp_tidak,0) as jumlah_diperiksa_gula,
  FORMAT(IFNULL((lp_ya/(lp_ya+lp_tidak))*100,0),2) as persen_gula,
  lp_pria,
  lp_wanita,
  lp_15_19,
  lp_20_24,
  lp_45_54,
  lp_55_59,
  lp_60_69,
  lp_70
FROM (SELECT
  `Provinces`.`name` AS `provinsi`, 
  `Cities`.`name` AS `kota`,
    -- kadar_lp_darah
   (
    COUNT(
      DISTINCT (
        CASE WHEN 
          ((lingkar_perut > 90 AND gender = "M") OR (lingkar_perut > 80 AND gender = "F"))
          AND (
            YEAR(date) - YEAR(birthdate) >= 15 
          )
        THEN patient_id END
      )
    )
  ) AS `lp_ya`, 
  (
    COUNT(
      DISTINCT (
        CASE WHEN 
          ((lingkar_perut <= 90 AND gender = "M") OR (lingkar_perut <= 80 AND gender = "F"))
          AND (
            YEAR(date) - YEAR(birthdate) >= 15 
          )
        THEN patient_id END
      )
    )
  ) AS `lp_tidak`,
  (
    COUNT(
      DISTINCT (
        CASE WHEN lingkar_perut > 90
        AND (
          YEAR(date) - YEAR(birthdate) >= 15 
        )
        AND gender = 'M' 
        THEN patient_id END
      )
    )
  ) AS `lp_pria`,
  (
    COUNT(
      DISTINCT (
        CASE WHEN lingkar_perut > 80
        AND (
          YEAR(date) - YEAR(birthdate) >= 15 
        )
        AND gender = 'F' 
        THEN patient_id END
      )
    )
  ) AS `lp_wanita`,
  (
    COUNT(
      DISTINCT (
        CASE WHEN (((lingkar_perut > 90 AND gender = "M") OR (lingkar_perut > 80 AND gender = "F")) OR ((lingkar_perut <= 90 AND gender = "M") OR (lingkar_perut <= 80 AND gender = "F")))
        AND (
          YEAR(date) - YEAR(birthdate) BETWEEN 15 AND 19 
        )
        THEN patient_id END
      )
    )
  ) AS `lp_15_19`,
  (
    COUNT(
      DISTINCT (
        CASE WHEN (((lingkar_perut > 90 AND gender = "M") OR (lingkar_perut > 80 AND gender = "F")) OR ((lingkar_perut <= 90 AND gender = "M") OR (lingkar_perut <= 80 AND gender = "F")))
        AND (
          YEAR(date) - YEAR(birthdate) BETWEEN 20 AND 44 
        )
        THEN patient_id END
      )
    )
  ) AS `lp_20_24`,
  ( 
    COUNT(
      DISTINCT (
        CASE WHEN (((lingkar_perut > 90 AND gender = "M") OR (lingkar_perut > 80 AND gender = "F")) OR ((lingkar_perut <= 90 AND gender = "M") OR (lingkar_perut <= 80 AND gender = "F")))
        AND (
          YEAR(date) - YEAR(birthdate) BETWEEN 45 AND 54 
        )
        THEN patient_id END
      )
    )
  ) AS `lp_45_54`,
  ( 
    COUNT(
      DISTINCT (
        CASE WHEN (((lingkar_perut > 90 AND gender = "M") OR (lingkar_perut > 80 AND gender = "F")) OR ((lingkar_perut <= 90 AND gender = "M") OR (lingkar_perut <= 80 AND gender = "F")))
        AND (
          YEAR(date) - YEAR(birthdate) BETWEEN 55 AND 59 
        )
        THEN patient_id END
      )
    )
  ) AS `lp_55_59`,
  
  ( 
    COUNT(
      DISTINCT (
        CASE WHEN (((lingkar_perut > 90 AND gender = "M") OR (lingkar_perut > 80 AND gender = "F")) OR ((lingkar_perut <= 90 AND gender = "M") OR (lingkar_perut <= 80 AND gender = "F")))
        AND (
          YEAR(date) - YEAR(birthdate) BETWEEN 60 AND 69
        )
        THEN patient_id END
      )
    )
  ) AS `lp_60_69`,
  
  ( 
    COUNT(
      DISTINCT (
        CASE WHEN (((lingkar_perut > 90 AND gender = "M") OR (lingkar_perut > 80 AND gender = "F")) OR ((lingkar_perut <= 90 AND gender = "M") OR (lingkar_perut <= 80 AND gender = "F")))
        AND (
          YEAR(date) - YEAR(birthdate) >= 70
        )
        THEN patient_id END
      )
    )
  ) AS `lp_70`
FROM 
  `registrations`
  INNER JOIN patients ON patients.id = registrations.patient_id
  RIGHT JOIN `cities` `Cities` ON Cities.id = registrations.city_id 
  AND year(registrations.date) = 2019 
  AND registrations.id IN (
    SELECT 
      MIN(R.id) 
    FROM 
      `registrations` AS R 
    WHERE 
      year(R.date) = 2019
    GROUP BY 
      patient_id
  ) 
  RIGHT JOIN `provinces` `Provinces` ON Provinces.id = Cities.province_id 

GROUP BY 
  `Cities`.`id`
) as coba