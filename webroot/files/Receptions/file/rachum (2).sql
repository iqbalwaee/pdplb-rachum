/*
 Navicat Premium Data Transfer

 Source Server         : CITRALARAS
 Source Server Type    : MySQL
 Source Server Version : 50727
 Source Host           : 103.82.241.204:3306
 Source Schema         : rachum

 Target Server Type    : MySQL
 Target Server Version : 50727
 File Encoding         : 65001

 Date: 08/03/2020 11:50:20
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for acl_phinxlog
-- ----------------------------
DROP TABLE IF EXISTS `acl_phinxlog`;
CREATE TABLE `acl_phinxlog`  (
  `version` bigint(20) NOT NULL,
  `migration_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `start_time` timestamp(0) NULL DEFAULT NULL,
  `end_time` timestamp(0) NULL DEFAULT NULL,
  `breakpoint` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`version`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of acl_phinxlog
-- ----------------------------
INSERT INTO `acl_phinxlog` VALUES (20141229162641, 'CakePhpDbAcl', '2018-02-01 10:04:00', '2018-02-01 10:04:01', 0);

-- ----------------------------
-- Table structure for acos
-- ----------------------------
DROP TABLE IF EXISTS `acos`;
CREATE TABLE `acos`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NULL DEFAULT NULL,
  `name` varchar(225) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `model` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `foreign_key` int(11) NULL DEFAULT NULL,
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `lft` int(11) NULL DEFAULT NULL,
  `rght` int(11) NULL DEFAULT NULL,
  `status` tinyint(1) NULL DEFAULT 0,
  `sort` int(11) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `lft`(`lft`, `rght`) USING BTREE,
  INDEX `alias`(`alias`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 454 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of acos
-- ----------------------------
INSERT INTO `acos` VALUES (1, NULL, 'controllers', NULL, NULL, 'controllers', 1, 186, 0, 0);
INSERT INTO `acos` VALUES (2, 1, 'Error', NULL, NULL, 'Error', 2, 3, 1, 0);
INSERT INTO `acos` VALUES (3, 1, 'Pages', NULL, NULL, 'Pages', 4, 15, 1, 0);
INSERT INTO `acos` VALUES (19, 1, 'Acl', NULL, NULL, 'Acl', 16, 17, 1, 0);
INSERT INTO `acos` VALUES (20, 1, 'Bake', NULL, NULL, 'Bake', 18, 19, 1, 0);
INSERT INTO `acos` VALUES (21, 1, 'DebugKit', NULL, NULL, 'DebugKit', 20, 47, 1, 0);
INSERT INTO `acos` VALUES (22, 21, 'Composer', NULL, NULL, 'Composer', 21, 24, 1, 0);
INSERT INTO `acos` VALUES (23, 22, 'checkDependencies', NULL, NULL, 'checkDependencies', 22, 23, 1, 0);
INSERT INTO `acos` VALUES (24, 21, 'MailPreview', NULL, NULL, 'MailPreview', 25, 32, 1, 0);
INSERT INTO `acos` VALUES (25, 24, 'index', NULL, NULL, 'index', 26, 27, 1, 0);
INSERT INTO `acos` VALUES (26, 24, 'sent', NULL, NULL, 'sent', 28, 29, 1, 0);
INSERT INTO `acos` VALUES (27, 24, 'email', NULL, NULL, 'email', 30, 31, 1, 0);
INSERT INTO `acos` VALUES (28, 21, 'Panels', NULL, NULL, 'Panels', 33, 38, 1, 0);
INSERT INTO `acos` VALUES (29, 28, 'index', NULL, NULL, 'index', 34, 35, 1, 0);
INSERT INTO `acos` VALUES (30, 28, 'view', NULL, NULL, 'view', 36, 37, 1, 4);
INSERT INTO `acos` VALUES (31, 21, 'Requests', NULL, NULL, 'Requests', 39, 42, 1, 0);
INSERT INTO `acos` VALUES (32, 31, 'view', NULL, NULL, 'view', 40, 41, 1, 4);
INSERT INTO `acos` VALUES (33, 21, 'Toolbar', NULL, NULL, 'Toolbar', 43, 46, 1, 0);
INSERT INTO `acos` VALUES (34, 33, 'clearCache', NULL, NULL, 'clearCache', 44, 45, 1, 0);
INSERT INTO `acos` VALUES (35, 1, 'Migrations', NULL, NULL, 'Migrations', 48, 49, 1, 0);
INSERT INTO `acos` VALUES (56, 1, 'AuditStash', NULL, NULL, 'AuditStash', 50, 51, 1, 0);
INSERT INTO `acos` VALUES (59, 1, 'Josegonzalez\\Upload', NULL, NULL, 'Josegonzalez\\Upload', 52, 53, 1, 0);
INSERT INTO `acos` VALUES (60, 1, 'AppSettings', NULL, NULL, 'AppSettings', 54, 57, 0, 1000);
INSERT INTO `acos` VALUES (61, 60, 'index', NULL, NULL, 'index', 55, 56, 0, 0);
INSERT INTO `acos` VALUES (62, 1, 'Dashboard', NULL, NULL, 'Dashboard', 58, 61, 0, 0);
INSERT INTO `acos` VALUES (63, 62, 'index', NULL, NULL, 'index', 59, 60, 0, 0);
INSERT INTO `acos` VALUES (64, 1, 'Errors', NULL, NULL, 'Errors', 62, 65, 1, 0);
INSERT INTO `acos` VALUES (65, 64, 'unauthorized', NULL, NULL, 'unauthorized', 63, 64, 1, 0);
INSERT INTO `acos` VALUES (73, 3, 'index', NULL, NULL, 'index', 5, 6, 1, 0);
INSERT INTO `acos` VALUES (74, 3, 'logout', NULL, NULL, 'logout', 7, 8, 1, 0);
INSERT INTO `acos` VALUES (75, 3, 'editProfile', NULL, NULL, 'editProfile', 9, 10, 1, 0);
INSERT INTO `acos` VALUES (76, 3, 'activitiesLog', NULL, NULL, 'activitiesLog', 11, 12, 1, 0);
INSERT INTO `acos` VALUES (77, 1, 'Master User', NULL, NULL, 'Users', 66, 77, 0, 801);
INSERT INTO `acos` VALUES (78, 77, 'index', NULL, NULL, 'index', 67, 68, 0, 0);
INSERT INTO `acos` VALUES (79, 77, 'view', NULL, NULL, 'view', 69, 70, 0, 4);
INSERT INTO `acos` VALUES (80, 77, 'add', NULL, NULL, 'add', 71, 72, 0, 1);
INSERT INTO `acos` VALUES (81, 77, 'edit', NULL, NULL, 'edit', 73, 74, 0, 2);
INSERT INTO `acos` VALUES (82, 77, 'delete', NULL, NULL, 'delete', 75, 76, 0, 3);
INSERT INTO `acos` VALUES (89, 3, NULL, NULL, NULL, 'uploadMedia', 13, 14, 1, 0);
INSERT INTO `acos` VALUES (357, 1, 'Master Group', NULL, NULL, 'UserGroups', 84, 97, 0, 802);
INSERT INTO `acos` VALUES (358, 357, 'index', NULL, NULL, 'index', 85, 86, 0, 0);
INSERT INTO `acos` VALUES (359, 357, 'view', NULL, NULL, 'view', 87, 88, 0, 1);
INSERT INTO `acos` VALUES (360, 357, 'add', NULL, NULL, 'add', 89, 90, 0, 2);
INSERT INTO `acos` VALUES (361, 357, 'edit', NULL, NULL, 'edit', 91, 92, 0, 3);
INSERT INTO `acos` VALUES (362, 357, 'delete', NULL, NULL, 'delete', 93, 94, 0, 4);
INSERT INTO `acos` VALUES (363, 357, 'configure', NULL, NULL, 'configure', 95, 96, 0, 5);
INSERT INTO `acos` VALUES (364, 1, NULL, NULL, NULL, 'CakePdf', 98, 99, 1, 0);
INSERT INTO `acos` VALUES (365, 1, NULL, NULL, NULL, 'WyriHaximus\\TwigView', 100, 101, 1, 0);
INSERT INTO `acos` VALUES (411, 1, 'Master Negara', NULL, NULL, 'Countries', 102, 113, 0, 101);
INSERT INTO `acos` VALUES (412, 411, NULL, NULL, NULL, 'index', 103, 104, 0, 0);
INSERT INTO `acos` VALUES (413, 411, NULL, NULL, NULL, 'view', 105, 106, 0, 1);
INSERT INTO `acos` VALUES (414, 411, NULL, NULL, NULL, 'add', 107, 108, 0, 2);
INSERT INTO `acos` VALUES (415, 411, NULL, NULL, NULL, 'edit', 109, 110, 0, 3);
INSERT INTO `acos` VALUES (416, 411, NULL, NULL, NULL, 'delete', 111, 112, 0, 4);
INSERT INTO `acos` VALUES (418, 1, 'Master Mata Uang', NULL, NULL, 'Currencies', 114, 125, 0, 102);
INSERT INTO `acos` VALUES (419, 418, NULL, NULL, NULL, 'index', 115, 116, 0, 0);
INSERT INTO `acos` VALUES (420, 418, NULL, NULL, NULL, 'view', 117, 118, 0, 1);
INSERT INTO `acos` VALUES (421, 418, NULL, NULL, NULL, 'add', 119, 120, 0, 2);
INSERT INTO `acos` VALUES (422, 418, NULL, NULL, NULL, 'edit', 121, 122, 0, 3);
INSERT INTO `acos` VALUES (423, 418, NULL, NULL, NULL, 'delete', 123, 124, 0, 4);
INSERT INTO `acos` VALUES (424, 1, 'Master Customer', NULL, NULL, 'Customers', 126, 137, 0, 103);
INSERT INTO `acos` VALUES (425, 424, NULL, NULL, NULL, 'index', 127, 128, 0, 0);
INSERT INTO `acos` VALUES (426, 424, NULL, NULL, NULL, 'view', 129, 130, 0, 1);
INSERT INTO `acos` VALUES (427, 424, NULL, NULL, NULL, 'add', 131, 132, 0, 2);
INSERT INTO `acos` VALUES (428, 424, NULL, NULL, NULL, 'edit', 133, 134, 0, 3);
INSERT INTO `acos` VALUES (429, 424, NULL, NULL, NULL, 'delete', 135, 136, 0, 4);
INSERT INTO `acos` VALUES (430, 1, 'Master Importir', NULL, NULL, 'Importers', 138, 149, 0, 104);
INSERT INTO `acos` VALUES (431, 430, NULL, NULL, NULL, 'index', 139, 140, 0, 0);
INSERT INTO `acos` VALUES (432, 430, NULL, NULL, NULL, 'view', 141, 142, 0, 1);
INSERT INTO `acos` VALUES (433, 430, NULL, NULL, NULL, 'add', 143, 144, 0, 2);
INSERT INTO `acos` VALUES (434, 430, NULL, NULL, NULL, 'edit', 145, 146, 0, 3);
INSERT INTO `acos` VALUES (435, 430, NULL, NULL, NULL, 'delete', 147, 148, 0, 4);
INSERT INTO `acos` VALUES (436, 1, 'Master Supplier', NULL, NULL, 'Suppliers', 150, 161, 0, 105);
INSERT INTO `acos` VALUES (437, 436, NULL, NULL, NULL, 'index', 151, 152, 0, 0);
INSERT INTO `acos` VALUES (438, 436, NULL, NULL, NULL, 'view', 153, 154, 0, 1);
INSERT INTO `acos` VALUES (439, 436, NULL, NULL, NULL, 'add', 155, 156, 0, 2);
INSERT INTO `acos` VALUES (440, 436, NULL, NULL, NULL, 'edit', 157, 158, 0, 3);
INSERT INTO `acos` VALUES (441, 436, NULL, NULL, NULL, 'delete', 159, 160, 0, 4);
INSERT INTO `acos` VALUES (442, 1, 'Master Barang', NULL, NULL, 'Products', 162, 173, 0, 106);
INSERT INTO `acos` VALUES (443, 442, NULL, NULL, NULL, 'index', 163, 164, 0, 0);
INSERT INTO `acos` VALUES (444, 442, NULL, NULL, NULL, 'view', 165, 166, 0, 1);
INSERT INTO `acos` VALUES (445, 442, NULL, NULL, NULL, 'add', 167, 168, 0, 2);
INSERT INTO `acos` VALUES (446, 442, NULL, NULL, NULL, 'edit', 169, 170, 0, 3);
INSERT INTO `acos` VALUES (447, 442, NULL, NULL, NULL, 'delete', 171, 172, 0, 4);
INSERT INTO `acos` VALUES (448, 1, NULL, NULL, NULL, 'Locations', 174, 185, 0, 107);
INSERT INTO `acos` VALUES (449, 448, NULL, NULL, NULL, 'index', 175, 176, 0, 0);
INSERT INTO `acos` VALUES (450, 448, NULL, NULL, NULL, 'view', 177, 178, 0, 1);
INSERT INTO `acos` VALUES (451, 448, NULL, NULL, NULL, 'add', 179, 180, 0, 2);
INSERT INTO `acos` VALUES (452, 448, NULL, NULL, NULL, 'edit', 181, 182, 0, 3);
INSERT INTO `acos` VALUES (453, 448, NULL, NULL, NULL, 'delete', 183, 184, 0, 4);

-- ----------------------------
-- Table structure for app_settings
-- ----------------------------
DROP TABLE IF EXISTS `app_settings`;
CREATE TABLE `app_settings`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `keyField` varchar(225) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `valueField` varchar(225) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `type` enum('text','long text','image','select') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of app_settings
-- ----------------------------
INSERT INTO `app_settings` VALUES (1, 'App.Name', 'PT. RACHUM AMANA BERKAH', 'text', 0);
INSERT INTO `app_settings` VALUES (2, 'App.Logo', '/webroot/img/logo.png', 'image', 0);
INSERT INTO `app_settings` VALUES (3, 'App.Logo.Login', '/webroot/img/logo_login.png', 'image', 0);
INSERT INTO `app_settings` VALUES (4, 'App.Logo.Width', '160', 'text', 0);
INSERT INTO `app_settings` VALUES (5, 'App.Logo.Height', '28', 'text', 0);
INSERT INTO `app_settings` VALUES (6, 'App.Logo.Login.Width', '400', 'text', 0);
INSERT INTO `app_settings` VALUES (7, 'App.Logo.Login.Height', '71', 'text', 0);
INSERT INTO `app_settings` VALUES (8, 'App.Login.Cover', '/webroot/assets/img/cover_login.jpg', 'image', 0);
INSERT INTO `app_settings` VALUES (9, 'App.Description', 'APLIKASI MANAGEMENT SALES, INVENTORY DAN ACCOUNTING', 'long text', 0);
INSERT INTO `app_settings` VALUES (10, 'App.Favico', '/webroot/img/favico.png', 'long text', 0);

-- ----------------------------
-- Table structure for aros
-- ----------------------------
DROP TABLE IF EXISTS `aros`;
CREATE TABLE `aros`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NULL DEFAULT NULL,
  `model` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `foreign_key` int(11) NULL DEFAULT NULL,
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `lft` int(11) NULL DEFAULT NULL,
  `rght` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `lft`(`lft`, `rght`) USING BTREE,
  INDEX `alias`(`alias`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of aros
-- ----------------------------
INSERT INTO `aros` VALUES (1, NULL, 'UserGroups', 1, NULL, 1, 4);

-- ----------------------------
-- Table structure for aros_acos
-- ----------------------------
DROP TABLE IF EXISTS `aros_acos`;
CREATE TABLE `aros_acos`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `aro_id` int(11) NOT NULL,
  `aco_id` int(11) NOT NULL,
  `_create` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0',
  `_read` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0',
  `_update` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0',
  `_delete` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `aro_id`(`aro_id`, `aco_id`) USING BTREE,
  INDEX `aco_id`(`aco_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 131 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of aros_acos
-- ----------------------------
INSERT INTO `aros_acos` VALUES (1, 1, 62, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (2, 1, 63, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (4, 1, 67, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (5, 1, 69, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (6, 1, 70, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (7, 1, 71, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (8, 1, 68, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (9, 1, 77, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (10, 1, 78, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (11, 1, 80, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (12, 1, 81, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (13, 1, 82, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (14, 1, 79, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (16, 1, 60, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (17, 1, 61, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (35, 1, 357, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (36, 1, 360, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (37, 1, 359, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (38, 1, 358, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (39, 1, 362, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (40, 1, 361, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (42, 1, 367, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (43, 1, 371, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (44, 1, 370, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (46, 1, 368, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (47, 1, 363, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (49, 1, 377, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (50, 1, 376, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (52, 1, 374, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (53, 1, 373, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (57, 1, 379, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (58, 1, 380, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (60, 1, 382, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (61, 1, 383, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (62, 1, 384, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (63, 1, 385, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (64, 1, 386, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (66, 1, 391, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (67, 1, 390, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (68, 1, 389, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (69, 1, 388, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (70, 1, 392, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (72, 1, 396, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (73, 1, 395, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (74, 1, 394, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (75, 1, 398, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (76, 1, 397, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (78, 1, 401, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (79, 1, 400, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (80, 1, 404, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (81, 1, 403, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (82, 1, 402, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (84, 1, 410, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (85, 1, 409, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (86, 1, 408, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (87, 1, 407, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (88, 1, 406, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (89, 1, 411, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (90, 1, 412, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (91, 1, 413, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (92, 1, 414, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (93, 1, 415, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (94, 1, 416, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (95, 1, 418, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (96, 1, 419, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (97, 1, 423, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (98, 1, 420, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (99, 1, 421, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (100, 1, 422, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (101, 1, 424, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (102, 1, 425, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (103, 1, 426, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (104, 1, 427, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (105, 1, 428, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (106, 1, 429, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (107, 1, 430, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (108, 1, 431, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (109, 1, 432, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (110, 1, 433, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (111, 1, 434, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (112, 1, 435, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (113, 1, 436, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (114, 1, 437, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (115, 1, 438, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (116, 1, 439, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (117, 1, 440, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (118, 1, 441, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (119, 1, 442, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (120, 1, 443, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (121, 1, 444, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (122, 1, 445, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (123, 1, 446, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (124, 1, 447, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (125, 1, 448, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (126, 1, 449, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (127, 1, 450, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (128, 1, 451, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (129, 1, 452, '1', '1', '1', '1');
INSERT INTO `aros_acos` VALUES (130, 1, 453, '1', '1', '1', '1');

-- ----------------------------
-- Table structure for audit_logs
-- ----------------------------
DROP TABLE IF EXISTS `audit_logs`;
CREATE TABLE `audit_logs`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `timestamp` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0),
  `user_id` int(11) NULL DEFAULT NULL,
  `controller` varchar(225) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `_action` varchar(225) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `type` varchar(250) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `primary_key` int(11) NULL DEFAULT NULL,
  `source` varchar(250) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `parent_source` varchar(250) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `original` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `changed` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `meta` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for countries
-- ----------------------------
DROP TABLE IF EXISTS `countries`;
CREATE TABLE `countries`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_by` int(11) NULL DEFAULT NULL,
  `created` datetime(0) NULL DEFAULT NULL,
  `modified_by` int(11) NULL DEFAULT NULL,
  `modified` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 258 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of countries
-- ----------------------------
INSERT INTO `countries` VALUES (1, 'Afghanistan', 1, 1, '2018-02-12 15:21:06', 1, '2018-10-11 15:51:56');
INSERT INTO `countries` VALUES (2, 'Albania', 1, 1, '2018-02-12 15:21:10', 1, '2018-10-11 09:42:39');
INSERT INTO `countries` VALUES (3, 'Algeria', 1, 1, '2018-02-12 15:21:14', 1, '2018-10-11 09:23:44');
INSERT INTO `countries` VALUES (6, 'American Samoa', 1, 1, '2018-02-12 15:21:16', 1, '2018-10-11 15:24:18');
INSERT INTO `countries` VALUES (7, 'Andorra', 1, 1, '2018-02-12 15:21:20', 1, '2018-10-11 15:24:22');
INSERT INTO `countries` VALUES (8, 'Angola', 1, 1, '2018-02-12 15:21:23', 1, '2018-10-11 15:24:38');
INSERT INTO `countries` VALUES (9, 'Anguilla', 1, 1, '2018-02-12 15:21:28', 1, '2018-10-11 15:24:32');
INSERT INTO `countries` VALUES (10, 'Antarctica', 1, 1, '2018-02-12 15:21:33', 1, '2018-10-11 15:26:49');
INSERT INTO `countries` VALUES (11, 'Antigua and Barbuda', 1, 1, '2018-02-12 15:21:35', 1, '2018-10-11 15:27:01');
INSERT INTO `countries` VALUES (12, 'Argentina', 1, 1, '2018-04-09 06:26:33', 1, '2018-09-12 13:39:40');
INSERT INTO `countries` VALUES (13, 'Armenia', 1, 1, '2018-05-03 23:26:26', 1, '2018-10-11 15:27:13');
INSERT INTO `countries` VALUES (14, 'Aruba', 1, 1, '2018-05-03 23:26:50', 1, '2018-10-11 15:21:26');
INSERT INTO `countries` VALUES (15, 'Australia', 1, 1, '2018-08-15 14:30:47', 1, '2018-09-12 13:40:51');
INSERT INTO `countries` VALUES (16, 'Austria', 1, 1, '2018-09-12 13:41:02', 1, '2018-09-12 13:41:02');
INSERT INTO `countries` VALUES (17, 'Azerbaijan', 1, 1, '2018-09-12 13:41:24', 1, '2018-09-12 13:41:24');
INSERT INTO `countries` VALUES (18, 'Bahamas', 1, 1, '2018-09-12 13:41:33', 1, '2018-09-12 13:41:33');
INSERT INTO `countries` VALUES (19, 'Bahrain', 1, 1, '2018-09-12 13:41:42', 1, '2018-09-12 13:41:42');
INSERT INTO `countries` VALUES (20, 'Bangladesh', 1, 1, '2018-09-12 13:41:52', 1, '2018-09-12 13:41:52');
INSERT INTO `countries` VALUES (21, 'Barbados', 1, 1, '2018-09-12 13:42:08', 1, '2018-09-12 13:42:08');
INSERT INTO `countries` VALUES (22, 'Belarus', 1, 1, '2018-09-12 13:43:43', 1, '2018-09-12 13:43:43');
INSERT INTO `countries` VALUES (23, 'Belgium', 1, 1, '2018-09-12 13:43:51', 1, '2018-09-12 13:43:51');
INSERT INTO `countries` VALUES (24, 'Belize', 1, 1, '2018-09-12 13:43:59', 1, '2018-09-12 13:43:59');
INSERT INTO `countries` VALUES (25, 'Benin', 1, 1, '2018-09-12 13:44:07', 1, '2018-09-12 13:44:07');
INSERT INTO `countries` VALUES (26, 'Bermuda', 1, 1, '2018-09-12 13:44:32', 1, '2018-09-12 13:44:32');
INSERT INTO `countries` VALUES (27, 'Bhutan', 1, 1, '2018-09-12 13:44:40', 1, '2018-09-12 13:44:40');
INSERT INTO `countries` VALUES (28, 'Bolivia', 1, 1, '2018-09-12 13:44:48', 1, '2018-09-12 13:44:48');
INSERT INTO `countries` VALUES (29, 'Bosnia & Herzegovina', 1, 1, '2018-09-12 13:45:17', 1, '2018-09-12 13:45:17');
INSERT INTO `countries` VALUES (30, 'Botswana', 1, 1, '2018-09-12 13:45:28', 1, '2018-09-12 13:45:28');
INSERT INTO `countries` VALUES (31, 'Bouvet Island', 1, 1, '2018-09-12 13:48:18', 1, '2018-09-12 13:48:18');
INSERT INTO `countries` VALUES (32, 'Brazil', 1, 1, '2018-09-12 13:48:49', 1, '2018-10-15 09:30:35');
INSERT INTO `countries` VALUES (33, 'British Indian Ocean Territory', 1, 1, '2018-09-12 13:49:15', 1, '2018-09-12 13:49:15');
INSERT INTO `countries` VALUES (34, 'Brunei Darussalam', 1, 1, '2018-09-12 13:49:30', 1, '2018-09-12 13:49:30');
INSERT INTO `countries` VALUES (35, 'Bulgaria', 1, 1, '2018-09-12 13:49:40', 1, '2018-09-12 13:49:40');
INSERT INTO `countries` VALUES (36, 'Burkina Faso', 1, 1, '2018-09-12 13:49:49', 1, '2018-09-12 13:49:49');
INSERT INTO `countries` VALUES (37, 'Burundi', 1, 1, '2018-09-12 13:49:57', 1, '2018-09-12 13:49:57');
INSERT INTO `countries` VALUES (38, 'Cambodia', 1, 1, '2018-09-12 13:50:26', 1, '2018-09-12 13:50:26');
INSERT INTO `countries` VALUES (39, 'Cameroon', 1, 1, '2018-09-12 13:50:35', 1, '2018-09-12 13:50:35');
INSERT INTO `countries` VALUES (40, 'Canada', 1, 1, '2018-09-12 13:50:43', 1, '2018-09-12 13:50:43');
INSERT INTO `countries` VALUES (41, 'Cape Verde', 1, 1, '2018-09-12 13:51:07', 1, '2018-09-12 13:51:07');
INSERT INTO `countries` VALUES (42, 'Cayman Islands', 1, 1, '2018-09-12 13:51:14', 1, '2018-09-12 13:51:14');
INSERT INTO `countries` VALUES (43, 'Central African Republic', 1, 1, '2018-09-12 13:52:20', 1, '2018-09-12 13:52:20');
INSERT INTO `countries` VALUES (44, 'Chad', 1, 1, '2018-09-12 13:52:26', 1, '2018-09-12 13:52:26');
INSERT INTO `countries` VALUES (45, 'Chile', 1, 1, '2018-09-12 13:52:42', 1, '2018-09-12 13:52:42');
INSERT INTO `countries` VALUES (46, 'China', 1, 1, '2018-09-12 13:52:48', 1, '2019-04-10 14:32:31');
INSERT INTO `countries` VALUES (47, 'Christmas Island', 1, 1, '2018-09-12 13:53:04', 1, '2018-09-12 13:53:04');
INSERT INTO `countries` VALUES (48, 'Cocos (Keeling) Islands', 1, 1, '2018-09-12 13:53:18', 1, '2018-09-12 13:53:18');
INSERT INTO `countries` VALUES (49, 'Colombia', 1, 1, '2018-09-12 13:53:30', 1, '2018-09-12 13:53:30');
INSERT INTO `countries` VALUES (50, 'Comoros', 1, 1, '2018-09-12 13:53:46', 1, '2018-09-12 13:53:46');
INSERT INTO `countries` VALUES (51, 'Congo', 1, 1, '2018-09-12 13:54:41', 1, '2018-09-12 13:54:41');
INSERT INTO `countries` VALUES (52, 'Congo, The Democratic Republic Of The', 1, 1, '2018-09-12 13:58:47', 1, '2018-09-12 13:58:47');
INSERT INTO `countries` VALUES (53, 'Cook Islands', 1, 1, '2018-09-12 13:59:01', 1, '2018-09-12 13:59:01');
INSERT INTO `countries` VALUES (54, 'Costa Rica', 1, 1, '2018-09-12 13:59:11', 1, '2018-09-12 13:59:11');
INSERT INTO `countries` VALUES (55, 'Cote D\'ivoire', 1, 1, '2018-09-12 13:59:42', 1, '2018-09-12 13:59:42');
INSERT INTO `countries` VALUES (56, 'Croatia', 1, 1, '2018-09-12 14:00:14', 1, '2018-09-12 14:00:14');
INSERT INTO `countries` VALUES (57, 'Cuba', 1, 1, '2018-09-12 14:00:22', 1, '2018-09-12 14:00:22');
INSERT INTO `countries` VALUES (58, 'Cyprus', 1, 1, '2018-09-12 14:00:30', 1, '2018-09-12 14:00:30');
INSERT INTO `countries` VALUES (59, 'Czech Republic', 1, 1, '2018-09-12 14:00:57', 1, '2018-09-12 14:00:57');
INSERT INTO `countries` VALUES (60, 'Denmark', 1, 1, '2018-09-12 14:01:05', 1, '2018-09-12 14:01:05');
INSERT INTO `countries` VALUES (61, 'Djibouti', 1, 1, '2018-09-12 14:01:17', 1, '2018-09-12 14:01:17');
INSERT INTO `countries` VALUES (62, 'Dominica', 1, 1, '2018-09-12 14:01:31', 1, '2018-09-12 14:01:31');
INSERT INTO `countries` VALUES (63, 'Dominican Republic', 1, 1, '2018-09-12 14:01:37', 1, '2018-09-12 14:01:37');
INSERT INTO `countries` VALUES (64, 'East Timor', 1, 1, '2018-09-12 14:01:46', 1, '2018-09-12 14:01:46');
INSERT INTO `countries` VALUES (65, 'Ecuador', 1, 1, '2018-09-12 14:01:52', 1, '2018-09-12 14:01:52');
INSERT INTO `countries` VALUES (66, 'Egypt', 1, 1, '2018-09-12 14:01:59', 1, '2018-09-12 14:01:59');
INSERT INTO `countries` VALUES (67, 'El Salvador', 1, 1, '2018-09-12 14:02:07', 1, '2018-09-12 14:02:07');
INSERT INTO `countries` VALUES (68, 'Equatorial Guinea', 1, 1, '2018-09-12 14:02:18', 1, '2018-09-12 14:02:18');
INSERT INTO `countries` VALUES (69, 'Eritrea', 1, 1, '2018-09-12 14:02:31', 1, '2018-09-12 14:02:31');
INSERT INTO `countries` VALUES (70, 'Estonia', 1, 1, '2018-09-12 14:02:41', 1, '2018-09-12 14:02:41');
INSERT INTO `countries` VALUES (71, 'Ethiopia', 1, 1, '2018-09-12 14:02:50', 1, '2018-09-12 14:02:50');
INSERT INTO `countries` VALUES (72, 'Falkland Islands (Malvinas)', 1, 1, '2018-09-12 14:51:14', 1, '2018-09-12 14:51:14');
INSERT INTO `countries` VALUES (73, 'Faroe Islands', 1, 1, '2018-09-12 14:51:21', 1, '2018-09-12 14:51:21');
INSERT INTO `countries` VALUES (74, 'Fiji', 1, 1, '2018-09-12 14:52:19', 1, '2018-09-12 14:52:19');
INSERT INTO `countries` VALUES (75, 'Finland', 1, 1, '2018-09-12 14:52:26', 1, '2018-09-12 14:52:26');
INSERT INTO `countries` VALUES (76, 'France', 1, 1, '2018-09-12 14:52:34', 1, '2018-09-12 14:52:34');
INSERT INTO `countries` VALUES (77, 'French Guiana', 1, 1, '2018-09-12 14:53:09', 1, '2018-09-12 14:53:09');
INSERT INTO `countries` VALUES (78, 'French Polynesia', 1, 1, '2018-09-12 14:53:21', 1, '2018-09-12 14:53:21');
INSERT INTO `countries` VALUES (79, 'French Southern Territories', 1, 1, '2018-09-12 14:53:34', 1, '2018-09-12 14:53:34');
INSERT INTO `countries` VALUES (80, 'Gabon', 1, 1, '2018-09-12 14:53:41', 1, '2018-09-12 14:53:41');
INSERT INTO `countries` VALUES (81, 'Gambia', 1, 1, '2018-09-12 14:53:47', 1, '2018-09-12 14:53:47');
INSERT INTO `countries` VALUES (82, 'Georgia', 1, 1, '2018-09-12 14:53:54', 1, '2018-09-12 14:53:54');
INSERT INTO `countries` VALUES (83, 'Germany', 1, 1, '2018-09-12 14:54:39', 1, '2018-09-12 14:54:39');
INSERT INTO `countries` VALUES (84, 'Ghana', 1, 1, '2018-09-12 14:54:45', 1, '2018-09-12 14:54:45');
INSERT INTO `countries` VALUES (85, 'Gibraltar', 1, 1, '2018-09-12 14:54:52', 1, '2018-09-12 14:54:52');
INSERT INTO `countries` VALUES (86, 'Greece', 1, 1, '2018-09-12 14:54:58', 1, '2018-09-12 14:54:58');
INSERT INTO `countries` VALUES (87, 'Greenland', 1, 1, '2018-09-12 14:55:06', 1, '2018-09-12 14:55:06');
INSERT INTO `countries` VALUES (88, 'Grenada', 1, 1, '2018-09-12 14:55:13', 1, '2018-09-12 14:55:13');
INSERT INTO `countries` VALUES (89, 'Guadeloupe', 1, 1, '2018-09-12 14:55:24', 1, '2018-09-12 14:55:24');
INSERT INTO `countries` VALUES (90, 'Guam', 1, 1, '2018-09-12 14:55:53', 1, '2018-09-12 14:55:53');
INSERT INTO `countries` VALUES (91, 'Guatemala', 1, 1, '2018-09-12 14:56:00', 1, '2018-09-12 14:56:00');
INSERT INTO `countries` VALUES (92, 'Guernsey', 1, 1, '2018-09-12 14:56:10', 1, '2018-09-12 14:56:10');
INSERT INTO `countries` VALUES (93, 'Guinea', 1, 1, '2018-09-12 14:56:19', 1, '2018-09-12 14:56:19');
INSERT INTO `countries` VALUES (94, 'Guinea Bissau', 1, 1, '2018-09-12 14:56:26', 1, '2018-09-12 14:56:26');
INSERT INTO `countries` VALUES (95, 'Guyana', 1, 1, '2018-09-12 14:56:37', 1, '2018-09-12 14:56:37');
INSERT INTO `countries` VALUES (96, 'Haiti', 1, 1, '2018-09-12 14:56:46', 1, '2018-09-12 14:56:46');
INSERT INTO `countries` VALUES (97, 'Heard Island and McDonald Islands', 1, 1, '2018-09-12 14:56:55', 1, '2018-09-12 14:56:55');
INSERT INTO `countries` VALUES (98, 'Holy See (Vatican City State)', 1, 1, '2018-09-12 14:57:03', 1, '2018-09-12 14:57:03');
INSERT INTO `countries` VALUES (99, 'Honduras', 1, 1, '2018-09-12 14:57:12', 1, '2018-09-12 14:57:12');
INSERT INTO `countries` VALUES (100, 'Hong Kong', 1, 1, '2018-09-12 14:57:19', 1, '2018-09-12 14:57:19');
INSERT INTO `countries` VALUES (101, 'Hungary', 1, 1, '2018-09-12 14:57:26', 1, '2018-09-12 14:57:26');
INSERT INTO `countries` VALUES (102, 'Iceland', 1, 1, '2018-09-12 14:57:32', 1, '2018-09-12 14:57:32');
INSERT INTO `countries` VALUES (103, 'India', 1, 1, '2018-09-12 14:57:38', 1, '2018-09-12 14:57:38');
INSERT INTO `countries` VALUES (104, 'Indonesia', 1, 1, '2018-09-12 14:57:45', 1, '2018-09-29 10:12:18');
INSERT INTO `countries` VALUES (105, 'Installations in International Waters', 1, 1, '2018-09-12 14:57:59', 1, '2018-09-12 14:57:59');
INSERT INTO `countries` VALUES (106, 'Iran, Islamic Republic of', 1, 1, '2018-09-12 14:58:12', 1, '2018-09-12 14:58:12');
INSERT INTO `countries` VALUES (107, 'Iraq', 1, 1, '2018-09-12 14:58:19', 1, '2018-09-12 14:58:19');
INSERT INTO `countries` VALUES (108, 'Ireland', 1, 1, '2018-09-12 14:58:26', 1, '2018-09-12 14:58:26');
INSERT INTO `countries` VALUES (109, 'Isle of Man', 1, 1, '2018-09-12 14:58:32', 1, '2018-09-12 14:58:32');
INSERT INTO `countries` VALUES (110, 'Israel', 1, 1, '2018-09-12 14:58:38', 1, '2018-09-12 14:58:38');
INSERT INTO `countries` VALUES (111, 'Italy', 1, 1, '2018-09-12 14:58:44', 1, '2018-09-12 14:58:44');
INSERT INTO `countries` VALUES (112, 'Jamaica', 1, 1, '2018-09-12 14:58:49', 1, '2018-09-12 14:58:49');
INSERT INTO `countries` VALUES (113, 'Japan', 1, 1, '2018-09-12 14:58:55', 1, '2019-03-11 11:29:14');
INSERT INTO `countries` VALUES (114, 'Jersey', 1, 1, '2018-09-12 14:59:00', 1, '2018-09-12 14:59:00');
INSERT INTO `countries` VALUES (115, 'Jordan', 1, 1, '2018-09-12 14:59:07', 1, '2018-09-12 14:59:07');
INSERT INTO `countries` VALUES (116, 'Kazakhstan', 1, 1, '2018-09-12 14:59:12', 1, '2018-09-12 14:59:12');
INSERT INTO `countries` VALUES (117, 'Kenya', 1, 1, '2018-09-12 14:59:17', 1, '2018-09-12 14:59:17');
INSERT INTO `countries` VALUES (118, 'Kiribati', 1, 1, '2018-09-12 14:59:22', 1, '2018-09-12 14:59:22');
INSERT INTO `countries` VALUES (119, 'Korea, Democratic People\'s Republic of', 1, 1, '2018-09-12 14:59:33', 1, '2018-09-12 14:59:33');
INSERT INTO `countries` VALUES (120, 'Korea, Republic of', 1, 1, '2018-09-12 14:59:41', 1, '2019-08-07 12:33:24');
INSERT INTO `countries` VALUES (121, 'Kuwait', 1, 1, '2018-09-12 14:59:50', 1, '2018-09-12 14:59:50');
INSERT INTO `countries` VALUES (122, 'Kyrgyzstan', 1, 1, '2018-09-12 14:59:59', 1, '2018-09-12 14:59:59');
INSERT INTO `countries` VALUES (123, 'Lao People\'s Democratic Republic', 1, 1, '2018-09-12 15:00:08', 1, '2018-09-12 15:00:08');
INSERT INTO `countries` VALUES (124, 'Latvia', 1, 1, '2018-09-12 15:00:16', 1, '2018-09-12 15:00:16');
INSERT INTO `countries` VALUES (125, 'Lebanon', 1, 1, '2018-09-12 15:00:23', 1, '2018-09-12 15:00:23');
INSERT INTO `countries` VALUES (126, 'Lesotho', 1, 1, '2018-09-12 15:00:30', 1, '2018-09-12 15:00:30');
INSERT INTO `countries` VALUES (127, 'Liberia', 1, 1, '2018-09-12 15:00:36', 1, '2018-09-12 15:00:36');
INSERT INTO `countries` VALUES (128, 'Libyan Arab Jamahiriya', 1, 1, '2018-09-12 15:00:45', 1, '2018-09-12 15:00:45');
INSERT INTO `countries` VALUES (129, 'Liechtenstein', 1, 1, '2018-09-12 15:00:55', 1, '2018-09-12 15:00:55');
INSERT INTO `countries` VALUES (130, 'Lithuania', 1, 1, '2018-09-12 15:01:05', 1, '2018-09-12 15:01:05');
INSERT INTO `countries` VALUES (131, 'Luxembourg', 1, 1, '2018-09-12 15:01:12', 1, '2018-09-12 15:01:12');
INSERT INTO `countries` VALUES (132, 'Macau', 1, 1, '2018-09-12 15:01:34', 1, '2018-09-12 15:01:34');
INSERT INTO `countries` VALUES (133, 'Macedonia', 1, 1, '2018-09-12 15:01:43', 1, '2018-09-12 15:01:43');
INSERT INTO `countries` VALUES (134, 'Madagascar', 1, 1, '2018-09-12 15:01:51', 1, '2018-09-12 15:01:51');
INSERT INTO `countries` VALUES (135, 'Malawi', 1, 1, '2018-09-12 15:02:41', 1, '2018-09-12 15:02:41');
INSERT INTO `countries` VALUES (136, 'Malaysia', 1, 1, '2018-09-12 15:02:51', 1, '2018-10-15 09:15:31');
INSERT INTO `countries` VALUES (137, 'Maldives', 1, 1, '2018-09-12 15:02:58', 1, '2018-09-12 15:02:58');
INSERT INTO `countries` VALUES (138, 'Mali', 1, 1, '2018-09-12 15:03:07', 1, '2018-09-12 15:03:07');
INSERT INTO `countries` VALUES (139, 'Malta', 1, 1, '2018-09-12 15:03:21', 1, '2018-09-12 15:03:21');
INSERT INTO `countries` VALUES (140, 'Marshall Islands', 1, 1, '2018-09-12 15:03:30', 1, '2018-09-12 15:03:30');
INSERT INTO `countries` VALUES (141, 'Martinique', 1, 1, '2018-09-12 15:03:38', 1, '2018-09-12 15:03:38');
INSERT INTO `countries` VALUES (142, 'Mauritania', 1, 1, '2018-09-12 15:03:46', 1, '2018-09-12 15:03:46');
INSERT INTO `countries` VALUES (143, 'Mauritius', 1, 1, '2018-09-12 15:03:53', 1, '2018-09-12 15:03:53');
INSERT INTO `countries` VALUES (144, 'Mayotte', 1, 1, '2018-09-12 15:04:00', 1, '2018-09-12 15:04:00');
INSERT INTO `countries` VALUES (145, 'Mexico', 1, 1, '2018-09-12 15:04:07', 1, '2018-09-12 15:04:07');
INSERT INTO `countries` VALUES (146, 'Micronesia, Federated States of', 1, 1, '2018-09-12 15:04:17', 1, '2018-09-12 15:04:17');
INSERT INTO `countries` VALUES (147, 'Moldova, Republic of', 1, 1, '2018-09-12 15:04:29', 1, '2018-09-12 15:04:29');
INSERT INTO `countries` VALUES (148, 'Monaco', 1, 1, '2018-09-12 15:04:38', 1, '2018-09-12 15:04:38');
INSERT INTO `countries` VALUES (149, 'Mongolia', 1, 1, '2018-09-12 15:04:46', 1, '2018-09-12 15:04:46');
INSERT INTO `countries` VALUES (150, 'Montenegro', 1, 1, '2018-09-12 15:04:57', 1, '2018-09-12 15:04:57');
INSERT INTO `countries` VALUES (151, 'Montserrat', 1, 1, '2018-09-12 15:05:12', 1, '2018-09-12 15:05:12');
INSERT INTO `countries` VALUES (152, 'Morocco', 1, 1, '2018-09-12 15:05:19', 1, '2018-09-12 15:05:19');
INSERT INTO `countries` VALUES (153, 'Mozambique', 1, 1, '2018-09-12 15:05:27', 1, '2018-09-12 15:05:27');
INSERT INTO `countries` VALUES (154, 'Myanmar', 1, 1, '2018-09-12 15:05:33', 1, '2018-09-12 15:05:33');
INSERT INTO `countries` VALUES (155, 'Namibia', 1, 1, '2018-09-12 15:05:54', 1, '2018-09-12 15:05:54');
INSERT INTO `countries` VALUES (156, 'Nauru', 1, 1, '2018-09-15 08:49:53', 1, '2018-09-15 08:49:53');
INSERT INTO `countries` VALUES (157, 'Nepal', 1, 1, '2018-09-15 08:50:05', 1, '2018-09-15 08:50:05');
INSERT INTO `countries` VALUES (158, 'Netherlands', 1, 1, '2018-09-15 08:50:48', 1, '2018-09-15 08:50:48');
INSERT INTO `countries` VALUES (159, 'Netherlands Antilles', 1, 1, '2018-09-15 08:51:24', 1, '2018-09-15 08:51:24');
INSERT INTO `countries` VALUES (160, 'New Caledonia', 1, 1, '2018-09-15 08:51:43', 1, '2018-09-15 08:51:53');
INSERT INTO `countries` VALUES (161, 'New Zealand', 1, 1, '2018-09-15 08:52:16', 1, '2018-09-15 08:52:16');
INSERT INTO `countries` VALUES (162, 'Nicaragua', 1, 1, '2018-09-15 08:52:26', 1, '2018-09-15 08:52:26');
INSERT INTO `countries` VALUES (163, 'Niger', 1, 1, '2018-09-15 08:52:34', 1, '2018-09-15 08:52:34');
INSERT INTO `countries` VALUES (164, 'Nigeria', 1, 1, '2018-09-15 08:52:45', 1, '2018-09-15 08:52:45');
INSERT INTO `countries` VALUES (165, 'Niue', 1, 1, '2018-09-15 08:52:52', 1, '2018-09-15 08:52:52');
INSERT INTO `countries` VALUES (166, 'Norfolk Island', 1, 1, '2018-09-15 08:53:57', 1, '2018-09-15 08:53:57');
INSERT INTO `countries` VALUES (167, 'Northern Mariana Island', 1, 1, '2018-09-15 08:54:23', 1, '2018-09-15 08:54:23');
INSERT INTO `countries` VALUES (168, 'Norway', 1, 1, '2018-09-15 08:54:31', 1, '2018-09-15 08:54:31');
INSERT INTO `countries` VALUES (169, 'Oman', 1, 1, '2018-09-15 08:54:37', 1, '2018-09-15 08:54:37');
INSERT INTO `countries` VALUES (170, 'Pakistan', 1, 1, '2018-09-15 08:54:43', 1, '2018-09-15 08:54:43');
INSERT INTO `countries` VALUES (171, 'Palau', 1, 1, '2018-09-15 08:54:49', 1, '2018-09-15 08:54:49');
INSERT INTO `countries` VALUES (172, 'Palistian Territory, Occupied', 1, 1, '2018-09-15 08:55:05', 1, '2018-09-15 08:55:05');
INSERT INTO `countries` VALUES (173, 'Panama', 1, 1, '2018-09-15 08:55:12', 1, '2018-09-15 08:55:12');
INSERT INTO `countries` VALUES (174, 'Papua New Guinea', 1, 1, '2018-09-15 08:55:27', 1, '2018-09-15 08:55:27');
INSERT INTO `countries` VALUES (175, 'Paraguay', 1, 1, '2018-09-15 08:55:52', 1, '2018-09-15 08:55:52');
INSERT INTO `countries` VALUES (176, 'Peru', 1, 1, '2018-09-15 08:56:00', 1, '2018-09-15 08:56:00');
INSERT INTO `countries` VALUES (177, 'Philippines', 1, 1, '2018-09-15 08:56:11', 1, '2019-03-21 15:31:46');
INSERT INTO `countries` VALUES (178, 'Pitcairn', 1, 1, '2018-09-15 08:56:23', 1, '2018-09-15 08:56:23');
INSERT INTO `countries` VALUES (179, 'Poland', 1, 1, '2018-09-15 08:56:34', 1, '2018-09-15 08:56:34');
INSERT INTO `countries` VALUES (180, 'Portugal', 1, 1, '2018-09-15 08:56:41', 1, '2018-09-15 08:56:41');
INSERT INTO `countries` VALUES (181, 'Puerto Rico', 1, 1, '2018-09-15 08:56:47', 1, '2018-09-15 08:56:47');
INSERT INTO `countries` VALUES (182, 'Qatar', 1, 1, '2018-09-15 08:56:53', 1, '2018-09-15 08:56:53');
INSERT INTO `countries` VALUES (183, 'Reunion', 1, 1, '2018-09-15 08:57:04', 1, '2018-09-15 08:57:04');
INSERT INTO `countries` VALUES (184, 'Romania', 1, 1, '2018-09-15 08:57:10', 1, '2018-09-15 08:57:10');
INSERT INTO `countries` VALUES (185, 'Russian Federation', 1, 1, '2018-09-15 08:57:26', 1, '2018-09-15 08:57:26');
INSERT INTO `countries` VALUES (186, 'Rwanda', 1, 1, '2018-09-15 08:57:32', 1, '2018-09-15 08:57:32');
INSERT INTO `countries` VALUES (187, 'Saint Helena', 1, 1, '2018-09-15 08:57:47', 1, '2018-09-15 08:57:47');
INSERT INTO `countries` VALUES (188, 'Saint Kitts And Nevis', 1, 1, '2018-09-15 08:58:02', 1, '2018-09-15 08:58:02');
INSERT INTO `countries` VALUES (189, 'Saint Lucia', 1, 1, '2018-09-15 08:58:10', 1, '2018-09-15 08:58:10');
INSERT INTO `countries` VALUES (190, 'Saint Pierre And Miquelon', 1, 1, '2018-09-15 08:58:31', 1, '2018-09-15 08:58:31');
INSERT INTO `countries` VALUES (191, 'Saint Vincent And The Grenadines', 1, 1, '2018-09-15 08:59:05', 1, '2018-09-15 08:59:05');
INSERT INTO `countries` VALUES (192, 'Samoa', 1, 1, '2018-09-15 08:59:11', 1, '2018-09-15 08:59:11');
INSERT INTO `countries` VALUES (193, 'San Marino ', 1, 1, '2018-09-15 08:59:18', 1, '2018-09-15 08:59:18');
INSERT INTO `countries` VALUES (194, 'Sao Tome And Principe', 1, 1, '2018-09-15 08:59:31', 1, '2018-09-15 08:59:31');
INSERT INTO `countries` VALUES (195, 'Saudi Arabia', 1, 1, '2018-09-15 08:59:41', 1, '2018-09-15 08:59:41');
INSERT INTO `countries` VALUES (196, 'Senegal', 1, 1, '2018-09-15 09:00:26', 1, '2018-09-15 09:00:26');
INSERT INTO `countries` VALUES (197, 'Serbia ', 1, 1, '2018-09-15 09:00:41', 1, '2018-09-15 09:00:41');
INSERT INTO `countries` VALUES (198, 'Seychelles', 1, 1, '2018-09-15 09:00:55', 1, '2018-09-15 09:00:55');
INSERT INTO `countries` VALUES (199, 'Sierra Leone', 1, 1, '2018-09-15 09:01:06', 1, '2018-09-15 09:01:06');
INSERT INTO `countries` VALUES (200, 'Singapore', 1, 1, '2018-09-15 09:01:12', 1, '2018-09-24 10:27:44');
INSERT INTO `countries` VALUES (201, 'Slovakia', 1, 1, '2018-09-15 09:01:18', 1, '2018-09-15 09:01:18');
INSERT INTO `countries` VALUES (202, 'Slovenia', 1, 1, '2018-09-15 09:01:23', 1, '2018-09-15 09:01:23');
INSERT INTO `countries` VALUES (203, 'Solomon Island', 1, 1, '2018-09-15 09:01:33', 1, '2018-09-15 09:01:33');
INSERT INTO `countries` VALUES (204, 'Somalia', 1, 1, '2018-09-15 09:01:39', 1, '2018-09-15 09:01:39');
INSERT INTO `countries` VALUES (205, 'South Africa', 1, 1, '2018-09-15 09:01:52', 1, '2018-09-15 09:01:52');
INSERT INTO `countries` VALUES (206, 'South Georgia And The South Sandwich ISL', 1, 1, '2018-09-15 09:02:27', 1, '2018-09-15 09:02:27');
INSERT INTO `countries` VALUES (207, 'Spain', 1, 1, '2018-09-15 09:02:36', 1, '2018-09-15 09:02:36');
INSERT INTO `countries` VALUES (208, 'Sri Langka', 1, 1, '2018-09-15 09:02:50', 1, '2018-09-15 09:02:50');
INSERT INTO `countries` VALUES (209, 'Sudan ', 1, 1, '2018-09-15 09:03:24', 1, '2018-09-15 09:03:24');
INSERT INTO `countries` VALUES (210, 'Suriname', 1, 1, '2018-09-15 09:03:43', 1, '2018-09-15 09:03:43');
INSERT INTO `countries` VALUES (211, 'Svalbard And Jan Mayen', 1, 1, '2018-09-15 09:03:56', 1, '2018-09-15 09:03:56');
INSERT INTO `countries` VALUES (212, 'Swaziland', 1, 1, '2018-09-15 09:04:07', 1, '2018-09-15 09:04:07');
INSERT INTO `countries` VALUES (213, 'Sweden', 1, 1, '2018-09-15 09:04:15', 1, '2018-09-15 09:04:15');
INSERT INTO `countries` VALUES (214, 'Switzerland', 1, 1, '2018-09-15 09:04:27', 1, '2018-09-15 09:04:27');
INSERT INTO `countries` VALUES (215, 'Syrian Arab Republic', 1, 1, '2018-09-15 09:04:45', 1, '2018-09-15 09:04:45');
INSERT INTO `countries` VALUES (216, 'Saint Barthelemy', 1, 1, '2018-09-15 09:05:16', 1, '2018-09-15 09:05:16');
INSERT INTO `countries` VALUES (217, 'Saint Martin (French Pait)', 1, 1, '2018-09-15 09:05:47', 1, '2018-09-15 09:05:47');
INSERT INTO `countries` VALUES (218, 'Sint Maarten (Dutch Part)', 1, 1, '2018-09-15 09:06:03', 1, '2018-09-15 09:06:03');
INSERT INTO `countries` VALUES (219, 'South Sudan', 1, 1, '2018-09-15 09:06:12', 1, '2018-09-15 09:06:12');
INSERT INTO `countries` VALUES (220, 'Taiwan, Province Of China', 1, 1, '2018-09-15 09:06:34', 1, '2018-09-15 09:07:02');
INSERT INTO `countries` VALUES (221, 'Tajikistan', 1, 1, '2018-09-15 09:07:17', 1, '2018-09-15 09:07:17');
INSERT INTO `countries` VALUES (222, 'Tanzania, United Republic Of', 1, 1, '2018-09-15 09:07:42', 1, '2018-09-15 09:07:42');
INSERT INTO `countries` VALUES (223, 'Thailand', 1, 1, '2018-09-15 09:07:47', 1, '2019-03-21 14:54:01');
INSERT INTO `countries` VALUES (224, 'Togo', 1, 1, '2018-09-15 09:07:52', 1, '2018-09-15 09:07:52');
INSERT INTO `countries` VALUES (225, 'Tokelau', 1, 1, '2018-09-15 09:08:02', 1, '2018-09-15 09:08:02');
INSERT INTO `countries` VALUES (226, 'Tonga', 1, 1, '2018-09-15 09:08:08', 1, '2018-09-15 09:08:08');
INSERT INTO `countries` VALUES (227, 'Trinidad And Tobago', 1, 1, '2018-09-15 09:08:17', 1, '2018-09-15 09:08:17');
INSERT INTO `countries` VALUES (228, 'Tunisia', 1, 1, '2018-09-15 09:08:26', 1, '2018-09-15 09:08:26');
INSERT INTO `countries` VALUES (229, 'Turkey', 1, 1, '2018-09-15 09:08:31', 1, '2018-09-15 09:08:31');
INSERT INTO `countries` VALUES (230, 'Turkmenistan', 1, 1, '2018-09-15 09:08:46', 1, '2018-09-15 09:08:46');
INSERT INTO `countries` VALUES (231, 'Turks And Caicos Island', 1, 1, '2018-09-15 09:09:05', 1, '2018-09-15 09:09:05');
INSERT INTO `countries` VALUES (232, 'Tuvalu', 1, 1, '2018-09-15 09:09:14', 1, '2018-09-15 09:09:14');
INSERT INTO `countries` VALUES (233, 'Timor Leste', 1, 1, '2018-09-15 09:09:26', 1, '2018-09-15 09:09:26');
INSERT INTO `countries` VALUES (234, 'Uganda', 1, 1, '2018-09-15 09:09:32', 1, '2018-09-15 09:09:32');
INSERT INTO `countries` VALUES (235, 'Ukraine', 1, 1, '2018-09-15 09:09:40', 1, '2018-09-15 09:09:40');
INSERT INTO `countries` VALUES (236, 'United Arab Emirates', 1, 1, '2018-09-15 09:09:55', 1, '2018-09-15 09:09:55');
INSERT INTO `countries` VALUES (237, 'United States', 1, 1, '2018-09-15 09:10:04', 1, '2018-10-15 13:45:49');
INSERT INTO `countries` VALUES (238, 'United States Minor Outlying Island', 1, 1, '2018-09-15 09:10:31', 1, '2018-09-15 09:10:31');
INSERT INTO `countries` VALUES (239, 'Uruguay', 1, 1, '2018-09-15 09:10:37', 1, '2018-09-15 09:10:38');
INSERT INTO `countries` VALUES (240, 'Uzbekistan', 1, 1, '2018-09-15 09:10:47', 1, '2018-09-15 09:10:47');
INSERT INTO `countries` VALUES (241, 'Vanuatu', 1, 1, '2018-09-15 09:10:53', 1, '2018-09-15 09:10:53');
INSERT INTO `countries` VALUES (242, 'Venezuela', 1, 1, '2018-09-15 09:10:59', 1, '2018-09-15 09:10:59');
INSERT INTO `countries` VALUES (243, 'Vietnam', 1, 1, '2018-09-15 09:11:04', 1, '2018-09-15 09:11:04');
INSERT INTO `countries` VALUES (244, 'Virgin Island, British', 1, 1, '2018-09-15 09:11:23', 1, '2018-09-15 09:11:23');
INSERT INTO `countries` VALUES (245, 'Virgin Island, U.S', 1, 1, '2018-09-15 09:11:40', 1, '2018-09-15 09:11:40');
INSERT INTO `countries` VALUES (246, 'Wallis And Futuna', 1, 1, '2018-09-15 09:11:55', 1, '2018-09-15 09:11:55');
INSERT INTO `countries` VALUES (247, 'Western Sahara', 1, 1, '2018-09-15 09:12:05', 1, '2018-09-15 09:12:05');
INSERT INTO `countries` VALUES (248, 'Yemen', 1, 1, '2018-09-15 09:12:11', 1, '2018-09-15 09:12:11');
INSERT INTO `countries` VALUES (249, 'Yugoslavia', 1, 1, '2018-09-15 09:12:19', 1, '2018-09-15 09:12:19');
INSERT INTO `countries` VALUES (250, 'Zambia', 1, 1, '2018-09-15 09:12:26', 1, '2018-09-15 09:12:26');
INSERT INTO `countries` VALUES (251, 'Zimbabwe', 1, 1, '2018-09-15 09:12:32', 1, '2018-09-15 09:12:32');
INSERT INTO `countries` VALUES (252, 'Bonaire, Sint Eustatius And Saba', 1, 1, '2018-09-15 09:32:09', 1, '2018-09-15 09:32:09');
INSERT INTO `countries` VALUES (253, 'Curacao', 1, 1, '2018-09-15 09:32:51', 1, '2018-09-15 09:32:51');
INSERT INTO `countries` VALUES (254, 'Former Czechoslovakia', 1, 1, '2018-09-15 09:33:41', 1, '2018-09-15 09:33:41');
INSERT INTO `countries` VALUES (255, 'United Kingdom', 1, 1, '2018-09-15 09:35:29', 1, '2018-10-11 15:18:52');
INSERT INTO `countries` VALUES (256, 'TURKI', 1, 1, '2018-09-19 09:53:28', 1, '2018-10-11 15:21:09');
INSERT INTO `countries` VALUES (257, 'Hong Kong-Tiongkok ', 1, 1, '2020-01-15 17:09:13', 1, '2020-01-15 17:09:13');

-- ----------------------------
-- Table structure for currencies
-- ----------------------------
DROP TABLE IF EXISTS `currencies`;
CREATE TABLE `currencies`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` char(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `name` varchar(225) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `kurs_idr` decimal(22, 2) NULL DEFAULT NULL,
  `status` tinyint(41) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created` datetime(0) NULL DEFAULT NULL,
  `modified_by` int(11) NULL DEFAULT NULL,
  `modified` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 29 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of currencies
-- ----------------------------
INSERT INTO `currencies` VALUES (1, 'THB', 'Bath Thailand ', 0.00, 1, 1, '2018-02-12 00:00:00', 1, '2019-03-11 10:58:23');
INSERT INTO `currencies` VALUES (2, 'KWD', 'Dinar Kuwait', 0.00, 1, 1, '2018-02-12 00:00:00', 1, '2019-03-11 10:58:14');
INSERT INTO `currencies` VALUES (3, 'USD', 'Dollar Amerika Serikat', 0.00, 1, 1, '2018-02-12 00:00:00', 1, '2018-10-15 13:49:09');
INSERT INTO `currencies` VALUES (4, 'BND', 'Dollar Brunei Darussalam', 13000.00, 1, 1, '2018-02-12 00:00:00', 1, '2019-03-11 10:58:04');
INSERT INTO `currencies` VALUES (7, 'HKD', 'Dollar Hong Kong', 13000.00, 1, 1, '2018-08-15 14:28:04', 1, '2019-03-11 10:57:51');
INSERT INTO `currencies` VALUES (8, 'CAD', 'Dollar Kanada', 0.00, 1, 1, '2018-09-13 09:32:04', 1, '2018-09-13 09:32:04');
INSERT INTO `currencies` VALUES (9, 'NZD', 'Dollar Selandia Baru', 0.00, 1, 1, '2018-09-13 09:32:33', 1, '2018-09-13 09:32:33');
INSERT INTO `currencies` VALUES (10, 'SGD', 'Dollar Singapura', 0.00, 1, 1, '2018-09-13 09:32:51', 1, '2018-09-13 09:32:51');
INSERT INTO `currencies` VALUES (11, 'EUR', 'Euro', 0.00, 1, 1, '2018-09-13 09:33:03', 1, '2018-09-13 09:33:03');
INSERT INTO `currencies` VALUES (12, 'CHF', 'Franc Swiss', 0.00, 1, 1, '2018-09-13 09:33:44', 1, '2018-09-13 09:33:44');
INSERT INTO `currencies` VALUES (13, 'DKK', 'Kroner Denmark', 0.00, 1, 1, '2018-09-13 09:34:32', 1, '2018-09-13 09:34:32');
INSERT INTO `currencies` VALUES (14, 'NOK', 'Kroner Norwegia', 0.00, 1, 1, '2018-09-13 09:34:57', 1, '2018-09-13 09:34:57');
INSERT INTO `currencies` VALUES (15, 'SEK', 'Kroner Swedia', 0.00, 1, 1, '2018-09-13 09:35:11', 1, '2018-09-13 09:35:11');
INSERT INTO `currencies` VALUES (16, 'MMK', 'Kyat Myanmar', 0.00, 1, 1, '2018-09-13 09:35:26', 1, '2018-09-13 09:35:26');
INSERT INTO `currencies` VALUES (17, 'PHP', 'Peso Philipina', 0.00, 1, 1, '2018-09-13 09:35:50', 1, '2018-09-13 09:35:50');
INSERT INTO `currencies` VALUES (18, 'GBP', 'Poundsterling Inggris', 0.00, 1, 1, '2018-09-13 09:36:12', 1, '2018-09-13 09:36:12');
INSERT INTO `currencies` VALUES (19, 'MYR', 'Ringgit Malaysia', 0.00, 1, 1, '2018-09-13 09:36:28', 1, '2018-09-13 09:36:28');
INSERT INTO `currencies` VALUES (20, 'SAR', 'Riyal Saudi Arabia', 0.00, 1, 1, '2018-09-13 09:36:41', 1, '2018-09-13 09:36:41');
INSERT INTO `currencies` VALUES (21, 'INR', 'Rupee India', 0.00, 1, 1, '2018-09-13 09:36:55', 1, '2018-09-13 09:36:55');
INSERT INTO `currencies` VALUES (22, 'PKR', 'Rupee Pakistan', 0.00, 1, 1, '2018-09-13 09:37:12', 1, '2018-09-13 09:37:12');
INSERT INTO `currencies` VALUES (23, 'LKR', 'Rupee Sri Langka', 0.00, 1, 1, '2018-09-13 09:37:31', 1, '2018-09-13 09:37:31');
INSERT INTO `currencies` VALUES (24, 'KRW', 'Won Korea', 0.00, 1, 1, '2018-09-13 09:37:46', 1, '2018-09-13 09:37:46');
INSERT INTO `currencies` VALUES (25, 'JPY', 'Yen Jepang', 0.00, 1, 1, '2018-09-13 09:37:56', 1, '2018-09-13 09:37:56');
INSERT INTO `currencies` VALUES (26, 'CNY', 'Yuan Renminbi Tiongkok', 0.00, 1, 1, '2018-09-13 09:38:09', 1, '2018-09-13 09:38:09');
INSERT INTO `currencies` VALUES (27, 'ZAR', 'RAND', 30000.00, 1, 1, '2018-09-19 09:51:55', 1, '2019-03-11 10:57:35');
INSERT INTO `currencies` VALUES (28, 'TST', 'TEST MATA UANG', 2000.00, 1, 1, '2020-03-03 16:54:43', NULL, '2020-03-03 16:54:43');

-- ----------------------------
-- Table structure for customers
-- ----------------------------
DROP TABLE IF EXISTS `customers`;
CREATE TABLE `customers`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` char(250) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `address` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `country_id` int(11) NULL DEFAULT NULL,
  `phone` char(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `fax` char(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `email` char(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `pic` char(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `pic_phone` char(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `status` tinyint(1) NULL DEFAULT NULL,
  `created` datetime(0) NULL DEFAULT NULL,
  `created_by` int(11) NULL DEFAULT NULL,
  `modified` datetime(0) NULL DEFAULT NULL,
  `modified_by` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of customers
-- ----------------------------
INSERT INTO `customers` VALUES (1, 'PT. RACHUM AMANA BERKAH', 'SATRIO TOWER LANTAI 6 UNIT 8, JL PROF DR SATRIO KAV. C4 RT.007 RW.002. KUNINGAN TIMUR SETIABUDI KOTA ADM. JAKARTA SELATAN DKI JAKARTA\r\n', 104, '', '', '', '', '', 1, '2019-04-10 14:34:26', 6, '2019-04-10 14:34:26', NULL);

-- ----------------------------
-- Table structure for expenditures
-- ----------------------------
DROP TABLE IF EXISTS `expenditures`;
CREATE TABLE `expenditures`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` char(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `date` date NOT NULL,
  `bc_type` varchar(225) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `bc_code` varchar(225) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `bc_date` date NOT NULL,
  `customer_id` int(11) NOT NULL,
  `register_code` varchar(225) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `register_date` date NULL DEFAULT NULL,
  `currency_id` int(11) NULL DEFAULT NULL,
  `kurs_idr` decimal(65, 2) NULL DEFAULT NULL,
  `file` varchar(225) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `file_dir` varchar(225) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `note` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `status` tinyint(2) NULL DEFAULT NULL,
  `created` datetime(0) NULL DEFAULT NULL,
  `created_by` int(11) NULL DEFAULT NULL,
  `modified` datetime(0) NULL DEFAULT NULL,
  `modified_by` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for expenditures_picking_lists
-- ----------------------------
DROP TABLE IF EXISTS `expenditures_picking_lists`;
CREATE TABLE `expenditures_picking_lists`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `expenditure_id` bigint(20) NULL DEFAULT NULL,
  `picking_list_id` bigint(20) NOT NULL,
  `created` datetime(0) NOT NULL,
  `created_by` int(11) NOT NULL,
  `modified` datetime(0) NULL DEFAULT NULL,
  `modified_by` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for expenditures_picking_lists_details
-- ----------------------------
DROP TABLE IF EXISTS `expenditures_picking_lists_details`;
CREATE TABLE `expenditures_picking_lists_details`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `expenditures_picking_list_id` bigint(20) NOT NULL,
  `picking_list_id` bigint(20) NOT NULL,
  `qty` decimal(65, 2) NULL DEFAULT NULL,
  `amount` decimal(65, 2) NULL DEFAULT NULL,
  `created` datetime(0) NOT NULL,
  `created_by` int(11) NOT NULL,
  `modified` datetime(0) NULL DEFAULT NULL,
  `modified_by` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for importers
-- ----------------------------
DROP TABLE IF EXISTS `importers`;
CREATE TABLE `importers`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` char(250) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `address` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `country_id` int(11) NULL DEFAULT NULL,
  `phone` char(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `fax` char(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `email` char(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `pic` char(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `pic_phone` char(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `status` tinyint(1) NULL DEFAULT NULL,
  `created` datetime(0) NULL DEFAULT NULL,
  `created_by` int(11) NULL DEFAULT NULL,
  `modified` datetime(0) NULL DEFAULT NULL,
  `modified_by` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of importers
-- ----------------------------
INSERT INTO `importers` VALUES (1, 'PT. RACHUM AMANA BERKAH', 'SATRIO TOWER LANTAI 6 UNIT 8, JL PROF DR SATRIO KAV. C4 RT.007 RW.002. KUNINGAN TIMUR SETIABUDI KOTA ADM. JAKARTA SELATAN DKI JAKARTA\r\n', 104, '', '', '', '', '', 1, '2019-04-10 14:34:26', 6, '2019-04-10 14:34:26', NULL);

-- ----------------------------
-- Table structure for locations
-- ----------------------------
DROP TABLE IF EXISTS `locations`;
CREATE TABLE `locations`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `status` tinyint(1) NULL DEFAULT NULL,
  `created_by` int(11) NULL DEFAULT NULL,
  `created` datetime(0) NULL DEFAULT NULL,
  `modified_by` int(11) NULL DEFAULT NULL,
  `modified` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of locations
-- ----------------------------
INSERT INTO `locations` VALUES (1, 'LAJUR A', 1, 6, '2019-04-10 14:35:22', 9, '2019-05-02 00:39:34');

-- ----------------------------
-- Table structure for picking_lists
-- ----------------------------
DROP TABLE IF EXISTS `picking_lists`;
CREATE TABLE `picking_lists`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(225) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `date` date NOT NULL,
  `customer_id` int(11) NOT NULL,
  `created` datetime(0) NOT NULL,
  `created_by` int(11) NOT NULL,
  `modified` datetime(0) NULL DEFAULT NULL,
  `modified_by` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for picking_lists_details
-- ----------------------------
DROP TABLE IF EXISTS `picking_lists_details`;
CREATE TABLE `picking_lists_details`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `picking_list_id` bigint(20) NOT NULL,
  `stock_location_id` bigint(20) NOT NULL,
  `qty` decimal(65, 2) NOT NULL,
  `created` datetime(0) NOT NULL,
  `created_by` int(11) NOT NULL,
  `modified` datetime(0) NULL DEFAULT NULL,
  `modified_by` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for products
-- ----------------------------
DROP TABLE IF EXISTS `products`;
CREATE TABLE `products`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `hs_code` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `status` tinyint(1) NULL DEFAULT NULL,
  `created_by` int(11) NULL DEFAULT NULL,
  `created` datetime(0) NULL DEFAULT NULL,
  `modified_by` int(11) NULL DEFAULT NULL,
  `modified` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `code`(`code`) USING BTREE,
  FULLTEXT INDEX `name`(`name`)
) ENGINE = InnoDB AUTO_INCREMENT = 162 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of products
-- ----------------------------
INSERT INTO `products` VALUES (116, '19CF092', NULL, '100% POLYESTER FABRICS', 1, 6, '2019-04-10 14:38:35', NULL, '2019-04-10 14:38:35');
INSERT INTO `products` VALUES (117, '19CF121', NULL, '100% POLYESTER FABRIC', 1, 9, '2019-04-16 12:23:09', 6, '2019-05-04 09:38:06');
INSERT INTO `products` VALUES (118, 'TSD-190217-1', NULL, '100% POLYESTER FABRIC', 1, 6, '2019-05-04 09:37:46', NULL, '2019-05-04 09:37:46');
INSERT INTO `products` VALUES (119, '19CF216-2', NULL, '100% POLYESTER FABRICS', 1, 10, '2019-06-26 10:54:19', NULL, '2019-06-26 10:54:19');
INSERT INTO `products` VALUES (120, '19CF216-1', NULL, '100% POLYESTER FABRICS', 1, 10, '2019-06-26 13:57:03', NULL, '2019-06-26 13:57:03');
INSERT INTO `products` VALUES (121, '19CF268-1', NULL, '100% POLYESTER FABRICS', 1, 9, '2019-07-15 11:16:47', NULL, '2019-07-15 11:16:47');
INSERT INTO `products` VALUES (122, '19CF268-2', NULL, '100% POLYESTER FABRICS', 1, 9, '2019-07-15 11:17:10', NULL, '2019-07-15 11:17:10');
INSERT INTO `products` VALUES (123, 'BK-27', NULL, '96% POLYESTER 4% SPANDEX POLYESTER DYED KNIT FABRIC', 1, 10, '2019-08-07 13:07:18', NULL, '2019-08-07 13:07:18');
INSERT INTO `products` VALUES (124, 'BK-37-1', NULL, '96% POLYESTER 4% SPANDEX POLYESTER DYED KNIT FABRIC, Merk: , Tipe: , Ukuran: , Spesifikasi lain: - KOREA, REPUBLIC OF', 1, 10, '2019-08-13 09:54:56', NULL, '2019-08-13 09:54:56');
INSERT INTO `products` VALUES (125, 'TSD-190619-1', NULL, '100% POLYESTER FABRICS,', 1, 10, '2019-08-16 10:28:57', NULL, '2019-08-16 10:28:57');
INSERT INTO `products` VALUES (126, 'ESD-19-1697-1', NULL, '95% POLYESTER 5% SPANDEX POLYESTER DYED KNIT FABRIC, Merk: , Tipe: , Ukuran: , Spesifikasi lain: - KOREA, REPUBLIC OF', 1, 10, '2019-08-27 11:20:00', NULL, '2019-08-27 11:20:00');
INSERT INTO `products` VALUES (127, 'BK-32-1', NULL, '95% POLYESTER 5% SPANDEX POLYESTER DYED KNIT FABRIC, Merk: , Tipe: , Ukuran: , Spesifikasi lain: - KOREA, REPUBLIC OF', 1, 10, '2019-08-30 09:57:08', NULL, '2019-08-30 09:57:08');
INSERT INTO `products` VALUES (128, 'BK-29-1', NULL, '95% POLYESTER 5% SPANDEX POLYESTER DYED KNIT FABRIC, Merk: , Tipe: , Ukuran: , Spesifikasi lain: - KOREA, REPUBLIC OF', 1, 10, '2019-08-30 10:31:20', NULL, '2019-08-30 10:31:20');
INSERT INTO `products` VALUES (129, 'BK-30', NULL, '96% POLYESTER 4% SPANDEX POLYESTER DYED KNIT FABRIC, Merk: , Tipe: , Ukuran: , Spesifikasi lain: - KOREA, REPUBLIC OF', 1, 10, '2019-09-14 08:37:31', NULL, '2019-09-14 08:37:31');
INSERT INTO `products` VALUES (130, 'BK-47', NULL, '96% POLYESTER 4% SPANDEX POLYESTER DYED KNIT FABRIC, Merk: , Tipe: , Ukuran: , Spesifikasi lain: - KOREA, REPUBLIC OF', 1, 10, '2019-09-30 08:02:59', NULL, '2019-09-30 08:02:59');
INSERT INTO `products` VALUES (131, 'BK-44-01', NULL, '96% POLYESTER 4% SPANDEK POLYESTER DYED KNIT FABRIC, Merk: , Tipe: , Ukuran: , Spesifikasi lain: - KOREA, REPUBLIC OF', 1, 10, '2019-10-08 08:05:34', NULL, '2019-10-08 08:05:34');
INSERT INTO `products` VALUES (132, 'AHJY002-01', NULL, '- TEXTILE WOVEN FABRIC OF 100% NON-TEXTURED POLYESTER FILAMENTS WITH DYE, Merk: , Tipe: , Ukuran: , Spesifikasi lain:', 1, 10, '2019-10-15 11:30:51', NULL, '2019-10-15 11:30:51');
INSERT INTO `products` VALUES (133, 'TSD-190909-01', NULL, '100% POLYESTER FABRICS, Merk: , Tipe: , Ukuran: , Spesifikasi lain: - CHINA', 1, 10, '2019-12-16 08:38:44', NULL, '2019-12-16 08:38:44');
INSERT INTO `products` VALUES (134, 'T1910028N/19452-01', NULL, 'KNITTED FABRIC, Merk: , Tipe: , Ukuran: , Spesifikasi lain: - TAIWAN, PROVINCE OF CHINA', 1, 10, '2020-01-15 14:46:53', NULL, '2020-01-15 14:46:53');
INSERT INTO `products` VALUES (135, 'T1910028N/19452-02', NULL, 'HANGTAG, Merk: , Tipe: , Ukuran: , Spesifikasi lain: - TAIWAN, PROVINCE OF CHINA', 1, 10, '2020-01-15 14:47:30', NULL, '2020-01-15 14:47:30');
INSERT INTO `products` VALUES (136, 'CG510019CX0010-01', NULL, 'KNITTED FABRIC, Merk: , Tipe: AO351, Ukuran: , Spesifikasi lain: - HONG KONG', 1, 10, '2020-01-16 08:08:54', NULL, '2020-01-16 08:08:54');
INSERT INTO `products` VALUES (137, 'CG510019CX0010-02', NULL, 'KNITTED FABRIC, Merk: , Tipe: AN136, Ukuran: , Spesifikasi lain: - HONG KONG', 1, 10, '2020-01-16 08:09:19', NULL, '2020-01-16 08:09:19');
INSERT INTO `products` VALUES (138, 'CG510019CX0010-03', NULL, 'KNITTED FABRIC, Merk: , Tipe: AN136, Ukuran: , Spesifikasi lain: - HONG KONG', 1, 10, '2020-01-16 08:09:45', NULL, '2020-01-16 08:09:45');
INSERT INTO `products` VALUES (139, 'HF20-012-01', NULL, 'KNITTED FABRIC (40%ORGANIC COTTON/48%HEMP/12%RECYECLED POLY, 160CM, 160GSM), Merk: , Tipe: KA9161 NATURAL HEMP LONGJING STRIPE, Ukuran: , Spesifikasi lain:', 1, 10, '2020-01-16 14:19:44', NULL, '2020-01-16 14:19:44');
INSERT INTO `products` VALUES (140, 'SCG1270/19-01', NULL, '100 % COTTON KNITTED FABRIC, Merk: , Tipe: AO355, Ukuran: , Spesifikasi lain: - HONG KONG', 1, 10, '2020-01-16 14:30:27', NULL, '2020-01-16 14:30:27');
INSERT INTO `products` VALUES (141, 'SCG1270/19-02', NULL, '100 % COTTON KNITTED FABRIC, Merk: , Tipe: AO355, Ukuran: , Spesifikasi lain: - HONG KONG', 1, 10, '2020-01-16 14:30:57', NULL, '2020-01-16 14:30:57');
INSERT INTO `products` VALUES (142, 'SCG1270/19-03', NULL, '100 % COTTON KNITTED FABRIC, Merk: , Tipe: AO355, Ukuran: , Spesifikasi lain: - HONG KONG', 1, 10, '2020-01-16 14:31:22', NULL, '2020-01-16 14:31:22');
INSERT INTO `products` VALUES (143, 'SCG1270/19-04', NULL, '100 % COTTON KNITTED FABRIC, Merk: , Tipe: AO355, Ukuran: , Spesifikasi lain: - HONG KONG', 1, 10, '2020-01-16 14:31:46', NULL, '2020-01-16 14:31:46');
INSERT INTO `products` VALUES (144, 'SCG1270/19-05', NULL, '100 % COTTON KNITTED FABRIC, Merk: , Tipe: AO355, Ukuran: , Spesifikasi lain: - HONG KONG', 1, 10, '2020-01-16 14:32:09', NULL, '2020-01-16 14:32:09');
INSERT INTO `products` VALUES (145, 'SCG1270/19-06', NULL, '100 % COTTON KNITTED FABRIC, Merk: , Tipe: AO355, Ukuran: , Spesifikasi lain: - HONG KONG', 1, 10, '2020-01-16 14:32:36', NULL, '2020-01-16 14:32:36');
INSERT INTO `products` VALUES (146, 'SCG1270/19-07', NULL, '100 % COTTON KNITTED FABRIC, Merk: , Tipe: AN520, Ukuran: , Spesifikasi lain: - HONG KONG', 1, 10, '2020-01-16 14:33:06', NULL, '2020-01-16 14:33:06');
INSERT INTO `products` VALUES (147, 'SCG1270/19-08', NULL, '100 % COTTON KNITTED FABRIC, Merk: , Tipe: AN520, Ukuran: , Spesifikasi lain: - HONG KONG', 1, 10, '2020-01-16 14:33:34', NULL, '2020-01-16 14:33:34');
INSERT INTO `products` VALUES (148, 'SCG1270/19-09', NULL, '100 % COTTON KNITTED FABRIC, Merk: , Tipe: AN444, Ukuran: , Spesifikasi lain: - HONG KONG', 1, 10, '2020-01-16 14:34:00', NULL, '2020-01-16 14:34:00');
INSERT INTO `products` VALUES (149, 'SCG1270/19-10', NULL, '100 % COTTON KNITTED FABRIC, Merk: , Tipe: AN444, Ukuran: , Spesifikasi lain: - HONG KONG', 1, 10, '2020-01-16 14:34:33', NULL, '2020-01-16 14:34:33');
INSERT INTO `products` VALUES (150, 'SCG1270/19-11', NULL, '100 % COTTON KNITTED FABRIC, Merk: , Tipe: AN444, Ukuran: , Spesifikasi lain: - HONG KONG', 1, 10, '2020-01-16 14:34:57', NULL, '2020-01-16 14:34:57');
INSERT INTO `products` VALUES (151, 'SCG1270/19-12', NULL, '100 % COTTON KNITTED FABRIC, Merk: , Tipe: AN444, Ukuran: , Spesifikasi lain: - HONG KONG', 1, 10, '2020-01-16 14:35:18', NULL, '2020-01-16 14:35:18');
INSERT INTO `products` VALUES (152, 'SCG1270/19-13', NULL, '100 % COTTON KNITTED FABRIC, Merk: , Tipe: AN444, Ukuran: , Spesifikasi lain: - HONG KONG', 1, 10, '2020-01-16 14:35:46', NULL, '2020-01-16 14:35:46');
INSERT INTO `products` VALUES (153, 'SCG1270/19-14', NULL, '100 % COTTON KNITTED FABRIC, Merk: , Tipe: AN444, Ukuran: , Spesifikasi lain: - HONG KONG', 1, 10, '2020-01-16 14:36:07', NULL, '2020-01-16 14:36:07');
INSERT INTO `products` VALUES (154, 'SCG1270/19-15', NULL, '100 % COTTON KNITTED FABRIC, Merk: , Tipe: AN444, Ukuran: , Spesifikasi lain: - HONG KONG', 1, 10, '2020-01-16 14:36:37', NULL, '2020-01-16 14:36:37');
INSERT INTO `products` VALUES (155, 'SCG1270/19-16', NULL, '100 % COTTON KNITTED FABRIC, Merk: , Tipe: AN444, Ukuran: , Spesifikasi lain: - HONG KONG', 1, 10, '2020-01-16 14:37:05', NULL, '2020-01-16 14:37:05');
INSERT INTO `products` VALUES (156, 'SCG1270/19-17', NULL, '100 % COTTON KNITTED FABRIC, Merk: , Tipe: AN444, Ukuran: , Spesifikasi lain: - HONG KONG', 1, 10, '2020-01-16 14:37:43', NULL, '2020-01-16 14:37:43');
INSERT INTO `products` VALUES (157, 'SCG1270/19-18', NULL, '100 % COTTON KNITTED FABRIC, Merk: , Tipe: AN444, Ukuran: , Spesifikasi lain: - HONG KONG', 1, 10, '2020-01-16 14:38:10', NULL, '2020-01-16 14:38:10');
INSERT INTO `products` VALUES (158, 'SCG1270/19-19', NULL, '100 % COTTON KNITTED FABRIC, Merk: , Tipe: AN444, Ukuran: , Spesifikasi lain: - HONG KONG', 1, 10, '2020-01-16 14:38:40', NULL, '2020-01-16 14:38:40');
INSERT INTO `products` VALUES (159, 'SCG1270/19-20', NULL, '100 % COTTON KNITTED FABRIC, Merk: , Tipe: AN444, Ukuran: , Spesifikasi lain: - HONG KONG', 1, 10, '2020-01-16 14:39:06', NULL, '2020-01-16 14:39:06');
INSERT INTO `products` VALUES (160, 'SCG1270/19-21', NULL, '100 % COTTON KNITTED FABRIC, Merk: , Tipe: AN444, Ukuran: , Spesifikasi lain: - HONG KONG', 1, 10, '2020-01-16 14:39:43', NULL, '2020-01-16 14:39:43');
INSERT INTO `products` VALUES (161, 'SCG1270/19-22', NULL, '60 % COTTON 40% POLYESTER KNITTED FABRIC, Merk: , Tipe: AN585, Ukuran: , Spesifikasi lain: - HONG KONG', 1, 10, '2020-01-16 14:41:39', NULL, '2020-01-16 14:41:39');

-- ----------------------------
-- Table structure for purchase_orders
-- ----------------------------
DROP TABLE IF EXISTS `purchase_orders`;
CREATE TABLE `purchase_orders`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` char(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `supplier_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `note` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `total_amount` decimal(65, 2) NULL DEFAULT NULL,
  `status` tinyint(2) NULL DEFAULT NULL,
  `created` datetime(0) NULL DEFAULT NULL,
  `created_by` int(11) NULL DEFAULT NULL,
  `modified` datetime(0) NULL DEFAULT NULL,
  `modified_by` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for purchase_orders_details
-- ----------------------------
DROP TABLE IF EXISTS `purchase_orders_details`;
CREATE TABLE `purchase_orders_details`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `purchase_order_id` bigint(20) NULL DEFAULT NULL,
  `product_id` int(11) NOT NULL,
  `qty` decimal(65, 2) NOT NULL,
  `price` decimal(20, 2) NULL DEFAULT 0.00,
  `subtotal` decimal(20, 2) NULL DEFAULT 0.00,
  `created` datetime(0) NULL DEFAULT NULL,
  `modified` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for put_aways
-- ----------------------------
DROP TABLE IF EXISTS `put_aways`;
CREATE TABLE `put_aways`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(225) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `date` date NOT NULL,
  `location_id` int(11) NOT NULL,
  `created` datetime(0) NOT NULL,
  `created_by` int(11) NOT NULL,
  `modified` datetime(0) NULL DEFAULT NULL,
  `modified_by` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for put_aways_details
-- ----------------------------
DROP TABLE IF EXISTS `put_aways_details`;
CREATE TABLE `put_aways_details`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `put_away_id` bigint(20) NOT NULL,
  `stock_id` bigint(20) NOT NULL,
  `qty` decimal(65, 2) NOT NULL,
  `created` datetime(0) NOT NULL,
  `created_by` int(11) NOT NULL,
  `modified` datetime(0) NULL DEFAULT NULL,
  `modified_by` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for receptions
-- ----------------------------
DROP TABLE IF EXISTS `receptions`;
CREATE TABLE `receptions`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `purchase_order_id` bigint(20) NULL DEFAULT NULL,
  `code` char(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `date` date NOT NULL,
  `bc_type` varchar(225) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `bc_code` varchar(225) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `bc_date` date NOT NULL,
  `importir_id` int(11) NOT NULL,
  `sender_id` int(11) NULL DEFAULT NULL,
  `register_code` varchar(225) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `register_date` date NULL DEFAULT NULL,
  `bl_code` varchar(225) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `bl_date` date NULL DEFAULT NULL,
  `currency_id` int(11) NULL DEFAULT NULL,
  `kurs_idr` decimal(65, 2) NULL DEFAULT NULL,
  `value_type` varchar(225) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `file` varchar(225) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `file_dir` varchar(225) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `note` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `status` tinyint(2) NULL DEFAULT NULL,
  `created` datetime(0) NULL DEFAULT NULL,
  `created_by` int(11) NULL DEFAULT NULL,
  `modified` datetime(0) NULL DEFAULT NULL,
  `modified_by` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for receptions_containers
-- ----------------------------
DROP TABLE IF EXISTS `receptions_containers`;
CREATE TABLE `receptions_containers`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `reception_id` bigint(20) NOT NULL,
  `code` varchar(225) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `size` varchar(225) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `type` varchar(225) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created` datetime(0) NOT NULL,
  `modified` datetime(0) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for receptions_containers_details
-- ----------------------------
DROP TABLE IF EXISTS `receptions_containers_details`;
CREATE TABLE `receptions_containers_details`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `purchase_orders_detail_id` bigint(20) NULL DEFAULT NULL,
  `receptions_container_id` bigint(20) NOT NULL,
  `product_id` int(11) NOT NULL,
  `qty` decimal(65, 2) NOT NULL,
  `unit` varchar(225) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `qty_packaging` decimal(65, 2) NULL DEFAULT NULL,
  `packaging` varchar(225) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `category` varchar(225) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `fasilitas` varchar(225) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `no_urut` varchar(225) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `country_id` int(11) NOT NULL,
  `value` decimal(65, 5) NOT NULL,
  `value_per_unit` decimal(65, 5) NULL DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `modified_by` int(11) NULL DEFAULT 0,
  `status` tinyint(1) NULL DEFAULT 0,
  `created` datetime(0) NOT NULL,
  `modified` datetime(0) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for stocks
-- ----------------------------
DROP TABLE IF EXISTS `stocks`;
CREATE TABLE `stocks`  (
  `id` bigint(20) NOT NULL,
  `reception_containers_detail_id` bigint(20) NULL DEFAULT NULL,
  `code` char(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `qty` decimal(65, 2) NULL DEFAULT NULL,
  `created` datetime(0) NULL DEFAULT NULL,
  `modified` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for stocks_locations
-- ----------------------------
DROP TABLE IF EXISTS `stocks_locations`;
CREATE TABLE `stocks_locations`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `stock_id` bigint(20) NULL DEFAULT NULL,
  `put_aways_detail_id` bigint(20) NULL DEFAULT NULL,
  `actual_stock` decimal(65, 2) NULL DEFAULT NULL,
  `created` datetime(0) NULL DEFAULT NULL,
  `modified` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for stocks_locations_logs
-- ----------------------------
DROP TABLE IF EXISTS `stocks_locations_logs`;
CREATE TABLE `stocks_locations_logs`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `stock_location_id` bigint(20) NULL DEFAULT NULL,
  `type` enum('IN','OUT') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `qty` int(65) NULL DEFAULT NULL,
  `referensi_id` bigint(20) NULL DEFAULT NULL,
  `referensi` enum('ADJUSMENT','EXPENDITURES') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created` datetime(0) NULL DEFAULT NULL,
  `modified` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for suppliers
-- ----------------------------
DROP TABLE IF EXISTS `suppliers`;
CREATE TABLE `suppliers`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` char(250) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `address` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `country_id` int(11) NULL DEFAULT NULL,
  `phone` char(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `fax` char(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `email` char(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `pic` char(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `pic_phone` char(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `status` tinyint(1) NULL DEFAULT NULL,
  `created` datetime(0) NULL DEFAULT NULL,
  `created_by` int(11) NULL DEFAULT NULL,
  `modified` datetime(0) NULL DEFAULT NULL,
  `modified_by` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of suppliers
-- ----------------------------
INSERT INTO `suppliers` VALUES (2, 'HANGZHOU CHAOFENG TEXTILE CO.,LTD', 'CHAIJIAWU VILLAGE, TANGXI TOWN, YUHANG, HANGZHOU, CHINA\r\n', 46, '', '', '', '', '', 1, '2019-04-10 14:33:55', 6, '2019-04-10 14:33:55', NULL);
INSERT INTO `suppliers` VALUES (3, 'DONGTAI TASUDI TEXTILE TECHNOLOGY CO.,LTD', 'NO.01 ZHENXING ROAD,HIGH-TECH PARK (TOUZAO TOWN) 224200,DONGTAI,CITY,JIANGSU,CHINA', 46, '', '', '', '', '', 1, '2019-05-04 09:41:07', 6, '2019-05-04 09:41:07', NULL);
INSERT INTO `suppliers` VALUES (4, 'BUKWANG TONGSANG CO.,LTD', '59,WARYONG-RO 70-GIL,SEO-GU DAEGU,SOUTH KOREA', 120, '', '', '', '', '', 1, '2019-08-07 12:54:30', 10, '2019-08-07 12:54:30', NULL);
INSERT INTO `suppliers` VALUES (5, 'EUNSUNG TRADING CORPORATION', '106 GYEONGEUN-RO,GUMI,CITY GYUNGBUK,KOREA', 120, '', '', '', '', '', 1, '2019-08-27 11:17:01', 10, '2019-08-27 11:17:01', NULL);
INSERT INTO `suppliers` VALUES (6, 'YIWU JINGYANG IMPORT AND EXPORT CO.,LTD', 'SHOP 3,UNIT3,BUILDING 1,TAO JIELING COMMUNITY FUTIAN\r\nSTREET,YIWU CITY,ZHEJIANG PROVINCE', 46, '', '', '', '', '', 0, '2019-10-15 11:48:18', 10, '2019-10-15 11:48:18', NULL);
INSERT INTO `suppliers` VALUES (7, 'YIWU JINGYANG IMPORT AND EXPORT CO.,LTD', 'SHOP 3,UNIT3,BUILDING 1,TAO JIELING COMMUNITY FUTIAN\r\nSTREET,YIWU CITY,ZHEJIANG PROVINCE', 46, '', '', '', '', '', 1, '2019-10-15 11:49:12', 10, '2019-10-15 11:49:12', NULL);
INSERT INTO `suppliers` VALUES (8, 'TEX-RAY INDUSTRIAL CO.,LTD', '6F,NO.426 LINSEN N. RD., TAIPEI CITY 10451, TAIPEI TAIWAN', 46, '', '', '', '', '', 1, '2020-01-15 14:43:35', 10, '2020-01-15 14:43:35', NULL);
INSERT INTO `suppliers` VALUES (9, 'JIANGYIN FUHUI TEXTILES LIMITED O/B FOUNTAIN SET', 'BLOCK A, 6/F., EASTERN SEA IND BLDG., 29-39 KWAI CHEONG\r\nROAD, KWAI CHUNG,N.T. HONG KONG', 257, '', '', '', '', '', 1, '2020-01-15 17:09:50', 10, '2020-01-15 17:09:50', NULL);
INSERT INTO `suppliers` VALUES (10, 'HEMP FORTEX INDUSTRIES (RUSHAN) LTD', 'EASTERN CITY INDUSTRIES ZONE, RUSHAN 264507, CHINA', 46, '', '', '', '', '', 1, '2020-01-16 14:19:03', 10, '2020-01-16 14:19:03', NULL);
INSERT INTO `suppliers` VALUES (11, 'COSMIC GEAR LIMITED', 'UNIT 812, 8/F., YUEN FAT INDUSTRIAL BUILDING,25 WANG CHIU\r\nROAD,KOWLOON BAY,KOWLOON,HONGKONG', 257, '3427-9988', '3016-8672', '', '', '', 1, '2020-01-16 14:29:50', 10, '2020-01-16 14:29:50', NULL);

-- ----------------------------
-- Table structure for user_groups
-- ----------------------------
DROP TABLE IF EXISTS `user_groups`;
CREATE TABLE `user_groups`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_by` int(11) NULL DEFAULT NULL,
  `created` datetime(0) NULL DEFAULT NULL,
  `modified_by` int(11) NULL DEFAULT NULL,
  `modified` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_groups
-- ----------------------------
INSERT INTO `user_groups` VALUES (1, 'ADMINISTRATOR', 1, 1, '2019-03-26 12:44:05', 1, '2019-03-26 12:44:22');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `password` char(60) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `name` varchar(225) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `email` varchar(225) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `user_group_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_by` int(11) NULL DEFAULT NULL,
  `created` datetime(0) NULL DEFAULT NULL,
  `modified_by` int(11) NULL DEFAULT NULL,
  `modified` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `username`(`username`) USING BTREE,
  INDEX `user_group`(`user_group_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, 'administrator', '$2y$10$eZFD.eY6CfjVDcPtl6XBy.1Y/5726/ZQz9XKaWP7Jbw/8WXU440be', 'administrator', 'administrator@email.com', 1, 1, 1, '2019-03-26 12:54:54', 1, '2019-03-26 12:54:54');

SET FOREIGN_KEY_CHECKS = 1;
